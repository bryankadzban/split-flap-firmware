// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

extern crate getopts;
extern crate core;
extern crate wifi;

use std::fs::File;

enum Mode {
    Dump,
    Update{
        index: Option<String>,
        ssid: Option<String>,
        key: Option<String>,
        group: Option<String>,
    },
    Clear{
        index: Option<String>,
        ssid: Option<String>,
    },
}

fn read_file(filename: String, config: &mut wifi::Config) -> Result<(), std::io::Error> {
    use std::io::Read;

    let mut f = File::open(filename.clone()).or_else(|_| {
        let mut cfg = wifi::Config::default();
        cfg.header = 0x30465053;
        write_file(filename.clone(), &cfg)?;
        File::open(filename)
    })?;
    let mut buffer = Vec::new();

    f.read_to_end(&mut buffer)?;
    while buffer.len() < 4 {
        let l = buffer.len();
        buffer.push(match l {
            0 => 0x53u8,
            1 => 0x50u8,
            2 => 0x46u8,
            3 => 0x30u8,
            _ => { unreachable!() },
        });
    }
    while buffer.len() < std::mem::size_of::<wifi::Config>() {
        buffer.push(0u8);
    }

    unsafe {
        std::ptr::copy_nonoverlapping(
            buffer.as_slice().as_ptr(),
            config as *mut wifi::Config as *mut u8,
            std::mem::size_of::<wifi::Config>());
    }

    Ok(())
}

fn write_file(filename: String, config: &wifi::Config) -> Result<(), std::io::Error> {
    use std::io::Write;
    let mut f = File::create(filename)?;
    let bytes = unsafe {
        let p = config as *const wifi::Config as *const u8;
        core::slice::from_raw_parts(p, core::mem::size_of::<wifi::Config>())
    };
    f.write_all(bytes)
}

fn find_network_index(cfg: &wifi::Config, index: &Option<String>, ssid: &Option<String>) -> Result<usize, String> {
    if index.is_some() {
        Ok(str::parse(&index.clone().unwrap()).unwrap())
    } else if ssid.is_some() {
        let ssid = ssid.clone().unwrap();
        for (j, n) in cfg.networks.iter().enumerate() {
            if n.ssid.as_str() == &ssid {
                return Ok(j);
            }
        }
        Err(format!("SSID '{}' does not exist in the file, maybe add it?", ssid))
    } else {
        Err(format!("Need to specify either an SSID or index to update"))
    }
}

fn main() {
    let args: Vec<String> = std::env::args().collect();
    let mut opts = getopts::Options::new();

    opts.parsing_style(getopts::ParsingStyle::FloatingFrees)
        .long_only(false)
        .optflag("d", "dump", "Dump configuration")
        .optflag("u", "update", "Update a network by SSID or index")
        .optflag("c", "clear", "Clear a network by SSID or index")
        .optopt("n", "index", "Network index", "INDEX")
        .optopt("s", "ssid", "Network SSID", "SSID")
        .optopt("k", "key", "WPA2 pre-shared key", "KEY")
        .optopt("g", "group", "Display-group name", "GROUP")
        .reqopt("f", "file", "Config filename", "FILE");

    let help = opts.usage("Manipulate wifi config file");

    let (op, file) = match opts.parse(args) {
        Ok(m) => {
            if !m.opts_present(&["dump".to_owned(), "update".to_owned(), "clear".to_owned()]) {
                panic!("{}\n\nNeed one of --dump, --update, --clear", help);
            }
            if m.opt_present("dump") {
                (Mode::Dump, m.opt_str("file").unwrap())
            } else if m.opt_present("update") {
                (Mode::Update{
                    index: m.opt_str("index"),
                    ssid: m.opt_str("ssid"),
                    key: m.opt_str("key"),
                    group: m.opt_str("group"),
                }, m.opt_str("file").unwrap())
            } else {
                (Mode::Clear{
                    index: m.opt_str("index"),
                    ssid: m.opt_str("ssid"),
                }, m.opt_str("file").unwrap())
            }
        },
        Err(x) => {
            panic!("Error parsing flags: {:#?}", x);
        }
    };

    let mut cfg = wifi::Config::default();
    read_file(file.clone(), &mut cfg).unwrap();

    match op {
        Mode::Dump => {
            println!("File contents: {:#?}", cfg);
        },
        Mode::Update{index, ssid, key, group} => {
            if group.is_some() {
                let g = group.unwrap();
                let g = g.as_bytes();
                let l = if g.len() < cfg.client_id.len() { g.len() } else { cfg.client_id.len() };
                cfg.client_id[0..l].clone_from_slice(&g[0..l]);
            }
            if ssid.is_some() || index.is_some() {
                let i = find_network_index(&cfg, &index, &ssid).unwrap_or_else(|s| {panic!("{}", s);});
                if ssid.is_some() {
                    cfg.networks[i].ssid = wifi::SSID::from_str(&ssid.unwrap());
                }
                if key.is_some() {
                    cfg.networks[i].wpa2_key = wifi::Wpa2Key::from_str(&key.unwrap());
                }
                if (cfg.network_count as usize) < (i+1) {
                    cfg.network_count = (i+1) as u32;
                }
            }
            write_file(file, &cfg).unwrap();
        },
        Mode::Clear{index, ssid} => {
            let i = find_network_index(&cfg, &index, &ssid).unwrap_or_else(|s| {panic!("{}", s);});

            if i >= cfg.network_count as usize {
                panic!("Network index {} is too big for the count; can't clear it", i);
            }

            cfg.network_count -= 1;
            for j in i..cfg.network_count as usize {
                cfg.networks[j] = cfg.networks[j+1];
            }
            cfg.networks[cfg.network_count as usize] = wifi::WifiNetworkConfig::default();
            write_file(file, &cfg).unwrap();
        },
    }
}
