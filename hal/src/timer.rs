// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use core::fmt;
use core::ops::{Add,Sub};
use stm32f2::TIM7;

#[derive(Copy, Clone)]
pub struct Time16(pub u16);
impl Add<Duration> for Time16 {
    type Output = Time16;
    fn add(self, rhs: Duration) -> Time16 {
        Time16(self.0.wrapping_add(rhs.0 as u16))
    }
}
impl Sub<Time16> for Time16 {
    type Output = Duration;
    fn sub(self, rhs: Time16) -> Duration {
        Duration(self.0.wrapping_sub(rhs.0).into())
    }
}
impl fmt::Display for Time16 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Display::fmt(&self.0, f)
    }
}

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct Duration(pub u32);
impl Duration {
    pub fn ms(val: u32) -> Duration { Duration(val) }
}
impl Add<Duration> for Duration {
    type Output = Duration;
    fn add(self, rhs: Duration) -> Duration {
        Duration(self.0 + rhs.0)
    }
}

pub struct SimpleTimer {}

impl SimpleTimer {
    pub fn configured(mut self) -> Self {
        self.configure();
        self
    }
    pub fn configure(&mut self) {
        // increment every 1ms
        TIM7().psc.write(59999);
        // reset every 65.536s (16-bit) ... and TIM7 is a 16-bit timer anyway
        TIM7().ar.write(65535);
        TIM7().die.set_uie(true);
        // Send an update event....
        TIM7().eg.set_ug(true);
        self.start();
    }
    pub fn start(&mut self) {
        TIM7().cr1.set_cen(true);
    }
    #[allow(dead_code)]
    pub fn stop(&mut self) {
        TIM7().cr1.set_cen(false);
    }
    pub fn counter(&self) -> u32 {
        TIM7().cnt.read()
    }
}
