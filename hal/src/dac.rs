// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#[cfg_attr(test, allow(unused_imports))]
use core::fmt::Write;
use stm32f2::{DmaTransaction, GpioMode, GpioPin, GpioPull, DAC, DMA1, TIM6};

const DAC1_CHANNEL: u32 = 7;
const DAC1_STREAM: usize = 5;

pub type Callback = dyn FnMut(&mut [u32]) + Sync;

pub struct Dac {
    dma_xaction: Option<DmaTransaction<(&'static mut [u32], &'static mut [u32])>>,
    buffer_fill: Option<&'static mut Callback>,
}

impl Dac {
    pub fn new(pin: GpioPin, buffer_fill: Option<&'static mut Callback>) -> Self {
        // Before we set up the DAC, configure the GPIO as analog to reduce parasitic
        // current consumption
        pin.configured(GpioMode::Analog, GpioPull::None);

        // Set up the timer to trigger DMA
        TIM6().cr2.set_mms(0x2);     // update events send TRGO
        TIM6().cr1.update()
            .set_arpe(false)  // don't buffer; we don't change ARR
            .set_opm(false)   // stay out of one-pulse mode
            .set_urs(true)    // counter-overrun causes an update, nothing else
            .set_udis(false); // update events are enabled
        TIM6().die.update()
            .set_ude(false)   // no DMA trigger; it goes through the DAC via TRGO
            .set_uie(false);  // no update-event interrupts, just DMA via DAC
        TIM6().psc.write(0);         // no prescaler
        // The input clock rate is 60MHz, and we need to trigger 48*4*1000 events/sec
        // 60_000000 / (48*4)*1000
        // =   60000 / (48*4)
        // =   15000 / 48
        // =        312.5
        // ARRRRG
        // Well, I guess we want to be higher frequency, and 312 is going to have to be close
        // enough.  That's 192307.7Hz, instead of 192000, a ~.16% error.  We can still inflate the
        // sample frequency by exactly 4x, and do a better (that is, larger-kernel and non-integer
        // frequency inflation) resampling on the server.  Feeding 48076.9Hz down the network is
        // feasible.
        // This means the register needs to be set to 311, because the CNT bounds are inclusive.
        TIM6().ar.write(311);
        // These timers are forced to upcounting mode only, where CNT goes from its current value
        // to ARR, then resets to the current value.  So ARR==311/CNT==0 to start means there are
        // 312 pulses between update events.
        TIM6().cnt.write(0);

        Dac{
            dma_xaction: None,
            buffer_fill: buffer_fill,
        }
    }

    pub fn enable(&self) {
        DAC().cr.set_boff1(false);   // buffer enabled
        DAC().dhr12r1.set_dhr(2047); // initial value
        DAC().cr.set_en1(true);      // DAC enabled; drives pin from here
    }

    // If enabled is false, then go high-impedance.
    pub fn set_enabled(&mut self, enabled: bool) {
        DAC().cr.set_en1(enabled);
    }

    // level here is only 12 bits in size.
    pub fn set_level(&mut self, level: u32) {
        DAC().dhr12r1.set_dhr(level);
    }

    pub fn dma_in_progress(&self) -> bool {
        self.dma_xaction.is_some()
    }
    pub fn current_buffer(&self) -> Option<u8> {
        if !self.dma_in_progress() {
            None
        } else {
            DMA1().stream(DAC1_STREAM).double_buf_target()
        }
    }

    pub fn with_inactive_buffer<F>(&mut self, f: &mut F)
        where F: FnMut(&mut [u32]) {

        if let Some(ref mut xaction) = self.dma_xaction {
            xaction.with_inactive_buffer(f);
        }
    }

    pub fn handle_dma_irq(&mut self) {
        //TIM6().cr1.set_cen(false);
        //TIM6().cnt.write(0);
        //self.dma_xaction.take();
        //println!("dma done");
        // Code above is used when we one-shot the DMA buffer.  This is used for the
        // double-buffered setup:
        if let Some(ref mut fill) = self.buffer_fill {
            if let Some(ref mut xaction) = self.dma_xaction {
                xaction.with_inactive_buffer(fill);
            }
        }
    }

    pub fn start_dma(&mut self, buf_1: &'static mut [u32], buf_2: &'static mut [u32]) {
        println!("dma start: {}, {}", buf_1.len(), buf_2.len());
        DAC().cr.update()
            .set_ten1(true)
            .set_tsel1(0)
            .set_wave1(0)
            .set_dmaen1(true);

        let stream = DMA1().stream(DAC1_STREAM);
        let tr = stream.double_buf_to_periph((&DAC().dhr12l1).into(), DAC1_CHANNEL, buf_1, buf_2);
        tr.start();
        self.dma_xaction = Some(tr);

        TIM6().cr1.set_cen(true);  // enable counter and thus DMA
    }
}
