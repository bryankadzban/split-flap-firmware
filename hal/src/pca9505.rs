// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use stm32f2;
use stm32f2::{GpioPin, I2C1, SYSTICK, DMA1, DmaBuffer};

use core::fmt;
use core::fmt::Write;
use core::cell::RefCell;
use utils::bits::{AsBytesMut, BitSetLe};


pub enum I2cMode {
    #[allow(dead_code)]
    Original,  // 100MHz
    Fast,      // 400MHz
}

pub struct I2cConfig {
    pub sda: GpioPin,  // PB7 AF4
    pub scl: GpioPin,  // PB6 AF4
    pub mode: I2cMode,
}

#[derive(Clone, Copy, Debug)]
pub enum I2cOp{
    SendStopOnError,
    SendStart,
    SendSetAddress,
    SendBeforeData,
    SendData,
    RecvStart,
    RecvSetAddress,
    RecvBeforeRead,
}

#[derive(Copy, Clone, Debug)]
pub struct I2cError {
    pub bus_error: bool,
    pub no_ack: bool,
    pub arbitration_lost: bool,
    pub xrun: bool,
    pub pec_error: bool,
    pub timeout: bool,
    pub op: I2cOp,
}
impl fmt::Display for I2cError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.bus_error { write!(f, "Bus Error")?; }
        if self.no_ack { write!(f, "No Ack")?; }
        if self.arbitration_lost { write!(f, "Arbitration Lost")?; }
        if self.xrun { write!(f, "Overrun/Underrun")?; }
        if self.pec_error { write!(f, "PEC Error")?; }
        if self.timeout { write!(f, "Timeout")?; }
        write!(f, " while {:?}", self.op)?;
        Ok(())
    }
}

fn check_error(status: &stm32f2::I2cStatus1Value, op: I2cOp) -> Result<CycleState, I2cError> {
    if status.is_error() {
        Err(I2cError{
            bus_error: status.berr(),
            no_ack: status.af(),
            arbitration_lost: status.arlo(),
            xrun: status.ovr(),
            pec_error: status.pecerr(),
            timeout: status.timeout(),
            op: op,
        })
    } else {
        Ok(CycleState::Done)
    }
}

#[derive(Debug)]
enum StatusState {
    Idle,
    CheckingStatus1(u32),
}

struct StatusWaiter {
    status_state: StatusState,
}
impl StatusWaiter {
    pub fn new() -> Self {
        Self{status_state: StatusState::Idle}
    }

    pub fn check_status1<F: Fn(stm32f2::I2cStatus1Value) -> bool>(&mut self, f: F, op: I2cOp) -> Result<CycleState, I2cError> {
        let new_status_state = match self.status_state {
            StatusState::Idle => StatusState::CheckingStatus1(0),
            StatusState::CheckingStatus1(i) => {
                if i > 20000 {
                    self.status_state = StatusState::Idle;
                    return Err(I2cError{
                        timeout: true,
                        no_ack: false,
                        xrun: false,
                        pec_error: false,
                        bus_error: false,
                        arbitration_lost: false,
                        op: op,
                    });
                }
                let status1 = I2C1().s1.read();
                match check_error(&status1, op) {
                    Err(err) => {
                        self.status_state = StatusState::Idle;
                        return Err(err);
                    },
                    _ => {}
                }
                if f(status1) {
                    StatusState::Idle
                } else {
                    StatusState::CheckingStatus1(i + 1)
                }
            },
        };

        self.status_state = new_status_state;

        Ok(match self.status_state {
            StatusState::Idle => CycleState::Done,
            _ => CycleState::InProgress,
        })
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
enum OperationState {
    Idle,
    WaitForSendStart,
    WaitForSendSetAddress,
    WaitForSendDmaEndOfTransfer(usize),   // iteration count, for timeouts
    WaitForSendByteTransferFinished,
    WaitForRecvStart,
    WaitForRecvSetAddress,
    WaitForRecvDmaEndOfTransfer(usize),   // iteration count, for timeouts
}
/*impl OperationState {
    fn tag(&self) -> OperationState {
        match *self {
            OperationState::WaitForSendDmaEndOfTransfer(_) => OperationState::WaitForSendDmaEndOfTransfer(0),
            OperationState::WaitForRecvDmaEndOfTransfer(_) => OperationState::WaitForRecvDmaEndOfTransfer(0),
            x => x,
        }

    }
}*/

pub enum CycleState {
    InProgress,
    Done,
}

const I2C1_RX_STREAM: usize = 5;
const I2C1_TX_STREAM: usize = 6;
const I2C1_CHANNEL: u32 = 1;

pub struct I2c {
    sda: GpioPin,
    scl: GpioPin,
    mode: I2cMode,
    waiter: StatusWaiter,
    operation_state: OperationState,
    buf: DmaBuffer<'static, u8>,
}
impl I2c {
    pub fn new(config: I2cConfig, buf: &'static mut [u8]) -> I2c {
        use stm32f2::GpioMode::AlternateOut;
        use stm32f2::GpioOutSpeed::Fast;
        use stm32f2::GpioPull;
        use stm32f2::GpioOutType;

        if buf.len() < 6 {
            panic!("I2c class needs a DMA buffer at least 6 bytes long");
        }

        let mut result = I2c{
          sda: config.sda.configured(
              AlternateOut(4, GpioOutType::OpenDrain, Fast), GpioPull::None),
          scl: config.scl.configured(
              AlternateOut(4, GpioOutType::OpenDrain, Fast), GpioPull::None),
          mode: config.mode,
          waiter: StatusWaiter::new(),
          operation_state: OperationState::Idle,
          buf: DmaBuffer::new(buf),
        };

        result.reset();
        result
    }
    
    pub fn clear_bus(&mut self) {
        I2C1().c1.set_swrst(true);
        I2C1().c1.set_swrst(false);
        // manually send a bus clear.
        self.scl.configure(stm32f2::GpioMode::Output(
                stm32f2::GpioOutType::OpenDrain,
                stm32f2::GpioOutSpeed::Fast,
                true),
                stm32f2::GpioPull::None);
        for _ in 0..9 {
            self.scl.set(false);
            SYSTICK().busywait_ms(2);
            self.scl.set(true);
            SYSTICK().busywait_ms(2);
        }
        self.scl.configure(stm32f2::GpioMode::AlternateOut(
                4, 
                stm32f2::GpioOutType::OpenDrain,
                stm32f2::GpioOutSpeed::Fast),
                stm32f2::GpioPull::None);
    }

    pub fn with_send_buffer<F: FnOnce(&mut [u8]) -> usize>(&mut self, f: F) {
        self.buf.set_start(0);
        let len = f(&mut *self.buf.borrow_mut_full());
        self.buf.set_len(len);
    }

    pub fn set_recv_count(&mut self, count: usize) {
        let l = self.buf.borrow_mut_full().len();
        if count >= l {
            panic!("recv_count {} out of range, len: {}", count, l);
        }
        self.buf.set_start(0);
        self.buf.set_len(count);
    }
    pub fn with_recv_buffer<F: FnOnce(&[u8])>(&mut self, f: F) {
        f(&*self.buf.borrow());
    }

    pub fn reset(&mut self) {
        I2C1().c1.set_swrst(true);
        I2C1().c1.set_swrst(false);

        if !self.sda.get() {
            self.clear_bus();
        }

        I2C1().c2.set_freq(30); // bus freq == 30MHz

        match self.mode {
            I2cMode::Original => {
                I2C1().cc.update()
                    .set_fs(false)  // 100kHz
                    .set_cc(150); // 30MHz / 100kHz / 2
                I2C1().trise.set_trise(0); // 1000ns * 30MHz + 1
            },
            I2cMode::Fast => {
                I2C1().cc.update()
                    .set_fs(true)  // Fast mode; 400kHz
                    .set_cc(25); // 30MHz / 400kHz / 3
                I2C1().trise.set_trise(10); // 300ns * 30MHz + 1
            }
        }
        I2C1().oa1.set_always_true(true);
        I2C1().c1.set_pe(true);
        SYSTICK().busywait_ms(5);
        //write!(debug::serial(), "reset(); Transitioning from {:?} to {:?}\n", self.operation_state, OperationState::Idle).unwrap();
        self.operation_state = OperationState::Idle;
    }

    pub fn update_for_send(&mut self, addr: u8) -> Result<CycleState, I2cError> {
        if I2C1().s1.read().is_error() {
            // Clear all the error flags
            I2C1().s1.update()
                .set_smbalert(false)
                .set_timeout(false)
                .set_pecerr(false)
                .set_ovr(false)
                .set_af(false)
                .set_arlo(false)
                .set_berr(false);
        }
        let result = self.update_for_send_internal(addr);
        if let Err(ref err) = result {
            if err.no_ack {
                I2C1().c1.set_stop(true);
                while match self.waiter.check_status1(|s1| s1.sb(), I2cOp::SendStopOnError) {
                    Ok(CycleState::InProgress) => { true },
                    Ok(CycleState::Done) => { false },
                    Err(e) => {
                        println!("check_status1 0x{:x} for SendStopOnError failed: {}", addr, e);
                        false
                    },
                } {}
            } else {
                self.reset();
            }
        }
        result
    }

    fn update_for_send_internal(&mut self, addr: u8) -> Result<CycleState, I2cError> {
        let new_operation_state = match self.operation_state {
            // This state machine is shared between all chips, so if we run into some kind of
            // error, we have to reset the state each time.
            OperationState::Idle => {
                I2C1().c1.set_start(true);
                OperationState::WaitForSendStart
            },
            OperationState::WaitForSendStart => {
                match self.waiter.check_status1(|s1| s1.sb(), I2cOp::SendStart) {
                    Err(err) => {
                        I2C1().c1.set_stop(true);
                        //write!(debug::serial(), "err {:?}; Transitioning from {:?} to {:?}\n", err, self.operation_state, OperationState::Idle).unwrap();
                        self.operation_state = OperationState::Idle;
                        return Err(err);
                    },
                    Ok(CycleState::InProgress) => OperationState::WaitForSendStart,
                    Ok(CycleState::Done) => {
                        I2C1().d.set_data(addr as u32 & 0xfe);
                        // set up DMA here, before the ADDR event
                        self.buf.configure(&I2C1().d, DMA1(), I2C1_TX_STREAM, stm32f2::DmaDirection::MemoryToPeripheral, I2C1_CHANNEL);
                        I2C1().c2.set_dmaen(true);
                        self.buf.start_transaction();
                        OperationState::WaitForSendSetAddress
                    },
                }
            },
            OperationState::WaitForSendSetAddress => {
                // NB: The std periph library also checks for txe and tra here ... but it alos
                // doesn't start DMA until *after* the addr bit goes to true, just before it reads
                // from s2.  Confusing, given the reference manual.  Let's go with the reference
                // unless it doesn't work.
                match self.waiter.check_status1(|s1| s1.addr(), I2cOp::SendSetAddress) {
                    Err(err) => {
                        self.buf.cleanup_transaction();
                        I2C1().c1.set_stop(true);
                        //write!(debug::serial(), "err {:?}; Transitioning from {:?} to {:?}\n", err, self.operation_state, OperationState::Idle).unwrap();
                        self.operation_state = OperationState::Idle;
                        return Err(err);
                    },
                    Ok(CycleState::InProgress) => OperationState::WaitForSendSetAddress,
                    Ok(CycleState::Done) => {
                        // Reading s1 and s2 is necessary for the peripheral to continue
                        // The read of s2 clears the ADDR condition
                        I2C1().s2.read();
                        // NB: no DMA operations required here
                        OperationState::WaitForSendDmaEndOfTransfer(0)
                    },
                }
            },
            OperationState::WaitForSendDmaEndOfTransfer(count) => {
                // wait here for EOT on the DMA engine, then disable DMA
                if self.buf.has_error() || count > 20000 {
                    self.buf.cleanup_transaction();  // cancel DMA
                    I2C1().c2.set_dmaen(false);
                    I2C1().c1.set_stop(true);
                    //write!(debug::serial(), "endoftransfer(send); Transitioning from {:?} to {:?}\n", self.operation_state, OperationState::Idle).unwrap();
                    self.operation_state = OperationState::Idle;
                    //write!(debug::serial(), "SendDmaEOT error; count: {}\n", count).unwrap();
                    return Err(I2cError{
                        bus_error: false,
                        no_ack: false,
                        arbitration_lost: false,
                        xrun: false,
                        pec_error: false,
                        timeout: count > 20000,
                        op: I2cOp::SendBeforeData,
                    })
                } else if !self.buf.transaction_done() {
                    OperationState::WaitForSendDmaEndOfTransfer(count + 1)
                } else {
                    self.buf.cleanup_transaction();
                    I2C1().c2.set_dmaen(false);
                    OperationState::WaitForSendByteTransferFinished
                }
            },
            OperationState::WaitForSendByteTransferFinished => {
                // wait here for BTF before doing set_stop(true) below
                match self.waiter.check_status1(|s1| s1.btf(), I2cOp::SendData) {
                    Err(err) => {
                        I2C1().c1.set_stop(true);
                        //write!(debug::serial(), "err {:?}; Transitioning from {:?} to {:?}\n", err, self.operation_state, OperationState::Idle).unwrap();
                        self.operation_state = OperationState::Idle;
                        return Err(err);
                    },
                    Ok(CycleState::InProgress) => OperationState::WaitForSendByteTransferFinished,
                    Ok(CycleState::Done) => {
                        I2C1().c1.set_stop(true);
                        OperationState::Idle
                    },
                }
            },
            ref wrong_value => {
                panic!("BUG: update_for_send called when state machine was in {:?}!", wrong_value);
            }
        };
        /*if new_operation_state.tag() != self.operation_state.tag() {
            write!(debug::serial(), "{:?}, Transitioning from {:?} to {:?}\n", self as *const Self, self.operation_state, new_operation_state).unwrap();
        }*/

        self.operation_state = new_operation_state;

        Ok(match self.operation_state {
            OperationState::Idle => CycleState::Done,
            _ => CycleState::InProgress
        })
    }

    pub fn update_for_recv(&mut self, addr: u8) -> Result<CycleState, I2cError> {
        if I2C1().s1.read().is_error() {
            // Clear all the error flags
            I2C1().s1.update()
                .set_smbalert(false)
                .set_timeout(false)
                .set_pecerr(false)
                .set_ovr(false)
                .set_af(false)
                .set_arlo(false)
                .set_berr(false);
        }
        let result = self.update_for_recv_internal(addr);
        if !result.is_ok() {
            self.reset();
        }
        result
    }

    fn update_for_recv_internal(&mut self, addr: u8) -> Result<CycleState, I2cError> {
        let new_operation_state = match self.operation_state {
            OperationState::Idle => {
                I2C1().c1.set_start(true);
                if self.buf.len() > 1 {
                    I2C1().c2.set_last(true);    // receives always only pull one sequence of bytes
                }
                OperationState::WaitForRecvStart
            },
            OperationState::WaitForRecvStart => {
                match self.waiter.check_status1(|s1| s1.sb(), I2cOp::RecvStart) {
                    Err(err) => {
                        I2C1().c1.set_stop(true);
                        self.operation_state = OperationState::Idle;
                        return Err(err);
                    },
                    Ok(CycleState::InProgress) => OperationState::WaitForRecvStart,
                    Ok(CycleState::Done) => {
                        I2C1().c1.set_ack(self.buf.len() != 0);    // FIXME: > 1?
                        I2C1().d.set_data(addr as u32 | 0x01);
                        if self.buf.len() != 0 {
                            self.buf.configure(&I2C1().d, DMA1(), I2C1_RX_STREAM, stm32f2::DmaDirection::PeripheralToMemory, I2C1_CHANNEL);
                            I2C1().c2.set_dmaen(true);
                            self.buf.start_transaction();
                        }
                        OperationState::WaitForRecvSetAddress
                    },
                }
            },
            OperationState::WaitForRecvSetAddress => {
                match self.waiter.check_status1(|s1| s1.addr(), I2cOp::RecvSetAddress) {
                    Err(err) => {
                        I2C1().c1.set_stop(true);
                        if self.buf.len() > 0 {
                            self.buf.cleanup_transaction();
                            I2C1().c2.set_dmaen(false);
                        }
                        self.operation_state = OperationState::Idle;
                        return Err(err);
                    },
                    Ok(CycleState::InProgress) => OperationState::WaitForRecvSetAddress,
                    Ok(CycleState::Done) => {
                        if self.buf.len() == 0 {
                            I2C1().c1.set_stop(true);
                            return Ok(CycleState::Done);
                        }
                        if self.buf.len() == 1 {
                            I2C1().c1.set_ack(false);
                        }
                        //I2C1().c1.set_ack(self.buf.len() > 2);
                        //I2C1().c1.set_pos(self.buf.len() == 2);
                        // Reading s1 and s2 is necessary for the peripheral to continue
                        // This second read clears the ADDR condition
                        I2C1().s2.read();
                        OperationState::WaitForRecvDmaEndOfTransfer(0)
                    },
                }
            },
            OperationState::WaitForRecvDmaEndOfTransfer(count) => {
                if self.buf.has_error() || count > 20000 {
                    self.buf.cleanup_transaction();  // cancel DMA
                    I2C1().c2.set_dmaen(false);
                    I2C1().c1.set_stop(true);
                    //write!(debug::serial(), "endoftransfer(recv); Transitioning from {:?} to {:?}\n", self.operation_state, OperationState::Idle).unwrap();
                    self.operation_state = OperationState::Idle;
                    //write!(debug::serial(), "RecvDmaEOT error; count: {}\n", count).unwrap();
                    return Err(I2cError{
                        bus_error: false,
                        no_ack: false,
                        arbitration_lost: false,
                        xrun: false,
                        pec_error: false,
                        timeout: count > 20000,
                        op: I2cOp::RecvBeforeRead,
                    })
                } else if !self.buf.transaction_done() {
                    OperationState::WaitForRecvDmaEndOfTransfer(count + 1)
                } else {
                    self.buf.cleanup_transaction();
                    I2C1().c2.set_dmaen(false);
                    I2C1().c1.set_stop(true);
                    OperationState::Idle
                }
            },
            ref wrong_value => {
                panic!("BUG: update_for_recv called when state machine was in {:?}!", wrong_value);
            },
        };

        self.operation_state = new_operation_state;

        Ok(match self.operation_state {
            OperationState::Idle => CycleState::Done,
            _ => CycleState::InProgress
        })
    }
}

pub enum PinType {
    Input,
    Output,
}

#[derive(Debug,PartialEq)]
enum UpdateState {
    Idle,
    SendingOutputs,
    SendingConfigChanges,
    SendingReadCommand,
    ReceivingInputs,
    //ProcessingNewInputs,
}

pub struct Pca9698<'a> {
    i2c: &'a RefCell<I2c>,
    i2c_addr: u8,
    inputs: [u8; 5],
    outputs: [u32; 2],
    io_config: [u32; 2],
    io_config_changes: [u32; 2],
    input_notify_funcs: [Option<&'a mut dyn FnMut(bool)>; 48],
    update_state: UpdateState,
}
impl<'a> Pca9698<'a> {
    pub fn new(i2c: &'a RefCell<I2c>, i2c_addr: u8) -> Self {
        Pca9698{
            i2c: i2c,
            i2c_addr: i2c_addr,
            inputs: [0u8; 5],
            outputs: [0u32; 2],
            io_config: [0u32; 2],
            io_config_changes: [0u32; 2],
            input_notify_funcs: [None, None, None, None, None, None, None, None,
                                 None, None, None, None, None, None, None, None,
                                 None, None, None, None, None, None, None, None,
                                 None, None, None, None, None, None, None, None,
                                 None, None, None, None, None, None, None, None,
                                 None, None, None, None, None, None, None, None,],
            update_state: UpdateState::Idle,
        }
    }
    pub fn update(&mut self) -> Result<CycleState, I2cError> {
        let mut i2c = self.i2c.borrow_mut();
        let new_state = match self.update_state {
            UpdateState::Idle => {
                i2c.with_send_buffer(|buf| {
                    buf[0] = 0x88;
                    buf[1..6].copy_from_slice(&self.outputs.as_mut_bytes()[0..5]);
                    6
                });
                UpdateState::SendingOutputs
            },
            UpdateState::SendingOutputs => {
                match i2c.update_for_send(self.i2c_addr) {
                    Err(err) => {
                        self.update_state = UpdateState::Idle;
                        return Err(err);
                    },
                    Ok(CycleState::InProgress) => UpdateState::SendingOutputs,
                    Ok(CycleState::Done) => {
                        if self.io_config_changes[0] != 0 || self.io_config_changes[1] != 0 {
                            i2c.with_send_buffer(|buf| {
                                buf[0] = 0x98;
                                buf[1..6].copy_from_slice(&self.io_config.as_mut_bytes()[0..5]);
                                6
                            });
                            UpdateState::SendingConfigChanges
                        } else {
                            i2c.with_send_buffer(|buf| { buf[0] = 0x80; 1});
                            UpdateState::SendingReadCommand
                        }
                    },
                }
            },
            UpdateState::SendingConfigChanges => {
                match i2c.update_for_send(self.i2c_addr) {
                    Err(err) => { return Err(err); },
                    Ok(CycleState::InProgress) => UpdateState::SendingConfigChanges,
                    Ok(CycleState::Done) => {
                        self.io_config_changes = [0, 0];
                        i2c.with_send_buffer(|buf| { buf[0] = 0x80; 1 });
                        UpdateState::SendingReadCommand
                    },
                }
            },
            UpdateState::SendingReadCommand => {
                match i2c.update_for_send(self.i2c_addr) {
                    Err(err) => { return Err(err); },
                    Ok(CycleState::InProgress) => UpdateState::SendingReadCommand,
                    Ok(CycleState::Done) => {
                        i2c.set_recv_count(5);
                        UpdateState::ReceivingInputs
                    },
                }
            },
            UpdateState::ReceivingInputs => {
                match i2c.update_for_recv(self.i2c_addr) {
                    Err(err) => { return Err(err); },
                    Ok(CycleState::InProgress) => UpdateState::ReceivingInputs,
                    Ok(CycleState::Done) => {
                        let mut index_offset = 0usize;
                        i2c.with_recv_buffer(|buf| {
                            for (cur, new) in self.inputs.iter_mut().zip(buf) {
                                let mut changed = *cur ^ *new;
                                *cur = *new;
                                while changed != 0 {
                                    let bit_index = changed.trailing_zeros();
                                    if let Some(ref mut func) = self.input_notify_funcs[bit_index as usize + index_offset] {
                                        println!("input {} changed to {}", bit_index + index_offset as u32, (*cur & (1 << bit_index)) != 0);
                                        func((*cur & (1 << bit_index)) != 0);
                                    }
                                    // clear the right-most bit
                                    changed = changed & (changed - 1);
                                }
                                index_offset += 8;
                            }
                        });
                        UpdateState::Idle
                    },
                }
            },
        };
        /*if self.update_state != new_state {
            write!(debug::serial(), "moving from {:?} to {:?}\n", self.update_state, new_state).unwrap();
        }*/
        self.update_state = new_state;

        Ok(match self.update_state {
            UpdateState::Idle => CycleState::Done,
            _ => CycleState::InProgress,
        })
    }
    pub fn invalidate_config(&mut self) {
        self.io_config_changes[0] = 0xff;
    }
    pub fn listen_input(&mut self, pin_index: u8, func: &'a mut dyn FnMut(bool)) {
        self.configure_pin(pin_index, PinType::Input);
        self.input_notify_funcs[pin_index as usize] = Some(func);
    }
    #[allow(dead_code)]
    pub fn get_pin(&self, pin_index: u8) -> bool {
        self.inputs.get_bit_le(pin_index.into())
    }
    pub fn set_pin(&mut self, pin_index: u8, value: bool) {
        self.outputs.set_bit_le(pin_index.into(), value);
    }
        
    pub fn configure_pin(&mut self, pin_index: u8, pin_type: PinType) {
        self.io_config_changes.set_bit_le(pin_index.into(), true);
        self.io_config.set_bit_le(pin_index.into(), match pin_type {
            PinType::Input => true,
            PinType::Output => false,
        });
    }
}

pub const EXPANDER_COUNT: usize = 8;

pub struct IoExpanders<'a> {
    io_expanders: [Option<&'a RefCell<Pca9698<'a>>>; EXPANDER_COUNT],
    i2c_failures: [u32; EXPANDER_COUNT],
    count: u8,
    current_expander: usize,
}
impl<'a> IoExpanders<'a> {
    pub fn new() -> Self {
        Self{
            io_expanders: [None; EXPANDER_COUNT],
            i2c_failures: [0u32; EXPANDER_COUNT],
            count: 0,
            current_expander: 0,
        }
    }
    pub fn add(&mut self, expander: &'a RefCell<Pca9698<'a>>) {
        self.io_expanders[self.count as usize] = Some(expander);
        self.count += 1;
    }
    fn increment_current_expander(&mut self) {
        while let None = {
            self.current_expander += 1;
            if self.current_expander == EXPANDER_COUNT { self.current_expander = 0; }
            self.io_expanders[self.current_expander]
        } {}
    }
    pub fn update(&mut self) {
        if self.count == 0 { return; }

        if let Some(io_expander) = self.io_expanders[self.current_expander] {
            let mut got_error = false;
            match io_expander.borrow_mut().update() {
                Err(err) => {
                    let i = self.current_expander;
                    if self.i2c_failures[i] < 10 || (self.i2c_failures[i] & 0x3ff) == 0 {
                        println!("Error talking to io_expander #{}: {}; attempt {}", i, err,
                                 self.i2c_failures[i]);
                    }
                    if err.no_ack {
                        self.i2c_failures[i] += 1;
                    }
                    got_error = true;
                },
                Ok(CycleState::Done) => {
                    // This can't loop forever, because self.count isn't zero (or we would have
                    // returned at the start of update() above).
                    self.increment_current_expander();
                },
                Ok(CycleState::InProgress) => {},
            }
            // Now that the borrow_mut is out of scope, we can re-borrow to clear
            if got_error {
                for expander in &self.io_expanders {
                    if let &Some(expander) = expander {
                        expander.borrow_mut().invalidate_config();
                    }
                }
            }
        } else {
            // This can't loop forever, because self.count isn't zero (or we would have returned at
            // the start of update() above).
            self.increment_current_expander();
        }
    }
}
