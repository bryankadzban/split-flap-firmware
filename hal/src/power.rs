// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use stm32f2::GpioPin;
use stm32f2::TIM5;

use core::fmt::Write;

use utils::func::FuncRef;

pub struct PowerConfig {
    pub ps_on:  GpioPin,
    pub pwr_ok: GpioPin,

    pub on_time_ms: u32,   // How long to stay powered up
}

// Both of these are milliseconds.
//
// BUF is how many ms to wait after the trigger to enable PS_ON#.
//
// PWR_OK_TIMEOUT is the maximum time to wait after enabling PS_ON# while
// watching for PWR_OK to be asserted.  The ATX spec says this can be up to 0.5
// seconds, so set the timeout to 600ms for a bit of buffer.  The PSU in use
// usually takes ~480ms according to the oscilloscope.  (Though it turns on the
// +5V rail after ~30ms.  I assume the rail is at 5V for a high-impedance draw
// like a scope, but doesn't have enough capacitor storage to drive a normal
// computer's much-lower-impedance -- and variable-impedance too -- load until
// later.)
const BUF_MS: u32 = 1;
const PWR_OK_TIMEOUT_MS: u32 = 600;

pub struct Power<'a> {
    pwr_ok: GpioPin,

    running: bool,

    power_on_callback: FuncRef<'a, ()>,
}

impl<'a> Power<'a> {
    pub fn new(config: PowerConfig) -> Power<'a> {
        use stm32f2::GpioMode::{Input, AlternateOut};
        use stm32f2::GpioOutSpeed::High;
        use stm32f2::GpioOutType::OpenDrain;
        use stm32f2::GpioPull;

        // General setup: The timer is inactive for a couple ms, so that when
        // the timer resets (and CNT is 0), the output is off.  Then it goes
        // active for config.on_time_ms, until CNT hits AR, at which point it
        // resets CNT to 0, goes inactive, and disables the timer due to OPM.

        // Start out by configuring the timer, so when we hook up the GPIO pin
        // to it we don't start driving PS_ON# low and active

        let timer = TIM5();

        timer.cce.set_cce(0, false);  // disabled while we configure it
        timer.cr1.set_cen(false);     // counter disabled too

        timer.ccmr1.update()
            .set_cc1s(0)       // 00 == output
            .set_oc1fe(false)  // fast-enable is disabled
            .set_oc1pe(false)  // preload disabled; CCR1 changes happen immediately
            .set_oc1m(7)       // 111 == PWM mode 2, active iff CNT > CCR1
            .set_oc1ce(false); // external triggers do not clear the timer output

        // both of these on channel 1
        timer.cce.update()
            .set_ccnp(0, false) // np cleared (also unused)
            .set_ccp(0, true);  // output is active-low
        // (active-low so that while the timer is >ccr1, the output is set
        // low, which pulls the PS_ON# signal active)

        timer.cr1.update()
            .set_arpe(false)  // auto-reload register is not buffered
            .set_cms(0)       // edge-aligned mode
            .set_dir(false)   // upcounter
            .set_opm(true)    // one-pulse mode
            .set_urs(false)   // update interrupts come from over/underflow too
            .set_udis(false); // events reset the counter

        timer.cr2.update()
            .set_ccds(false)  // DMA on counter event disabled
            .set_ti1s(false); // TI1 input connected to TIMx_CH1 pin

        timer.smc.write(0);        // disable slave mode

        timer.die.update()
            .set_tie(false)   // trigger-interrupts disabled
            .set_cc1ie(false) // channel 1 interrupts disabled
            .set_cc2ie(false) // channel 2
            .set_cc3ie(false) // channel 3
            .set_cc4ie(false) // channel 4
            .set_uie(false);  // interrupt on update events disabled

        // The clock is 60MHz, so 60 counts is 1us, so 60000 counts is 1ms.
        // ...And we need to count milliseconds.
        // The hardware adds 1 to the value.  Sigh.
        timer.psc.write(60000 - 1);

        // Program the limits
        timer.ccr1.write(BUF_MS);
        timer.ar.write(config.on_time_ms + BUF_MS + PWR_OK_TIMEOUT_MS);

        // Send an explicit event to make buffered registers (...PSC...) take effect
        timer.eg.set_ug(true);

        // Now that the limit is in, set the current count to the reset value.
        timer.cnt.write(0);

        // active-low, open-drain; initially off; no pullup needed
        // AF2 is "connected to TIM5_CH1"
        config.ps_on.configured(
                   AlternateOut(2, OpenDrain, High), GpioPull::None);

        let p = Power{
            running: false,

            // ATX spec says this is push-pull
            pwr_ok: config.pwr_ok.configured(Input, GpioPull::None),

            power_on_callback: FuncRef::new(),
        };

        // enable
        timer.cce.set_cce(0, true);  // this will set the GPIO pin inactive

        println!("PS_ON# timer configured");

        p
    }

    pub fn trigger<F: 'a + FnMut()>(&mut self, callback: F) {
        self.reset();
        TIM5().cr1.set_cen(true);
        self.running = true;
        //self.power_on_callback.place() <- callback;
        self.power_on_callback = FuncRef::from(callback);
    }

    pub fn reset(&mut self) {
        TIM5().cr1.set_cen(false);
        TIM5().cnt.write(0);
        self.running = false;
    }

    pub fn running(&self) -> bool {
        self.running
    }

    pub fn pwr_ok(&self) -> bool {
        // Don't return pwr_ok until after our callback has been fired
        self.power_on_callback.is_set()
    }
    pub fn poll(&mut self) {
        if self.pwr_ok.get() {
            if let Some(f) = self.power_on_callback.get() {
                println!("Calling callback");
                f();
            }
            self.power_on_callback = FuncRef::new();
        }
    }
}
