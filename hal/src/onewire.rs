// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use stm32f2;
#[cfg(not(test))]
use arena;
use core::cell::RefCell;
use core::fmt::Write;
use core::mem::MaybeUninit;
use stm32f2::debug;
#[cfg(not(test))]
use utils::queue::IrqMutex;
#[cfg(not(test))]
use arena::ArenaFuncQueue;
use utils::bits::BitSetLe;

#[cfg(not(test))]
use stm32f2::TIM4 as TIMER;

#[cfg(test)]
use std;

enum SearchState {
    ReadBit = 0,
    ReadInvBit,
    WriteBit,
}


#[derive(Debug)]
struct Conflicts {
    buffer: [u32; 4],
}
impl Conflicts {
    fn new() -> Conflicts {
        Conflicts{
            buffer: [0, 0, 0, 0],
        }
    }
    #[allow(dead_code)]
    fn clear(&mut self) {
        self.buffer = [0, 0, 0, 0];
    }
    fn get(&self, index: usize) -> bool {
        self.buffer.get_bit_le(index)
    }
    fn set(&mut self, index: usize, val: bool) {
        self.buffer.set_bit_le(index, val);
    }
    fn rightmost_set_bit(&self) -> usize {
        for (i, val) in self.buffer.iter().enumerate().rev() {
            let leading = val.leading_zeros();
            if leading < 32 {
                return i * 32 + 31 - leading as usize;
            }
        }
        return 0xffffffff;
    }
    /*#[cfg(test)]
    fn stdout() -> std::io::Stdout {
        std::io::stdout()
    }
    #[cfg(not(test))]
    fn stdout() -> debug::DebugWriter {
        debug::stdout()
    }*/

    // set index, clear everything more rightward than it
    fn set_and_clear_after(&mut self, mut index: usize) {
        //#[cfg(test)]
        //use std::io::Write;

        self.buffer.set_bit_le(index, true);
        //write!(Self::stdout(), "buffer: {:?}\n", self.buffer).unwrap();
        index += 1;
        let mask = (1 << (index & 31)) - 1;
        //write!(Self::stdout(), "mask: {:X}\n", mask).unwrap();
        self.buffer[index >> 5] &= mask;
        //write!(Self::stdout(), "buffer: {:?}\n", self.buffer).unwrap();
        for i in ((index >> 5) + 1)..self.buffer.len() {
            self.buffer[i] = 0;
        }
        //write!(Self::stdout(), "buffer: {:?}\n", self.buffer).unwrap();
    }
    fn empty(&self) -> bool {
        self.buffer == [0, 0, 0, 0]
    }
}
#[cfg(test)]
mod test_conflicts{
    use onewire::Conflicts;

    #[test]
    fn test_set_and_clear_after() {
        let mut conflicts = Conflicts{
            buffer: [0, 0, 0, 0],
        };

        conflicts.set_and_clear_after(5);
        assert_eq!(5, conflicts.rightmost_set_bit());
        conflicts.set_and_clear_after(31);
        assert_eq!(31, conflicts.rightmost_set_bit());
        conflicts.set(31, false);
        assert_eq!(5, conflicts.rightmost_set_bit());  // set_and_clear_after(31) did not clear 5
        conflicts.set_and_clear_after(3);
        assert_eq!(3, conflicts.rightmost_set_bit());
        conflicts.set(3, false);
        assert_eq!(0xffffffff, conflicts.rightmost_set_bit());
    }

    #[test]
    fn test_rightmost_set_bit() {
        let mut conflicts = Conflicts{
            buffer: [0, 0, 0, 0],
        };
        assert_eq!(0xffffffff, conflicts.rightmost_set_bit());

        conflicts.set(5, true);
        assert_eq!(5, conflicts.rightmost_set_bit());
        conflicts.set(3, true);
        assert_eq!(5, conflicts.rightmost_set_bit());
        conflicts.set(31, true);
        assert_eq!(31, conflicts.rightmost_set_bit());
        conflicts.set(48, true);
        assert_eq!(48, conflicts.rightmost_set_bit());
        conflicts.set(59, true);
        assert_eq!(59, conflicts.rightmost_set_bit());
        conflicts.set(125, true);
        assert_eq!(125, conflicts.rightmost_set_bit());
        conflicts.set(127, true);
        assert_eq!(127, conflicts.rightmost_set_bit());
        conflicts.set(127, false);
        assert_eq!(125, conflicts.rightmost_set_bit());
        conflicts.set(125, false);
        assert_eq!(59, conflicts.rightmost_set_bit());
    }
}

pub struct SyncRefCell<T>(RefCell<T>);
impl<T> SyncRefCell<T> {
    pub fn borrow_mut(&self) -> core::cell::RefMut<T> {
        self.0.borrow_mut()
    }
    pub fn borrow(&self) -> core::cell::Ref<T> {
        self.0.borrow()
    }
}
unsafe impl<T: Sync> Sync for SyncRefCell<T> {}

#[cfg(not(test))]
struct RealtimeState<'a> {
    buffer: [u8; 16],
    conflicts: Conflicts,
    resolved_conflicts: Conflicts,
    pin: stm32f2::GpioPin,
    bit_pos: usize,
    bit_len: usize,
    func_queue: &'a ArenaFuncQueue<'a>,
    parent_handle: MaybeUninit<arena::ArenaHandle<'a, SyncRefCell<OneWireBus<'a>>>>,
    presence: bool,
    search_state: SearchState,
    crc_valid: bool,
}

#[cfg(not(test))]
impl<'a> RealtimeState<'a> {
    pub fn timer_interrupt_for_reset(&mut self) {
        if TIMER().s.cc1if() {
            TIMER().s.set_cc1if(false);
            // pull down
            self.pin.set(false);
        } else if TIMER().s.cc2if() {
            TIMER().s.set_cc2if(false);
            // release
            self.pin.set(true);
        } else if TIMER().s.cc3if() {
            TIMER().s.set_cc3if(false);
            self.presence = !self.pin.get();
        } else if TIMER().s.cc4if() {
            TIMER().s.set_cc4if(false);

            TIMER().stop();
            TIMER().reset_cci();

            let parent_handle = unsafe { &*self.parent_handle.as_ptr() };
            self.func_queue.push_back(move || {
                parent_handle.get(&arena::GLOBAL_ARENA).borrow_mut().run();
            });
        }
    }
    pub fn timer_interrupt_for_send(&mut self) {
        if TIMER().s.cc1if() {
            TIMER().s.set_cc1if(false);
            // pull down
            self.pin.set(false);
        } else if TIMER().s.cc2if() {
            // set value
            TIMER().s.set_cc2if(false);
            self.pin.set(self.buffer.get_bit_le(self.bit_pos));
        } else if TIMER().s.cc3if() {
            TIMER().s.set_cc3if(false);
            // release
            self.pin.set(true);
        } else if TIMER().s.cc4if() {
            TIMER().s.set_cc4if(false);
            // send next bit
            self.bit_pos += 1;
            if self.bit_pos >= self.bit_len {
                // ...or reset, if we're at the end
                TIMER().stop();
                TIMER().reset_cci();

                let parent_handle = unsafe { &*self.parent_handle.as_ptr() };
                self.func_queue.push_back(move || {
                    parent_handle.get(&arena::GLOBAL_ARENA).borrow_mut().run();
                });
            }
        }
    }
    pub fn timer_interrupt_for_receive(&mut self) {
        if TIMER().s.cc1if() {
            TIMER().s.set_cc1if(false);
            // pull down
            self.pin.set(false);
            // and reset crc validity
            self.crc_valid = false;
        } else if TIMER().s.cc2if() {
            TIMER().s.set_cc2if(false);
            // release
            self.pin.set(true);
        } else if TIMER().s.cc3if() {
            TIMER().s.set_cc3if(false);
            // take sample
            let val = self.pin.get();
            self.buffer.set_bit_le(self.bit_pos, val);
            // next bit
            self.bit_pos += 1;
            if self.bit_pos >= self.bit_len {
                // ...or reset, if we're at the end
                TIMER().stop();
                TIMER().reset_cci();

                let parent_handle = unsafe { &*self.parent_handle.as_ptr() };
                self.func_queue.push_back(move || {
                    let mut parent = parent_handle.get(&arena::GLOBAL_ARENA).borrow_mut();
                    {
                        let mut realtime = parent.realtime.lock();
                        let len = realtime.bit_len >> 3;
                        realtime.crc_valid = len == 1 || compute_crc(&realtime.buffer[0..len - 1]) == realtime.buffer[len - 1]
                    };
                    parent.run();
                });
            }
        }
    }
    pub fn timer_interrupt_for_wait1(&mut self) {
        if TIMER().s.cc1if() {
            TIMER().s.set_cc1if(false);
            // pull down
            self.pin.set(false);
        } else if TIMER().s.cc2if() {
            TIMER().s.set_cc2if(false);
            // release
            self.pin.set(true);
        } else if TIMER().s.cc3if() {
            TIMER().s.set_cc3if(false);
            // take sample
            let val = self.pin.get();
            if val {
                // when we sample a 1 bit, reset and move the state machine along
                TIMER().stop();
                TIMER().reset_cci();

                let parent_handle = unsafe { &*self.parent_handle.as_ptr() };
                self.func_queue.push_back(move || {
                    parent_handle.get(&arena::GLOBAL_ARENA).borrow_mut().run();
                });
            }
        }
    }
    pub fn timer_interrupt_for_search(&mut self) {
        if TIMER().s.cc1if() {
            TIMER().s.set_cc1if(false);
            if self.bit_pos >= self.bit_len {
                TIMER().stop();
                TIMER().reset_cci();
                let parent_handle = unsafe { &*self.parent_handle.as_ptr() };
                self.func_queue.push_back(move || {
                    let mut parent = parent_handle.get(&arena::GLOBAL_ARENA).borrow_mut();
                    {
                        let realtime = parent.realtime.lock();
                        let len = realtime.bit_len >> 3;
                        let data = &realtime.buffer[0..len];
                        println!("Device responded; checksum valid: {}, conflicts: {:?}, resolved: {:?}", compute_crc(data) == 0, realtime.conflicts, realtime.resolved_conflicts);
                        debug::hexdump(data, "  ");
                        parent.add_device(data);
                    }
                    parent.run();
                });
            } else {
                // pull down
                self.pin.set(false);
            }
        } else if TIMER().s.cc2if() {
            TIMER().s.set_cc2if(false);
            match self.search_state {
                SearchState::WriteBit => {
                    self.pin.set(self.buffer.get_bit_le(self.bit_pos));
                },
                _ => self.pin.set(true),
            }
        } else if TIMER().s.cc3if() {
            TIMER().s.set_cc3if(false);
            let _ = self.pin.get();

            self.search_state = match self.search_state {
                SearchState::ReadBit => {

                    self.presence = self.pin.get();
                    SearchState::ReadInvBit
                },
                SearchState::ReadInvBit => {
                    let bit = self.presence;
                    let inv_bit = self.pin.get();
                    if bit == !inv_bit {
                        // all devices have the same bit at this index
                        self.buffer.set_bit_le(self.bit_pos, bit);
                        self.resolved_conflicts.set(self.bit_pos, false);
                    } else if bit {
                        TIMER().stop();
                        TIMER().reset_cci();
                        let bit_pos = self.bit_pos;
                        let parent_handle = unsafe { &*self.parent_handle.as_ptr() };
                        self.func_queue.push_back(move || {
                            println!("No devices responded at bit {}", bit_pos);
                            parent_handle.get(&arena::GLOBAL_ARENA).borrow_mut().run();
                        });
                    } else {
                        // conflict
                        let bit_pos = self.bit_pos;
                        if self.conflicts.get(bit_pos) && self.conflicts.rightmost_set_bit() == bit_pos {
                            self.buffer.set_bit_le(self.bit_pos, true);
                            self.conflicts.set(bit_pos, false);
                            self.resolved_conflicts.set_and_clear_after(bit_pos);
                        } else {
                            if self.resolved_conflicts.get(bit_pos) {
                                self.buffer.set_bit_le(bit_pos, true);
                            } else {
                                self.buffer.set_bit_le(bit_pos, false);
                                self.conflicts.set(bit_pos, true);
                            }
                        }
                    }
                    SearchState::WriteBit
                },
                SearchState::WriteBit => {
                    // (The bit was already written by the channel-2 interrupt).
                    self.bit_pos += 1;
                    SearchState::ReadBit
                },
            }
        } else if TIMER().s.cc4if() {
            TIMER().s.set_cc4if(false);
            // release if written
            self.pin.set(true);
        }
    }
}

#[derive(Debug, Clone, Copy)]
struct OneWireTempSensor {
    pub addr: [u8; 8],
}
impl OneWireTempSensor {
    pub fn new(addr: &[u8]) -> Option<Self> {
        let mut owts = OneWireTempSensor{addr: [0u8; 8]};
        owts.addr.clone_from_slice(addr);
        Some(owts)
    }
}

#[cfg(not(test))]
pub struct OneWireBus<'a> {
    realtime: &'a IrqMutex<RealtimeState<'a>>,

    timer_interrupt_for_reset_thunk: &'a stm32f2::Thunk<'a, IrqMutex<RealtimeState<'a>>>,
    timer_interrupt_for_send_thunk: &'a stm32f2::Thunk<'a, IrqMutex<RealtimeState<'a>>>,
    timer_interrupt_for_receive_thunk: &'a stm32f2::Thunk<'a, IrqMutex<RealtimeState<'a>>>,
    timer_interrupt_for_wait1_thunk: &'a stm32f2::Thunk<'a, IrqMutex<RealtimeState<'a>>>,
    timer_interrupt_for_search_thunk: &'a stm32f2::Thunk<'a, IrqMutex<RealtimeState<'a>>>,

    interrupt_mapping: Option<stm32f2::InterruptMapping<'a, IrqMutex<RealtimeState<'a>>>>,

    state: State,

    devices: [Option<OneWireTempSensor>; 8],
    current_device: usize,
    last_temp: Option<u16>,
}

#[derive(Debug)]
enum TemperatureCommand {
    Convert,
    Read,
}
impl TemperatureCommand {
    pub fn as_byte(&self) -> u8 {
        match *self {
            TemperatureCommand::Convert => 0x44,
            TemperatureCommand::Read => 0xbe,
        }
    }
}

#[derive(Debug)]
enum OnewireCommand {
    Search,
    AddressSingle([u8; 8], TemperatureCommand),
    #[allow(dead_code)]
    AddressAll(TemperatureCommand),
}
impl OnewireCommand {
    pub fn with_bytes<F: FnOnce(&[u8])>(&self, cb: F) {
        match *self {
            OnewireCommand::Search => cb(&[0xf0]),
            OnewireCommand::AddressSingle(addr, ref cmd) => {
                let mut buf = [0x55u8; 10];
                buf[1..9].clone_from_slice(&addr);
                buf[9] = cmd.as_byte();
                cb(&buf);
            },
            OnewireCommand::AddressAll(ref cmd) => cb(&[0xcc, cmd.as_byte()]),
        }
    }
}

#[derive(PartialEq)]
enum State {
    Idle,
    Resetting,
    SendingSearchCommand,
    Searching,
    ResettingForConversion,
    SendingConvertCommand,
    WaitingForConversion,
    Resetting2,
    SendingReadCommand,
    Reading,
}

#[cfg(not(test))]
impl<'a> OneWireBus<'a> {
    pub fn new(func_queue: &'a ArenaFuncQueue<'a>, pin: stm32f2::GpioPin) -> &'a SyncRefCell<Self> {
        // TODO: this should probably require pins...
        let realtime = arena::GLOBAL_ARENA.place(IrqMutex::new(RealtimeState{
                buffer: [0u8; 16],
                conflicts: Conflicts::new(),
                resolved_conflicts: Conflicts::new(),
                pin: pin,
                bit_pos: 0,
                bit_len: 0,
                func_queue: func_queue,
                parent_handle: MaybeUninit::uninit(),
                presence: false,
                search_state: SearchState::ReadBit,
                crc_valid: false,
        }));
        let result = arena::GLOBAL_ARENA.place(SyncRefCell(RefCell::new(OneWireBus{
            realtime: realtime,
            interrupt_mapping: None,

            timer_interrupt_for_reset_thunk: arena::GLOBAL_ARENA.place(stm32f2::Thunk::new(
                Self::timer_interrupt_for_reset, realtime)),
            timer_interrupt_for_send_thunk: arena::GLOBAL_ARENA.place(stm32f2::Thunk::new(
                Self::timer_interrupt_for_send, realtime)),
            timer_interrupt_for_receive_thunk: arena::GLOBAL_ARENA.place(stm32f2::Thunk::new(
                Self::timer_interrupt_for_receive, realtime)),
            timer_interrupt_for_wait1_thunk: arena::GLOBAL_ARENA.place(stm32f2::Thunk::new(
                Self::timer_interrupt_for_wait1, realtime)),
            timer_interrupt_for_search_thunk: arena::GLOBAL_ARENA.place(stm32f2::Thunk::new(
                Self::timer_interrupt_for_search, realtime)),

            state: State::Idle,

            devices: [None; 8],
            current_device: 0,
            last_temp: None,
        })));
        unsafe {
            realtime.lock().parent_handle.as_mut_ptr().write(arena::ArenaHandle::from(result));
        }
        result
    }
    pub fn start(&mut self) {
        self.reset_search();
        self.reset();
        self.state = State::Resetting;
    }
    pub fn start_conversion(&mut self) {
        let mut found = false;
        for i in (self.current_device + 1)..(self.current_device + self.devices.len() + 1) {
            if self.devices[i & (self.devices.len() - 1)].is_some() {
                self.current_device = i & (self.devices.len() - 1);
                self.reset();
                self.state = State::ResettingForConversion;
                found = true;
                break;
            }
        }
        if !found {
            self.reset_search();
            self.reset();
            self.state = State::Resetting;
        }
    }
    pub fn conversion_done(&self) -> bool {
        self.state == State::Idle
    }
    pub fn last_temp_in_c(&self) -> Option<u16> {
        self.last_temp.and_then(|temp| {
            let int_temp = temp >> 4;
            if temp & 0xf > 7 {
                Some(int_temp + 1)
            } else {
                Some(int_temp)
            }
        })
    }

    fn add_device(&mut self, address: &[u8]) {
        let mut found = false;
        for i in 0..self.devices.len() {
            if self.devices[i].is_none() {
                self.devices[i] = OneWireTempSensor::new(address);
                found = true;
                break;
            }
        }
        if !found {
            println!("Couldn't find a slot for temp sensor with address:");
            debug::hexdump(address, "  ");
            panic!("out of temp sensor slots");
        }
    }

    fn run(&mut self) {
        self.state = match self.state {
            State::Idle => {
                println!("Doing nothing as we are idle");
                State::Idle
            },
            State::Resetting => {
                if !self.realtime.lock().presence {
                    State::Idle
                } else {
                    self.send(OnewireCommand::Search);
                    State::SendingSearchCommand
                }
            },
            State::SendingSearchCommand => {
                self.continue_search(8);
                State::Searching
            },
            State::Searching => {
                let found_last_device = { self.realtime.lock().conflicts.empty() };
                if found_last_device {
                    println!("Found last device; returning to idle");
                    State::Idle
                } else {
                    self.reset();
                    State::Resetting
                }
            },
            State::ResettingForConversion => {
                let addr = &self.devices[self.current_device].unwrap().addr;
                self.send(OnewireCommand::AddressSingle(*addr, TemperatureCommand::Convert));
                State::SendingConvertCommand
            },
            State::SendingConvertCommand => {
                self.wait1();
                State::WaitingForConversion
            },
            State::WaitingForConversion => {
                self.reset();
                State::Resetting2
            },
            State::Resetting2 => {
                let addr = &self.devices[self.current_device].unwrap().addr;
                self.send(OnewireCommand::AddressSingle(*addr, TemperatureCommand::Read));
                State::SendingReadCommand
            },
            State::SendingReadCommand => {
                self.receive(9);
                State::Reading
            },
            State::Reading => {
                let realtime = self.realtime.lock();
                if realtime.crc_valid {
                    let temp = (realtime.buffer[0] as u32) | ((realtime.buffer[1] as u32) << 8);
                    /*print!("Done; temperature is ");
                    print_fixed(temp);
                    println!(" degrees C");*/
                    self.last_temp = Some(temp as u16);
                } else {
                    self.last_temp = None;
                    self.devices[self.current_device] = None;
                }
                State::Idle
            }
        }
    }

    fn reset(&mut self) {

        self.interrupt_mapping = None;

        {
            let mut realtime = self.realtime.lock();
            realtime.presence = false;
        }

        
        TIMER().cr1.set_cen(false);

        // increment every 5 us (300)
        TIMER().psc.write(299);
        // reset after 965us
        TIMER().ar.write(194);

        // assert reset at 5us
        TIMER().ccr1.write(1);
        // release bus at 490us; min is 480us
        TIMER().ccr2.write(98);
        // sample presence at 560us (70us after release; device has 60us max to respond)
        TIMER().ccr3.write(112);
        // end at 960us, min
        TIMER().ccr4.write(192);

        TIMER().die.update()
            .set_cc1ie(true)
            .set_cc2ie(true)
            .set_cc3ie(true)
            .set_cc4ie(true);

        TIMER().s.update()
            .set_cc1if(false)
            .set_cc2if(false)
            .set_cc3if(false)
            .set_cc4if(false);

        TIMER().cnt.write(193);
        // Send an update event....
        TIMER().eg.set_ug(true);
        self.interrupt_mapping = Some(stm32f2::InterruptMapping::new(
                self.timer_interrupt_for_reset_thunk, stm32f2::Interrupt::TIM4));

        // start
        TIMER().cr1.set_cen(true);
    }

    fn send(&mut self, cmd: OnewireCommand) {
        // abort current interrupt handling
        self.interrupt_mapping = None;
        cmd.with_bytes(|buf| {
            let mut realtime = self.realtime.lock();
            realtime.buffer[0..buf.len()].clone_from_slice(buf);
            realtime.bit_pos = 0;
            realtime.bit_len = buf.len() * 8;
        });

        TIMER().cr1.set_cen(false);

        // increment every 5 us (300)
        TIMER().psc.write(299);
        // reset after 80us
        TIMER().ar.write(16);

        // pull down at the start
        TIMER().ccr1.write(0);
        // set value after 10us
        TIMER().ccr2.write(2);
        // release value after 50us more; min 60us from pull-down to this release
        TIMER().ccr3.write(12);
        // reset for the next bit after 10us more; min 1us
        TIMER().ccr4.write(14);

        TIMER().die.reset()
            .set_cc1ie(true)
            .set_cc2ie(true)
            .set_cc3ie(true)
            .set_cc4ie(true);

        TIMER().s.update()
            .set_cc1if(false)
            .set_cc2if(false)
            .set_cc3if(false)
            .set_cc4if(false);
        TIMER().cnt.write(15);
        // Send an update event....
        TIMER().eg.set_ug(true);

        self.interrupt_mapping = Some(stm32f2::InterruptMapping::new(
                self.timer_interrupt_for_send_thunk, stm32f2::Interrupt::TIM4));
        // start
        TIMER().cr1.set_cen(true);
    }
    fn receive(&mut self, len: usize) {
        // abort current interrupt handling
        self.interrupt_mapping = None;
        {
            let mut realtime = self.realtime.lock();
            realtime.bit_pos = 0;
            realtime.bit_len = len * 8;
        }

        TIMER().cr1.set_cen(false);

        // increment every 5 us (300)
        TIMER().psc.write(299);
        // reset after 80us
        TIMER().ar.write(16);

        // pull down at start
        TIMER().ccr1.write(0);
        // release after 5us
        TIMER().ccr2.write(1);
        // sample, then increment or reset, after 5us more
        // max sample time is 15us after reset, and 10us after release since we delay 5us
        // also need to leave enough time for the pullup to raise the bus when reading a 1 bit
        TIMER().ccr3.write(2);

        TIMER().die.update()
            .set_cc1ie(true)
            .set_cc2ie(true)
            .set_cc3ie(true)
            .set_cc4ie(false);

        TIMER().s.update()
            .set_cc1if(false)
            .set_cc2if(false)
            .set_cc3if(false)
            .set_cc4if(false);
        TIMER().cnt.write(15);

        // Send an update event....
        TIMER().eg.set_ug(true);

        self.interrupt_mapping = Some(stm32f2::InterruptMapping::new(
                self.timer_interrupt_for_receive_thunk, stm32f2::Interrupt::TIM4));
        // start
        TIMER().cr1.set_cen(true);
    }
    fn wait1(&mut self) {
        // abort current interrupt handling
        self.interrupt_mapping = None;
        {
            let mut realtime = self.realtime.lock();
            realtime.bit_pos = 0;
            realtime.bit_len = 8 * 8;
        }

        TIMER().cr1.set_cen(false);

        // increment every 5 us (300)
        TIMER().psc.write(299);
        // reset every 5ms
        TIMER().ar.write(1000);

        // pull down at start
        TIMER().ccr1.write(0);
        TIMER().die.set_cc1ie(true);

        // release after 5us
        TIMER().ccr2.write(1);
        TIMER().die.set_cc2ie(true);

        // sample and reset after 5 more us; same timings as for receive
        TIMER().ccr3.write(2);
        TIMER().die.set_cc3ie(true);

        TIMER().die.set_cc4ie(false);

        TIMER().s.set_cc1if(false);
        TIMER().s.set_cc2if(false);
        TIMER().s.set_cc3if(false);
        TIMER().s.set_cc4if(false);
        TIMER().cnt.write(15);

        // Send an update event....
        TIMER().eg.set_ug(true);

        self.interrupt_mapping = Some(stm32f2::InterruptMapping::new(
                self.timer_interrupt_for_wait1_thunk, stm32f2::Interrupt::TIM4));
        // start
        TIMER().cr1.set_cen(true);
    }

    fn reset_search(&mut self) {
        {
            let mut realtime = self.realtime.lock();
            realtime.conflicts.clear();
            realtime.resolved_conflicts.clear();
        }
        for i in 0..8 {
            self.devices[i] = None;
        }
        self.last_temp = None;
    }

    fn continue_search(&mut self, len: usize) {
        // abort current interrupt handling
        self.interrupt_mapping = None;
       {
            let mut realtime = self.realtime.lock();
            realtime.bit_pos = 0;
            realtime.bit_len = len * 8;
            realtime.search_state = SearchState::ReadBit;
        }

        TIMER().cr1.set_cen(false);

        // increment every 5 us (300)
        TIMER().psc.write(299);
        // reset every 80us
        TIMER().ar.write(16);

        // pull down at the start
        TIMER().ccr1.write(0);
        // release after 5us
        TIMER().ccr2.write(1);
        // sample normal, sample inverted, or write, after 5us more
        TIMER().ccr3.write(2);
        // release bit if written after 50us more (60us total)
        TIMER().ccr4.write(12);

        TIMER().die.update()
            .set_cc1ie(true)
            .set_cc2ie(true)
            .set_cc3ie(true)
            .set_cc4ie(true);

        TIMER().s.update()
            .set_cc1if(false)
       	    .set_cc2if(false)
       	    .set_cc3if(false)
       	    .set_cc4if(false);
        TIMER().cnt.write(15);

        // Send an update event....
        TIMER().eg.set_ug(true);

        self.interrupt_mapping = Some(stm32f2::InterruptMapping::new(
                self.timer_interrupt_for_search_thunk, stm32f2::Interrupt::TIM4));
        // start
        TIMER().cr1.set_cen(true);
    }


    unsafe extern "C" fn timer_interrupt_for_reset(mutex: &IrqMutex<RealtimeState>) {
        mutex.lock().timer_interrupt_for_reset();
    }
    unsafe extern "C" fn timer_interrupt_for_send(mutex: &IrqMutex<RealtimeState>) {
        mutex.lock().timer_interrupt_for_send();
    }
    unsafe extern "C" fn timer_interrupt_for_receive(mutex: &IrqMutex<RealtimeState>) {
        mutex.lock().timer_interrupt_for_receive();
    }
    unsafe extern "C" fn timer_interrupt_for_wait1(mutex: &IrqMutex<RealtimeState>) {
        mutex.lock().timer_interrupt_for_wait1();
    }
    unsafe extern "C" fn timer_interrupt_for_search(mutex: &IrqMutex<RealtimeState>) {
        mutex.lock().timer_interrupt_for_search();
    }
}

fn compute_crc(buf: &[u8]) -> u8 {
    let mut result = 0u8;
    for b in buf {
        let byte = *b;
        for i in 0..8 {
            let carry_bit = result & 1;
            let new_bit = (byte >> i) & 1;
            result = result >> 1;
            if (carry_bit ^ new_bit) == 0x1 {
                result = result ^ 0x8c;
            }
        }
    }
    result
}

#[cfg(test)]
mod test {
    use onewire::compute_crc;

    #[test]
    fn test_crc() {
        assert_eq!(0xba, compute_crc(&[0x28, 0x3e, 0x21, 0x0e, 0x07, 0x00, 0x00]));

        assert_eq!(0x5c, compute_crc(&[0x3a, 0xf3]));
        assert_eq!(0x0d, compute_crc(&[0x1c, 0x3d, 0x57]));
    }
}

/*fn print_fixed(val: u32) {
    let int_part = val >> 4;
    let frac_part = val & 0xf;
    let mut decimal = 0u32;
    let mut place = 5000u32;
    for i in 0..4 {
        if ((frac_part >> (3 - i)) & 1) == 1 {
            decimal += place;
        }
        place = place >> 1;
    }
    print!("{}.{:04}", int_part, decimal);
}*/
