// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use utils::bits::{AsBytes, AsU32sLe, BitOpsLe};
use core::fmt::Write;
use stm32f2;
use stm32f2::USB_HS;
use stm32f2::atomic::AtomicUsizeBitfield;
use stm32f2::debug;
use stm32f2::debug::DebugBuffer;
use core::fmt;
#[cfg(not(test))]
use arena::GLOBAL_ARENA;

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Direction {
    HostToDevice = 0,
    DeviceToHost = 1,
}

#[derive(Clone, Copy, Debug)]
pub enum RequestType {
    Standard = 0,
    Class = 1,
    Vendor = 2,
    Reserved = 3,
}

#[derive(Clone, Copy, Debug)]
pub enum Recipient {
    Device = 0, 
    Interface = 1,
    Endpoint = 2,
    Other = 3,
}
impl Recipient {
    pub fn from_primitive(val: u8) -> Result<Self, ParseError> {
        match val {
            0 => Ok(Recipient::Device),
            1 => Ok(Recipient::Interface),
            2 => Ok(Recipient::Endpoint),
            3 => Ok(Recipient::Other),
            _ => Err(ParseError::UnknownRecipient(val)),
        }
    }
}

#[derive(Clone, Copy, Debug)]
#[allow(dead_code)]
pub enum TransferType {
    Control,
    Isochronous(SynchronizationType, UsageType),
    Bulk,
    Interrupt,
}
impl TransferType {
    fn as_eptyp(self) -> u32 {
        match self {
            TransferType::Control => 0,
            TransferType::Isochronous(_, _) => 1,
            TransferType::Bulk => 2,
            TransferType::Interrupt => 3,
        }
    }
}

#[derive(Clone, Copy, Debug)]
#[allow(dead_code)]
pub enum SynchronizationType {
    None,
    Asynchronous,
    Adaptive,
    Synchronous,
}

#[derive(Clone, Copy, Debug)]
#[allow(dead_code)]
pub enum UsageType {
    DataEndpoint,
    FeedbackEndpoint,
    ExplicitFeedbackDataEndpoint,
}

#[derive(Clone, Copy, Debug)]
pub enum ParseError {
    UnknownStandardRequest(Recipient, Direction, u8),
    UnknownDescriptorType(u8),
    UnknownRecipient(u8),
}

#[derive(Clone, Copy, Debug)]
pub enum Request {
    #[allow(dead_code)]
    StandardDevice(StandardDeviceRequest),

    DeviceGetStatus,
    DeviceClearFeature(u16),
    DeviceSetFeature(u16),
    DeviceSetAddress(u8),
    DeviceGetDescriptor{typ: DescriptorType, index: u8, lang: u16},
    DeviceSetDescriptor{typ: DescriptorType, index: u8, lang: u16},
    DeviceGetConfiguration,
    DeviceSetConfiguration(u8),

    InterfaceGetStatus{index: u16},
    InterfaceClearFeature{index: u16, feature: u16},
    InterfaceSetFeature{index: u16, feature: u16},
    InterfaceGetInterface{index: u16},
    InterfaceSetInterface{index: u16, alternative_setting: u8},

    EndpointGetStatus{index: u16},
    EndpointClearFeature{index: u16, feature: u16},
    EndpointSetFeature{index: u16, feature: u16},
    EndpointSyncFrame{index: u16},

    Class(u8),
    Vendor(u8),
    Reserved(u8),
}
impl Request {
    fn from_packet(packet: &SetupPacket) -> Result<Request, ParseError> {
        use self::Direction::{DeviceToHost, HostToDevice};
        use self::Recipient::{Device, Interface, Endpoint};

        match packet.request_type() {
            RequestType::Class => { return Ok(Request::Class(packet.raw_request())) },
            RequestType::Vendor => { return Ok(Request::Vendor(packet.raw_request())) },
            RequestType::Reserved => { return Ok(Request::Reserved(packet.raw_request())) },
            RequestType::Standard => {},
        }

        let recipient = Recipient::from_primitive(packet.raw_recipient())?;
        match (recipient, packet.direction(), packet.raw_request()) {
            (Device, DeviceToHost, 0) => Ok(Request::DeviceGetStatus),
            (Device, HostToDevice, 1) => Ok(Request::DeviceClearFeature(packet.value())),
            (Device, HostToDevice, 3) => Ok(Request::DeviceSetFeature(packet.value())),
            (Device, HostToDevice, 5) => Ok(Request::DeviceSetAddress(packet.value() as u8)),
            (Device, DeviceToHost, 6) => Ok(Request::DeviceGetDescriptor{
                typ: DescriptorType::from_primitive(((packet.value() & 0xff00) >> 8) as u8)?,
                index: ((packet.value() & 0x00ff) >> 0) as u8,
                lang: packet.index(),
            }),
            (Device, HostToDevice, 7) => Ok(Request::DeviceSetDescriptor{
                typ: DescriptorType::from_primitive(((packet.value() & 0xff00) >> 8) as u8)?,
                index: ((packet.value() & 0x00ff) >> 0) as u8,
                lang: packet.index(),
            }),
            (Device, DeviceToHost, 8) => Ok(Request::DeviceGetConfiguration),
            (Device, HostToDevice, 9) => Ok(Request::DeviceSetConfiguration(packet.value() as u8)),

            (Interface, DeviceToHost, 0) => Ok(Request::InterfaceGetStatus{index: packet.index()}),
            (Interface, HostToDevice, 1) => Ok(Request::InterfaceClearFeature{index: packet.index(), feature: packet.value()}),
            (Interface, HostToDevice, 3) => Ok(Request::InterfaceSetFeature{index: packet.index(), feature: packet.value()}),
            (Interface, DeviceToHost, 10) => Ok(Request::InterfaceGetInterface{index: packet.index()}),
            (Interface, HostToDevice, 17) => Ok(Request::InterfaceSetInterface{index: packet.index(), alternative_setting: packet.value() as u8}),

            (Endpoint, DeviceToHost, 0) => Ok(Request::EndpointGetStatus{index: packet.index()}),
            (Endpoint, HostToDevice, 1) => Ok(Request::EndpointClearFeature{index: packet.index(), feature: packet.value()}),
            (Endpoint, HostToDevice, 3) => Ok(Request::EndpointSetFeature{index: packet.index(), feature: packet.value()}),
            (Endpoint, DeviceToHost, 18) => Ok(Request::EndpointSyncFrame{index: packet.index()}),

            (recipient, direction, raw_req) => Err(ParseError::UnknownStandardRequest(
                                                   recipient, direction, raw_req)),
        }
    }
}

#[derive(Clone, Copy, Debug)]
pub enum StandardDeviceRequest {
}
impl StandardDeviceRequest {
}

#[derive(Clone, Copy, Debug)]
pub enum DescriptorType {
    Device = 1,
    Configuration = 2,
    String = 3,
    Interface = 4,
    Endpoint = 5,
    DeviceQualifier = 6,
}
impl DescriptorType {
    pub fn from_primitive(val: u8) -> Result<Self, ParseError> {
        match val {
            1 => Ok(DescriptorType::Device),
            2 => Ok(DescriptorType::Configuration),
            3 => Ok(DescriptorType::String),
            4 => Ok(DescriptorType::Interface),
            5 => Ok(DescriptorType::Endpoint),
            6 => Ok(DescriptorType::DeviceQualifier),
            _ => Err(ParseError::UnknownDescriptorType(val)),
        }
    }
}



// Builds a table of USB string descriptors
#[cfg(not(test))]
pub struct StringTableBuilder<'a> {
    buf: &'a mut [&'a[u8]],
    len: usize,
}
#[cfg(not(test))]
impl<'a> StringTableBuilder<'a> {
    pub fn new(capacity: usize) -> Self {
        StringTableBuilder{
            buf: GLOBAL_ARENA.make_slice::<&[u8]>(capacity, &[]),
            len: 0,
        }
    }
    pub fn add(&mut self, s: &str) -> u8 {
        self.raw_add(make_string_descriptor(s))
    }
    pub fn add_bytes(&mut self, s: &[u8]) -> u8 {
        let desc = GLOBAL_ARENA.make_slice::<u8>(s.len() + 2, 0);
        desc[0] = desc.len() as u8;
        desc[1] = 3;
        desc[2..].clone_from_slice(&s);
        self.raw_add(desc)
    }
    pub fn build(self) -> &'a [&'a [u8]] {
        &self.buf[0..self.len]
    }
    fn raw_add(&mut self, val: &'a [u8]) -> u8 {
        assert!(self.len < 256);
        self.buf[self.len] = val;
        let result = self.len as u8;
        self.len += 1;
        result
    }
}

#[cfg(not(test))]
pub struct Config {
    device_descriptor: DeviceDescriptor,
    config_descriptor: ConfigDescriptor,
}
#[cfg(not(test))]
impl Config {
    fn max_packet_size(&self, epnum: usize) -> Option<u32> {
        if epnum == 0 {
            return Some(self.device_descriptor.max_packet_size as u32);
        }
        for ep in self.config_descriptor.interfaces[0].endpoints {
            if ep.endpoint_num as usize == epnum {
                return Some(ep.max_packet_size as u32);
            }
        }
        None
    }
}

#[cfg(not(test))]
struct RawDescriptors<'a> {
    device_descriptor: &'a[u8],
    config_descriptor: &'a[u8],
    string_descriptors: &'a[&'a [u8]],
}
#[cfg(not(test))]
impl<'a> RawDescriptors<'a> {
    fn new(config: &Config) -> RawDescriptors<'a> {
        let mut table_builder = StringTableBuilder::new(4);
        table_builder.add_bytes(&[0x09, 0x04]);
        let device_descriptor = config.device_descriptor.make_raw(&mut table_builder);
        let config_descriptor = config.config_descriptor.to_bytes();
        RawDescriptors{
            device_descriptor: device_descriptor,
            config_descriptor: config_descriptor,
            string_descriptors: table_builder.build(),
        }
    }
}

#[cfg(not(test))]
pub struct DeviceDescriptor {
    device_class: u8,
    device_sub_class: u8,
    device_protocol: u8,
    max_packet_size: u8,
    vendor_id: u16,
    product_id: u16,
    device_release_num: u16,
    manufacturer: &'static str,
    product: &'static str,
    serial_num: &'static str,
}
#[cfg(not(test))]
impl DeviceDescriptor {
    fn make_raw<'a>(&self, string_table: &mut StringTableBuilder) -> &'a [u8] {
        let result = GLOBAL_ARENA.make_slice::<u8>(18, 0);
        assert_eq!(result.len(), self.serialize(result, string_table));
        result
    }

    fn serialize(&self, buf: &mut [u8], string_table: &mut StringTableBuilder) -> usize {
        if buf.len() < 18 {
            panic!("buf too small");
        }
        // sizeof descriptor
        buf[0] = 18;
        // bDescriptorType = Device
        buf[1] = 1;
        // USB version 2.0
        buf[2] = 0x00;
        buf[3] = 0x20;

        buf[4] = self.device_class;
        buf[5] = self.device_sub_class;
        buf[6] = self.device_protocol;
        buf[7] = self.max_packet_size;

        buf[8] = ((self.vendor_id & 0x00ff) >> 0) as u8;
        buf[9] = ((self.vendor_id & 0xff00) >> 8) as u8;

        buf[10] = ((self.product_id & 0x00ff) >> 0) as u8;
        buf[11] = ((self.product_id & 0xff00) >> 8) as u8;

        buf[12] = ((self.device_release_num & 0x00ff) >> 0) as u8;
        buf[13] = ((self.device_release_num & 0xff00) >> 8) as u8;

        buf[14] = string_table.add(self.manufacturer);
        buf[15] = string_table.add(self.product);
        buf[16] = string_table.add(self.serial_num);
        
        // num configurations
        buf[17] = 1;
        18
    }
}

#[cfg(not(test))]
const CONFIG: Config = Config{
    device_descriptor: DeviceDescriptor{
        device_class: 0,
        device_sub_class: 0,
        device_protocol: 0,
        max_packet_size: 64,

        // Pretend to be a generic FTDI serial device
        vendor_id: 0x18d1,
        product_id: 0x1002,
        device_release_num: 0x0600,

        manufacturer: "Search SRE",
        product: "Flipboard",
        serial_num: "v0.0",
    },
    config_descriptor: ConfigDescriptor{
        configuration_value: 1,
        max_power: 250,
        self_powered: false,
        remote_wakeup: false,
        interfaces: &[
            InterfaceDescriptor{
                alternate_setting: 0,
                interface_class: 0xff,
                interface_sub_class: 0xff,
                interface_protocol: 0xff,
                endpoints: &[
                    EndpointDescriptor{
                        direction: Direction::DeviceToHost,
                        endpoint_num: 1,
                        transfer_type: TransferType::Bulk,
                        max_packet_size: 64,
                        interval: 0,
                    },
                    EndpointDescriptor{
                        direction: Direction::HostToDevice,
                        endpoint_num: 2,
                        transfer_type: TransferType::Bulk,
                        max_packet_size: 64,
                        interval: 0,
                    },
                ],
            },
        ],
    },
};



#[cfg(not(test))]
pub struct ConfigDescriptor {
    configuration_value: u8,
    // in 2 mA units
    max_power: u8,
    self_powered: bool,
    remote_wakeup: bool,
    interfaces: &'static [InterfaceDescriptor],
}

#[cfg(not(test))]
impl ConfigDescriptor {
    const SIZE: usize = 9;
    pub fn to_bytes<'a>(&self) -> &'a [u8] {
        let result = GLOBAL_ARENA.make_slice::<u8>(self.total_size(), 0);
        assert_eq!(result.len(), self.serialize(result));
        result
    }
    fn total_size(&self) -> usize {
        let mut result = Self::SIZE + 9 * self.interfaces.len();
        for iface in self.interfaces {
            result += iface.endpoints.len() * 7;
        }
        result
    }
    fn serialize(&self, buf: &mut [u8]) -> usize {
        let total_size = self.total_size();
        if buf.len() < 9 || buf.len() < total_size || total_size > u16::max_value() as usize {
            panic!("buf too small");
        }
        // sizeof descriptor
        buf[0] = 9;
        // bDescriptorType = Configuration
        buf[1] = 2;
        buf[2] = ((total_size & 0x00ff) >> 0) as u8;
        buf[3] = ((total_size & 0xff00) >> 8) as u8;
        buf[4] = self.interfaces.len() as u8;
        buf[5] = self.configuration_value;
        // iConfiguration
        buf[6] = 0;
        buf[7] = (1 << 7) | // must be 1 (USB 1.0 bus powered)
                 if self.self_powered { 1 << 6 } else { 0 } |
                 if self.remote_wakeup { 1 << 5 } else { 0 };
        buf[8] = self.max_power;
        let mut offset = 9;
        for (index, iface) in self.interfaces.iter().enumerate() {
            offset += iface.serialize(&mut buf[offset..], index as u8);
        }
        offset
    }
}
pub struct InterfaceDescriptor {
    alternate_setting: u8,
    interface_class: u8,
    interface_sub_class: u8,
    interface_protocol: u8,
    endpoints: &'static [EndpointDescriptor],
}
impl InterfaceDescriptor {
    fn serialize(&self, buf: &mut [u8], index: u8) -> usize {
        if buf.len() < 9 {
            panic!("buf too small");
        }
        // sizeof descriptor
        buf[0] = 9;
        // bDescriptorType = Interface
        buf[1] = 4;
        buf[2] = index;
        buf[3] = self.alternate_setting;
        buf[4] = self.endpoints.len() as u8;
        buf[5] = self.interface_class;
        buf[6] = self.interface_sub_class;
        buf[7] = self.interface_protocol;
        // iInterface
        buf[8] = 0;
        let mut offset = 9;
        for (index, endpoint) in self.endpoints.iter().enumerate() {
            offset += endpoint.serialize(&mut buf[offset..], index as u8);
        }
        offset
    }
}
pub struct EndpointDescriptor {
    direction: Direction,
    endpoint_num: u8,
    transfer_type: TransferType,
    max_packet_size: u16,
    interval: u8,
}
impl EndpointDescriptor {
    #[inline(always)]
    fn serialize(&self, buf: &mut [u8], _index: u8) -> usize {
        if buf.len() < 7 {
            panic!("buf too small");
        }
        // sizeof descriptor
        buf[0] = 7;
        // bDescriptorType = endpoint
        buf[1] = 5;
        buf[2] = self.endpoint_num & 0x7 | match self.direction {
            Direction::HostToDevice => 0,
            Direction::DeviceToHost => (1 << 7)
        };
        buf[3] = match self.transfer_type {
            TransferType::Control => 0,
            TransferType::Isochronous(sync_type, usage_type) => {
                1 | match sync_type {
                    SynchronizationType::None => (0 << 2),
                    SynchronizationType::Asynchronous => (1 << 2),
                    SynchronizationType::Adaptive => (2 << 2),
                    SynchronizationType::Synchronous => (3 << 3),
                } | match usage_type {
                    UsageType::DataEndpoint => (0 << 4),
                    UsageType::FeedbackEndpoint => (1 << 4),
                    UsageType::ExplicitFeedbackDataEndpoint => (2 << 4),
                }
            }
            TransferType::Bulk => 2,
            TransferType::Interrupt => 3,
        };

        buf[4] = ((self.max_packet_size & 0x00ff) >> 0) as u8;
        buf[5] = ((self.max_packet_size & 0xff00) >> 8) as u8;
        buf[6] = self.interval;
        7
    }
}

pub struct SetupPacket {
    pub buf: [u32; 2],
}
impl SetupPacket {
    pub fn new() -> SetupPacket {
        SetupPacket{
            buf: [0u32; 2],
        }
    }
    pub fn direction(&self) -> Direction { 
        match self.buf.get_bits_le(7..8) {
            0 => Direction::HostToDevice,
            1 => Direction::DeviceToHost,
            _ => unreachable!(),
        }
    }
    pub fn request_type(&self) -> RequestType { 
        match self.buf.get_bits_le(5..7) {
            0 => RequestType::Standard,
            1 => RequestType::Class,
            2 => RequestType::Vendor,
            3 => RequestType::Reserved,
            _ => unreachable!(),
        }
    }
    pub fn raw_recipient(&self) -> u8 { 
        self.buf.get_bits_le(0..5) as u8
    }
    #[inline(always)]
    pub fn raw_request(&self) -> u8 {
        self.buf.as_bytes()[1]
    }
    pub fn value(&self) -> u16 { self.buf.get_bits_le(16..32) as u16 }
    pub fn index(&self) -> u16 { self.buf.get_bits_le(32..48) as u16 }
    pub fn length(&self) -> u16 { self.buf.get_bits_le(48..64) as u16 }
}
impl fmt::Debug for SetupPacket {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "SetupPacket{{\n  direction: {:?},\n   recipient: {:?},\n  request: {:?},\n  value: {},\n  index: {},\n  length: {},\n}}",
               self.direction(), self.raw_recipient(), self.raw_request(),
               self.value(), self.index(), self.length())
    }
}

pub struct UsbIo {
    pub data_minus_pin: stm32f2::GpioPin,
    pub data_plus_pin: stm32f2::GpioPin,
}

#[cfg(not(test))]
impl debug::Reader for Usb {
    fn have_new_data(&self) {
        // Ignore for now.  TODO: when we're using real interrupts, kick them off here.
    }
}

#[cfg(not(test))]
pub struct Usb {
    descriptors: RawDescriptors<'static>,
    last_setup_packet: SetupPacket,
    configured: bool,

    reader_id: Option<debug::ReaderId>,

    buffer: &'static DebugBuffer<'static>,
    xfer_ok: AtomicUsizeBitfield,
}
#[cfg(not(test))]
impl Usb {
    pub fn new(io: UsbIo, buffer: &'static DebugBuffer<'static>) -> Usb {
        use stm32f2::{GpioMode, GpioPull, GpioOutType, GpioOutSpeed};

        io.data_minus_pin.configured(
            GpioMode::AlternateOut(12, GpioOutType::OpenDrain, GpioOutSpeed::High),
            GpioPull::None);
        io.data_plus_pin.configured(
            GpioMode::AlternateOut(12, GpioOutType::OpenDrain, GpioOutSpeed::High),
            GpioPull::None);

        let result = Usb{
            descriptors: RawDescriptors::new(&CONFIG),
            last_setup_packet: SetupPacket::new(),
            configured: false,

            reader_id: None,

            buffer: buffer,
            xfer_ok: AtomicUsizeBitfield::new(0xffffffffusize),
        };
        
        // use internal USB 1.1 PHY
        USB_HS().gusbcfg.set_phsel(true);
        // reset core as the phy has changed
        result.reset();
        USB_HS().gccfg.update()
            // Disable bus voltage sensing
            .set_vbusasen(true)
            .set_vbusbsen(true)
            .set_novbussens(true)
            // turn on the core
            .set_pwrdwn(true);
        wait_phy_clocks(20);

        
        USB_HS().gahbcfg.update()
            // disable DMA
            .set_dmaen(false)
            // burst lenth = single
            .set_hbstlen(0)
            // Set txfifo empty interrupt to fire when fifo is completely empty
            .set_txfelvl(true)
            .set_ptxfelvl(true);

        USB_HS().gusbcfg.update()
            // disable host negotiation protocol
            .set_hnpcap(false)
            // disable session request protocol
            .set_srpcap(false);

        USB_HS().gintmsk.update()
            .set_otgint(true)
            .set_mmism(true);

        // Force peripheral mode
        USB_HS().gusbcfg.set_fdmod(true);

        USB_HS().dcfg.update()
            // USB Full speed using internal PHY
            .set_dspd(1) // should be 3?
            .set_pfivl(0);

        USB_HS().gintmsk.update()
            .set_usbrst(true)
            .set_enumdnem(true)
            .set_esuspm(true)
            .set_usbsuspm(true)
            .set_sofm(true);

        // Enable global interrupts
        USB_HS().gahbcfg.set_gint(true);

        result
    }
    pub fn configured(&self) -> bool {
        self.configured
    }
    pub fn set_reader_id(&mut self, reader_id: debug::ReaderId) {
        self.reader_id = Some(reader_id);
    }
    pub fn poll(&mut self) {

        let interrupts = USB_HS().gintsts.read();
        if interrupts.cidschg() {
            println!("USB connector ID status changed to {}",
                     if USB_HS().gotgctl.cidsts() { "B" } else { "A" });
            USB_HS().gintsts.clear_cidschg();
        }
        if interrupts.usbrst() {
            USB_HS().dctl.set_rwusig(false);

            USB_HS().daintmsk.write((1 << 16) | 1);
            USB_HS().diepmsk.write(0xffffffff);
            USB_HS().doepmsk.write(0xffffffff);
            for i in 0..8 {
                USB_HS().doept[i].ctl.set_nak();
            }
            {
                // Allocate FIFO memory; all of these are units of 32-bit words
                let mut addr = 0u32;
                USB_HS().grxfsiz.set_rxfd(512);
                addr += 512;

                let mut packet_size = CONFIG.max_packet_size(0).unwrap();
                // TX0 start address
                USB_HS().tx0fsiz.reset()
                    .set_start_addr(addr)
                    .set_depth(packet_size * 2 / 4);  // 1 packet, 1 extra space for SETUP
                addr += packet_size * 2 / 4;

                packet_size = CONFIG.max_packet_size(1).unwrap();
                USB_HS().dieptxf[0].update()
                    .set_start_addr(addr)
                    .set_depth(packet_size * 32 / 4);  // 32 packets for efficiency

                // addr += packet_size * 32 / 4;
            }

            USB_HS().doept[0].siz.update()
                .set_stupcnt(3)
                .set_pktcnt(1)
                .set_xfrsiz(8 * 3);
            println!("USB reset");
            USB_HS().gintsts.clear_usbrst();
        }
        if interrupts.usbsusp() {
            println!("USB suspend");
            USB_HS().gintsts.clear_usbsusp();
        }
        if interrupts.esusp() {
            println!("USB Early suspend");
            USB_HS().gintsts.clear_esusp();
        }
        if interrupts.sof() {
            //println!("USB start of frame");
            USB_HS().gintsts.clear_sof();
        }
        if interrupts.eopf() {
            //println!("USB end of periodic frame");
            USB_HS().gintsts.clear_eopf();
        }
        if interrupts.srqint() {
            println!("USB new session detected");
            USB_HS().gintsts.clear_srqint();
        }
        if interrupts.enumdne() {
            println!("USB Enumeration Done");
            // make endpoint a control endpoint
            USB_HS().diept[0].ctl.update()
                .set_eptyp(0)
                .set_mpsiz(CONFIG.max_packet_size(0).unwrap())
                .set_txfnum(0)
                .set_usbaep(true);
            USB_HS().gintsts.clear_enumdne();
        }
        if interrupts.rxflvl() {
            let pkt = USB_HS().grxstsp.pop();
            let wcnt = ((pkt.bcnt() + 3) / 4) as usize;
            let fifo_reg = &USB_HS().fifo[pkt.epnum() as usize][0];
            let mut buf = [0u32; 24];
            for i in 0..wcnt {
                buf[i] = fifo_reg.read();
            }

            if pkt.pktsts() == 6 {
                self.last_setup_packet.buf[0] = buf[0];
                self.last_setup_packet.buf[1] = buf[1];
            } else if pkt.pktsts() == 4 {
            } else if pkt.bcnt() > 0 {
                println!("USB received packet:");
                debug::hexdump(&buf.as_bytes()[0..pkt.bcnt() as usize], "  ");
            }
        }

        self.unbuffer();

        let daint = USB_HS().daint.read();
        if (daint & (1 << 16)) != 0 {
            let doeptint = USB_HS().doept[0].int.read();
            if doeptint.stup() {
                USB_HS().doept[0].int.clear_stup();
                let req = Request::from_packet(&self.last_setup_packet);
                {
                    let result: Option<&[u8]> = match req {
                        Ok(Request::DeviceGetDescriptor{typ: DescriptorType::Device, ..}) => {
                            Some(&self.descriptors.device_descriptor)
                        },

                        Ok(Request::DeviceGetDescriptor{typ: DescriptorType::Configuration, index: 0, ..}) => {
                            // TODO(kor): do truncation for everything..
                            Some(&self.descriptors.config_descriptor)
                        },
                        Ok(Request::DeviceGetDescriptor{typ: DescriptorType::String, index, ..}) => {
                            match self.descriptors.string_descriptors.get(index as usize) {
                                Some(x) => Some(*x),
                                None => None,
                            }
                        },
                        Ok(Request::DeviceSetAddress(_)) => {
                            println!("SetAddress: {}", self.last_setup_packet.value());
                            USB_HS().dcfg.set_dad(self.last_setup_packet.value().into());
                            Some(&[])
                        },
                        Ok(Request::DeviceSetConfiguration(1)) => {
                            let mut txfnum = 1;
                            for interface in CONFIG.config_descriptor.interfaces {
                                for endpoint in interface.endpoints {
                                    println!("Initializing endpoint {}", endpoint.endpoint_num);
                                    match endpoint.direction {
                                        Direction::DeviceToHost => {  // IN
                                            USB_HS().diept[endpoint.endpoint_num as usize].ctl.update()
                                                .set_eptyp(endpoint.transfer_type.as_eptyp())
                                                .set_mpsiz(endpoint.max_packet_size.into())
                                                .set_txfnum(txfnum)
                                                .set_usbaep(true);
                                            USB_HS().daintmsk.write_bit(endpoint.endpoint_num, true);
                                            txfnum += 1;
                                        },
                                        Direction::HostToDevice => {  // OUT
                                            USB_HS().doept[endpoint.endpoint_num as usize].ctl.update()
                                                .set_eptyp(endpoint.transfer_type.as_eptyp())
                                                .set_mpsiz(endpoint.max_packet_size.into())
                                                .set_usbaep(true);
                                            USB_HS().daintmsk.write_bit(16 + endpoint.endpoint_num, true);
                                        },
                                    }
                                }
                            }
                            self.configured = true;
                            println!("Configuration has been set...");
                            Some(&[])
                        },
                        Ok(Request::Vendor(raw_req)) => {
                            let pkt = &self.last_setup_packet;
                            let recipient = Recipient::from_primitive(pkt.raw_recipient());
                            match (recipient, pkt.direction(), raw_req) {
                                (Ok(Recipient::Device), Direction::HostToDevice, 0..=4) => {
                                    Some(&[])
                                },
                                (Ok(Recipient::Device), Direction::DeviceToHost, 5) => {
                                    Some(b"\x00")
                                },
                                (Ok(Recipient::Device), Direction::DeviceToHost, 10) => {
                                    Some(b"\x10")
                                },
                                (Ok(Recipient::Device), Direction::HostToDevice, 9) => {
                                    Some(&[])
                                },
                                x => {
                                    println!("Unknown vendor request: {:?}", x);
                                    None
                                },
                            }
                        },
                        x @ _ => {
                            println!("Unknown request: {:?}", x);
                            None
                        }
                    };
                    if let Some(data) = result {
                        let max_len = self.last_setup_packet.length() as usize;
                        if data.len() > max_len {
                            self.send(0, &data[0..max_len], true);
                        } else {
                            self.send(0, data, true);
                        }
                    } else {
                        USB_HS().diept[0].ctl.set_stall(true);
                    }
                }
                self.enable_rx();
            }
        }

        self.check_interrupts();
    }
    fn enable_rx(&mut self) {
        USB_HS().doept[0].ctl.update()
            .clear_nak()
            .set_epena();
    }
    fn check_interrupts_for_in_ep(&self, epnum: usize) {
        let dieptint = USB_HS().diept[epnum].int.read();
        if dieptint.xfrc() {
            USB_HS().diept[epnum].int.clear_xfrc();
            self.xfer_ok.set_bit(epnum, true);
        }
        if dieptint.epdisd() {
            println!("USB IN endpoint disabled");
            USB_HS().diept[epnum].int.clear_epdisd();
        }
        if dieptint.toc() {
            println!("USB IN timeout condition");
            USB_HS().diept[epnum].int.clear_toc();
        }
        if dieptint.ittxfe() {
            USB_HS().diept[epnum].int.clear_ittxfe();
        }
        if dieptint.inepne() {
            USB_HS().diept[epnum].int.clear_inepne();
        }
        if dieptint.txfe() {
        }
        if dieptint.txfifoudrn() {
            println!("USB IN transmit fifo underrun");
            USB_HS().diept[epnum].int.clear_txfifoudrn();
        }
        if dieptint.bna() {
            println!("USB IN buffer not available");
            USB_HS().diept[epnum].int.clear_bna();
        }
        if dieptint.pktdrpsts() {
            println!("USB IN packet dropped");
            USB_HS().diept[epnum].int.clear_pktdrpsts();
        }
        if dieptint.berr() {
            println!("USB IN babble error");
            USB_HS().diept[epnum].int.clear_berr();
        }
        if dieptint.nak() {
            USB_HS().diept[epnum].int.clear_nak();
        }
    }
    fn check_interrupts_for_out_ep(&self, epnum: usize) {
        let doeptint = USB_HS().doept[epnum].int.read();
        if doeptint.xfrc() {
            USB_HS().doept[epnum].int.clear_xfrc();
        }
        if doeptint.epdisd() {
            println!("USB OUT endpoint disabled");
            USB_HS().doept[epnum].int.clear_epdisd();
        }
        if doeptint.otepdis() {
            USB_HS().doept[epnum].int.clear_otepdis();
        }
        if doeptint.b2bstup() {
            println!("USB out back-to-back SETUP packets received");
            USB_HS().doept[epnum].int.clear_b2bstup();
        }
        if doeptint.nyet() {
            println!("USB OUT NYET");
            USB_HS().doept[epnum].int.clear_nyet();
        }
    }
    fn check_interrupts(&self) {
        let daint = USB_HS().daint.read();
        for i in 0..16 {
            if (daint & (1<<i)) != 0 {
                self.check_interrupts_for_in_ep(i);
            }
            if (daint & (1<<(i+16))) != 0 {
                self.check_interrupts_for_out_ep(i);
            }
        }
    }
    fn unbuffer(&mut self) {
        if !self.configured {
            return;
        }
        if let Some(id) = self.reader_id {
            let state = self.buffer.get_bytes(id);
            if state.buffer.len() == 0 {
                return;
            }
            let sent = self.send(1, &state.buffer, false);
            self.buffer.bytes_consumed(sent.wrapping_add(state.overran), id);
        }
    }
    fn send(&self, epnum: usize, buf: &[u8], wait: bool) -> usize {
        let byte_len = buf.len();
        let mut flat_iter = buf.iter();
        let fifo_chunk = if epnum == 0 {
            // half the FIFO size since the rest is needed for something else.
            USB_HS().tx0fsiz.depth() / 2 * 4
        } else {
            // 1/4th the fifo size, minus 64 bytes from trial-and-error.
            USB_HS().dieptxf[epnum-1].depth() * 4 / 4 - 64
        } as usize;
        let mut to_send = if byte_len > fifo_chunk { fifo_chunk } else { byte_len };
        let mut sent = self.send_chunk(epnum, &mut flat_iter, to_send, wait);
        if sent < to_send {
            return sent;
        }
        while sent < byte_len {
            to_send = if byte_len-sent > fifo_chunk { fifo_chunk } else { byte_len-sent };
            let chunk_sent = self.send_chunk(epnum, &mut flat_iter, to_send, wait);
            sent += chunk_sent;
            if chunk_sent < to_send {
                break;
            }
        }
        sent
    }
    fn send_chunk<'a, I: Iterator<Item=&'a u8>>(&self, epnum: usize, buf: &mut I, byte_len: usize, wait: bool) -> usize {
        let word_len: usize = (byte_len + 3) >> 2;

        // Wait for TxFIFO to have space if requested
        if !self.xfer_ok.get_bit(epnum) {
            let int = &USB_HS().diept[epnum].int;
            if wait {
                while !int.xfrc() {
                }
                int.clear_xfrc();
                self.xfer_ok.set_bit(epnum, true);
                // Also wait for endpoint space available
                while (USB_HS().diept[epnum].dtxfsts.ineptsav() as usize) < word_len {
                }
            } else {
                if int.xfrc() {
                    int.clear_xfrc();
                    self.xfer_ok.set_bit(epnum, true);
                } else {
                    // Caller doesn't want to wait, and there's no way to send anything; indicate 0.
                    return 0usize;
                }
            }
        }

        let max_packet = CONFIG.max_packet_size(epnum).unwrap();
        let packets = if byte_len > 0 { (byte_len as u32 + max_packet - 1) / max_packet} else { 1 };
        USB_HS().diept[epnum].siz.reset()
            .set_pktcnt(packets)
            .set_xfrsiz(byte_len as u32);

        USB_HS().diept[epnum].ctl.clear_nak();
        USB_HS().diept[epnum].ctl.set_epena();

        let fifo_reg = &USB_HS().fifo[epnum][0];
        let mut writes = 0usize;
        for val in buf.as_u32s_le() {
            fifo_reg.write(val);
            writes += 1;
            if writes >= word_len {
                break;
            }
        }
        //let fifo_space = USB_HS().diept[epnum].dtxfsts.ineptsav();
        self.xfer_ok.set_bit(epnum, false);
        byte_len
    }
    fn reset(&self) {
        // Without this delay, we sometimes freeze up waiting for grstctl.csrst() below...
        wait_phy_clocks(1);
        // Wait for bus to become idle
        while !USB_HS().grstctl.ahbidl() {
        }
        // Initiate core soft reset
        USB_HS().grstctl.set_csrst();
        // Wait for reset to complete
        while USB_HS().grstctl.csrst() {
        }
        wait_phy_clocks(3);
    }
}

fn wait_phy_clocks(count: usize) {
    busy_wait(count*120/7);
}

#[inline(never)]
fn busy_wait(count: usize) {
    for _ in 0..count {
        unsafe {
            asm!("" :::: "volatile");
        }
    }
}

#[cfg(not(test))]
fn make_string_descriptor<'a>(s: &str) -> &'a [u8] {
    let size = utf16_byte_len(s) + 2;
    assert!(size < 256);
    let result = GLOBAL_ARENA.make_slice::<u8>(size, 0);
    result[0] = size as u8;
    // string descriptor
    result[1] = 3;
    copy_as_utf16_le(s, &mut result[2..]);
    result
}

fn utf16_byte_len(s: &str) -> usize {
    let mut size = 0usize;
    for c in s.chars() {
        size += if c >= '\u{10000}' { 4 } else { 2 };
    }
    size
}
fn copy_as_utf16_le(s: &str, dest: &mut [u8]) {
    let mut i = 0;
    for c in s.chars() {
        let c = c as u32;
        if c < 0x10000 {
            dest[i + 0] = ((c & 0x00ff) >> 0) as u8;
            dest[i + 1] = ((c & 0xff00) >> 8) as u8;
            i += 2;
        } else {
            let v = c - 0x10000;
            let high = 0xd800 | ((v & 0xffc00) >> 10);
            let low = 0xdc00 | ((v & 0x003ff) >> 0);
            dest[i + 0] = ((high & 0x00ff) >> 0) as u8;
            dest[i + 1] = ((high & 0xff00) >> 8) as u8;
            dest[i + 2] = ((low & 0x00ff) >> 0) as u8;
            dest[i + 3] = ((low & 0xff00) >> 8) as u8;
            i += 4;
        }
    }
}

#[cfg(test)]
fn to_utf16_le<'a>(s: &str, buf: &'a mut [u8]) -> &'a [u8] {
    let size = utf16_byte_len(s);

    copy_as_utf16_le(s, buf);
    &buf[0..size]
}

#[cfg(test)]
mod test {
    use usb::to_utf16_le;

    #[test]
    fn test_as_utf16_le() {
        let mut buf = [0u8; 4096];
        assert_eq!(&[0u8; 0], to_utf16_le("", &mut buf));
        assert_eq!(&[0x48, 0x00, 0x69, 0x00, 0xfe, 0xff,
                     0x01, 0xd8, 0x37, 0xdc, 0x52, 0xd8, 0x62, 0xdf],
                   to_utf16_le("Hi\u{fffe}\u{10437}\u{24b62}", &mut buf));
    }
}
