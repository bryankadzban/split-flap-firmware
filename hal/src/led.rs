// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use stm32f2::TIM2;

// python -c 'import math ; [255 - int(math.log(256-x) / math.log(256) * 256) for x in xrange(0, 256)]'
//
// ...and then truncate the -1 at the start to 0.
const LOGS: [u8; 256] = [
    0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2,
    2, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5,
    6, 6, 6, 6, 6, 7, 7, 7, 7, 8, 8, 8, 8, 8, 9, 9,
    9, 9, 10, 10, 10, 10, 10, 11, 11, 11, 11, 12, 12, 12, 12, 13,
    13, 13, 13, 14, 14, 14, 14, 14, 15, 15, 15, 16, 16, 16, 16, 17,
    17, 17, 17, 18, 18, 18, 18, 19, 19, 19, 19, 20, 20, 20, 21, 21,
    21, 21, 22, 22, 22, 23, 23, 23, 24, 24, 24, 24, 25, 25, 25, 26,
    26, 26, 27, 27, 27, 28, 28, 28, 29, 29, 29, 30, 30, 30, 31, 31,
    31, 32, 32, 33, 33, 33, 34, 34, 34, 35, 35, 36, 36, 36, 37, 37,
    38, 38, 38, 39, 39, 40, 40, 41, 41, 42, 42, 42, 43, 43, 44, 44,
    45, 45, 46, 46, 47, 47, 48, 48, 49, 49, 50, 50, 51, 51, 52, 53,
    53, 54, 54, 55, 56, 56, 57, 57, 58, 59, 59, 60, 61, 61, 62, 63,
    63, 64, 65, 66, 66, 67, 68, 69, 70, 70, 71, 72, 73, 74, 75, 76,
    77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 88, 89, 90, 91, 93, 94,
    95, 97, 98, 100, 102, 103, 105, 107, 109, 111, 113, 115, 117, 120, 122, 125,
    127, 130, 134, 137, 141, 145, 149, 154, 159, 166, 173, 181, 191, 205, 223, 255
];

#[cfg(not(feature = "devicetest"))]
pub struct Color{
    red: u8,
    green: u8,
    blue: u8,
}
impl Color {
    pub fn rgb(red: u8, green: u8, blue: u8) -> Self {
        Self{red: red, green: green, blue: blue}
    }
    pub fn magenta() -> Self { Self{red: 255, green: 0, blue: 255} }
    pub fn red() -> Self { Self{red: 255, green: 0, blue: 0} }
    pub fn yellow() -> Self { Self{red: 255, green: 255, blue: 0} }
    pub fn green() -> Self { Self{red: 0, green: 255, blue: 0} }
    pub fn cyan() -> Self { Self{red: 0, green: 255, blue: 255} }
    pub fn white() -> Self { Self{red: 255, green: 255, blue: 255} }

    pub fn scale(&self, scaledown: u16) -> Self {
        Self{
            red: ((self.red as u16 * scaledown) >> 8) as u8,
            green: ((self.green as u16 * scaledown) >> 8) as u8,
            blue: ((self.blue as u16 * scaledown) >> 8) as u8,
        }
    }

    fn r(&self) -> u8 { self.red }
    fn g(&self) -> u8 { self.green }
    fn b(&self) -> u8 { self.blue }
}

pub struct Led {}
impl Led {
    pub fn new() -> Self { Self{} }
    pub fn configured(mut self) -> Self {
        self.configure();
        self
    }
    fn configure(&mut self) {
        use stm32f2::{GPIOA, GpioMode, GpioOutType, GpioOutSpeed, GpioPull};
        // TIM2 drives four GPIO outputs

        // disable, turn off one-pulse, set upcounter (direction false), don't buffer ARR, 1x clock
        // division
        TIM2().cr1.reset();

        // divide the 60MHz clock input by 15, for a 4MHz count
        TIM2().psc.write(14);
        // count from 0 to 255 then reset
        TIM2().ar.write(255);
        // the PWM frequency is 15625Hz, with a resolution of 8 bits
        TIM2().die.reset();   // no interrupts

        //(GPIOA pin 0 is driven by TIM2 channel 1, but we don't need to drive that pin)
        // GPIOA pin 1 is driven by TIM2 channel 2
        // GPIOA pin 2 is driven by TIM2 channel 3
        // GPIOA pin 3 is driven by TIM2 channel 4
        TIM2().cce.reset();   // all channels need to be disabled when editing ccmr
        TIM2().ccmr1.reset()
            .set_oc1pe(true)  // refman says we need to preload in pwm mode :-(
            .set_oc1m(7)
            .set_oc2pe(true)
            .set_oc2m(7);
        TIM2().ccmr2.reset()
            .set_oc3pe(true)
            .set_oc3m(7)
            .set_oc4pe(true)
            .set_oc4m(7);
        // Don't bother changing polarity; PWM mode 2 (7 above) handles that
        TIM2().cce.reset()
            .set_cce(1, true)
            .set_cce(2, true)
            .set_cce(3, true);
        TIM2().ccr2.write(0);
        TIM2().ccr3.write(0);
        TIM2().ccr4.write(0);

        TIM2().eg.set_ug(true);

        TIM2().cr1.set_cen(true);

        // AF1 for these three pins is "driven by TIM2, channel 2/3/4", see above
        GPIOA().pin(1).configure(
            GpioMode::AlternateOut(1, GpioOutType::PushPull, GpioOutSpeed::Fast), GpioPull::None);
        GPIOA().pin(2).configure(
            GpioMode::AlternateOut(1, GpioOutType::PushPull, GpioOutSpeed::Fast), GpioPull::None);
        GPIOA().pin(3).configure(
            GpioMode::AlternateOut(1, GpioOutType::PushPull, GpioOutSpeed::Fast), GpioPull::None);
    }

    pub fn set_color(&self, color: Color) {
        TIM2().ccr2.write(LOGS[usize::from(color.r())].into());
        TIM2().ccr3.write(LOGS[usize::from(color.g())].into());
        TIM2().ccr4.write(LOGS[usize::from(color.b())].into());
    }
}
