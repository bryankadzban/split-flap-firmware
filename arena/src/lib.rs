// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#![no_std]

#![feature(core_intrinsics)]
#![feature(ptr_internals)]

#[macro_use]
extern crate stm32f2;

use core::alloc::Layout;

#[cfg(not(feature = "devicetest"))]
extern crate utils;

use core::marker::PhantomData;
use core::ptr::Unique;
use stm32f2::atomic::AtomicPtrCell;
#[cfg(not(feature = "devicetest"))]
use utils::queue;

use core::fmt::Write;


extern "C" {
    static mut __global_arena_start: u8;
    static mut __global_arena_end: u8;
}

pub static GLOBAL_ARENA: FixedArena<'static> = FixedArena{
    start: AtomicPtrCell::new(unsafe { &__global_arena_start as *const u8 as *mut u8 }),
    end: AtomicPtrCell::new(unsafe { &__global_arena_end as *const u8 as *mut u8 }),
    phantom: PhantomData::<&mut [u8]>,
};


#[cfg(not(feature = "devicetest"))]
pub struct ArenaHandle<'a, T: 'a> {
    ptr: Unique<T>,
    phantom: PhantomData<&'a T>,
}

#[cfg(not(feature = "devicetest"))]
impl<'a, T> ArenaHandle<'a, T> {
    pub fn from(val: &'a T) -> Self {
        ArenaHandle{
            ptr: val.into(),
            phantom: PhantomData,
        }
    }
    pub fn get(&self, _arena: &'a FixedArena) -> &'a T {
        // TODO: Verify ptr is in arena
        unsafe { &*self.ptr.as_ptr() }
    }
}
#[cfg(not(feature = "devicetest"))]
impl<'a, T> Copy for ArenaHandle<'a, T> {}
#[cfg(not(feature = "devicetest"))]
impl<'a, T> Clone for ArenaHandle<'a, T> {
    fn clone(&self) -> Self { *self }
}
#[cfg(not(feature = "devicetest"))]
unsafe impl<'a, T> Send for ArenaHandle<'a, T> {}

#[cfg(arena_debug)]
fn do_if_debug<F: FnOnce()>(f: F) {
    f();
}

#[cfg(not(arena_debug))]
fn do_if_debug<F: FnOnce()>(_: F) {
}

#[cfg(test)]
mod test {
    use ::{ArenaHandle, FixedArena};
    use core::cell::RefCell;

    fn func2(arena: &FixedArena, handle: ArenaHandle<RefCell<usize>>) {
        assert!(*handle.get(arena).borrow_mut() == 42);
        let handle2 = handle;
        assert!(*handle2.get(arena).borrow_mut() == 42);
        assert!(*handle.get(arena).borrow_mut() == 42);
    }
    #[test]
    fn fixed_arena() {
        let mut buf = [0u8; 128];
        let arena = FixedArena::new(&mut buf);
        let val = arena.place_mut(RefCell::new(35usize));
        *val.borrow_mut() = 42;
        func2(&arena, ArenaHandle::from(val));
    }
    #[test]
    fn arena_alignment() {
        let mut buf = [0u8; 128];
        let arena = FixedArena::new(&mut buf);
        let start = arena.start.get();
        unsafe {
            assert_eq!(arena.alloc(1, 1), start.offset(0));
            assert_eq!(arena.alloc(1, 1), start.offset(1));
            assert_eq!(arena.alloc(1, 4), start.offset(4));
            assert_eq!(arena.alloc(1, 4), start.offset(8));
            assert_eq!(arena.alloc(8, 2), start.offset(10));
            assert_eq!(arena.alloc(8, 4), start.offset(20));
            assert_eq!(arena.alloc(8, 4), start.offset(28));
        }
    }
    #[test]
    fn make_slice() {
        let mut buf = [0u8; 128];
        {
            let arena = FixedArena::new(&mut buf);
            let val = arena.make_slice::<u8>(6, 33);
            assert_eq!(val[0], 33);
            assert_eq!(val[1], 33);
            assert_eq!(val[3], 33);
            assert_eq!(val[4], 33);
            assert_eq!(val[5], 33);
        }
        assert_eq!(buf[0], 33);
        assert_eq!(buf[6], 0);
    }
}

pub struct FixedArena<'a> {
    start: AtomicPtrCell<u8>,
    end: AtomicPtrCell<u8>,
    phantom: PhantomData<&'a mut [u8]>
}
impl<'a> FixedArena<'a> {
    #[cfg(test)]
    pub fn new(buf: &'a mut [u8]) -> Self {
        unsafe {
            let start = buf.as_mut_ptr();
            FixedArena {
                start: AtomicPtrCell::new(start),
                end: AtomicPtrCell::new(start.offset(buf.len() as isize)),
                phantom: PhantomData::<&mut [u8]>,
            }
        }
    }

    #[cfg(not(feature = "devicetest"))]
    pub fn make_slice<T: Copy>(&self, len: usize, init_value: T) -> &mut [T] {
        let ptr = self.alloc(Layout::new::<T>()) as *mut T;
        unsafe {
            for i in 0..len {
                *ptr.offset(i as isize) = init_value;
            }
            core::slice::from_raw_parts_mut(ptr, len)
        }
    }

    pub fn place<T: 'a>(&'a self, t: T) -> &'a T {
        /*
        // TODO(kor): Put this back in; it is dangerous.
        if core::intrinsics::needs_drop::<T>() {
            panic!("Cannot use arena with drop types");
        }
        */
        do_if_debug(|| {
            println!("allocating {}: {} bytes; ",
                     core::intrinsics::type_name::<T>(),
                     core::mem::size_of::<T>());
        });
        let l = Layout::new::<T>();
        let ptr = self.alloc(l) as *const T as *mut T;
        unsafe {
            core::intrinsics::copy_nonoverlapping(
                &t as *const T,
                ptr,
                l.size());
            &*ptr
        }
    }
    pub fn place_mut<T: 'a>(&'a self, t: T) -> &'a mut T {
        if core::intrinsics::needs_drop::<T>() {
            panic!("Cannot use arena with drop types");
        }
        do_if_debug(|| {
            println!("allocating {}: {} bytes; ",
                     core::intrinsics::type_name::<T>(),
                     core::mem::size_of::<T>());
        });
        let l = Layout::new::<T>();
        let ptr = self.alloc(l) as *mut T;
        unsafe {
            core::intrinsics::copy_nonoverlapping(
                &t as *const T,
                ptr,
                l.size());
            &mut *ptr
        }
    }
    fn alloc(&self, layout: Layout) -> *mut u8 {
        unsafe {
            let mut p: *mut u8;
            // Oh wow this is weird...  but I guess that's how you would make a do/while loop.
            // https://gist.github.com/huonw/8435502
            while {
                let s = self.start.get();
                p = ((s as usize) & !(layout.align() - 1)) as *mut u8;
                if p != s {
                    p = p.offset(layout.align() as isize);
                }
                let e = p.offset(layout.size() as isize);
                if e > self.end.get() || e < p {
                    panic!("Out of memory in arena");
                }
                !self.start.set(s, e).is_ok()
            } {}
            do_if_debug(|| {
                println!("  bytes left: {}", (self.end.get() as usize) - (self.start.get() as usize));
            });
            p
        }
    }
}

#[cfg(not(feature = "devicetest"))]
pub type ArenaFuncQueue<'a> = queue::FuncQueue<'a, ()>;

