// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#![no_std]

extern crate hal;
extern crate arena;
extern crate utils;

use hal::timer::{Duration, Time16};
use core::cell::{Cell, RefCell};
use arena::GLOBAL_ARENA;
use utils::func::FuncRef;

pub enum When {
    #[allow(dead_code)]
    At(Time16),
    AfterMs(u16),
}
impl When {
    fn abs(&self, now: Time16) -> Time16 {
        match *self {
            When::At(val) => val,
            When::AfterMs(duration) => now + Duration(duration as u32),
        }
    }
}



pub struct Slot {
    alarm_index: usize,
}
struct Alarm<'a> {
    time:   Time16,
    func:   FuncRef<'a, ()>,
}

struct SimpleSchedulerState<'a> {
    alarms: [Option<Alarm<'a>>; 256],
    next_index: usize,
    last_notify: Option<Time16>,
    now: Time16,
}
pub struct SimpleScheduler<'a> {
    state: RefCell<SimpleSchedulerState<'a>>,
}
impl<'a> SimpleScheduler<'a> {
    pub fn new(now: Time16) -> Self {
        SimpleScheduler {
            state: RefCell::new(SimpleSchedulerState{
                alarms: [None, None, None, None, None, None, None, None,
                         None, None, None, None, None, None, None, None,
                         None, None, None, None, None, None, None, None,
                         None, None, None, None, None, None, None, None,
                         None, None, None, None, None, None, None, None,
                         None, None, None, None, None, None, None, None,
                         None, None, None, None, None, None, None, None,
                         None, None, None, None, None, None, None, None,
                         None, None, None, None, None, None, None, None,
                         None, None, None, None, None, None, None, None,
                         None, None, None, None, None, None, None, None,
                         None, None, None, None, None, None, None, None,
                         None, None, None, None, None, None, None, None,
                         None, None, None, None, None, None, None, None,
                         None, None, None, None, None, None, None, None,
                         None, None, None, None, None, None, None, None,
                         None, None, None, None, None, None, None, None,
                         None, None, None, None, None, None, None, None,
                         None, None, None, None, None, None, None, None,
                         None, None, None, None, None, None, None, None,
                         None, None, None, None, None, None, None, None,
                         None, None, None, None, None, None, None, None,
                         None, None, None, None, None, None, None, None,
                         None, None, None, None, None, None, None, None,
                         None, None, None, None, None, None, None, None,
                         None, None, None, None, None, None, None, None,
                         None, None, None, None, None, None, None, None,
                         None, None, None, None, None, None, None, None,
                         None, None, None, None, None, None, None, None,
                         None, None, None, None, None, None, None, None,
                         None, None, None, None, None, None, None, None,
                         None, None, None, None, None, None, None, None],
                next_index: 0,
                last_notify: Some(now),
                now: now,
            })
        }
    }
    pub fn new_slot(&self) -> Slot {
        let mut state = self.state.borrow_mut();
        if state.next_index >= state.alarms.len() {
            panic!("All timer slots have been allocated");
        }
        let result = Slot {
            alarm_index: state.next_index
        };
        state.next_index += 1;
        result
    }

    pub fn set<F: 'a + FnMut() -> ()> (&self, slot: &Slot, when: When, func: F) {
        let mut state = self.state.borrow_mut();
        let time = when.abs(state.now);
        state.alarms[slot.alarm_index] = Some(Alarm{
            time:   time,
            func:   FuncRef::from(func),
        });
    }
    pub fn cancel(&self, slot: &Slot) {
        let mut state = self.state.borrow_mut();
        state.alarms[slot.alarm_index] = None;
    }
    pub fn notify_time(&self, now: Time16) {
        self.state.borrow_mut().now = now;
        loop {
            if let Some(mut alarm) = self.find_next_alarm(now) {
                if let Some(f) = alarm.func.get() {
                    f();
                }
            } else {
                break;
            }
        }
        self.state.borrow_mut().last_notify = Some(now);
    }
    fn find_next_alarm(&self, now: Time16) -> Option<Alarm<'a>> {
        let mut state = self.state.borrow_mut();
        if let Some(last_notify) = state.last_notify {
            for alarm in state.alarms.iter_mut() {
                let should_fire = match *alarm {
                    Some(ref a) => now - a.time <= now - last_notify,
                    _ => false,
                };
                if should_fire {
                    return alarm.take();
                }
            }
        }
        return None;
    }

}

pub struct RefScheduler<'a, M: 'a> {
    pub sched: &'a SimpleScheduler<'a>,
    machine: Cell<Option<&'a RefCell<M>>>,
}
impl<'a, M> RefScheduler<'a, M> {
    pub fn new_slot(&self) -> Slot {
        self.sched.new_slot()
    }
    pub fn set<F: 'a + Fn(&mut M)> (&'a self, slot: &Slot, when: When, func: F) {
        let machine = &self.machine;
        self.sched.set(slot, when, move || {
            if let Some(ref m) = machine.get() {
                func(&mut *m.borrow_mut());
            }
        });
    }
    pub fn cancel(&self, slot: &Slot) {
        self.sched.cancel(slot);
    }
}

pub fn make_machine<'a, M, F: FnOnce(&'a RefScheduler<'a, M>) -> M>(sched: &'a SimpleScheduler<'a>, func: F) -> &'a RefCell<M> {
    let ref_scheduler = GLOBAL_ARENA.place(RefScheduler::<'a, M>{
        sched: sched,
        machine: Cell::new(None),
    });
    let result = GLOBAL_ARENA.place(RefCell::new(func(ref_scheduler)));
    ref_scheduler.machine.set(Some(result));
    result
}
