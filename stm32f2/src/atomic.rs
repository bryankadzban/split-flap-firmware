// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use core::cell::UnsafeCell;
use core::fmt::Write;
use core::sync::atomic::{AtomicUsize, AtomicIsize, AtomicBool, AtomicPtr, Ordering};
use core::ops::{Deref, DerefMut};
use debug::{Reader, serial};

macro_rules! atomic_cell {
    ($mytype:ident, $celltype:ident, $innertype:ty) => {
        pub struct $mytype {
            value: $celltype,
        }
        impl $mytype {
            #[allow(dead_code)]
            pub const fn new(value: $innertype) -> $mytype {
                $mytype{
                    value: $celltype::new(value),
                }
            }
            pub fn get(&self) -> $innertype {
                self.value.load(Ordering::Acquire)
            }
            pub fn set(&self, current: $innertype, value: $innertype) -> Result<$innertype, $innertype> {
                self.value.compare_exchange(current, value, Ordering::Release, Ordering::Relaxed)
            }
            #[allow(dead_code)]
            pub fn swap(&self, new: $innertype) -> $innertype {
                self.value.swap(new, Ordering::SeqCst)
            }
        }
    };
    ($mytype:ident) => {
        pub struct $mytype<T> {
            value: AtomicPtr<T>,
        }
        impl<T> $mytype<T> {
            pub const fn new(value: *mut T) -> $mytype<T> {
                $mytype{
                    value: AtomicPtr::new(value),
                }
            }
            pub fn get(&self) -> *mut T {
                self.value.load(Ordering::Acquire)
            }
            #[allow(dead_code)]
            pub fn set(&self, current: *mut T, value: *mut T) -> Result<*mut T, *mut T> {
                self.value.compare_exchange(current, value, Ordering::Release, Ordering::Relaxed)
            }
        }
    }
}

atomic_cell!(AtomicUsizeCell, AtomicUsize, usize);
atomic_cell!(AtomicIsizeCell, AtomicIsize, isize);
atomic_cell!(AtomicBoolCell, AtomicBool, bool);
atomic_cell!(AtomicPtrCell);

impl<T> Reader for AtomicRefCell<T> where T: Reader {
    fn have_new_data(&self) {
        self.borrow().have_new_data();
    }
}

const UNUSED: usize = 0usize;
const WRITING: usize = !0usize;

pub struct AtomicRefCell<T> {
    refs: AtomicUsizeCell,
    value: UnsafeCell<T>,
}
impl<T: Sized> AtomicRefCell<T> {
    pub const fn new(value: T) -> AtomicRefCell<T> {
        AtomicRefCell{
            refs: AtomicUsizeCell::new(UNUSED),
            value: UnsafeCell::new(value),
        }
    }
    pub fn borrow(&self) -> AtomicBorrow<T> {
        AtomicBorrow::new(&self.refs, &self.value)
    }
    pub fn borrow_mut(&self) -> AtomicBorrowMut<T> {
        match AtomicBorrowMut::new(&self.refs, &self.value) {
            Ok(b) => b,
            Err(WRITING) => {
                serial().write_str("Tried to mutably-borrow a mutably-borrowed AtomicRefCell\n").unwrap();
                panic!("double mutable");
            },
            Err(_) => {
                serial().write_str("Tried to mutably-borrow an AtomicRefCell with refcnt>0\n").unwrap();
                panic!("already immutable");
            }
        }
    }
}

unsafe impl<T: Sized + Sync> Sync for AtomicRefCell<T> {}

pub struct AtomicBorrow<'a, T: 'a> {
    refs: &'a AtomicUsizeCell,
    value: &'a UnsafeCell<T>,
}
impl<'a, T: Sized> AtomicBorrow<'a, T> {
    pub fn new(refs: &'a AtomicUsizeCell, value: &'a UnsafeCell<T>) -> AtomicBorrow<'a, T> {
        while {
            let r = refs.get();
            if r == WRITING {
                panic!("Tried to borrow a mutably-borrowed AtomicRefCell");
            }
            !refs.set(r, r + 1).is_ok()
        } {}
        AtomicBorrow{
            refs: refs,
            value: value,
        }
    }
}
impl<'a, T: Sized> Drop for AtomicBorrow<'a, T> {
    #[inline]
    fn drop(&mut self) {
        while {
            let r = self.refs.get();
            !self.refs.set(r, r - 1).is_ok()
        } {}
    }
}
impl<'a, T: Sized> Clone for AtomicBorrow<'a, T> {
    #[inline]
    fn clone(&self) -> AtomicBorrow<'a, T> {
        // Since this AtomicBorrow exists, we know the refs field
        // is not set to WRITING.
        while {
            let r = self.refs.get();
            !self.refs.set(r, r + 1).is_ok()
        } {}
        AtomicBorrow{
            refs: self.refs,
            value: self.value,
        }
    }
}
impl<'a, T: Sized> Deref for AtomicBorrow<'a, T> {
    type Target = T;

    #[inline]
    fn deref(&self) -> &T {
        unsafe { &*self.value.get() }
    }
}

pub struct AtomicBorrowMut<'a, T: 'a> {
    refs: &'a AtomicUsizeCell,
    value: &'a UnsafeCell<T>,
}
impl<'a, T: Sized> AtomicBorrowMut<'a, T> {
    pub fn new(refs: &'a AtomicUsizeCell, value: &'a UnsafeCell<T>) -> Result<AtomicBorrowMut<'a, T>, usize> {
        refs.set(UNUSED, WRITING).and(Ok(AtomicBorrowMut{
            refs: refs,
            value: value,
        }))
    }
}
impl<'a, T: Sized> Drop for AtomicBorrowMut<'a, T> {
    #[inline]
    fn drop(&mut self) {
        self.refs.set(WRITING, UNUSED).expect("Somehow AtomicBorrowMut got un-borrowed?");
    }
}
impl<'a, T: Sized> Deref for AtomicBorrowMut<'a, T> {
    type Target = T;

    #[inline]
    fn deref(&self) -> &T {
        unsafe { &*self.value.get() }
    }
}
impl<'a, T: Sized> DerefMut for AtomicBorrowMut<'a, T> {
    #[inline]
    fn deref_mut(&mut self) -> &mut T {
        unsafe { &mut *self.value.get() }
    }
}

#[cfg(not(feature = "devicetest"))]
pub struct AtomicUsizeBitfield(AtomicUsizeCell);
#[cfg(not(feature = "devicetest"))]
impl AtomicUsizeBitfield {
    pub const fn new(value: usize) -> AtomicUsizeBitfield {
        AtomicUsizeBitfield(AtomicUsizeCell::new(value))
    }
    pub fn get(&self) -> usize {
        self.0.get()
    }
    pub fn set(&self, current: usize, new: usize) -> Result<usize, usize> {
        self.0.set(current, new)
    }
    pub fn get_bit(&self, bit: usize) -> bool {
        (self.0.get() & (1 << bit)) != 0
    }
    pub fn set_bit(&self, bit: usize, value: bool) {
        while {
            let v = self.get();
            let newv: usize;
            if value {
                newv = v | (1 << bit);
            } else {
                newv = v & !(1 << bit);
            }
            !self.set(v, newv).is_ok()
        } {}
    }
}
