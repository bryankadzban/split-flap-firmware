// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use core;

#[repr(C)]
pub struct Register(u32);

impl Register {
    pub fn read(&self) -> u32 {
        unsafe { core::intrinsics::volatile_load(&self.0) }
    }
    pub fn read_bit(&self, bit: u8) -> bool {
        if (self.read() >> bit) & 1 == 1 { true } else { false }
    }
    pub fn read_bits(&self, index: u8, width: u8) -> u32 {
        let mask = (1 << width) - 1;
        (self.read() & (mask << index)) >> index
    }

    pub fn write_mask(&self, mask: u32, value: u32) {
        if mask == 0xffffffff {
            self.write(value);
            return;
        }
        let not_mask = !mask;
        let masked_value = value & mask;
        disable_interrupts();
        let current_value = self.read();
        self.write((current_value & not_mask) | masked_value);
        enable_interrupts();
    }
    pub fn write_bit(&self, bit: u8, value: bool) {
        // whee, bitbanding
        let addr = &self.0 as *const u32 as usize;
        // shift left by 5 because the alias region is a u32 for each bit.
        // shift bit left by 2 to get it lined up with addr's shifted position.
        // 0x42000000 is the start of the alias region for peripheral memory.
        // XXX: This won't work for NVIC, SCB, or USB_FS.  Fortunately we use USB_HS, the SCB
        // accesses always write a full u32, and the NVIC code always writes a u32 with just one
        // bit set to clear or set one bit in the real register.  So we never call into this
        // function for those blocks.
        let aliasaddr = (addr << 5 | (bit as usize) << 2 | 0x42000000) as *mut u32;
        unsafe {
            core::intrinsics::volatile_store(aliasaddr, if value { 1 } else { 0 });
        };
    }
    pub fn write_bits(&self, index: u8, width: u8, value: u32) {
        let mask = (1 << width) - 1;
        self.write_mask(mask << index, value << index);
    }
    pub fn write(&self, value: u32) {
        unsafe {
            core::intrinsics::volatile_store(&self.0 as *const u32 as *mut u32, value);
        }
    }
}

#[derive(Clone, Copy)]
pub struct PackedU32(pub u32);
impl PackedU32 {
    #[cfg(not(feature = "devicetest"))]
    fn set_mask(&mut self, mask: u32, value: u32) {
        self.0 = (self.0 & !mask) | (value & mask);
    }
    pub fn get_bit(&self, bit: u8) -> bool {
        if (self.0 >> bit) & 1 == 1 { true } else { false }
    }
    pub fn get_bits(&self, index: u8, width: u8) -> u32 {
        let mask = (1 << width) - 1;
        (self.0 & (mask << index)) >> index
    }
    #[cfg(not(feature = "devicetest"))]
    pub fn set_bit(&mut self, bit: u8, value: bool) {
        self.set_mask(1 << bit, (if value { 1 } else { 0 }) << bit);
    }
    #[cfg(not(feature = "devicetest"))]
    pub fn set_bits(&mut self, index: u8, width: u8, value: u32) {
        let mask = (1 << width) - 1;
        self.set_mask(mask << index, value << index);
    }
    #[cfg(not(feature = "devicetest"))]
    pub fn u32(&self) -> u32 { self.0 }
}

pub struct RegisterMutator<'a> {
    register: &'a Register,
    pub mask: u32,
    pub value: u32,
}

impl<'a> RegisterMutator<'a> {
    pub fn update(register: &'a Register) -> RegisterMutator {
        RegisterMutator{register: register, mask: 0, value: 0}
    }
    pub fn reset(register: &'a Register, factory_default: u32) -> RegisterMutator {
        RegisterMutator{register: register, mask: 0xffffffff, value: factory_default}
    }
    fn set_mask(&mut self, mask: u32, value: u32) {
        self.mask |= mask;
        self.value = (self.value & !mask) | (value & mask);
    }
    pub fn set_bit(&mut self, bit: u8, value: bool) {
        self.set_mask(1 << bit, (if value { 1 } else { 0 }) << bit);
    }
    pub fn set_bits(&mut self, index: u8, width: u8, value: u32) {
        let mask = (1 << width) - 1;
        self.set_mask(mask << index, (value & mask) << index);
    }
}
impl<'a> Drop for RegisterMutator<'a> {
    fn drop(&mut self) {
        self.register.write_mask(self.mask, self.value);
    }
}


#[cfg(feature = "cortex")]
pub fn disable_interrupts() {
    unsafe {
        asm!("cpsid i" :::: "volatile");
    }
}

#[cfg(feature = "cortex")]
pub fn enable_interrupts() {
    unsafe {
        asm!("cpsie i" :::: "volatile");
    }
}


#[cfg(not(feature = "cortex"))]
pub fn enable_interrupts() {}

#[cfg(not(feature = "cortex"))]
pub fn disable_interrupts() {}


