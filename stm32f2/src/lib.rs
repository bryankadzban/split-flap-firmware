// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#![cfg_attr(not(test), no_std)]

#![allow(dead_code)]

#![feature(const_fn)]
#![feature(core_intrinsics)]
#![feature(asm)]

#[cfg(test)]
extern crate core;

pub mod debug;
pub mod atomic;
pub mod register;

use register::{PackedU32, Register, RegisterMutator};
use core::fmt;

use atomic::{AtomicUsizeCell,AtomicIsizeCell,AtomicRefCell};
use core::ops::{Deref,DerefMut};
use core::fmt::Write;

extern "C" {
    #[no_mangle]
    #[link_section = ".init_vector"]
    pub static mut __INIT_VECTOR: [Option<unsafe extern "C" fn()>; 97];
}

#[allow(non_camel_case_types)]
#[derive(Clone, Copy)]
pub enum Interrupt {
    WWDG = 0,
    PVD,
    TAMP_STAMP,
    RTC_WKUP,
    FLASH,
    RCC,
    EXTI0,
    EXTI1,
    EXTI2,
    EXTI3,
    EXTI4,
    DMA1_Stream0,
    DMA1_Stream1,
    DMA1_Stream2,
    DMA1_Stream3,
    DMA1_Stream4,

    DMA1_Stream5,
    DAM1_Stream6,
    ADC,
    CAN1_TX,
    CAN1_RX0,
    CAN1_RX1,
    CAN1_SCE,
    EXTI9_5,
    TIM1_BRK_TIM9,
    TIM1_UP_TIM10,
    TIM1_TRG_COM_TIM11,
    TIM1_CC,
    TIM2,
    TIM3,
    TIM4,
    I2C1_EV,

    I2C1_ER,
    I2C2_EV,
    I2C2_ER,
    SPI1,
    SPI2,
    USART1,
    USART2,
    USART3,
    EXT15_10,
    RTC_Alarm,
    OTG_FS_WKUP,
    TIM8_BRK_TIM12,
    TIM8_UP_TIM13,
    TIM8_TRG_COM_TIM14,
    TIM8_CC,
    DMA1_Stream7,

    FSMC,
    SDIO,
    TIM5,
    SPI3,
    UART4,
    UART5,
    TIM6_DAC,
    TIM7,
    DMA2_Stream0,
    DMA2_Stream1,
    DMA2_Stream2,
    DMA2_Stream3,
    DMA2_Stream4,
    ETH,
    ETH_WKUP,
    CAN2_TX,

    CAN2_RX0,
    CAN2_RX1,
    CAN2_SCE,
    OTG_FS,
    DMA2_Stream5,
    DMA2_Stream6,
    DMA2_Stream7,
    USART6,
    I2C3_EV,
    I2C3_ER,
    OTG_HS_EP1_OUT,
    OTG_HS_EP1_IN,
    OTG_HS_WKUP,
    OTG_HS,
    DCMI,
    CRYP,

    HASH_RNG,
}

enum FieldType {
    ReadWrite,
    ReadOnly,
    // Same as ReadWrite, but value can only be set to 1 (and bit-banding will always be used to
    // set the value, which may cause trouble with interrupt masks)
    SetOnly,
    // Same as SetOnly, but when set directly, the entire word will be written, with all other bits
    // set to 0. Do not mix with ReadWrite or SetOnly fields in the same register. Useful for
    // clearing specific bits in interrupt registers without clearing all the firing interrupts
    // (due to the read-modify-write done by the bit-banding hardware).
    SetOnlyByWord,
}

// First arg is whether to build a fn pair for the mutator or register (mutator is true).
// Second and third args are the setter and getter fn names.
// Fourth arg is the field type (see the FieldType enum above).
// Fifth arg is the first bit index, or the ident argument if it's block-based.
// Sixth arg is the bit count if the field is more than one bit wide, or the multiplier if it's
// block-based.
// Seventh arg is the value to be added, if the field is block-based.
macro_rules! field {
    (true, $setter:ident, $field:ident, ReadWrite, $bit:expr) => {
        pub fn $setter(mut self, val: bool) -> Self { self.0.set_bit($bit, val); self }
    };
    (true, $setter:ident, $field:ident, ReadOnly, $bit:expr) => {
    };
    (true, $setter:ident, $field:ident, SetOnly, $bit:expr) => {
        pub fn $setter(mut self) -> Self { self.0.set_bit($bit, true); self }
    };
    (true, $setter:ident, $field:ident, SetOnlyByWord, $bit:expr) => {
        pub fn $setter(mut self) -> Self { self.0.set_bit($bit, true); self }
    };
    (true, $setter:ident, $field:ident, ReadWrite, $bit:expr, $count:expr) => {
        pub fn $setter(mut self, val: u32) -> Self { self.0.set_bits($bit, $count, val); self }
    };
    (true, $setter:ident, $field:ident, ReadOnly, $bit:expr, $count:expr) => {
    };
    (true, $setter:ident, $field:ident, ReadWrite, $arg:ident, $mul:expr, $add:expr) => {
        pub fn $setter(mut self, $arg: u8, val: bool) -> Self { self.0.set_bit($arg*$mul+$add, val); self }
    };
    (true, $setter:ident, $field:ident, ReadOnly, $arg:ident, $mul:expr, $add:expr) => {
    };
    (true, $setter:ident, $field:ident, SetOnly, $arg:ident, $mul:expr, $add:expr) => {
        pub fn $setter(mut self, $arg: u8) -> Self { self.0.set_bit($arg*$mul+$add, true); self }
    };
    (true, $setter:ident, $field:ident, SetOnlyByWord, $arg:ident, $mul:expr, $add:expr) => {
        pub fn $setter(mut self, $arg: u8) -> Self { self.0.set_bit($arg*$mul+$add, true); self }
    };
    (false, $setter:ident, $field:ident, ReadWrite, $bit:expr) => {
        pub fn $setter(&self, val: bool) { self.0.write_bit($bit, val); }
        pub fn $field(&self) -> bool { self.0.read_bit($bit) }
    };
    (false, $setter:ident, $field:ident, ReadOnly, $bit:expr) => {
        pub fn $field(&self) -> bool { self.0.read_bit($bit) }
    };
    (false, $setter:ident, $field:ident, SetOnly, $bit:expr) => {
        pub fn $setter(&self) { self.0.write_bit($bit, true); }
        pub fn $field(&self) -> bool { self.0.read_bit($bit) }
    };
    (false, $setter:ident, $field:ident, SetOnlyByWord, $bit:expr) => {
        pub fn $setter(&self) { self.0.write(1 << $bit); }
        pub fn $field(&self) -> bool { self.0.read_bit($bit) }
    };
    (false, $setter:ident, $field:ident, ReadWrite, $bit:expr, $count:expr) => {
        pub fn $setter(&self, val: u32) { self.0.write_bits($bit, $count, val); }
        pub fn $field(&self) -> u32 { self.0.read_bits($bit, $count) }
    };
    (false, $setter:ident, $field:ident, ReadOnly, $bit:expr, $count:expr) => {
        pub fn $field(&self) -> u32 { self.0.read_bits($bit, $count) }
    };
    (false, $setter:ident, $field:ident, ReadWrite, $arg:ident, $mul:expr, $add:expr) => {
        pub fn $setter(&self, $arg: u8, val: bool) { self.0.write_bit($arg*$mul+$add, val); }
        pub fn $field(&self, $arg: u8) -> bool { self.0.read_bit($arg*$mul+$add) }
    };
    (false, $setter:ident, $field:ident, ReadOnly, $arg:ident, $mul:expr, $add:expr) => {
        pub fn $field(&self, $arg: u8) -> bool { self.0.read_bit($arg*$mul+$add) }
    };
    (false, $setter:ident, $field:ident, SetOnly, $arg:ident, $mul:expr, $add:expr) => {
        pub fn $setter(&self, $arg: u8) { self.0.write_bit($arg*$mul+$add, true); }
        pub fn $field(&self, $arg: u8) -> bool { self.0.read_bit($arg*$mul+$add) }
    };
    (false, $setter:ident, $field:ident, SetOnlyByWord, $arg:ident, $mul:expr, $add:expr) => {
        pub fn $setter(&self, $arg: u8) { self.0.write(1 << ($arg*$mul+$add)); }
        pub fn $field(&self, $arg: u8) -> bool { self.0.read_bit($arg*$mul+$add) }
    };
}

// Generate a register struct, its mutator, and its impl
// First two args are the name of the register type and the name of the mutator type.
// Next is the factory-default value of the register.
// Next is an optional pair of function name and value-struct type; if present, a function is
// generated to return an instance of the value type.
// Next is a sequence of paren-grouped args to pass to a sequence of field!() macro invocations, to
// generate the fields in the register.
macro_rules! register {
    ($register:ident, $mutator:ident, $factory_default:expr, $(($($arg:tt),*)),*) => {
        pub struct $mutator<'a>(RegisterMutator<'a>);
        impl<'a> $mutator<'a> {
            $(field!(true, $($arg),*);)*
        }

        #[repr(C)]
        pub struct $register(Register);
        impl $register {
            $(field!(false, $($arg),*);)*

            pub fn update(&self) -> $mutator {
                $mutator(RegisterMutator::update(&self.0))
            }
            pub fn reset(&self) -> $mutator {
                $mutator(RegisterMutator::reset(&self.0, $factory_default))
            }
        }
    };
    ($register:ident, $mutator:ident, $factory_default:expr, $valuefn:ident, $value:ident, $(($($arg:tt),*)),*) => {
        pub struct $mutator<'a>(RegisterMutator<'a>);
        impl<'a> $mutator<'a> {
            $(field!(true, $($arg),*);)*
        }

        #[repr(C)]
        pub struct $register(Register);
        impl $register {
            $(field!(false, $($arg),*);)*

            pub fn $valuefn(&self) -> $value {
                $value(PackedU32(self.0.read()))
            }

            pub fn update(&self) -> $mutator {
                $mutator(RegisterMutator::update(&self.0))
            }
            pub fn reset(&self) -> $mutator {
                $mutator(RegisterMutator::reset(&self.0, $factory_default))
            }
        }
    };
}

// Generate a getter for a field in a Value struct
macro_rules! value_field {
    ($field:ident, $bit:expr) => {
        pub fn $field(&self) -> bool { self.0.get_bit($bit) }
    };
    ($field:ident, $bit:expr, $count:expr) => {
        pub fn $field(&self) -> u32 { self.0.get_bits($bit, $count) }
    };
    ($field:ident, $arg:ident, $mul:expr, $add:expr) => {
        pub fn $field(&self, $arg: u8) -> bool { self.0.get_bit($arg*$mul+$add) }
    };
}

// Generate a write!() call for a field in a Value struct
macro_rules! display {
    // 1-bit fields use simple boolean presence/absence of the field name
    ($name:ident, $sym:tt, $bit:tt) => {
        |me: &$name, f: &mut fmt::Formatter| -> fmt::Result {
            if me.$sym() {
                write!(f, "{} ", stringify!($sym))
            } else {
                Ok(())
            }
        }
    };
    // Multi-bit fields show the field name and its (hex) value
    ($name:ident, $sym:tt, $bit:tt, $count:tt) => {
        |me: &$name, f: &mut fmt::Formatter| -> fmt::Result {
            write!(f, "{}: 0x{:x} ", stringify!($sym), me.$sym())
        }
    };
    // There are no block-based Value structs as yet.
}

// Generate a Value struct and its Display impl
macro_rules! value {
    ($name:ident, $interesting:ident, $body:expr, $(($sym:tt, $($arg:tt),*)),*) => {
        pub struct $name(PackedU32);
        impl $name {
            $(value_field!($sym, $($arg),*);)*

            pub fn $interesting(&self) -> bool {
                $body(self)
            }
        }

        impl fmt::Display for $name {
            fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
                $( display!($name, $sym, $($arg),*)(self, f)?; )*
                Ok(())
            }
        }
    };
}

register!(
    GpTimerCr1, GpTimerCr1Mutator, 0x0000,
    // Counter enable
    (set_cen, cen, ReadWrite, 0),
    // Update disable
    (set_udis, udis, ReadWrite, 1),
    // Update Request source
    (set_urs, urs, ReadWrite, 2),
    // One-pulse mode
    (set_opm, opm, ReadWrite, 3),
    // Direction: false=upcounter, true=downcounter
    (set_dir, dir, ReadWrite, 4),
    // Center-aligned mode selection
    (set_cms, cms, ReadWrite, 5, 2),
    // Auto-reload preload enable: 0: not buffered, 1: buffered
    (set_arpe, arpe, ReadWrite, 7),
    // filter clock division: 0: 1x, 1: 2x, 3: 4x
    (set_ckd, ckd, ReadWrite, 8, 2));

register!(
    GpTimerCr2, GpTimerCr2Mutator, 0x0000,
    // Capture/compare DMA selection
    (set_ccds, ccds, ReadWrite, 3),
    // Master Mode selection
    (set_mms, mms, ReadWrite, 4, 3),
    // TI1 selection: 0: CH1, 1: CH1 xor CH2 xor CH3
    (set_ti1s, ti1s, ReadWrite, 7));

register!(
    GpTimerDie, GpTimerDieMutator, 0x0000,
    // Update interrupt enabled
    (set_uie, uie, ReadWrite, 0),
    // Capture/compare X interrupt enabled
    (set_cc1ie, cc1ie, ReadWrite, 1),
    (set_cc2ie, cc2ie, ReadWrite, 2),
    (set_cc3ie, cc3ie, ReadWrite, 3),
    (set_cc4ie, cc4ie, ReadWrite, 4),
    // Trigger interrupt enabled
    (set_tie, tie, ReadWrite, 6));

register!(
    GpTimerS, GpTimerSMutator, 0x0000,
    // Update interrupt flag; clear by software
    (set_uif, uif, ReadWrite, 0),
    // Capture/compare X interrupt flag
    (set_cc1if, cc1if, ReadWrite, 1),
    (set_cc2if, cc2if, ReadWrite, 2),
    (set_cc3if, cc3if, ReadWrite, 3),
    (set_cc4if, cc4if, ReadWrite, 4),
    // Trigger interrupt flag
    (set_tif, tif, ReadWrite, 6),
    // Capture/compare X overcapture interrupt flag
    (set_cc1of, cc1of, ReadWrite, 9),
    (set_cc2of, cc2of, ReadWrite, 10),
    (set_cc3of, cc3of, ReadWrite, 11),
    (set_cc4of, cc4of, ReadWrite, 12));

register!(
    GpTimerEg, GpTimerEgMutator, 0x0000,
    // Update generation
    (set_ug, ug, ReadWrite, 0),
    // Trigger generation
    (set_tg, tg, ReadWrite, 6));

register!(
    GpTimerCcmr1, GpTimerCcmr1Mutator, 0x0000,
    // 0: CC1 is output
    // 1: CC1 is input; IC1 -> TI1;
    // 2: CC1 is input; IC1 -> TI2
    // 3: CC1 is input; IC1 -> TRC
    (set_cc1s, cc1s, ReadWrite, 0, 2),
    // output-compare fast enable
    (set_oc1fe, oc1fe, ReadWrite, 2),
    // output-compare preload enable
    (set_oc1pe, oc1pe, ReadWrite, 3),
    // 0: frozen
    // 1: set active on match
    // 2: set inactive on match
    // 3: toggle on match
    // 4: force inactive
    // 5: force active
    // 6: PWM mode 1 (active when CNT < CCR1, whether counting up or down)
    // 7: PWM mode 2 (active when CNT > CCR1, whether counting up or down)
    (set_oc1m, oc1m, ReadWrite, 4, 3),
    // clear-enable
    (set_oc1ce, oc1ce, ReadWrite, 7),

    // 0: CC2 is output
    // 1: CC2 is input; IC2 -> TI1;
    // 2: CC2 is input; IC2 -> TI2
    // 3: CC2 is input; IC2 -> TRC
    (set_cc2s, cc2s, ReadWrite, 8, 2),
    // output-compare fast enable
    (set_oc2fe, oc2fe, ReadWrite, 10),
    // output-compare preload enable
    (set_oc2pe, oc2pe, ReadWrite, 11),
    // same as oc1m above
    (set_oc2m, oc2m, ReadWrite, 12, 3),
    // clear-enable
    (set_oc2ce, oc2ce, ReadWrite, 15));

register!(
    GpTimerCcmr2, GpTimerCcmr2Mutator, 0x0000,
    // 0: CC3 is output
    // 1: CC3 is input; IC3 -> TI1;
    // 2: CC3 is input; IC3 -> TI2
    // 3: CC3 is input; IC3 -> TRC
    (set_cc3s, cc3s, ReadWrite, 0, 2),
    // output-compare fast enable
    (set_oc3fe, oc3fe, ReadWrite, 2),
    // output-compare preload enable
    (set_oc3pe, oc3pe, ReadWrite, 3),
    // same as oc1m above
    (set_oc3m, oc3m, ReadWrite, 4, 3),
    // clear-enable
    (set_oc3ce, oc3ce, ReadWrite, 7),

    // 0: CC4 is output
    // 1: CC4 is input; IC4 -> TI1;
    // 2: CC4 is input; IC4 -> TI2
    // 3: CC4 is input; IC4 -> TRC
    (set_cc4s, cc4s, ReadWrite, 8, 2),
    // output-compare fast enable
    (set_oc4fe, oc4fe, ReadWrite, 10),
    // output-compare preload enable
    (set_oc4pe, oc4pe, ReadWrite, 11),
    // same as oc1m above
    (set_oc4m, oc4m, ReadWrite, 12, 3),
    // clear-enable
    (set_oc4ce, oc4ce, ReadWrite, 15));

register!(
    GpTimerEnable, GpTimerEnableMutator, 0x0000,
    // is input capture or output compare (depending on config) enabled?
    // channel goes from 0 to 3; bit position 0/4/8/12
    (set_cce, cce, ReadWrite, chan, 4, 0),
    // polarity
    // for output compare, keep CCxNP clear.  CCxP determines level:
    //   0: active-high
    //   1: active-low.
    // for input capture, CCxNP/CCxP together determine polarity:
    //  00: rising edge, input not inverted
    //  01: falling edge, input inverted
    //  10: reserved, do not use
    //  11: both edges, input not inverted (do not use for encoder mode)
    (set_ccp, ccp, ReadWrite, chan, 4, 1),
    (set_ccnp, ccnp, ReadWrite, chan, 4, 3));


#[repr(C)]
pub struct GpTimerBlock {
    pub cr1: GpTimerCr1,  // 14.4.1
    pub cr2: GpTimerCr2,
    pub smc: Register,  // .3 slave-mode-control
    pub die: GpTimerDie, // .4 DMA/Interrupt enable

    pub s: GpTimerS,
    pub eg: GpTimerEg,  // .6 event generation
    pub ccmr1: GpTimerCcmr1,  // .7 capture/compare mode
    pub ccmr2: GpTimerCcmr2,  // .8

    pub cce: GpTimerEnable,  // .9  capture/compare enable
    pub cnt: Register,  // .10 counter
    pub psc: Register,  // .11 prescaler
    pub ar: Register,  // .12 auto-reload

    pub reserved1: Register, 
    pub ccr1: Register,  // .13 capture/compare
    pub ccr2: Register,  // .14
    pub ccr3: Register,  // .15

    pub ccr4: Register,  // .16
    pub reserved2: Register, 
    pub dc: Register,  // .17 DMA control
    pub dma: Register,  // .18 DMA address

    pub t2_o: Register,  // .19 TIM2 option
    pub t5_o: Register,  // .20 TIM5 option
}

impl GpTimerBlock  {
    pub fn stop(&self) {
        self.cr1.set_cen(false);
    }
    pub fn reset_cci(&self) {
        self.die.update()
            .set_cc1ie(false)
            .set_cc2ie(false)
            .set_cc3ie(false)
            .set_cc4ie(false);
        self.s.update()
            .set_cc1if(false)
            .set_cc2if(false)
            .set_cc3if(false)
            .set_cc4if(false);
    }
}

register!(
    DmaTimerCr1, DmaTimerCr1Mutator, 0x0000,
    // Counter enable
    (set_cen, cen, ReadWrite, 0),
    // Update disable
    (set_udis, udis, ReadWrite, 1),
    // Update Request source
    (set_urs, urs, ReadWrite, 2),
    // One-pulse mode 
    (set_opm, opm, ReadWrite, 3),
    // Auto-reload preload enable: 0: not buffered, 1: buffered
    (set_arpe, arpe, ReadWrite, 7));

register!(
    DmaTimerCr2, DmaTimerCr2Mutator, 0x0000,
    (set_mms, mms, ReadWrite, 4, 3));

register!(
    DmaTimerDie, DmaTimerDieMutator, 0x0000,
    // Update causes interrupt
    (set_uie, uie, ReadWrite, 0),
    // Update causes DMA request
    (set_ude, ude, ReadWrite, 8));

register!(
    DmaTimerS, DmaTimerSMutator, 0x0000,
    // Update interrupt flag; clear by software
    (set_uif, uif, ReadWrite, 0));

register!(
    DmaTimerEg, DmaTimerEgMutator, 0x0000,
    // Update generation
    (set_ug, ug, ReadWrite, 0));

#[repr(C)]
pub struct DmaTimerBlock {
    pub cr1: DmaTimerCr1,  // 16.4.1
    pub cr2: DmaTimerCr2,  // .2
    pub reserved1: Register,
    pub die: DmaTimerDie,  // .3 DMA/Interrupt enable

    pub s: DmaTimerS,    // .4 status
    pub eg: DmaTimerEg,  // .5 event generation
    pub reserved2: Register,
    pub reserved3: Register,
    pub reserved4: Register,
    pub cnt: Register,  // .6 counter
    pub psc: Register,  // .7 prescaler
    pub ar: Register,  // .8 auto-reload
}

impl DmaTimerBlock  {
    pub fn stop(&self) {
        self.cr1.set_cen(false);
    }
}

#[repr(C)]
pub struct IwdgBlock {
    pub key: Register, // 0:15 key (0xaaaa == reset, 0xcccc == start, 0x5555 == configure
    pub prescaler: Register, // 0:2 prescaler (4 << N)
    pub reload: Register, // 0:11 counter reload
    pub status: Register, // bit 0: set during prescalar update, bit 1, set during reload update
}

#[derive(Clone, Copy)]
pub struct GpioPin {
    pub block: &'static GpioBlock,
    pub bitband_idr: usize,
    pub bitband_odr: usize,
    pub index: u8,
}
impl GpioPin {
    pub fn get(&self) -> bool {
        if unsafe { core::intrinsics::volatile_load(self.bitband_idr as *const u32) } == 1 { true } else { false }
    }
    pub fn set(&mut self, val: bool) {
        unsafe { core::intrinsics::volatile_store(self.bitband_odr as *mut u32, if val { 1 } else { 0 }); }
    }

    pub fn configured(mut self, mode: GpioMode, pull: GpioPull) -> GpioPin {
        self.configure(mode, pull);
        self
    }
    pub fn configure(&mut self, mode: GpioMode, pull: GpioPull) {

        self.reg_write(&self.block.otype, 1, match mode {
            GpioMode::Output(GpioOutType::PushPull, _, _) => 0,
            GpioMode::Output(GpioOutType::OpenDrain, _, _) => 1,
            GpioMode::AlternateOut(_, GpioOutType::PushPull, _) => 0,
            GpioMode::AlternateOut(_, GpioOutType::OpenDrain, _) => 1,
            _ => 0
        });
        self.reg_write(&self.block.ospeed, 2, match mode {
            GpioMode::Output(_, GpioOutSpeed::Low, _) => 0,
            GpioMode::Output(_, GpioOutSpeed::Medium, _) => 1,
            GpioMode::Output(_, GpioOutSpeed::Fast, _) => 2,
            GpioMode::Output(_, GpioOutSpeed::High, _) => 3,
            GpioMode::AlternateOut(_, _, GpioOutSpeed::Low) => 0,
            GpioMode::AlternateOut(_, _, GpioOutSpeed::Medium) => 1,
            GpioMode::AlternateOut(_, _, GpioOutSpeed::Fast) => 2,
            GpioMode::AlternateOut(_, _, GpioOutSpeed::High) => 3,

            _ => 0
        });
        self.reg_write(&self.block.pupd, 2, match pull {
            GpioPull::None => 0,
            GpioPull::Up => 1,
            GpioPull::Down => 2,
        });
        if let 0..=7 = self.index {
            self.reg_write(&self.block.afrl, 4, match mode {
                GpioMode::AlternateIn(a) => a as u32,
                GpioMode::AlternateOut(a, _, _) => a as u32,
                _ => 0
            });
        }
        if let 8..=15 = self.index {
            self.reg_write(&self.block.afrh, 4, match mode {
                GpioMode::AlternateIn(a) => a as u32,
                GpioMode::AlternateOut(a, _, _) => a as u32,
                _ => 0,
            });
        }
        if let GpioMode::Output(_, _, init_val) = mode {
            self.set(init_val);
        }
        self.reg_write(&self.block.mode, 2, match mode {
            GpioMode::Input => 0,
            GpioMode::Output(_, _, _) => 1,
            GpioMode::AlternateIn(_) => 2,
            GpioMode::AlternateOut(..) => 2,
            GpioMode::Analog => 3,
        });
    }
    pub fn with_interrupts(mut self, config: GpioIrqConfig) -> GpioPin {
        match gpio_block_index(self.block) {
            Some(block_index) => {
                // These four could maybe be changed to bitband, but eh, this is only ever called
                // once, and that would inflate the size of GpioPin by another 16 bytes.  This is
                // fine for now.
                self.reg_write(&EXTI().im, 1, if config.irq { 1 } else { 0 });
                self.reg_write(&EXTI().em, 1, if config.wakeup { 1 } else { 0 });
                self.reg_write(&EXTI().rts, 1, if config.rising { 1 } else { 0 });
                self.reg_write(&EXTI().fts, 1, if config.falling { 1 } else { 0 });
                SYSCFG().ic[((self.index as usize) >> 2) & 0x3].write_bits(
                    self.index & 0x3, 4, block_index.into());
            },
            _ => {
                panic!("Unknown block");
            }
        }
        self
    }

    fn reg_read(&self, reg: &'static Register, width: u8) -> u32{
        let shift = width * self.index;
        let mask = (1 << width) - 1;
        (reg.read() & (mask << shift)) >> shift
    }

    fn reg_write(&mut self, reg: &'static Register, width: u8, value: u32) {
        let shift = width * self.index;
        let mask = (1 << width) - 1;
        reg.write_mask(mask << shift, value << shift);
    }
}

fn bitband(addr: usize, bit: usize) -> usize {
    (addr << 5) | (bit << 2) | 0x42000000
}

#[repr(C)]
pub struct GpioBlock {
    pub mode: Register, // 2x16, 00 = input, 1 = output, 2 = alternate, 3 = analog
    pub otype: Register, // 1x16: 0 = push-pull, 1 = open-drain
    pub ospeed: Register, // 2x16: 00 = low, 01 = medium, 10 = fast, 11 = high
    pub pupd: Register, // 2x16: 00 == none, 01 = pull-up, 10 = pull-down, 11 = reserved
    pub idr: Register, // 1x16: input data
    pub odr: Register, // 1x16: output data
    pub bssr: Register, // 0:15 port n set, 16:31 port n reset,
    // Lock port config until next reset
    pub lck: Register, // 0:15 lock key, 16: 0 = not active, 1 = active
    // Alternate function matrix
    pub afrl: Register, // 4x8 ports 0..7 1111=event
    pub afrh: Register, // 4x8 port 8..16
}
impl GpioBlock {
    pub fn pin(&'static self, index: u8) -> GpioPin {
        GpioPin{
            block: self,
            bitband_idr: bitband(&self.idr as *const Register as usize, index.into()),
            bitband_odr: bitband(&self.odr as *const Register as usize, index.into()),
            index: index,
        }
    }
}

pub struct U8Register(u8);
impl U8Register {
    pub fn write(&self, val: u8) {
        unsafe { core::intrinsics::volatile_store(&self.0 as *const u8 as *mut u8, val); }
    }
}

#[repr(C)]
pub struct NvicBlock {
    // 0xe000e100
    // interrupt set-enable registers
    pub iser: [Register; 3],
    pub reserved1: [u32; 29],
    // 0xe000e180
    // interrupt clear-enable registers
    pub icer: [Register; 3],
    pub reserved2: [u32; 29],
    // 0xe000e200
    // interrupt set-pending registers
    pub ispr: [Register; 3],
    pub reserved3: [u32; 29],
    // 0xe000e280
    // interrupt clear-pending registers
    pub icpr: [Register; 3],
    pub reserved4: [u32; 29],
    // 0xe000e300
    // interrupt active-bit registers
    pub iabr: [Register; 3],
    pub reserved5: [u32; 29],
    // 0xe000e380
    pub reserved6: [u32; 32],
    // 0xe000e400
    // interrupt priority registers (8 bits per interrupt, without the 16 system handlers)
    // per the ARM documentation, "these registers are byte accessible" on cortex m3:
    // http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.dui0552a/Cihgjeed.html
    pub ip: [U8Register; 240],
}
impl NvicBlock {
    pub fn clear(&self) {
        for r in self.icer.iter() {
            r.write(0xffffffff);
        }
        for r in self.icpr.iter() {
            r.write(0xffffffff);
        }
    }
    pub fn enable(&self, int: Interrupt) {
        let num = int as usize;
        let index = num >> 5;
        if index < self.iser.len() {
            self.iser[index].write(1 << (num & 0x1f));
        }
    }
    pub fn disable(&self, int: Interrupt) {
        let num = int as usize;
        let index = num >> 5;
        if index < self.iser.len() {
            self.icer[index].write(1 << (num & 0x1f));
        }
    }
    pub fn is_enabled(&self, int: Interrupt) -> bool {
        let num = int as usize;
        let index = num >> 5;
        if index < self.iser.len() {
            self.iser[index].read_bit((num & 0x1f) as u8)
        } else {
            false
        }
    }
    pub fn set_priority(&self, int: Interrupt, priority: u8) {
        let num = int as usize - 16;
        if num < self.ip.len() {
            self.ip[num].write(priority);
        }
    }
    pub fn set_all_prios_except(&self, int: Interrupt, priority: u8) {
        let num = int as usize - 16;
        if num >= self.ip.len() { return }
        for i in 0..self.ip.len() {
            if i != num { self.ip[i].write(priority); }
        }
    }
    pub fn any_active(&self) -> bool {
        (self.iabr[0].read() | self.iabr[1].read() | self.iabr[2].read()) != 0
    }
}

#[repr(C)]
pub struct ExtiBlock {
    // interrupt mask
    pub im: Register,
    // event mask
    pub em: Register,
    // rising trigger selection
    pub rts: Register,
    // falling trigger selection
    pub fts: Register,
    // software interrupt event
    pub swie: Register,
    // pending
    pub p: Register,
}

register!(InterruptControlStateRegister, InterruptControlStateRegisterMutator, 0x0000,
    (set_vectactive, vectactive, ReadOnly, 0, 9),
    (set_rettobase, rettobase, ReadOnly, 11),
    (set_vectpending, vectpending, ReadOnly, 12, 10));

#[repr(C)]
pub struct SystemControlBlock {
    // cpuid bitfields
    pub cpuid: Register,
    // interrupt-control state
    pub icsr: InterruptControlStateRegister,
    // vector table offset register
    pub vtor: Register,
    // application interrupt and reset-control register
    pub aircr: Register,
    // system-control register
    pub scr: Register,
    // configuration-control register
    pub ccr: Register,
    // system handler priority registers:
    // mem_manage, bus_fault, usage_fault, reserved
    // reserved, reserved, reserved, svcall
    // debug_monitor, reserved, pendsv, systick
    pub sh_prio: [u8; 12],
    // system handler control and state register
    // fault-active bits:
    //  0: mem-fault
    //  1: bus-fault
    //  3: usage-fault
    //  7: svcall
    //  8: debug-monitor
    // 10: pendsv
    // 11: systick
    pub shcsr: Register,
}

impl SystemControlBlock {
    pub fn any_fault_active(&self) -> bool {
        self.shcsr.read() & 0xd8b != 0
    }
}

#[repr(C)]
pub struct SyscfgBlock {
    pub memrmp: Register,
    pub pmc: Register,
    pub ic: [Register; 4],
}

register!(
    UsartSr, UsartSrMutator, 0x00C00000,
    // parity error
    (set_pe, pe, ReadOnly, 0),
    // framing error
    (set_fe, fe, ReadOnly, 1),
    // noise detected flag
    (set_nf, nf, ReadOnly, 2),
    // overrun error
    (set_ore, ore, ReadOnly, 3),
    // idle line detected
    (set_idle, idle, ReadOnly, 4),
    // read data register not empty
    (set_rxne, rxne, ReadWrite, 5),
    // transmission complete
    (set_tc, tc, ReadWrite, 6),
    // transmission data register empty
    (set_txe, txe, ReadOnly, 7),
    // line break detected
    (set_lbd, lbd, ReadWrite, 8),
    // CTS flag
    (set_cts, cts, ReadWrite, 9));

register!(
    UsartBrr, UsartBrrMutator, 0x00000000,
    (set_div_fraction, div_fraction, ReadWrite, 0, 4),
    (set_div_mantissa, div_mantissa, ReadWrite, 4, 11));

register!(
    UsartCr, UsartCrMutator, 0x00000000,
    // send break
    (set_sbk, sbk, ReadWrite, 0),
    // receiver wakeup
    (set_rwu, rwu, ReadWrite, 1),
    // receiver enable
    (set_re, re, ReadWrite, 2),
    // transmitter enable
    (set_te, te, ReadWrite, 3),
    // idle interrupt enable
    (set_idleid, idleid, ReadWrite, 4),
    // rxne interrupt enable
    (set_rxneie, rxneie, ReadWrite, 5),
    // transmission complete interrupt enable
    (set_tcie, tcie, ReadWrite, 6),
    // txe interrupt enable
    (set_txeie, txeie, ReadWrite, 7),
    // pe interrupt enable
    (set_peie, peie, ReadWrite, 8),
    // parity selection: 0=even 1=odd
    (set_ps, ps, ReadWrite, 9),
    // parity control enable
    (set_pce, pce, ReadWrite, 10),
    // wakeup method: 0=idle line, 1=address mark
    (set_wake, wake, ReadWrite, 11),
    // word length 0=1stop 8data nstop  1=1stop 9data nstop
    (set_m, m, ReadWrite, 12),
    // usart enable
    (set_ue, ue, ReadWrite, 13),
    // oversampling: 0=16, 1=8
    (set_over8, over8, ReadWrite, 15));

register!(
    UsartCr2, UsartCr2Mutator, 0x00000000,
    // finish this
    // 0=1, 1=0.5, 2=2, 3=1.5
    (set_stop, stop, ReadWrite, 13, 2));

register!(
    UsartCr3, UsartCr3Mutator, 0x00000000,
    (set_dmar, dmar, ReadWrite, 6),
    (set_dmat, dmat, ReadWrite, 7));

#[repr(C)]
pub struct UsartBlock {
    pub s: UsartSr,
    pub d: Register,
    pub br: Register,
    pub c: UsartCr,
    pub c2: UsartCr2,
    pub c3: UsartCr3,
    pub gtp: Register,
}

register!(
    RccCfgr, RccCfgrMutator, 0x00000000,
    (set_mco1, mco1, ReadWrite, 21, 2));

register!(
    RccPllCfg, RccPllCfgMutator, 0x24003010, read, RccPllCfgValue,);

register!(
    RccApb2Rst, RccApb2RstMutator, 0x00000000,
    // send break
    (set_usart1rst, usart1rst, ReadWrite, 4),
    (set_sdiorst, sdiorst, ReadWrite, 11));

register!(
    RccApb1Enr, RccApb1EnrMutator, 0x00000000,
    (set_tim2en, tim2en, ReadWrite, 0),
    (set_tim3en, tim3en, ReadWrite, 1),
    (set_tim4en, tim4en, ReadWrite, 2),
    (set_tim5en, tim5en, ReadWrite, 3),
    (set_tim6en, tim6en, ReadWrite, 4),
    (set_tim7en, tim7en, ReadWrite, 5),
    (set_tim12en, tim12en, ReadWrite, 6),
    (set_tim13en, tim13en, ReadWrite, 7),
    (set_tim14en, tim14en, ReadWrite, 8),
    (set_wwdgen, wwdgen, ReadWrite, 11),
    (set_i2c1en, i2c1en, ReadWrite, 21),
    (set_dacen, dacen, ReadWrite, 29));

register!(
    RccApb2Enr, RccApb2EnrMutator, 0x00000000,
    (set_tim1en, tim1en, ReadWrite, 0),
    (set_tim8en, tim8en, ReadWrite, 1),
    (set_usart1en, usart1en, ReadWrite, 4),
    (set_sdioen, sdioen, ReadWrite, 11),
    (set_tim9en, tim9en, ReadWrite, 16),
    (set_tim10en, tim10en, ReadWrite, 17),
    (set_tim11en, tim11en, ReadWrite, 18));

register!(
    RccAhb1Enr, RccAhb1EnrMutator, 0x00000000,
    (set_gpioaen, gpioaen, ReadWrite, 0),
    (set_gpioben, gpioben, ReadWrite, 1),
    (set_gpiocen, gpiocen, ReadWrite, 2),
    (set_gpioden, gpioden, ReadWrite, 3),
    (set_dma1en, dma1en, ReadWrite, 21),
    (set_dma2en, dma2en, ReadWrite, 22),
    (set_otghsen, otghsen, ReadWrite, 29));

value!(
    RccPllCfgValue, is_interesting, |_: &RccPllCfgValue| { true },
    (q, 24, 4),
    (src, 22),
    (p, 16, 2),
    (n, 6, 9),
    (m, 0, 6));

#[repr(C)]
pub struct RccBlock {
    pub a00: Register,
    pub pllcfg: RccPllCfg,
    pub cfgr: RccCfgr,
    pub a0c: Register,
    pub a10: Register,
    pub a14: Register,
    pub a18: Register,
    pub a1c: Register,
    pub a20: Register,
    pub apb2rst: RccApb2Rst,
    pub a28: Register,
    pub a2c: Register,
    pub ahb1en: RccAhb1Enr,
    pub a34: Register,
    pub a38: Register,
    pub a3c: Register,

    pub apb1en: RccApb1Enr,
    pub apb2en: RccApb2Enr,
    pub a48: Register,
    pub a4c: Register,
}

register!(
    FlashStatus, FlashStatusMutator, 0x00000000,
    (clear_eop, eop, SetOnlyByWord, 0),
    (clear_operr, operr, SetOnlyByWord, 1),
    (clear_wrperr, wrperr, SetOnlyByWord, 4),
    (clear_pgaerr, pgaerr, SetOnlyByWord, 5),
    (clear_pgperr, pgperr, SetOnlyByWord, 6),
    (clear_pgserr, pgserr, SetOnlyByWord, 7),
    (set_bsy, bsy, ReadOnly, 16));

register!(
    FlashControl, FlashControlMutator, 0x80000000,
    (set_pg, pg, ReadWrite, 0),
    (set_ser, ser, ReadWrite, 1),
    (set_mer, mer, ReadWrite, 2),
    (set_snb, snb, ReadWrite, 3, 5),
    (set_psize, psize, ReadWrite, 8, 2),
    (set_strt, strt, ReadWrite, 16),
    (set_eopie, eopie, ReadWrite, 24),
    (set_errie, errie, ReadWrite, 25),
    (set_lock, lock, ReadWrite, 31));

register!(
    FlashOptControl, FlashOptControlMutator, 0x0FFFAAED,
    (set_optlock, optlock, ReadWrite, 0),
    (set_optstrt, optstrt, ReadWrite, 1),
    (set_bor_level, bor_level, ReadWrite, 2, 2),
    (set_wdg_sw, wdg_sw, ReadWrite, 5),
    (set_nrst_stop, nrst_stop, ReadWrite, 6),
    (set_nrst_stdby, nrst_stdby, ReadWrite, 7),
    (set_rdp, rdp, ReadWrite, 8, 8),
    (set_wrp, wrp, ReadWrite, i, 1, 16));

#[repr(C)]
pub struct FlashBlock {
    pub acr: Register,
    pub key: Register,
    pub optkey: Register,
    pub s: FlashStatus,
    pub c: FlashControl,
    pub optc: FlashOptControl,
}

register!(
    SdioPower, SdioPowerMutator, 0x00000000,
    (set_pwrctrl, pwrctrl, ReadWrite, 0, 2));

register!(
    SdioClkc, SdioClkcMutator, 0x00000000,
    (set_clkdiv, clkdiv, ReadWrite, 0, 8),
    (set_clken, clken, ReadWrite, 8),
    (set_pwrsav, pwrsav, ReadWrite, 9),
    (set_bypass, bypass, ReadWrite, 10),
    // 0: default; 1: 4-wide, 2: 8-wide
    (set_widbus, widbus, ReadWrite, 11, 2),
    (set_negedge, negedge, ReadWrite, 13),
    (set_hwfc_en, hwfc_en, ReadWrite, 14));

register!(
    SdioCmd, SdioCmdMutator, 0x00000000,
    (set_cmdindex, cmdindex, ReadWrite, 0, 6),
    (set_waitresp, waitresp, ReadWrite, 6, 2),
    (set_waitint, waitint, ReadWrite, 8),
    (set_waitpend, waitpend, ReadWrite, 9),
    (set_cpsmen, cpsmen, ReadWrite, 10),
    (set_sdio_suspend, sdio_suspend, ReadWrite, 11),
    (set_encmdcompl, encmdcompl, ReadWrite, 12),
    (set_nien, nien, ReadWrite, 13),
    (set_atacmd, atacmd, ReadWrite, 14));

register!(
    SdioRespCmd, SdioRespCmdMutator, 0x00000000,
    (set_respcmd, respcmd, ReadWrite, 0, 6));

register!(
    SdioDlen, SdioDlenMutator, 0x00000000,
    (set_datalength, datalength, ReadWrite, 0, 25));

register!(
    SdioDctrl, SdioDctrlMutator, 0x00000000,
    (set_dten, dten, ReadWrite, 0),
    (set_dtdir, dtdir, ReadWrite, 1),
    (set_dtmode, dtmode, ReadWrite, 2),
    (set_dmaen, dmaen, ReadWrite, 3),
    (set_dblocksize, dblocksize, ReadWrite, 4, 4),
    (set_rwstart, rwstart, ReadWrite, 8),
    (set_rwstop, rwstop, ReadWrite, 9),
    (set_rwmod, rwmod, ReadWrite, 10),
    (set_sdioen, sdioen, ReadWrite, 11));

#[cfg(not(feature = "devicetest"))]
register!(
    SdioSta, SdioStaMutator, 0x00000000, read, SdioStaValue,);

#[cfg(not(feature = "devicetest"))]
value!(
    SdioStaValue, is_error, |me: &SdioStaValue| { me.0.u32() & 0x23f != 0 },
    (ccrcfail, 0),
    (dcrcfail, 1),
    (ctimeout, 2),
    (dtimeout, 3),
    (txunderr, 4),
    (rxoverr, 5),
    (cmdrend, 6),
    (cmdsent, 7),
    (dataend, 8),
    (stbiterr, 9),
    (dbckend, 10),
    (cmdact, 11),
    (txact, 12),
    (rxact, 13),
    (txfifohe, 14),
    (rxfifohf, 15),
    (txfifof, 16),
    (rxfifof, 17),
    (txfifoe, 18),
    (rxfifoe, 19),
    (txdavl, 20),
    (rxdavl, 21),
    (sdioit, 22),
    (ceataend, 23));

register!(
    SdioFifoCnt, SdioFifoCntMutator, 0x00000000,
    (set_fifocount, fifocount, ReadWrite, 0, 24));

#[repr(C)]
#[cfg(not(feature = "devicetest"))]
pub struct SdioBlock {
    pub power: SdioPower,
    pub clkc: SdioClkc,
    pub arg: Register,
    pub cmd: SdioCmd,

    pub respcmd: SdioRespCmd,
    pub resp1: Register,
    pub resp2: Register,
    pub resp3: Register,

    pub resp4: Register,
    pub dtimer: Register,
    pub dlen: SdioDlen,
    pub dctrl: SdioDctrl,

    pub dcount: Register,
    pub sta: SdioSta,
    pub icr: Register,
    pub mask: Register,

    pub reserved1: [Register; 2], 
    pub fifocnt: SdioFifoCnt,
    pub reserved2: Register,

    pub reserved3: [Register; 12], 
    pub fifo: [Register; 32],
}

pub struct DmaStream {
    parent_block: &'static DmaBlock,
    block: &'static DmaStreamBlock,
    is: &'static Register,
    ifc: &'static Register,
    n: usize,
    pub is_bit_offset: u8,
}
impl DmaStream {
    pub fn clear_interrupt_bits(&self) {
        self.ifc.write(
            1 << self.is_bit_offset |
            1 << (self.is_bit_offset + 2) |
            1 << (self.is_bit_offset + 3) |
            1 << (self.is_bit_offset + 4) |
            1 << (self.is_bit_offset + 5));
    }
    pub fn enabled(&self) -> bool {
        self.block.c.en()
    }
    pub fn enable(&self) {
        self.block.c.set_en(true);
    }
    pub fn hack_disable(&self) {
        self.block.c.set_en(false);
    }
    pub fn nleft(&self) -> u32 {
        self.block.ndt.read()
    }
    // Fifo error interrupt
    pub fn feif(&self) -> bool { self.is.read_bit(self.is_bit_offset) }
    pub fn clear_feif(&self) { self.ifc.write(1 << self.is_bit_offset) }

    // direct-mode error interrupt
    pub fn dmeif(&self) -> bool { self.is.read_bit(self.is_bit_offset + 2) }
    pub fn clear_dmeif(&self) { self.ifc.write(1 << (self.is_bit_offset + 2)) }

    // transfer error interrupt
    pub fn teif(&self) -> bool { self.is.read_bit(self.is_bit_offset + 3) }
    pub fn clear_teif(&self) { self.ifc.write(1 << (self.is_bit_offset + 3)) }

    // half transfer interrupt
    pub fn htif(&self) -> bool { self.is.read_bit(self.is_bit_offset + 4) }
    pub fn clear_htif(&self) { self.ifc.write(1 << (self.is_bit_offset + 4)) }

    // transfer complete interrupt
    pub fn tcif(&self) -> bool { self.is.read_bit(self.is_bit_offset + 5) }
    pub fn clear_tcif(&self) { self.ifc.write(1 << (self.is_bit_offset + 5)) }

    // None if not double-buffered; 0 or 1 depending on current buffer otherwise
    pub fn double_buf_target(&self) -> Option<u8> {
        if !self.block.c.dbm() {
            None
        } else if self.block.c.ct() {
            Some(1)
        } else {
            Some(0)
        }
    }

    pub fn setup(&self, periph: u32, mem: u32, n: u32) -> &DmaStreamConfigRegister {
        self.clear_interrupt_bits();
        self.block.par.write(periph);
        self.block.m0ar.write(mem);
        self.block.ndt.write(n);
        &self.block.c
    }

    pub fn abort(&self) {
        self.block.c.set_en(false);
        // Wait for DMA transfer to stop
        while self.block.c.en() {
        }
        self.block.c.reset()
            .set_pfctrl(false)
            .set_pburst(0)
            .set_mburst(0)
            .set_pl(0);
        self.block.par.write(0);
        self.block.m0ar.write(0);
        self.block.m1ar.write(0);
    }

    pub fn start_mem<'a>(&self, dest: &'a mut [u8], src: &'a [u8]) -> DmaTransaction<(&'a mut [u8], &'a [u8])> {
        self.abort();
        self.setup(src.as_ptr() as usize as u32, dest.as_mut_ptr() as usize as u32,
                   core::cmp::min(dest.len(), src.len()) as u32).update()
            .set_dir(2) // memory-to-memory
            .set_msize(0)
            .set_psize(0)
            .set_minc(true)
            .set_pinc(true)
            .set_tcie(true)
            .set_teie(true)
            .set_en(true);
        DmaTransaction{
            block: self.parent_block,
            stream: self.n,
            buffer: (dest, src),
        }
    }
    pub fn from_sdio_periph<'a, T: Sized>(&self, src: &Register, channel: u32, dest: &'a mut [T]) -> DmaTransaction<&'a mut [T]> {
        // These flags are all SDIO-specific.  Do not change them; create another from_periph
        // variant if other values are required.
        let sz = match core::mem::size_of::<T>() {
            1 => 0,
            2 => 1,
            4 => 2,
            n => { panic!("Invalid DMA item size, must be 1/2/4: {}", n); },
        };
        self.abort();
        self.setup(src as *const Register as usize as u32, dest.as_ptr() as usize as u32,
                   dest.len() as u32).update()
            .set_chsel(channel)
            .set_dir(0) // periph-to-mem
            .set_msize(sz)   // these should probably hardcode to 32 bits... shrug
            .set_psize(sz)
            //.set_pfctrl(true)  // peripheral flow control does not work right
            .set_pburst(1)   // burst mode inc by 4
            .set_mburst(1)   // same, on memory side
            .set_pl(2)       // high priority, but not highest
            .set_minc(true)
            .set_pinc(false)
            .set_tcie(false)
            .set_teie(false);
        self.block.fc.update()
            .set_dmdis(true)   // use fifo
            // fifo threshold to full, since it's 4 words and needs to be an integer number of
            // memory burst transfers (which are one word in size and 4 transfers per burst as
            // configured above).
            .set_fth(3);
        DmaTransaction{
            block: self.parent_block,
            stream: self.n,
            buffer: dest,
        }
    }
    pub fn to_periph<'a, T: Sized>(&self, dest: &Register, channel: u32, src: &'a [T]) -> DmaTransaction<&'a [T]> {
        let sz = match core::mem::size_of::<T>() {
            1 => 0,
            2 => 1,
            4 => 2,
            n => { panic!("Invalid DMA item size, must be 1/2/4: {}", n); },
        };
        self.abort();
        self.setup(dest as *const Register as usize as u32, src.as_ptr() as usize as u32,
                   src.len() as u32).update()
            .set_chsel(channel)
            .set_dir(1) // mem-to-periph
            .set_msize(sz)
            .set_psize(sz)
            .set_minc(true)
            .set_pinc(false)
            .set_tcie(true)
            .set_teie(true);
        DmaTransaction{
            block: self.parent_block,
            stream: self.n,
            buffer: src,
        }
    }
    pub fn double_buf_to_periph<'a, T: Sized>(&self, dest: &Register, channel: u32, src_1: &'a mut [T], src_2: &'a mut [T]) -> DmaTransaction<(&'a mut [T], &'a mut [T])> {
        let sz = match core::mem::size_of::<T>() {
            1 => 0,
            2 => 1,
            4 => 2,
            n => { panic!("Invalid double-buffer DMA item size, must be 1/2/4: {}", n); },
        };
        if src_1.len() != src_2.len() {
            panic!("Slices must be the same length; got {} and {}", src_1.len(), src_2.len());
        }
        self.abort();
        self.block.par.write(dest as *const Register as usize as u32);
        self.block.m0ar.write(src_1.as_ptr() as usize as u32);
        self.block.m1ar.write(src_2.as_ptr() as usize as u32);
        self.block.ndt.write(src_1.len() as u32);
        self.block.c.update()
            .set_chsel(channel)
            .set_dir(1)  // mem-to-periph
            .set_msize(sz)
            .set_psize(sz)
            .set_minc(true)
            .set_pinc(false)
            .set_tcie(true)
            .set_teie(true)
            .set_dbm(true)  // double-buffer mode, which automatically sets CIRC
            .set_ct(false); // start on buffer 0
        DmaTransaction{
            block: self.parent_block,
            stream: self.n,
            buffer: (src_1, src_2),
        }
    }
}
impl fmt::Display for DmaStream {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.feif() { write!(f, "feif ")?; }
        if self.dmeif() { write!(f, "dmeif ")?; }
        if self.teif() { write!(f, "teif ")?; }
        if self.htif() { write!(f, "htif ")?; }
        if self.tcif() { write!(f, "tcif ")?; }
        Ok(())
    }
}

pub enum DmaDirection {
    PeripheralToMemory,
    MemoryToPeripheral,
    MemoryToMemory,
}

pub struct DmaTransactionState {
    block: &'static DmaBlock,
    stream: usize,
}
impl DmaTransactionState {
    fn stream(&self) -> DmaStream {
        self.block.stream(self.stream)
    }
}

const UNUSED: usize = 0usize;
const WRITING: usize = !0usize;

pub struct DmaBuffer<'a, T: 'a + Sized> {
    refs: AtomicUsizeCell,
    buf_start: *mut T,
    buf_end: *mut T,
    _marker: core::marker::PhantomData<&'a mut [T]>,
    start: AtomicIsizeCell,
    len: AtomicUsizeCell,
    pending_txn_state: AtomicRefCell<Option<DmaTransactionState>>,
}
impl<'a, T: Sized + core::fmt::LowerHex> DmaBuffer<'a, T> {
    pub fn new(buf: &'a mut [T]) -> Self {
        let ptr = buf.as_mut_ptr();
        Self{
            refs: AtomicUsizeCell::new(UNUSED),
            buf_start: ptr,
            buf_end: unsafe { ptr.offset((buf.len() as isize) - 1) },
            _marker: core::marker::PhantomData,
            start: AtomicIsizeCell::new(0),
            len: AtomicUsizeCell::new(0),
            pending_txn_state: AtomicRefCell::new(None),
        }
    }
    pub fn set_start(&self, start: isize) {
        // Nobody else can have a ref to the buffer while the start is being changed
        let _marker = self.borrow_mut();
        // Bounds check
        if start < 0 || start as usize > (self.buf_end as usize - self.buf_start as usize) {
            panic!("set_start out of bounds; start: {}, size: {}", start, (self.buf_end as usize - self.buf_start as usize));
        }
        // Because we have an exclusive ref, nobody else should be able to change self.start
        self.start.set(self.start.get(), start).expect("Should never be an IRQ changing start");
    }
    pub fn set_len(&self, len: usize) {
        // Nobody else can have a ref to the buffer while the length is being set
        let _marker = self.borrow_mut();
        // Bounds check
        if len > (self.buf_end as usize - self.buf_start as usize - self.start.get() as usize + 1) {
            panic!("set_len out of bounds; len: {}, start: {}, size: {}", len, self.start.get(), (self.buf_end as usize - self.buf_start as usize + 1));
        }
        // Because we have an exclusive ref, nobody else should be able to change self.start
        self.len.set(self.len.get(), len).expect("Should never be an IRQ changing len");
    }
    pub fn len(&self) -> usize { self.len.get() }
    #[allow(dead_code)]
    #[cfg(not(test))]
    pub fn log_buffer(&self, addr: u8) {
        let buf = unsafe {
            core::slice::from_raw_parts(self.buf_start.offset(self.start.get()), self.len.get())
        };
        write!(debug::stdout(), "addr {:#4x}, start {}, len {}, buf ", addr, self.start.get(), self.len.get()).unwrap();
        for i in 0..self.len.get() {
            write!(debug::stdout(), "{:#4x} ", buf[i]).unwrap();
        }
        write!(debug::stdout(), "\n").unwrap();
    }
    pub fn configure(&self, reg: &I2cData, block: &'static DmaBlock, stream: usize, dir: DmaDirection, channel: u32) {
        //debug::serial().write_str("configure\n").unwrap();
        // Before doing anything, make sure we have exclusive access to the DmaBuffer
        while {
            // XXX: If refs is not UNUSED at the start, should we panic, or wait like this?
            // Waiting will either recover, or panic eventually, when the watchdog hits.
            !self.refs.set(UNUSED, WRITING).is_ok()
        } {}
        let s = block.stream(stream);
        let sz = match core::mem::size_of::<T>() {
            1 => 0,
            2 => 1,
            4 => 2,
            n => { panic!("Invalid DMA item size, must be 1/2/4: {}", n); },
        };
        s.abort();
        s.setup(reg as *const I2cData as usize as u32,
                unsafe { self.buf_start.offset(self.start.get()) } as usize as u32,
                self.len.get() as u32).update()
            .set_chsel(channel)
            .set_dir(match dir {
                DmaDirection::PeripheralToMemory => 0,
                DmaDirection::MemoryToPeripheral => 1,
                DmaDirection::MemoryToMemory => 2,
            })
            .set_msize(sz)
            .set_psize(sz)
            .set_minc(true)
            .set_pinc(match dir {
                DmaDirection::MemoryToMemory => true,
                _ => false,
            })
            .set_tcie(false)    // is this ok?
            .set_teie(true);
        *self.pending_txn_state.borrow_mut() = Some(DmaTransactionState{block: block, stream: stream});
        self.refs.set(WRITING, UNUSED).expect("Somehow we got un-borrowed mid-configure?!");
    }
    pub fn start_transaction(&self) {
        //debug::serial().write_str("start_transaction\n").unwrap();
        // Before doing anything, make sure we have exclusive access to the DmaBuffer
        while {
            // XXX: If refs is not UNUSED at the start, should we panic, or wait like this?
            // Waiting will either recover, or panic eventually, when the watchdog hits.
            !self.refs.set(UNUSED, WRITING).is_ok()
        } {}
        match *self.pending_txn_state.borrow() {
            Some(ref state) => {
                //write!(debug::stdout(), "start: {}\n", state.stream().nleft()).unwrap();
                state.stream().enable();
            },
            None => { panic!("unexpected missing pending_txn_state"); },
        }
    }
    pub fn transaction_done(&self) -> bool {
        match *self.pending_txn_state.borrow() {
            None => { panic!("transaction_done called with no transaction state"); },
            Some(ref state) => {
                //write!(debug::stdout(), "transaction_done: {}\n", state.stream().nleft()).unwrap();
                state.stream().tcif()
            },
        }
    }
    pub fn has_error(&self) -> bool {
        if self.refs.get() != WRITING {
            panic!("Somehow DmaBuffer got un-borrowed on error check?");
        }
        match *self.pending_txn_state.borrow() {
            None => { panic!("check_error called with no transaction state"); },
            Some(ref state) => {
                let stream = state.stream();
                stream.feif() || stream.dmeif() || stream.teif()
            },
        }
    }
    pub fn cleanup_transaction(&self) {
        //debug::serial().write_str("cleanup_transaction\n").unwrap();
        if self.refs.get() != WRITING {
            panic!("Somehow DmaBuffer got un-borrowed during transaction, at start?");
        }
        {
            let state = self.pending_txn_state.borrow_mut().take();
            let state = state.expect("cleanup_transaction called with None pending");
            //write!(debug::stdout(), "cleanup: {}\n", state.stream().nleft()).unwrap();
            state.stream().abort();
        }
        // Now let other people use us again
        self.refs.set(WRITING, UNUSED).expect("Somehow DmaBuffer got un-borrowed mid-release?");
    }
    pub fn borrow(&self) -> DmaBufferBorrow<T> {
        //debug::serial().write_str("borrow\n").unwrap();
        DmaBufferBorrow::new(&self.refs, unsafe {
            // FIXME: Should we do something to ensure all of these .get() calls are consistent?
            core::slice::from_raw_parts(self.buf_start.offset(self.start.get()), self.len.get())
        })
    }
    pub fn borrow_mut(&self) -> DmaBufferBorrowMut<T> {
        //write!(debug::serial(), "borrow_mut: {:#x}\n", &self.refs as *const AtomicUsizeCell as usize).unwrap();
        match DmaBufferBorrowMut::new(&self.refs, unsafe {
            core::slice::from_raw_parts_mut(self.buf_start.offset(self.start.get()), self.len.get())
        }) {
            Ok(b) => b,
            Err(WRITING) => {
                debug::serial().write_str("Tried to mutably-borrow a mutably-borrowed DmaBuffer\n").unwrap();
                panic!("double mutable");
            },
            Err(_) => {
                debug::serial().write_str("Tried to mutably-borrow a DmaBuffer with refcnt>0\n").unwrap();
                panic!("already immutable");
            }
        }
    }
    pub fn borrow_mut_full(&self) -> DmaBufferBorrowMut<T> {
        //debug::serial().write_str("borrow_mut_full\n").unwrap();
        match DmaBufferBorrowMut::new(&self.refs, unsafe {
            core::slice::from_raw_parts_mut(self.buf_start, self.buf_end as usize - self.buf_start as usize + 1)
        }) {
            Ok(b) => b,
            Err(WRITING) => {
                debug::serial().write_str("Tried to mutably-borrow a mutably-borrowed DmaBuffer\n").unwrap();
                panic!("double mutable");
            },
            Err(_) => {
                debug::serial().write_str("Tried to mutably-borrow a DmaBuffer with refcnt>0\n").unwrap();
                panic!("already immutable");
            }
        }
    }
}

// This doesn't seem to be needed now.  If it is in the future, the raw pointers will need to be
// changed to atomic pointers.
//unsafe impl<T: Sized + Sync> Sync for DmaBuffer<T> {}

pub struct DmaBufferBorrow<'a, T: 'a> {
    refs: &'a AtomicUsizeCell,
    buf: &'a [T],
}
impl<'a, T: Sized> DmaBufferBorrow<'a, T> {
    pub fn new(refs: &'a AtomicUsizeCell, buf: &'a [T]) -> DmaBufferBorrow<'a, T> {
        while {
            let r = refs.get();
            if r == WRITING {
                panic!("Tried to borrow a mutably-borrowed (or DMAing) DmaBuffer");
            }
            !refs.set(r, r + 1).is_ok()
        } {}
        DmaBufferBorrow{
            refs: refs,
            buf: buf,
        }
    }
}
impl<'a, T: Sized> Drop for DmaBufferBorrow<'a, T> {
    #[inline]
    fn drop(&mut self) {
        //debug::serial().write_str("Borrow::drop\n").unwrap();
        while {
            let r = self.refs.get();
            !self.refs.set(r, r - 1).is_ok()
        } {}
    }
}
impl<'a, T: Sized> Clone for DmaBufferBorrow<'a, T> {
    #[inline]
    fn clone(&self) -> DmaBufferBorrow<'a, T> {
        //debug::serial().write_str("clone\n").unwrap();
        // Since this DmaBufferBorrow exists, we know the refs field
        // is not set to WRITING.
        while {
            let r = self.refs.get();
            !self.refs.set(r, r + 1).is_ok()
        } {}
        DmaBufferBorrow{
            refs: self.refs,
            buf: self.buf,
        }
    }
}
impl<'a, T: Sized> Deref for DmaBufferBorrow<'a, T> {
    type Target = [T];

    #[inline]
    fn deref(&self) -> &Self::Target { self.buf }
}

pub struct DmaBufferBorrowMut<'a, T: 'a> {
    refs: &'a AtomicUsizeCell,
    buf: &'a mut [T],
}
impl<'a, T: Sized> DmaBufferBorrowMut<'a, T> {
    pub fn new(refs: &'a AtomicUsizeCell, buf: &'a mut [T]) -> Result<DmaBufferBorrowMut<'a, T>, usize> {
        refs.set(UNUSED, WRITING).and_then(move |_| {
            //write!(debug::serial(), "new({:#x}) succeeded\n", refs as *const AtomicUsizeCell as usize).unwrap();
            Ok(DmaBufferBorrowMut{
                refs: refs,
                buf: buf,
            })
        })
    }
}
impl<'a, T: Sized> Drop for DmaBufferBorrowMut<'a, T> {
    #[inline]
    fn drop(&mut self) {
        self.refs.set(WRITING, UNUSED).expect("Somehow DmaBufferBorrowMut got un-borrowed?");
        //write!(debug::serial(), "drop({:#x}) succeeded\n", self.refs as *const AtomicUsizeCell as usize).unwrap();
    }
}
impl<'a, T: Sized> Deref for DmaBufferBorrowMut<'a, T> {
    type Target = [T];

    #[inline]
    fn deref(&self) -> &Self::Target { self.buf }
}
impl<'a, T: Sized> DerefMut for DmaBufferBorrowMut<'a, T> {
    #[inline]
    fn deref_mut(&mut self) -> &mut Self::Target { self.buf }
}

pub struct DmaTransaction<T> {
    block: &'static DmaBlock,
    stream: usize,
    buffer: T,
}
impl<T> DmaTransaction<T> {
    pub fn start(&self) {
        self.block.stream(self.stream).block.c.set_en(true);
    }
    pub fn done(&self) -> bool {
        self.block.stream(self.stream).tcif()
    }
}
impl<'a, U> DmaTransaction<(&'a mut [U], &'a mut [U])> {
    pub fn with_inactive_buffer<F>(&mut self, mut f: F)
        where F: FnMut(&mut [U]) {

        self.block.stream(self.stream).double_buf_target().map(|id| {
            if id == 0 {
                f(self.buffer.1);
            } else {
                f(self.buffer.0);
            }
        }).expect("with_inactive_buffer called while not double-buffering");
    }
}
impl<T> core::ops::Drop for DmaTransaction<T> {
    fn drop(&mut self) {
        self.block.stream(self.stream).abort();
    }
}

register!(
    DmaStreamConfigRegister, DmaStreamConfigRegisterMutator, 0x00000000,
    // enabled
    (set_en, en, ReadWrite, 0),
    // direct mode error interrupt enable
    (set_dmiei, dmiei, ReadWrite, 1),
    // transfer error interrupt enable
    (set_teie, teie, ReadWrite, 2),
    // half transfer error interrupt enable
    (set_htie, htie, ReadWrite, 3),
    // transfer complete interrupt enable
    (set_tcie, tcie, ReadWrite, 4),
    // peripheral flow controller
    (set_pfctrl, pfctrl, ReadWrite, 5),
    // data transfer direction; 0: periph->mem, 1: mem->periph, 2: mem->mem
    (set_dir, dir, ReadWrite, 6, 2),
    // circular
    (set_circ, circ, ReadWrite, 8),
    // peripheral increment mode; 0 -> fixed, 1 -> increment by psize
    (set_pinc, pinc, ReadWrite, 9),
    // memory increment mode; 0 -> fixed, 1 -> increment by msize
    (set_minc, minc, ReadWrite, 10),
    // peripheral data size: 0: 8-bit, 1: 16-bit, 2: 32-bit
    (set_psize, psize, ReadWrite, 11, 2),
    // memory data size: 0: 8-bit, 1: 16-bit, 2: 32-bit
    (set_msize, msize, ReadWrite, 13, 2),
    // peripheral increment offset size
    (set_pincos, pincos, ReadWrite, 15),
    // Priority level: higher numbers are more important
    (set_pl, pl, ReadWrite, 16, 2),
    // double-buffer mode
    (set_dbm, dbm, ReadWrite, 18),
    // current target (in double buffer mode)
    (set_ct, ct, ReadWrite, 19),
    // peripheral burst config: 0: single, 1: incr4, 2: incr8, 3: incr16
    (set_pburst, pburst, ReadWrite, 21, 2),
    // memory burst config: 0: single, 1: incr4, 2: incr8, 3: incr16
    (set_mburst, mburst, ReadWrite, 23, 2),
    // channel selection
    (set_chsel, chsel, ReadWrite, 25, 3));

register!(
    DmaStreamFifoConfigRegister, DmaStreamFifoConfigRegisterMutator, 0x00000021,
    // fifo threshold selection; 0=1/4 full, 1=1/2 full, 2=3/4 full, 3=full
    (set_fth, fth, ReadWrite, 0, 2),
    // direct mode disable
    (set_dmdis, dmdis, ReadWrite, 2),
    // fifo status
    // 0: 0%-25%, 1: 25%-50%, 2: 50%-75%, 3: 75%-100%, 4: 0%, 5: 100%
    (set_fs, fs, ReadOnly, 3, 2),
    // fifo error interrupt enable
    (set_feie, feie, ReadWrite, 7));

#[repr(C)]
pub struct DmaStreamBlock {
    pub c: DmaStreamConfigRegister,
    // number of data items
    pub ndt: Register,
    // peripheral address register
    pub par: Register,
    // memory 0 address register
    pub m0ar: Register,
    // memory 1 address register
    pub m1ar: Register,
    // fifo config
    pub fc: DmaStreamFifoConfigRegister,
}

pub const DMA_INTERRUPT_FLAG_OFFSETS: [u8; 4] = [
    0, 6, 16, 22
];

#[repr(C)]
pub struct DmaBlock {
    // interrupt status
    pub is: [Register; 2],
    // interrupt flag clear
    pub ifc: [Register; 2],
    pub streams: [DmaStreamBlock; 8],
}
impl DmaBlock {
    pub fn stream(&'static self, n: usize) -> DmaStream {
        DmaStream {
            parent_block: &self,
            block: &self.streams[n],
            is: &self.is[(n >> 2) & 0x1],
            ifc: &self.ifc[(n >> 2) & 0x1],
            n: n,
            is_bit_offset: DMA_INTERRUPT_FLAG_OFFSETS[n & 0x3],
        }
    }
}

register!(
    I2cControl1, I2cControl1Mutator, 0x0000,
    (set_pe, pe, ReadWrite, 0),
    (set_smbus, smbus, ReadWrite, 1),
    (set_smbtype, smbtype, ReadWrite, 3),
    (set_enarp, enarp, ReadWrite, 4),
    (set_enpec, enpec, ReadWrite, 5),
    (set_engc, engc, ReadWrite, 6),
    (set_nostretch, nostretch, ReadWrite, 7),
    (set_start, start, ReadWrite, 8),
    (set_stop, stop, ReadWrite, 9),
    (set_ack, ack, ReadWrite, 10),
    (set_pos, pos, ReadWrite, 11),
    (set_pec, pec, ReadWrite, 12),
    (set_alert, alert, ReadWrite, 13),
    (set_swrst, swrst, ReadWrite, 15));

register!(
    I2cControl2, I2cControl2Mutator, 0x0000,
    (set_freq, freq, ReadWrite, 0, 6),
    (set_iterren, iterren, ReadWrite, 8),
    (set_itevten, itevten, ReadWrite, 9),
    (set_itbufen, itbufen, ReadWrite, 10),
    (set_dmaen, dmaen, ReadWrite, 11),
    (set_last, last, ReadWrite, 12));

register!(
    I2cOwnAddr1, I2cOwnAddr1Mutator, 0x0000,
    (set_addr, addr, ReadWrite, 0, 10),
    (set_always_true, always_true, ReadWrite, 14),
    (set_addrmode, addrmode, ReadWrite, 15));

register!(
    I2cOwnAddr2, I2cOwnAddr2Mutator, 0x0000,
    (set_endual, endual, ReadWrite, 0),
    (set_addr, addr, ReadWrite, 1, 7));

register!(
    I2cData, I2cDataMutator, 0x0000,
    (set_data, data, ReadWrite, 0, 8));

// Don't call the individual getters here!  You'll cause peripheral state-machine changes.
register!(
    I2cStatus1, I2cStatus1Mutator, 0x0000, read, I2cStatus1Value,
    (set_smbalert, smbalert, ReadWrite, 15),
    (set_timeout, timeout, ReadWrite, 14),
    (set_pecerr, pecerr, ReadWrite, 12),
    (set_ovr, ovr, ReadWrite, 11),
    (set_af, af, ReadWrite, 10),
    (set_arlo, arlo, ReadWrite, 9),
    (set_berr, berr, ReadWrite, 8));

value!(
    I2cStatus1Value, is_error, |me: &I2cStatus1Value| {
        me.berr() || me.af() || me.arlo() || me.ovr()
    },
    (sb, 0),
    (addr, 1),
    (btf, 2),
    (add10, 3),
    (stopf, 4),
    (rxne, 6),
    (txe, 7),
    (berr, 8),
    (arlo, 9),
    (af, 10),
    (ovr, 11),
    (pecerr, 12),
    (timeout, 14),
    (smbalert, 15));

register!(
    I2cStatus2, I2cStatus2Mutator, 0x0000, read, I2cStatus2Value,);

value!(
    I2cStatus2Value, is_err, |_: &I2cStatus2Value| { false },
    (msl, 0),
    (busy, 1),
    (tra, 2),
    (gencall, 4),
    (smbdefault, 5),
    (smbhost, 6),
    (dualf, 7),
    (pec, 8, 8));

register!(
    I2cClockControl, I2cClockControlMutator, 0x0000,
    (set_cc, cc, ReadWrite, 0, 12),
    (set_duty, duty, ReadWrite, 14),
    (set_fs, fs, ReadWrite, 15));

register!(
    I2cTRise, I2cTRiseMutator, 0x0002,
    (set_trise, trise, ReadWrite, 0, 6));

#[repr(C)]
pub struct I2cBlock {
    pub c1: I2cControl1,
    pub c2: I2cControl2,
    pub oa1: I2cOwnAddr1,
    pub oa2: I2cOwnAddr2,

    pub d: I2cData,
    pub s1: I2cStatus1,
    pub s2: I2cStatus2,
    pub cc: I2cClockControl,

    pub trise: I2cTRise,
}

register!(
    DacCr, DacCrMutator, 0x00000000,
    // Channel 1 enable
    (set_en1, en1, ReadWrite, 0),
    // Output-buffer disable, channel 1
    (set_boff1, boff1, ReadWrite, 1),
    // Trigger enable, channel 1
    (set_ten1, ten1, ReadWrite, 2),
    // Trigger selection, channel 1
    //  (in order: TRGO for timer 6, 8, 7, 5, 2, 4, then external line9, then software)
    //  (only used if ten1 == true)
    (set_tsel1, tsel1, ReadWrite, 3, 3),
    // Noise-or-triangle-wave generation enable, channel 1
    //  (00 -> no wave, 01 -> noise, 10 or 11 -> triangle wave)
    //  (only used if ten1 == true)
    (set_wave1, wave1, ReadWrite, 6, 2),
    // Mask/amplitude selector, channel 1
    (set_mamp1, mamp1, ReadWrite, 8, 4),
    // DMA enable, channel 1
    (set_dmaen1, dmaen1, ReadWrite, 12),
    // DMA underrun interrupt enable, channel 1
    (set_dmaudrie1, dmaudrie1, ReadWrite, 13),
    // Channel 2 enable
    (set_en2, en2, ReadWrite, 16),
    // Output-buffer disable, channel 2
    (set_boff2, boff2, ReadWrite, 17),
    // Trigger enable, channel 2
    (set_ten2, ten2, ReadWrite, 18),
    // Trigger selection, channel 2
    //  (in order: TRGO for timer 6, 8, 7, 5, 2, 4, then external line9, then software)
    //  (only used if ten2 == true)
    (set_tsel2, tsel2, ReadWrite, 19, 3),
    // Noise-or-triangle-wave generation enable, channel 2
    //  (00 -> no wave, 01 -> noise, 10 or 11 -> triangle wave)
    //  (only used if ten2 == true)
    (set_wave2, wave2, ReadWrite, 22, 2),
    // Mask/amplitude selector, channel 2
    (set_mamp2, mamp2, ReadWrite, 24, 4),
    // DMA enable, channel 2
    (set_dmaen2, dmaen2, ReadWrite, 28),
    // DMA underrun interrupt enable, channel 2
    (set_dmaudrie2, dmaudrie2, ReadWrite, 29));

register!(
    DacSwTrigr, DacSwTrigrMutator, 0x00000000,
    (set_swtrig1, swtrig1, ReadWrite, 0),
    (set_swtrig2, swtrig2, ReadWrite, 1));

register!(
    DacDHR12R, DacDHR12RMutator, 0x00000000,
    (set_dhr, dhr, ReadWrite, 0, 12));

// For DMA support
impl<'a> From<&'a DacDHR12R> for &'a Register {
    fn from(reg: &DacDHR12R) -> &Register {
        &reg.0
    }
}

register!(
    DacDHR12L, DacDHR12LMutator, 0x00000000,
    (set_dhr, dhr, ReadWrite, 4, 12));

// For DMA support
impl<'a> From<&'a DacDHR12L> for &'a Register {
    fn from(reg: &DacDHR12L) -> &Register {
        &reg.0
    }
}

register!(
    DacDHR8R, DacDHR8RMutator, 0x00000000,
    (set_dhr, dhr, ReadWrite, 0, 8));

// Data output register (readonly)
register!(
    DacDOR, DacDORMutator, 0x00000000,
    (set_dor, dor, ReadWrite, 0, 12));

register!(
    DacSR, DacSRMutator, 0x00000000,
    (clear_dmaudr1, dmaudr1, SetOnlyByWord, 13),
    (clear_dmaudr2, dmaudr2, SetOnlyByWord, 29));

#[repr(C)]
pub struct DacBlock {
    pub cr: DacCr,
    pub swtrigr: DacSwTrigr,
    pub dhr12r1: DacDHR12R,
    pub dhr12l1: DacDHR12L,
    pub dhr8r1: DacDHR8R,
    pub dhr12r2: DacDHR12R,
    pub dhr12l2: DacDHR12L,
    pub dhr8r2: DacDHR8R,
    pub dhr12rd: Register,  // Dual-channel register, right-aligned
    pub dhr12ld: Register,  // Dual-channel register, left-aligned
    pub dhr8rd: Register,   // Dual-channel register, both bytes in the bottom 16 bits
    pub dor1: DacDOR,
    pub dor2: DacDOR,
    pub sr: DacSR,
}

register!(
    OtgHsGOtgCtl, OtgHsGOtgCtlMutator, 0x00000800,
    // session request success
    (set_srqscs, srqscs, ReadOnly, 0),
    // session request
    (set_srq, srq, ReadWrite, 1),
    // host negotiation success
    (set_hngscs, hngscs, ReadOnly, 8),
    // HNP request
    (set_hnprq, hnprq, ReadWrite, 9),
    // host HNP enable
    (set_hshnpen, hshnpen, ReadWrite, 10),
    // device HNP enable
    (set_dhnpen, dhnpen, ReadWrite, 11),
    // connector ID status; 0=A-device, 1=B-device
    (set_cidsts, cidsts, ReadOnly, 16),
    // debounce time; 0=long (100ms + 2us); 1=2us
    (set_dbct, dbct, ReadOnly, 17),
    // A-session valid
    (set_asvld, asvld, ReadOnly, 18),
    // B-session valid
    (set_bsvld, bsvld, ReadOnly, 19));

register!(
    OtgHsGOtgInt, OtgHsGOtgIntMutator, 0x00000000,
    // Session end detected
    (clear_sedet, sedet, SetOnlyByWord, 2),
    // Session request success status change
    (clear_srsschg, srsschg, SetOnlyByWord, 8),
    // Host negotiation success status change
    (clear_hnsschg, hnsschg, SetOnlyByWord, 9),
    // Host negotiation detected
    (clear_hngdet, hngdet, SetOnlyByWord, 17),
    // A-device timeout change
    (clear_adtochg, adtochg, SetOnlyByWord, 18),
    // Debounce done
    (clear_dbcdne, dbcdne, SetOnlyByWord, 19));

register!(
    OtgHsGAhbCfg, OtgHsGAhbCfgMutator, 0x00000000,
    // Global interrupt mask
    (set_gint, gint, ReadWrite, 0),
    // Burst length/type
    // 0=single, 1=incr, 3=incr4, 5=incr8, 7=incr16
    (set_hbstlen, hbstlen, ReadWrite, 1, 4),
    // DMA enable
    (set_dmaen, dmaen, ReadWrite, 5),
    // TxFIFO empty level; interrupt fires at 0=half-empty, 1=empty
    (set_txfelvl, txfelvl, ReadWrite, 7),
    // Periodic TxFIFO empty level; interrupt fires at 0=half-empty, 1=empty
    (set_ptxfelvl, ptxfelvl, ReadWrite, 8));

register!(
    OtgHsGUsbCfg, OtgHsGUsbCfgMutator, 0x00000A00,
    // FS timeout calibration
    (set_tocal, tocal, ReadWrite, 0, 3),
    // PHY selection; 0=usb2 ULPI PHY, 1=usb1.1 built-in
    (set_phsel, phsel, ReadWrite, 6),
    // Capable of Session Request Protocol
    (set_srpcap, srpcap, ReadWrite, 8),
    // Capable of Host Negotiation Protocol
    (set_hnpcap, hnpcap, ReadWrite, 9),
    // USB turnaround time
    (set_trdt, trdt, ReadWrite, 10, 4),
    // Low power clock select; 0=480Mhz 1=48MHz
    (set_phylpcs, phylpcs, ReadWrite, 15),
    // Forced host mode
    (set_fhmod, fhmod, ReadWrite, 29),
    // Forced peripheral mode
    (set_fdmod, fdmod, ReadWrite, 30));


register!(
    OtgHsGRstCtl, OtgHsGRstCtlMutator, 0x20000000,
    // Core soft reset
    (set_csrst, csrst, SetOnly, 0),
    // HCLK soft reset
    (set_hsrst, hsrst, SetOnly, 1),
    // Host frame counter reset
    (set_fcrst, fcrst, SetOnly, 2),
    // RxFIFO flush
    (set_rxfflsh, rxfflsh, SetOnly, 4),
    // TxFIFO flush
    (set_txfflsh, txfflsh, SetOnly, 5),
    // number of TxFIFO to flush
    (set_txfnum, txfnum, ReadWrite, 6, 5),
    // true if DMA is in progress
    (set_dmareq, dmareq, ReadOnly, 30),
    // true if AHB bus is idle
    (set_ahbidl, ahbidl, ReadOnly, 31));

register!(
    OtgHsGcCfg, OtgHsGcCfgMutator, 0x00000000,
    // 0=power-down, 1=no-power-down
    (set_pwrdwn, pwrdwn, ReadWrite, 16),
    // Enable I2C bus connection to external PHY
    (set_i2cpaden, i2cpaden, ReadWrite, 17),
    // Enable vbus sensing "A" device
    (set_vbusasen, vbusasen, ReadWrite, 18),
    // Enable vbus sensing "B" device
    (set_vbusbsen, vbusbsen, ReadWrite, 19),
    // SOF output enable
    (set_sofouten, sofouten, ReadWrite, 20),
    // no vbus sensing
    (set_novbussens, novbussens, ReadWrite, 21));

#[cfg(not(feature = "devicetest"))]
register!(
    OtgHsGIntSts, OtgHsGIntStsMutator, 0x04000020, read, OtgHsGIntValue,
    // current mode of operation
    (set_cmod, cmod, ReadOnly, 0),
    // Mode mismatch interrupt
    (clear_mmis, mmis, SetOnlyByWord, 1),
    // OTG interrupt
    (set_otgint, otgint, ReadOnly, 2),
    // Start of frame
    (clear_sof, sof, SetOnlyByWord, 3),
    // RxFIFO nonempty
    (set_rxflvl, rxflvl, ReadOnly, 4),
    // Nonperiodic TxFIFO empty (host)
    (set_nptxfe, nptxfe, ReadOnly, 5),
    // Global IN nonperiodic NAK effective
    (set_ginakeff, ginakeff, ReadOnly, 6),
    // Global OUT NAK effective
    (set_gonakeff, gonakeff, ReadOnly, 7),
    // Early suspend
    (clear_esusp, esusp, SetOnlyByWord, 10),
    // USB suspend
    (clear_usbsusp, usbsusp, SetOnlyByWord, 11),
    // USB reset
    (clear_usbrst, usbrst, SetOnlyByWord, 12),
    // Enumeration done
    (clear_enumdne, enumdne, SetOnlyByWord, 13),
    // Isochronous OUT packet dropped
    (clear_isoodrp, isoodrp, SetOnlyByWord, 14),
    // End of periodic frame interrupt
    (clear_eopf, eopf, SetOnlyByWord, 15),
    // IN endpoint interrupt
    (set_iepint, iepint, ReadOnly, 18),
    // OUT endpoint interrupt
    (set_oepint, oepint, ReadOnly, 19),
    // Incomplete isochronous IN transfer
    (clear_iisoixfr, iisoixfr, SetOnlyByWord, 20),
    // Incomplete periodic transfer
    (clear_ipxfr, ipxfr, SetOnlyByWord, 21),
    // Data fetch suspended
    (set_datafsusp, datafsusp, ReadOnly, 22),
    // Host port interrupt
    (set_hprtint, hprtint, ReadOnly, 24),
    // Host channels interrupt
    (set_hcint, hcint, ReadOnly, 25),
    // Periodic TxFIFO empty
    (set_ptxfe, ptxfe, ReadOnly, 26),
    // Connector ID status change
    (clear_cidschg, cidschg, SetOnlyByWord, 28),
    // Disconnect detected interrupt
    (clear_discint, discint, SetOnlyByWord, 29),
    // Session request / new session detected
    (clear_srqint, srqint, SetOnlyByWord, 30),
    // Resume/remote wakeup detected
    (clear_wkupint, wkupint, SetOnlyByWord, 31));

#[cfg(not(feature = "devicetest"))]
value!(
    OtgHsGIntValue, has_interesting_interrupts, |me: &OtgHsGIntValue| {
        (me.0.u32() & !0x4000031) != 0
    },
    (cmod, 0),
    (mmis, 1),
    (otgint, 2),
    (sof, 3),
    (rxflvl, 4),
    (nptxfe, 5),
    (ginakeff, 6),
    (gonakeff, 7),
    (esusp, 10),
    (usbsusp, 11),
    (usbrst, 12),
    (enumdne, 13),
    (isoodrp, 14),
    (eopf, 15),
    (iepint, 18),
    (oepint, 19),
    (iisoixfr, 20),
    (ipxfr, 21),
    (datafsusp, 22),
    (hprtint, 24),
    (hcint, 25),
    (ptxfe, 26),
    (cidschg, 28),
    (discint, 29),
    (srqint, 30),
    (wkupint, 31));

register!(
    OtgHsGIntMsk, OtgHsGIntMskMutator, 0x00000000,
    // Mode mismatch interrupt
    (set_mmism, mmism, ReadWrite, 1),
    // OTG interrupt
    (set_otgint, otgint, ReadWrite, 2),
    // Start of frame
    (set_sofm, sofm, ReadWrite, 3),
    // RxFIFO nonempty
    (set_rxflvlm, rxflvlm, ReadWrite, 4),
    // Nonperiodic TxFIFO empty (host)
    (set_nptxfem, nptxfem, ReadWrite, 5),
    // Global IN nonperiodic NAK effective
    (set_ginakeffm, ginakeffm, ReadWrite, 6),
    // Global OUT NAK effective
    (set_gonakeffm, gonakeffm, ReadWrite, 7),
    // Early suspend
    (set_esuspm, esuspm, ReadWrite, 10),
    // USB suspend
    (set_usbsuspm, usbsuspm, ReadWrite, 11),
    // USB reset
    (set_usbrst, usbrst, ReadWrite, 12),
    // Enumeration done
    (set_enumdnem, enumdnem, ReadWrite, 13),
    // Isochronous OUT packet dropped
    (set_isoodrpm, isoodrpm, ReadWrite, 14),
    // End of periodic frame interrupt
    (set_eopfm, eopfm, ReadWrite, 15),
    // IN endpoint interrupt
    (set_iepint, iepint, ReadWrite, 18),
    // OUT endpoint interrupt
    (set_oepint, oepint, ReadWrite, 19),
    // Incomplete isochronous IN transfer
    (set_iisoixfrm, iisoixfrm, ReadWrite, 20),
    // Incomplete periodic transfer
    (set_ipxfrm, ipxfrm, ReadWrite, 21),
    // Data fetch suspended
    (set_fsuspm, fsuspm, ReadWrite, 22),
    // Host port interrupt
    (set_prtim, prtim, ReadOnly, 24),
    // Host channels interrupt
    (set_hcim, hcim, ReadWrite, 25),
    // Periodic TxFIFO empty
    (set_ptxfem, ptxfem, ReadWrite, 26),
    // Connector ID status change
    (set_cidschgm, cidschgm, ReadWrite, 28),
    // Disconnect detected interrupt
    (set_discint, discint, ReadWrite, 29),
    // Session request / new session detected
    (set_srqim, srqim, ReadWrite, 30),
    // Resume/remote wakeup detected
    (set_wuim, wuim, ReadWrite, 31));

register!(
    OtgHsGRxStsR, OtgHsGRxStsRMutator, 0x00000000, read, OtgHsGRxStsValue,);

register!(
    OtgHsGRxStsP, OtgHsGRxStsPMutator, 0x00000000, pop, OtgHsGRxStsValue,);

value!(
    OtgHsGRxStsValue, is_error, |_: &OtgHsGRxStsValue| { false },
    (epnum, 0, 4),
    (bcnt, 4, 11),
    (dpid, 15, 2),
    (pktsts, 17, 4),
    (frmnum, 21, 4));

register!(
    OtgHsGRxFSiz, OtgHsGRxFSizMutator, 0x00000200,
    // Rx FIFO size in 32-bit words
    (set_rxfd, rxfd, ReadWrite, 0, 16));

register!(
    OtgHsTxFSiz, OtgHsTxFSizMutator, 0x00000200,
    // Tx FIFO start address
    (set_start_addr, start_addr, ReadWrite, 0, 16),
    // Tx FIFO size in 32-bit words
    (set_depth, depth, ReadWrite, 16, 16));

register!(
    OtgHsDCfg, OtgHsDCfgMutator, 0x02200000,
    // Device speed: 0=high 1=full-ulpi-phy 3=full-internal-phy
    (set_dspd, dspd, ReadWrite, 0, 2),
    // Nonzero-length status OUT handshake
    (set_nzlsohsk, nzlsohsk, ReadWrite, 2),
    // Device address
    (set_dad, dad, ReadWrite, 4, 7),
    // Periodic frame interval 0=80% 1=85% 2=90% 3=95%
    (set_pfivl, pfivl, ReadWrite, 11, 2),
    // Periodic scheduling interval 0=25% 1=50% 2=75%
    (set_perschivl, perschivl, ReadWrite, 24, 2));

register!(
    OtgHsDCtl, OtgHsDCtlMutator, 0x00000000,
    // Remote wakeup signaling
    (set_rwusig, rwusig, ReadWrite, 0),
    // Global in NAK status
    (set_ginsts, ginsts, ReadOnly, 2),
    // Global out NAK status
    (set_gonsts, gonsts, ReadOnly, 3));

register!(
    OtgHsDSts, OtgHsDStsMutator, 0x00000010,
    // Enumerated speed; 0=high-speed 3=full-speed
    (set_enumspd, enumspd, ReadOnly, 1, 2));

register!(
    OtgHsDOEptSiz, OtgHsDOEptSizMutator, 0x00000000,
    // Transfer size; fire interrupt after this many bytes are transferred
    (set_xfrsiz, xfrsiz, ReadWrite, 0, 19),
    // Packet count; the number of packets that make up xfrsiz
    (set_pktcnt, pktcnt, ReadWrite, 19, 10),
    (set_stupcnt, stupcnt, ReadWrite, 29, 2));

register!(
    OtgHsDIEptSiz, OtgHsDIEptSizMutator, 0x00000000,
    // Transfer size; fire interrupt after this many bytes are transferred
    (set_xfrsiz, xfrsiz, ReadWrite, 0, 19),
    // Packet count; the number of packets that make up xfrsiz
    (set_pktcnt, pktcnt, ReadWrite, 19, 10),
    (set_stupcnt, stupcnt, ReadWrite, 29, 2));

register!(
    OtgHsDOEptCtl, OtgHsDOEptCtlMutator, 0x00000000,
    // maximum packet size; // 0=64, 1=32, 2=16, 3=8
    (set_mpsiz, mpsiz, ReadWrite, 0, 2),
    // endpoint is active
    (set_usbaep, usbaep, ReadWrite, 15),
    // the core is transmitting NAKs
    (set_naksts, naksts, ReadOnly, 17),
    // Endpoint type: 0=control 1=isochronous, 2=bulk, 3=interrupt
    (set_eptyp, eptyp, ReadWrite, 18, 2),
    // Stall handshake
    (set_stall, stall, ReadWrite, 21),
    // Clear NAK
    (clear_nak, get_clear_nak, SetOnly, 26),
    // Set NAK
    (set_nak, get_set_nak, SetOnly, 27),
    // Endpoint disable; must wait for disabled interrupt after setting
    (set_epdis, epdis, ReadWrite, 30),
    // Endpoint enable
    (set_epena, epena, SetOnly, 31));

register!(
    OtgHsDIEptCtl, OtgHsDIEptCtlMutator, 0x00000000,
    // maximum packet size
    (set_mpsiz, mpsiz, ReadWrite, 0, 11),  // yes, this is different from out epctl 0
    // endpoint is active
    (set_usbaep, usbaep, ReadWrite, 15),
    // even/odd frame
    (set_eonum, eonum, ReadOnly, 16),
    // the core is transmitting NAKs
    (set_naksts, naksts, ReadOnly, 17),
    // Endpoint type: 0=control 1=isochronous, 2=bulk, 3=interrupt
    (set_eptyp, eptyp, ReadWrite, 18, 2),
    // Stall handshake
    (set_stall, stall, ReadWrite, 21),
    // TxFIFO number
    (set_txfnum, txfnum, ReadWrite, 22, 4),
    // Clear NAK
    (clear_nak, get_clear_nak, SetOnly, 26),
    // Set NAK
    (set_nak, get_set_nak, SetOnly, 27),
    // Copy PID from DATA0 (use with interrupt/bulk IN endpoints)
    (set_sd0pid, sd0pid, SetOnly, 28),
    // Set even frame (use with isochronous IN endpoints)
    (set_sevenfrm, sevenfrm, SetOnly, 28),  // yes, this aliases
    // Set odd frame (use with isochronous IN endpoints)
    (set_soddfrm, soddfrm, SetOnly, 29),
    // Endpoint disable; must wait for disabled interrupt after setting
    (set_epdis, epdis, ReadWrite, 30),
    // Endpoint enable
    (set_epena, epena, SetOnly, 31));

register!(
    // Number of 32-bit words free in TxFIFO
    OtgHsDTxFSts, OtgHsDTxFStsMutator, 0x00000000,
    (set_ineptsav, ineptsav, ReadOnly, 0, 16));

value!(
    OtgHsDOEpIntValue, is_error, |_: &OtgHsDOEpIntValue| { false },
    (xfrc, 0),
    (epdisd, 1),
    (stup, 3),
    (otepdis, 4),
    (b2bstup, 6),
    (nyet, 14));

register!(
    OtgHsDIEpInt, OtgHsDIEpIntMutator, 0x00000080, read, OtgHsDIEpIntValue,
    // Transfer completed
    (clear_xfrc, xfrc, SetOnlyByWord, 0),
    // Endpoint disabled
    (clear_epdisd, epdisd, SetOnlyByWord, 1),
    // Timeout condition
    (clear_toc, toc, SetOnlyByWord, 3),
    // IN token received when TxFIFO is empty
    (clear_ittxfe, ittxfe, SetOnlyByWord, 4),
    // IN endpoint NAK effective
    (clear_inepne, inepne, SetOnlyByWord, 6),
    // Transmit FIFO empty
    (clear_txfe, txfe, ReadOnly, 7),
    // Transmit FIFO underrun
    (clear_txfifoudrn, txfifoudrn, SetOnlyByWord, 8),
    // Buffer not available
    (clear_bna, bna, SetOnlyByWord, 9),
    // Packet dropped status
    (clear_pktdrpsts, pktdrpsts, SetOnlyByWord, 11),
    // Babble Error interrupt
    (clear_berr, berr, SetOnlyByWord, 12),
    // NAK interrupt
    (clear_nak, nak, SetOnlyByWord, 13));

value!(
    OtgHsDIEpIntValue, is_error, |_: &OtgHsDIEpIntValue| { false },
    (xfrc, 0),
    (epdisd, 1),
    (toc, 3),
    (ittxfe, 4),
    (inepne, 6),
    (txfe, 7),
    (txfifoudrn, 8),
    (bna, 9),
    (pktdrpsts, 11),
    (berr, 12),
    (nak, 13));

register!(
    OtgHsDOEpInt, OtgHsDOEpIntMutator, 0x00000080, read, OtgHsDOEpIntValue,
    // Transfer completed
    (clear_xfrc, xfrc, SetOnlyByWord, 0),
    // Endpoint disabled
    (clear_epdisd, epdisd, SetOnlyByWord, 1),
    // SETUP phase done
    (clear_stup, stup, SetOnlyByWord, 3),
    // OUT token received when endpoint disabled
    (clear_otepdis, otepdis, SetOnlyByWord, 4),
    // Back-to-back SETUP packets received
    (clear_b2bstup, b2bstup, SetOnlyByWord, 6),
    // NYET interrupt
    (clear_nyet, nyet, SetOnlyByWord, 14));

#[repr(C)]
pub struct OtgHsDIEpt {
    pub ctl: OtgHsDIEptCtl,
    reserved1: Register,
    pub int: OtgHsDIEpInt,
    reserved3: Register,

    pub siz: OtgHsDIEptSiz,
    pub dma: Register,
    pub dtxfsts: OtgHsDTxFSts,
    pub dmab: Register,
}

#[repr(C)]
pub struct OtgHsDOEpt {
    pub ctl: OtgHsDOEptCtl,
    reserved1: Register,
    pub int: OtgHsDOEpInt,
    reserved3: Register,

    pub siz: OtgHsDOEptSiz,
    pub dma: Register,
    reserved4: Register,
    pub dmab: Register,
}

#[repr(C)]
#[cfg(not(feature = "devicetest"))]
pub struct OtgHsBlock {
    pub gotgctl: OtgHsGOtgCtl,
    pub gotgint: OtgHsGOtgInt,
    pub gahbcfg: OtgHsGAhbCfg,
    pub gusbcfg: OtgHsGUsbCfg,

    pub grstctl: OtgHsGRstCtl,
    pub gintsts: OtgHsGIntSts,
    pub gintmsk: OtgHsGIntMsk,
    pub grxstsr: OtgHsGRxStsR,

    pub grxstsp: OtgHsGRxStsP,
    pub grxfsiz: OtgHsGRxFSiz,
    pub tx0fsiz: OtgHsTxFSiz,
    pub gnptxsts: Register,

    reserved1: Register,
    reserved2: Register,
    pub gccfg: OtgHsGcCfg,
    pub hs_cid: Register,

    reserved3: [Register; 48],

    pub hptxfsiz: Register,
    pub dieptxf: [OtgHsTxFSiz; 7],

    reserved4: [Register; 440],

    pub dcfg: OtgHsDCfg,
    pub dctl: OtgHsDCtl,
    pub dsts: OtgHsDSts,
    reserved5: Register,

    pub diepmsk: Register,
    pub doepmsk: Register,
    pub daint: Register,
    pub daintmsk: Register,

    reserved6: [Register; 56],
    
    pub diept: [OtgHsDIEpt; 8],

    reserved7: [Register; 64],

    pub doept: [OtgHsDOEpt; 8],

    reserved8: [Register; 256],

    pub fifo: [[Register; 1024]; 8],
}

register!(
    WindowWatchdogControl, WindowWatchdogControlMutator, 0x0000007F,
    (set_t, t, ReadWrite, 0, 6),
    (set_wdga, wdga, ReadWrite, 7));

register!(
    WindowWatchdogConfig, WindowWatchdogConfigMutator, 0x0000007F,
    (set_w, w, ReadWrite, 0, 6),
    (set_wdgtb, wdgtb, ReadWrite, 7, 2),
    (set_ewi, ewi, ReadWrite, 9));

register!(
    WindowWatchdogStatus, WindowWatchdogStatusMutator, 0x00000000,
    (set_ewif, ewif, ReadWrite, 0));

#[repr(C)]
pub struct WindowWatchdogBlock {
    pub c: WindowWatchdogControl,
    pub cf: WindowWatchdogConfig,
    pub s: WindowWatchdogStatus,
}

register!(
    RtcTime, RtcTimeMutator, 0x00000000,
    (set_second_units, second_units, ReadWrite, 0, 4),
    (set_second_tens, second_tens, ReadWrite, 4, 3),
    (set_minute_units, minute_units, ReadWrite, 8, 4),
    (set_minute_tens, minute_tens, ReadWrite, 12, 3),
    (set_hour_units, hour_units, ReadWrite, 16, 4),
    (set_hour_tens, hour_tens, ReadWrite, 20, 2),
    (set_pm, pm, ReadWrite, 22));

register!(
    RtcDate, RtcDateMutator, 0x00000000,
    (set_date_units, date_units, ReadWrite, 0, 4),
    (set_date_tens, date_tens, ReadWrite, 4, 2),
    (set_month_units, month_units, ReadWrite, 8, 4),
    (set_month_tens, month_tens, ReadWrite, 12),
    // 1 -> 7 Monday -> Sunday; 0 forbidden
    (set_weekday_units, weekday_units, ReadWrite, 13, 3),
    (set_year_units, year_units, ReadWrite, 16, 4),
    (set_year_tens, year_tens, ReadWrite, 20, 4));

#[repr(C)]
pub struct RtcBlock {
    pub t: RtcTime,
    pub d: RtcDate,
    pub c: Register,
    pub is: Register,
    pub pre: Register,
    pub wut: Register,
    pub calib: Register,
    pub alrma: Register,
    pub alrmb: Register,
    pub wp: Register,
    reserved1: [Register; 2],
    pub tst: Register,
    pub tsd: Register,
    reserved2: [Register; 2],
    pub tafc: Register,
    reserved3: [Register; 3],
    pub bkp: [Register; 20],
}

register!(
    SpiControl1, SpiControl1Mutator, 0x0000,
    // 0: first clock transition captures data; 1: second clock transition
    (set_cpha, cpha, ReadWrite, 0),
    // 0: CK low when idle; 1: CK high when idle
    (set_cpol, cpol, ReadWrite, 1),
    (set_mstr, mstr, ReadWrite, 2),
    // baud rate
    (set_br, br, ReadWrite, 3, 3),
    // enable
    (set_spe, spe, ReadWrite, 6),
    (set_lsbfirst, lsbfirst, ReadWrite, 7),
    // slave select internal; value forced onto NSS pin when ssm is true
    (set_ssi, ssi, ReadWrite, 8),
    // software slave management
    (set_ssm, ssm, ReadWrite, 9),
    (set_rxonly, rxonly, ReadWrite, 10),
    // 0: 8-bit frame; 1: 16-bit frame
    (set_dff, dff, ReadWrite, 11),
    (set_crcnext, crcnext, ReadWrite, 12),
    (set_crcen, crcen, ReadWrite, 13),
    (set_bidioe, bidioe, ReadWrite, 14),
    // 0: 2-line unidirectional; 1: 1-line bidirectional
    (set_bidimode, bidimode, ReadWrite, 15));

register!(
    SpiControl2, SpiControl2Mutator, 0x0000,
    (set_rxdmaen, rxdmaen, ReadWrite, 0),
    (set_txdmaen, txdmaen, ReadWrite, 1),
    (set_ssoe, ssoe, ReadWrite, 2),
    // 0: Motorola frame; 1: TI frame
    (set_frf, frf, ReadWrite, 4),
    (set_errie, errie, ReadWrite, 5),
    (set_rxneie, rxneie, ReadWrite, 6),
    (set_txeie, txeie, ReadWrite, 7));

register!(
    SpiStatus, SpiStatusMutator, 0x0002,
    (set_rxne, rxne, ReadOnly, 0),
    (set_txe, txe, ReadOnly, 1),
    (set_chside, chside, ReadOnly, 2),
    (set_udr, udr, ReadOnly, 3),
    (set_crcerr, crcerr, ReadWrite, 4),
    (set_modf, modf, ReadOnly, 5),
    (set_ovr, ovr, ReadOnly, 6),
    (set_bsy, bsy, ReadOnly, 7),
    (set_tifrfe, tifrfe, ReadOnly, 8));

register!(
    SpiData, SpiDataMutator, 0x0000,
    (set_data, data, ReadWrite, 0, 16));

register!(
    SpiCrcPolynomial, SpiCrcPolynomialMutator, 0x0007,
    (set_crcpoly, crcpoly, ReadWrite, 0, 16));

register!(
    SpiCrc, SpiCrcMutator, 0x0000,
    (set_crc, crc, ReadOnly, 0, 16));

register!(
    SpiI2sConfig, SpiI2sConfigMutator, 0x0000,
    // 0: 16-bit, 1: 32-bit
    (set_chlen, chlen, ReadWrite, 0),
    // 00: 16-bit, 01: 24-bit, 10: 32-bit, 11: reserved
    (set_datlen, datlen, ReadWrite, 1, 2),
    // 0: steady state is low, 1: high
    (set_ckpol, ckpol, ReadWrite, 3),
    // 00: I2S Philips, 01: MSB justified, 10: LSB justified, 11: PCM
    (set_i2sstd, i2sstd, ReadWrite, 4, 2),
    // 0: short, 1: long
    (set_pcmsync, pcmsync, ReadWrite, 7),
    // 00: slave/tx, 01: slave/rx, 10: master/tx, 11: master/rx
    (set_i2scfg, i2scfg, ReadWrite, 8, 2),
    // enable
    (set_i2se, i2se, ReadWrite, 10),
    // 0: SPI, 1: I2S
    (set_i2smod, i2smod, ReadWrite, 11));

register!(
    SpiI2sPrescaler, SpiI2sPrescalerMutator, 0x0002,
    // 0 and 1 are reserved
    (set_i2sdiv, i2sdiv, ReadWrite, 0, 8),
    // 0: divider is i2sdiv*2; 1: divider is i2sdiv*2+1
    (set_odd, odd, ReadWrite, 8),
    // master-clock output-enable
    (set_mckoe, mckoe, ReadWrite, 9));

#[repr(C)]
pub struct SpiBlock {
    pub cr1: SpiControl1,
    pub cr2: SpiControl2,
    pub s: SpiStatus,
    pub d: SpiData,
    pub crcp: SpiCrcPolynomial,
    pub rxcrc: SpiCrc,
    pub txcrc: SpiCrc,
    pub i2scfg: SpiI2sConfig,
    pub i2sp: SpiI2sPrescaler,
}

value!(
    SysTickStatusValue, interesting, |_: &SysTickStatusValue| { false },
    // has count hit zero since the last time the bit was read?
    (countflag, 16));

register!(
    SysTickControlStatus, SysTickControlStatusMutator, 0x00000000, read, SysTickStatusValue,
    // countflag is bit 16, but we can't bitband it at this register's address, so force it to go
    // through the Value struct instead; treat that bit as reserved here.
    //(set_countflag, countflag, ReadOnly, 16),
    // true: counts based on processor clock; false: external clock (on stm32f207, this is the
    // processor clock divided by 8)
    (set_clksource, clksource, ReadWrite, 2),
    // is the SysTick interrupt triggered when count hits zero?
    (set_tickint, tickint, ReadWrite, 1),
    // is the SysTick counter enabled?
    (set_enable, enable, ReadWrite, 0));

register!(
    SysTickReloadValue, SysTickReloadValueMutator, 0x00000000,
    // reset value when the counter hits zero
    (set_reload, reload, ReadWrite, 0, 24));

register!(
    SysTickCurrentValue, SysTickCurrentValueMutator, 0x00000000,
    // current counter value; writing anything resets the field to 0, and reset countflag too
    (set_current, current, ReadWrite, 0, 24));

register!(
    SysTickCalibration, SysTickCalibrationMutator, 0x00000000,
    // true: no reference clock provided, so clksource is always true
    (set_noref, noref, ReadOnly, 31),
    // true: tenms value is inexact or not provided
    (set_skew, skew, ReadOnly, 30),
    // reload value to make SysTick count for 10ms; zero implies unknown
    (set_tenms, tenms, ReadOnly, 0, 24));

#[repr(C)]
pub struct SysTickBlock {
    pub csr: SysTickControlStatus,
    pub rvr: SysTickReloadValue,
    pub cvr: SysTickCurrentValue,
    pub calib: SysTickCalibration,
}

impl SysTickBlock {
    pub fn busywait_ms(&self, wait_ms: u32) {
        // The clock runs at 120MHz / 8, since we set clksource to false.
        // So 1ms is 15000 clock cycles.
        // Also, as this is a 24-bit counter, we can only delay for up to 1118ms.  That should be
        // fine but noting it here just in case.
        self.rvr.reset().set_reload(wait_ms * 15000);
        self.cvr.reset().set_current(0);
        self.csr.reset()
            .set_clksource(false)
            .set_tickint(false)
            .set_enable(true);  // Don't use bitbanding for this; the register is at a bad address

        while !self.csr.read().countflag() {}

        self.csr.reset()
            .set_enable(false);
    }
}

register!(
    DwtControl, DwtControlMutator, 0x40000000,
    // number of comparators: should always be 4 for cortex-m3
    (set_numcomp, numcomp, ReadOnly, 28, 4),
    // cycle count event enabled: postcnt triggers
    (set_cycevtena, cycevtena, ReadWrite, 22),
    // trigger an event every 256 "folded" (zero-cycle) instructions
    (set_foldevtena, foldevtena, ReadWrite, 21),
    // trigger an event every 256 "LSU" events
    (set_lsuevtena, lsuevtena, ReadWrite, 20),
    // trigger an event every 256 cycles while sleeping
    (set_sleepevtena, sleepevtena, ReadWrite, 19),
    // trigger an event every 256 cycles of interrupt overhead
    (set_excevtena, excevtena, ReadWrite, 18),
    // trigger an event every 256 cycles of multi-cycle instructions
    (set_cpievtena, cpievtena, ReadWrite, 17),
    // interrupt event tracing (events above trigger an interrupt)
    (set_exctrcena, exctrcena, ReadWrite, 16),
    // PC sample event (on postcnt)
    (set_pcsamplena, pcsamplena, ReadWrite, 12),
    // feed a pulse to ITM_SYNCENA from cyccnt:
    // 00: no sync
    // 01: tap at bit 24
    // 10: tap at bit 26
    // 11: tap at bit 28
    (set_synctap, synctap, ReadWrite, 10, 2),
    // bit to tap on cyccnt, which triggers postcnt changes
    // false: bit 6, true: bit 10
    (set_cyctap, cyctap, ReadWrite, 9),
    // post-scalar counter for cyctap
    (set_postcnt, postcnt, ReadWrite, 5, 4),
    // post-scalar reload value for cyctap
    (set_postpreset, postpreset, ReadWrite, 1, 4),
    // enable cyccnt counter
    (set_cyccntena, cyccntena, ReadWrite, 0));

#[repr(C)]
pub struct DwtBlock {
    pub ctrl: DwtControl,
    pub cyccnt: Register,
}

impl DwtBlock {
    pub fn busywait_ms(&self, ms: u32) {
        self.busywait_clocks(ms * 60000);
    }
    pub fn busywait_clocks(&self, clocks: u32) {
        let limit = clocks;
        self.cyccnt.write(0);
        self.ctrl.reset()
            .set_cyccntena(true);
        while self.cyccnt.read() < limit {}
        self.ctrl.reset()
            .set_cyccntena(false);
    }
}

register!(
    DebugExceptionMonitorControl, DebugExceptionMonitorControlMutator, 0x0000,
    // trace enable (data watchpoint trace, instrumentation and embedded trace macrocells,
    // trace port interface unit)
    (set_trcena, trcena, ReadWrite, 24),
    // true: monitor woken by MON_PEND; false: woken by debug exception
    (set_mon_req, mon_req, ReadWrite, 19),
    // when MON_EN=1, steps the core
    (set_mon_step, mon_step, ReadWrite, 18),
    // set the monitor pending: activate it when priority permits
    (set_mon_pend, mon_pend, ReadWrite, 17),
    // monitor enabled
    (set_mon_en, mon_en, ReadWrite, 16),
    // vector-catching: invoke debug monitor on hard faults
    (set_vc_harderr, vc_harderr, ReadWrite, 10),
    // vector-catching: invoke debug monitor on interrupt/exception service errors
    (set_vc_interr, vc_interr, ReadWrite, 9),
    // vector-catching: invoke debug monitor on bus errors
    (set_vc_buserr, vc_buserr, ReadWrite, 8),
    // vector-catching: invoke debug monitor on usage-fault state errors
    (set_vc_staterr, vc_staterr, ReadWrite, 7),
    // vector-catching: invoke debug monitor on usage-fault-enabled checking errors
    (set_vc_chkerr, vc_chkerr, ReadWrite, 6),
    // vector-catching: invoke debug monitor on usage-fault due to no coprocessor
    (set_vc_nocperr, vc_nocperr, ReadWrite, 5),
    // vector-catching: invoke debug monitor on memory-management faults
    (set_vc_mmerr, vc_mmerr, ReadWrite, 4),
    // vector-catching: halt running system if core reset occurs
    (set_vc_corereset, vc_corereset, ReadWrite, 0));

#[repr(C)]
pub struct DebugExceptionMonitorControlBlock {
    pub demcr: DebugExceptionMonitorControl,
}

register!(
    FlashPatchControl, FlashPatchControlMutator, 0x0000,
    // Number of banks of code comparators, 16 comparators per bank.
    (set_num_code2, num_code2, ReadOnly, 12, 3),
    // Number of literal comparators
    (set_num_lit, num_lit, ReadOnly, 8, 4),
    // Number of code comparators outside the banks
    (set_num_code1, num_code1, ReadOnly, 4, 4),
    // Must set key (bit 1) to 1 with each write to enable (bit 0)
    (set_key_enable, key_enable, ReadWrite, 0, 2));

register!(
    FlashPatchRemap, FlashPatchRemapMutator, 0x0000,
    // Remap base address.  The full remap address is built from here:
    // - Top 3 bits are 001
    // - Next 24 bits come from this remap field
    // - Next 3 bits depend on the register used to do the remap (FP_COMP0 through 7)
    // - Last 2 bits are the two LSBs of the original address
    (set_remap, remap, ReadWrite, 5, 24));

register!(
    FlashPatchComparator, FlashPatchComparatorMutator, 0x0000,
    // 00: remap to the remap address
    // 01: set BKPT on lower halfword (instruction comparators only, FP_COMP0 through 5)
    // 10: set BKPT on higher halfword (instruction comparators only)
    // 11: set BKPT on both halfwords (instruction comparators only)
    (set_replace, replace, ReadWrite, 30, 2),
    // comparison address
    (set_comp, comp, ReadWrite, 2, 27),
    // enable comparison
    (set_enable, enable, ReadWrite, 0));

#[repr(C)]
pub struct FlashPatchBlock {
    pub fp_ctrl: FlashPatchControl,
    pub fp_remap: FlashPatchRemap,
    pub fp_comp: [FlashPatchComparator; 8],
}

pub enum GpioOutType {
    PushPull,
    OpenDrain,
}

pub enum GpioOutSpeed {
    Low,
    Medium,
    Fast,
    High,
}

pub enum GpioMode {
    Input,
    // last param is initial value
    Output(GpioOutType, GpioOutSpeed, bool),
    AlternateIn(u8),
    AlternateOut(u8, GpioOutType, GpioOutSpeed),
    Analog,
}

pub enum GpioPull {
    None,
    Down,
    Up,
}

pub struct GpioIrqConfig {
    pub irq: bool,
    pub wakeup: bool,
    pub rising: bool,
    pub falling: bool,
}

fn gpio_block_index(block: &GpioBlock) -> Option<u8> {
    if block as *const GpioBlock == GPIOA_ADDR { return Some(0); }
    if block as *const GpioBlock == GPIOB_ADDR { return Some(1); }
    if block as *const GpioBlock == GPIOC_ADDR { return Some(2); }
    if block as *const GpioBlock == GPIOD_ADDR { return Some(3); }
    if block as *const GpioBlock == GPIOE_ADDR { return Some(4); }
    if block as *const GpioBlock == GPIOF_ADDR { return Some(5); }
    if block as *const GpioBlock == GPIOG_ADDR { return Some(6); }
    if block as *const GpioBlock == GPIOH_ADDR { return Some(7); }
    if block as *const GpioBlock == GPIOI_ADDR { return Some(8); }
    None
}

macro_rules! block {
    ($addrname:ident, $structname:ty, $addr:expr, $name:ident) => {
        const $addrname: *mut $structname = $addr as *mut $structname;
        #[allow(non_snake_case)]
        pub fn $name() -> &'static $structname { unsafe { &*$addrname } }
    };
}

block!(TIM2_ADDR, GpTimerBlock, 0x40000000usize, TIM2);  // 32-bit
block!(TIM3_ADDR, GpTimerBlock, 0x40000400usize, TIM3);  // 16-bit
block!(TIM4_ADDR, GpTimerBlock, 0x40000800usize, TIM4);  // 16-bit
block!(TIM5_ADDR, GpTimerBlock, 0x40000c00usize, TIM5);  // 32-bit
block!(TIM6_ADDR, DmaTimerBlock, 0x40001000usize, TIM6);
block!(TIM7_ADDR, DmaTimerBlock, 0x40001400usize, TIM7);
block!(TIM12_ADDR, GpTimerBlock, 0x40001800usize, TIM12);
block!(TIM13_ADDR, GpTimerBlock, 0x40001c00usize, TIM13);
block!(TIM14_ADDR, GpTimerBlock, 0x40002000usize, TIM14);

block!(RTC_ADDR, RtcBlock, 0x40002800usize, RTC);
block!(WWDG_ADDR, WindowWatchdogBlock, 0x40002c00usize, WWDG);
block!(IWDG_ADDR, IwdgBlock, 0x40003000usize, IWDG);

block!(SPI3_ADDR, SpiBlock, 0x40003800usize, SPI3);
block!(SPI2_ADDR, SpiBlock, 0x40003c00usize, SPI2);

block!(USART2_ADDR, UsartBlock, 0x40004400usize, USART2);
block!(USART3_ADDR, UsartBlock, 0x40004800usize, USART3);
block!(UART4_ADDR, UsartBlock, 0x40004c00usize, UART4);
block!(UART5_ADDR, UsartBlock, 0x40005000usize, UART5);

block!(I2C1_ADDR, I2cBlock, 0x40005400usize, I2C1);
block!(I2C2_ADDR, I2cBlock, 0x40005800usize, I2C2);
block!(I2C3_ADDR, I2cBlock, 0x40005c00usize, I2C3);

block!(DAC_ADDR, DacBlock, 0x40007400usize, DAC);

block!(TIM1_ADDR, GpTimerBlock, 0x40010000usize, TIM1);  // PWM2
block!(TIM8_ADDR, GpTimerBlock, 0x40010400usize, TIM8);  // PWM1

block!(USART1_ADDR, UsartBlock, 0x40011000usize, USART1);
block!(USART6_ADDR, UsartBlock, 0x40011400usize, USART6);

#[cfg(not(feature = "devicetest"))]
block!(SDIO_ADDR, SdioBlock, 0x40012c00usize, SDIO);

block!(SPI1_ADDR, SpiBlock, 0x40013000usize, SPI1);

block!(SYSCFG_ADDR, SyscfgBlock, 0x40013800usize, SYSCFG);
block!(EXTI_ADDR, ExtiBlock, 0x40013c00usize, EXTI);

block!(TIM9_ADDR, GpTimerBlock, 0x40014000usize, TIM9);
block!(TIM10_ADDR, GpTimerBlock, 0x40014400usize, TIM10);
block!(TIM11_ADDR, GpTimerBlock, 0x40014800usize, TIM11);

block!(GPIOA_ADDR, GpioBlock, 0x40020000usize, GPIOA);
block!(GPIOB_ADDR, GpioBlock, 0x40020400usize, GPIOB);
block!(GPIOC_ADDR, GpioBlock, 0x40020800usize, GPIOC);
block!(GPIOD_ADDR, GpioBlock, 0x40020c00usize, GPIOD);
block!(GPIOE_ADDR, GpioBlock, 0x40021000usize, GPIOE);
block!(GPIOF_ADDR, GpioBlock, 0x40021400usize, GPIOF);
block!(GPIOG_ADDR, GpioBlock, 0x40021800usize, GPIOG);
block!(GPIOH_ADDR, GpioBlock, 0x40021c00usize, GPIOH);
block!(GPIOI_ADDR, GpioBlock, 0x40022000usize, GPIOI);

block!(RCC_ADDR, RccBlock, 0x40023800usize, RCC);
block!(FLASH_ADDR, FlashBlock, 0x40023c00usize, FLASH);

block!(DMA1_ADDR, DmaBlock, 0x40026000usize, DMA1);
block!(DMA2_ADDR, DmaBlock, 0x40026400usize, DMA2);

#[cfg(not(feature = "devicetest"))]
block!(USB_HS_ADDR, OtgHsBlock, 0x40040000usize, USB_HS);

block!(DWT_ADDR, DwtBlock, 0xe0001000usize, DWT);

block!(SYSTICK_ADDR, SysTickBlock, 0xe000e010usize, SYSTICK);

block!(NVIC_ADDR, NvicBlock, 0xe000e100usize, NVIC);
block!(SCB_ADDR, SystemControlBlock, 0xe000ed00usize, SCB);

block!(DEMCB_ADDR, DebugExceptionMonitorControlBlock, 0xe000edfcusize, DEMCB);
block!(FPB_ADDR, FlashPatchBlock, 0xe0002000usize, FPB);

#[allow(non_upper_case_globals)]
pub static mut fancy_reg: u32 = 0xf83714;

pub struct Thunk<'a, T: 'a> {
    pub data: [usize; 4],
    arg: core::marker::PhantomData<&'a T>,
}

impl<'a, T> Thunk<'a, T> {
    pub fn new(func: unsafe extern "C" fn(arg: &T), arg: &'a T) -> Self {
        unsafe {
            Thunk{
                // 4802  ldr r0, [pc, #8]    ; arg
                // 4901  ldr r1, [pc, #4]    ; func
                // 4708  bx r1
                // 0000  unreachable
                data: [
                    0x49014802,
                    0x00004708,
                    core::mem::transmute(func),  // bottom bit from func; thumb or non-thumb
                    core::mem::transmute(arg as *const T),
                ],
                arg: core::marker::PhantomData::<&'a T>,
            }
        }
    }
    
    pub unsafe fn stub(&self) -> unsafe extern "C" fn() {
        // offset by one so that when the stub is called, we're in thumb mode
        core::mem::transmute((self.data.as_ptr() as *const u8).offset(1))
    }
}

pub struct InterruptMapping<'a, T: 'a + Sync> {
    thunk: &'a Thunk<'a, T>,
    int: Interrupt,
    old_vector: Option<unsafe extern "C" fn()>,
    was_enabled: bool,
}

impl<'a, T: Sync> InterruptMapping<'a, T> {
    pub fn new(thunk: &'a Thunk<'a, T>, int: Interrupt) -> Self {
        let result = InterruptMapping{
            thunk: thunk,
            int: int,
            old_vector: unsafe { __INIT_VECTOR[int as usize + 16] },
            was_enabled: NVIC().is_enabled(int),
        };
        NVIC().disable(result.int);
        unsafe {
            __INIT_VECTOR[int as usize + 16] = Some(result.thunk.stub());
        }
        NVIC().enable(result.int);
        result
    }
}
impl<'a, T: Sync> core::ops::Drop for InterruptMapping<'a, T> {
    fn drop(&mut self) {
        NVIC().disable(self.int);
        unsafe {
            __INIT_VECTOR[self.int as usize + 16] = self.old_vector.take();
        }
        if self.was_enabled {
            NVIC().enable(self.int);
        }
    }
}
