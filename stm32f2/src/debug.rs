// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#![macro_use]

use core;
#[cfg(not(test))]
use core::fmt::Write;
use core::intrinsics::copy_nonoverlapping;
use core::sync::atomic::{AtomicUsize, Ordering};
use atomic::{AtomicUsizeCell, AtomicBoolCell, AtomicPtrCell, AtomicRefCell};

use {USART1,NVIC,SCB,DMA2,UsartBlock,Interrupt,DmaTransaction};

#[cfg(test)]
use std;
#[cfg(test)]
use std::io::Write;

pub trait Reader: Sync {
    // Called when the DebugBuffer has new data for this particular reader to send.
    fn have_new_data(&self);
}

impl<T> Reader for Option<T> where T: Reader {
    fn have_new_data(&self) {
        if let Some(ref r) = *self {
            r.have_new_data();
        }
    }
}

const READER_MAX: usize = 4usize;
const MAGIC_1: usize = 0xf25f2c6a;
const MAGIC_2: usize = 0x69280f5e;

pub struct DebugBufferMeta {
    buf_head: AtomicUsizeCell,
    buf_allocated: AtomicUsizeCell,
    magic1: AtomicUsize,
    magic2: AtomicUsize,
}
impl DebugBufferMeta {
    pub const fn new() -> Self {
        DebugBufferMeta{
            buf_head: AtomicUsizeCell::new(0),
            buf_allocated: AtomicUsizeCell::new(0),
            magic1: AtomicUsize::new(0),
            magic2: AtomicUsize::new(0),
        }
    }
}

pub struct DebugBuffer<'a> {
    phantom_buf: core::marker::PhantomData<&'a mut [u8]>,

    // This doesn't use an array because fn new needs to be const, an array would need to be
    // mutable, and const fns can't accept mutable references.  :-/
    buf_ptr: AtomicPtrCell<u8>,
    buf_end: AtomicPtrCell<u8>,

    // head advances to allocated when committing
    // allocated advances when giving a message space
    // each tail (in readers) advances when unbuffering
    buf_head: &'a AtomicUsizeCell,
    buf_allocated: &'a AtomicUsizeCell,

    readers: AtomicPtrCell<[Option<ReaderState<'a>>; READER_MAX]>,
    reader_count: AtomicUsizeCell,
}

impl<'a> DebugBuffer<'a> {
    // Distance from buf_end to buf_start HAD BETTER BE a power of 2!
    pub fn new(buf: &'a mut [u8], meta: &'a DebugBufferMeta, readers: &'a mut [Option<ReaderState>; READER_MAX]) -> DebugBuffer<'a> {
        let old_buffer_valid = meta.magic1.load(Ordering::Relaxed) == MAGIC_1 &&
            meta.magic2.load(Ordering::Relaxed) == MAGIC_2;
        meta.magic1.store(MAGIC_1, Ordering::Relaxed);
        meta.magic2.store(MAGIC_2, Ordering::Relaxed);
        if old_buffer_valid {
            let buf_mask = buf.len() - 1;
            let head = meta.buf_head.get();
            let rewound_head = if head > buf.len() { (head & buf_mask) + buf.len() } else { head };
            meta.buf_head.set(head, rewound_head).unwrap();
            meta.buf_allocated.set(meta.buf_allocated.get(), rewound_head).unwrap();
        } else {
            meta.buf_head.set(meta.buf_head.get(), 0).unwrap();
            meta.buf_allocated.set(meta.buf_allocated.get(), 0).unwrap();
        }
        let result = DebugBuffer{
            phantom_buf: core::marker::PhantomData,
            buf_ptr: AtomicPtrCell::new(buf.as_mut_ptr()),
            buf_end: AtomicPtrCell::new(unsafe { buf.as_mut_ptr().offset(buf.len() as isize) }),
            buf_head: &meta.buf_head,
            buf_allocated: &meta.buf_allocated,
            readers: AtomicPtrCell::new(readers as *const [Option<ReaderState>; READER_MAX] as *mut [Option<ReaderState>; READER_MAX]),
            reader_count: AtomicUsizeCell::new(0usize),
        };
        if old_buffer_valid {
            result.prepend("Previous contents of debug buffer:\n".as_bytes());
        }
        result
    }
    fn prepend(&self, src: &[u8]) {
        let buf_head = self.buf_head.get();
        let buf_size = (self.buf_end.get() as usize) - (self.buf_ptr.get() as usize);
        let buf_mask = buf_size - 1;
        let buf_tail = if buf_head > buf_size { buf_head - buf_size } else { 0 };

        // TODO(kor): Handle when there's not enough space between head and tail for src
        let t = buf_tail & buf_mask;
        let initial_copy_len = core::cmp::min(src.len(), buf_size - t);
        unsafe {
            copy_nonoverlapping(&src[0], self.buf_ptr.get().offset(t as isize), initial_copy_len);
            if src.len() > initial_copy_len {
                copy_nonoverlapping(&src[initial_copy_len], self.buf_ptr.get(),
                                    core::cmp::min(src.len() - initial_copy_len, t));
            }
        }
    }
    fn buf_tail(&self, blocking_only: bool) -> usize {
        let mut tail = usize::max_value();
        let readers = unsafe { &*self.readers.get() };
        for i in 0..self.reader_count.get() {
            if let Some(ref r) = readers[i] {
                if !blocking_only || r.can_block.get() {
                    let t = r.buf_tail.get();
                    if t < tail {
                        tail = t;
                    }
                }
            }
        }
        if tail == usize::max_value() { self.buf_head.get() } else { tail }
    }
    fn notify_readers(&self, old_head: usize) {
        for i in 0..self.reader_count.get() {
            if let Some(ref r) = unsafe { &*self.readers.get() }[i] {
                let t = r.buf_tail.get();
                // Only notify readers that either have never been notified, or whose current tail
                // matches the old head value.  In the former case they need to start a first DMA
                // transaction; in the latter case they have finished DMAing previous data, and need
                // to restart.
                //
                // Note that interrupts might call into here with an old_head value that's too new.
                // That's fine, because the readers' tails will be long before that (because that
                // head pointer will be after the "real" previous head that the readers' calls to
                // get_bytes() will return data from), and the "t == old_head" condition won't fire.
                // Then when the non-interrupt buffer() call calls into notify_readers, its old_head
                // value *will* match, and everyone will be notified based on the top-level buffer()
                // execution.  Luckily for us.
                if r.notified.set(false, true).is_ok() || (t == old_head && t != self.buf_head.get()) {
                    r.reader.have_new_data();
                }
            }
        }
    }
    #[cfg(not(feature = "devicetest"))]
    pub fn prime_reader(&self, reader_id: ReaderId) {
        if let Some(ref r) = unsafe { &*self.readers.get() }[reader_id.0] {
            if r.notified.set(false, true).is_ok() {
                r.reader.have_new_data();
            }
        }
    }
    pub fn buffer_str(&self, buf: &str) {
        self.buffer(buf.as_bytes());
    }
    pub fn buffer(&self, mut buf: &[u8]) {
        while buf.len() > 0 {
            //write!(serial(), "---- buf.len(): {} ", buf.len()).unwrap();
            /*if buf.len() < 16 {
                for i in 0..buf.len() {
                    if buf[i] > 31 {
                        serial().write_byte(buf[i]).unwrap();
                    }
                }
            } else {
                for i in 0..16 {
                    if buf[i] > 31 {
                        serial().write_byte(buf[i]).unwrap();
                    }
                }
            }
            serial().write_byte(10).unwrap();*/
            let bytes_written = self.buffer_some(buf);
            if bytes_written == 0 {
                //write!(serial(), "---- bytes_written == 0\n").unwrap();
                break;
            }
            //write!(serial(), "---- bytes_written == {}\n", bytes_written).unwrap();
            buf = &buf[bytes_written..]
        }
    }
    #[cfg(test)]
    fn in_interrupt() -> bool {
        false
    }
    #[cfg(not(test))]
    fn in_interrupt() -> bool {
        NVIC().any_active() || SCB().any_fault_active()
    }
    #[cfg_attr(feature = "devicetest", inline(never))]
    pub fn buffer_some(&self, buf: &[u8]) -> usize {
        //write!(serial(), "buffer_some:\n").unwrap();
        if buf.len() == 0 {
            return 0;
        }
        let in_interrupt = Self::in_interrupt();
        //write!(serial(), "in_interrupt: {}\n", in_interrupt).unwrap();
        let mut send_buf;
        let buf_size = (self.buf_end.get() as usize) - (self.buf_ptr.get() as usize);
        let buf_mask = buf_size - 1;
        let mut allocation: usize;
        let mut new_a: usize;
        let mut toplevel: bool;
        loop {
            allocation = self.buf_allocated.get();
            let space_left = buf_size.wrapping_sub(allocation.wrapping_sub(self.buf_tail(true)));
            //write!(serial(), "space_left: {}\n", space_left).unwrap();
            // This is safe to do here (no need to commit anything) for the reason outlined below:
            // other interrupting buffer calls will have committed themselves because we never
            // updated the buf_allocated offset.
            if space_left == 0 || space_left > 0x7fffffff {
                // Delay a while here, waiting for an interrupt to unbuffer some data

                // Well, we can't do that delay while in interrupt context, so just bail in that
                // case...
                if in_interrupt {
                    return 0;
                }
                continue;
            }
            if space_left < buf.len() {
                // Not enough space for this string anymore; drop the extra bits.
                // But note that if we get interrupted and need to try again, the interrupt might
                // have unbuffered some data.  So we keep around the original buf for the entire
                // loop, and set send_buf to a sub-slice of the appropriate size each time through.
                send_buf = &buf[0..space_left];
            } else {
                send_buf = &buf[0..buf.len()];
            }
            // First, check if another buffer is in progress (allocated, not committed)
            // NB: If we get interrupted between here and the end of the while loop, the allocated
            // pointer will get advanced, and the compare-and-swap will fail, so we won't exit the
            // loop, and we'll re-set toplevel.  (And in fact it will be correct.  If we get
            // interrupted, then whoever interrupts us will be responsible for committing, and will
            // have done so.)  As soon as that compare-and-swap succeeds, an interrupt that calls
            // into here will get !toplevel, correctly.
            // In short: It only works because interrupts form a stack.
            toplevel = self.buf_head.get() == allocation;
            new_a = allocation.wrapping_add(send_buf.len());
            if self.buf_allocated.set(allocation, new_a).is_ok() { break; }
        }
        //write!(serial(), "allocation success; {} -> {}, toplevel {}\n", allocation, new_a, toplevel).unwrap();
        let masked_all = allocation & buf_mask;
        let p = unsafe { self.buf_ptr.get().offset(masked_all as isize) };
        if masked_all + send_buf.len() > buf_size  {
            let first = buf_size - masked_all;
            unsafe {
                copy_nonoverlapping(send_buf.as_ptr(), p, first);
                copy_nonoverlapping(send_buf.as_ptr().offset(first as isize), self.buf_ptr.get(), send_buf.len() - first);
            }
        } else {
            unsafe {
                copy_nonoverlapping(send_buf.as_ptr(), p, send_buf.len());
            }
        }
        //write!(serial(), "copy finished\n").unwrap();
        if !toplevel {
            // This write will get committed when the toplevel buffer instance finishes.
            return send_buf.len();
        }
        //write!(serial(), "head: {}, allocated: {}\n", self.buf_head.get(), self.buf_allocated.get()).unwrap();
        // Commit everything.  Actually this will never loop on buf_head, because nobody else ever
        // updates it, but the Result does still need to be checked.  Also, we might loop on
        // buf_allocated, because another interrupt might happen in between the get() for it and the
        // set() on buf_head, so we just need to make sure we save the *original* buf_head to pass
        // down to notify_readers.  (See the comments in there for how this is safe in the face of
        // interrupts that also call it.)
        let original_h = self.buf_head.get();
        while {
            let h = self.buf_head.get();
            // Interrupts can happen between buf_allocated.get() here and buf_head.set(), so we need
            // to check their equality afterward again, and keep going until they're equal after the
            // .set() call.
            !self.buf_head.set(h, self.buf_allocated.get()).is_ok() ||
                self.buf_head.get() != self.buf_allocated.get()
        } {}
        //write!(serial(), "notify_readers\n").unwrap();

        self.notify_readers(original_h);
        let l = send_buf.len();
        //write!(serial(), "done\n").unwrap();
        l
    }
    #[cfg(feature = "devicetest")]
    pub fn internal_bits(&self) -> (usize, usize, &[Option<ReaderState<'a>>]) {
        (self.buf_head.get(), self.buf_allocated.get(), unsafe { &*self.readers.get() })
    }
    #[cfg(feature = "devicetest")]
    pub fn reset_for_testing(&self) {
        self.buf_head.set(self.buf_head.get(), 0).unwrap();
        self.buf_allocated.set(self.buf_allocated.get(), 0).unwrap();
        for i in 0..self.reader_count.get() {
            if let Some(ref r) = unsafe { &*self.readers.get() }[i] {
                let ok: Result<bool, bool> = Ok(true);
                r.notified.set(true, false).or(ok).unwrap();
                r.buf_tail.set(r.buf_tail.get(), 0).unwrap();
            }
        }
    }
    pub fn register_reader(&self, reader: &'a dyn Reader, can_block: bool) -> ReaderId {
        //write!(serial(), "register_reader()\n").unwrap();
        let mut c: usize;
        while {
            c = self.reader_count.get();
            if c == READER_MAX {
                panic!("Failed to register reader; >= {} already registered", READER_MAX);
            }
            !self.reader_count.set(c, c+1).is_ok()
        } {}

        let readers = unsafe { &mut *self.readers.get() };
        let buf_head = self.buf_head.get();
        let buf_size = (self.buf_end.get() as usize) - (self.buf_ptr.get() as usize);
        let buf_tail = if buf_head > buf_size { buf_head - buf_size } else { 0 };

        readers[c] = Some(ReaderState::new(reader, can_block, buf_tail));
        ReaderId(c)
    }
    fn get_reader(&self, reader_id: ReaderId) -> &Option<ReaderState> {
        let readers = unsafe { &*self.readers.get() };
        &readers[reader_id.0]
    }
    // The "overran" return value is how many bytes got discarded if the head pointer wrapped around
    // this reader's tail.  For this to work right, that many bytes need to be added to the "bytes"
    // arg to bytes_consumed if the reader doesn't need writers to block.
    pub fn get_bytes(&self, reader_id: ReaderId) -> BufferState {
        if let &Some(ref r) = self.get_reader(reader_id) {
            let buf_mask = (self.buf_end.get() as usize) - (self.buf_ptr.get() as usize) - 1;
            let mut t = r.buf_tail.get();
            let h = self.buf_head.get();
            //write!(serial(), "-- tail: {}, head: {}\n", t, h).unwrap();
            if h == t {
                return BufferState::new(None, &[], 0usize)
            }
            let mut overran = 0usize;
            if h.wrapping_sub(t) > buf_mask + 1 {
                overran = h.wrapping_sub(t).wrapping_sub(buf_mask + 1);
                t = h.wrapping_sub(buf_mask + 1);
                //write!(serial(), "-- tail: {}, head: {}, overran: {}\n", t, h, overran).unwrap();
            }
            let start = unsafe { self.buf_ptr.get().offset((t & buf_mask) as isize) };
            let len = (self.buf_end.get() as usize) - (start as usize);
            if (h & buf_mask) <= (t & buf_mask) {
                //write!(serial(), "-- len: {}, h: {}\n", len, h & buf_mask).unwrap();
                BufferState::new(Some(r),
                                 unsafe { core::slice::from_raw_parts(start, len) },
                                 overran)
            } else {
                //write!(serial(), "-- len: {}\n", h.wrapping_sub(t)).unwrap();
                BufferState::new(Some(r),
                                 unsafe { core::slice::from_raw_parts(start, h.wrapping_sub(t)) },
                                 overran)
            }
        } else {
            //write!(serial(), "-- no reader, 0/0/0 return\n").unwrap();
            BufferState::new(None, &[], 0usize)
        }
    }
    // Call this after successfully sending out "bytes" bytes from the slices returned from
    // get_bytes().
    pub fn bytes_consumed(&self, bytes: usize, reader_id: ReaderId) {
        if let &Some(ref r) = self.get_reader(reader_id) {
            //write!(serial(), "bytes_consumed: {}\n", bytes).unwrap();
            while {
                let t = r.buf_tail.get();
                !r.buf_tail.set(t, t.wrapping_add(bytes)).is_ok()
            } {}
            //write!(serial(), "advanced by \"bytes\", got to {}\n", r.buf_tail.get()).unwrap();
        }
    }
}

// Interpose a DebugWriter in between stdout() and the DebugBuffer, because DebugBuffer needs to
// stay static-lifetime, which means it can't be mutable, but the write! macro requires its writer
// to borrow self mutably.  (As does the core::fmt::Write trait.)  The DebugWriter can be a
// temporary mutable wrapper struct that simply forwards to the non-mutable-self-reference methods
// on DebugBuffer (which themselves don't need self to be mutable, because they use atomics for
// safe sharing), so each instance of it can be mutable without issues.

#[cfg_attr(test, allow(dead_code))]
pub struct DebugWriter {
    buffer: &'static Option<&'static DebugBuffer<'static>>,
}

impl core::fmt::Write for DebugWriter {
    fn write_str(&mut self, s: &str) -> Result<(), core::fmt::Error> {
        if let Some(ref b) = *self.buffer {
            b.buffer_str(s);
        }
        Ok(())
    }
    fn write_char(&mut self, c: char) -> Result<(), core::fmt::Error> {
        if let Some(ref b) = *self.buffer {
            // evil...
            b.buffer(&[c as u8]);
        }
        Ok(())
    }
}

#[derive(Clone, Copy)]
pub struct ReaderId(usize);
pub struct ReaderState<'a> {
    reader: &'a dyn Reader,
    can_block: AtomicBoolCell,
    needs_block_downgrade: bool,
    notified: AtomicBoolCell,
    buf_tail: AtomicUsizeCell,
}
impl<'a> ReaderState<'a> {
    pub const fn new(reader: &'a dyn Reader, can_block: bool, buf_tail: usize) -> Self {
        ReaderState{
            reader: reader,
            can_block: AtomicBoolCell::new(can_block),
            needs_block_downgrade: !can_block,
            notified: AtomicBoolCell::new(false),
            // Using zero here might break if all other readers are farther along and a new
            // buffer() operation allocates past the end.  But in practice we'll always register
            // readers at startup, before we buffer more than 8k of prints.  So it'll work out fine
            // -- whereas if we try to find the min, we'll miss a bunch of data that has already
            // been sent to all the other readers.
            //
            // ...Although, on the other hand, if head has just wrapped around and is sitting at
            // (say) 1, then setting this to 0 will miss basically the entire buffer.  That seems
            // like the best of a bunch of problematic options.
            buf_tail: AtomicUsizeCell::new(buf_tail),
        }
    }
    #[cfg(feature = "devicetest")]
    pub fn tail(&self) -> usize {
        self.buf_tail.get()
    }
    pub fn upgrade_blocking(&self) {
        if self.needs_block_downgrade {
            while {
                !self.can_block.set(self.can_block.get(), true).is_ok()
            } {}
        }
    }
    pub fn downgrade_blocking(&self) {
        if self.needs_block_downgrade {
            while {
                !self.can_block.set(self.can_block.get(), false).is_ok()
            } {}
        }
    }
}

pub struct BufferState<'a> {
    reader: Option<&'a ReaderState<'a>>,
    pub buffer: &'a [u8],
    pub overran: usize,
}
impl<'a> BufferState<'a> {
    pub fn new(reader: Option<&'a ReaderState<'a>>, buf: &'a [u8], overran: usize) -> Self {
        if let Some(ref r) = reader {
            r.upgrade_blocking();
        }
        BufferState{
            reader: reader,
            buffer: buf,
            overran: overran,
        }
    }
}
impl<'a> Drop for BufferState<'a> {
    fn drop(&mut self) {
        if let Some(ref r) = self.reader {
            r.downgrade_blocking();
        }
    }
}

pub static mut DEBUG: Option<&'static DebugBuffer> = None;
pub static mut SERIAL: Option<&'static SerialDma> = None;

#[allow(non_snake_case)]
pub unsafe extern "C" fn Handle_DMA2_Stream7() {
    if let Some(ref s) = SERIAL {
        let stream = DMA2().stream(UART1_TX_STREAM);
        if stream.htif() { stream.clear_htif(); }
        if stream.teif() {
            write!(stdout(), "transfer error!\n").unwrap();
            stream.clear_teif();
        }
        if stream.dmeif() {
            write!(stdout(), "direct-mode error!\n").unwrap();
            stream.clear_dmeif();
        }
        if stream.feif() {
            if !stream.tcif() {
                write!(stdout(), "fifo error!\n").unwrap();
            }
            stream.clear_feif();
        }
        if stream.tcif() {
            stream.clear_tcif();
            s.queue_dma_chunk();
        }
    }
}

const UART1_TX_STREAM: usize = 7;
const UART1_TX_CHANNEL: u32 = 4;

pub struct SerialDma {
    buffer: &'static DebugBuffer<'static>,
    reader_id: Option<ReaderId>,
    paused: AtomicBoolCell,
    xfer_size: AtomicUsizeCell,
    dma_xaction: AtomicRefCell<Option<DmaTransaction<&'static [u8]>>>,
    buffer_state: AtomicRefCell<Option<BufferState<'static>>>,
}

impl SerialDma {
    #[cfg(not(feature = "devicetest"))]
    pub const fn new(buffer: &'static DebugBuffer<'static>) -> Self {
        SerialDma{
            buffer: buffer,
            reader_id: Some(ReaderId(0usize)),  // HACK: only works because we know our one caller
            paused: AtomicBoolCell::new(false),
            xfer_size: AtomicUsizeCell::new(0usize),
            dma_xaction: AtomicRefCell::new(None),
            buffer_state: AtomicRefCell::new(None),
        }
    }

    #[cfg(not(feature = "devicetest"))]
    pub fn configure(&self) {

        // enable DMA
        // channel 4, stream 7, DMA2
        USART1().c3.set_dmat(true);

    }

    pub fn dma_going(&self) -> bool {
        DMA2().streams[UART1_TX_STREAM].c.en()
    }

    pub fn pause_dma(&self) {
        self.paused.set(false, true).ok();
    }
    pub fn unpause_dma(&self) {
        self.paused.set(true, false).ok();
    }

    pub fn queue_dma_chunk(&self) {
        // This interrupt can't happen until the DMA transaction is started at the end of this
        // function, so we don't have to worry about atomics being updated.  (But the atomics still
        // need to exist for the compiler.)

        if let Some(ref r) = self.reader_id {
            // In to_periph() we set a bunch of registers that also get cleared by the Drop
            // impl on DmaTransaction.  So drop the old transaction here (if there is one) to
            // make sure we don't screw up the next one.
            //
            // The only possible way this borrow_mut() could panic is if we're in an interrupt
            // handler and the non-interrupt code also has a mutable reference (because there is no
            // plain borrow() of self.dma_xaction anywhere in the codebase).  The non-interrupt
            // code can't be down after tr.start() trying to store the DMA transaction away,
            // because interrupts are disabled there, so the DMA copy can't have run an IRQ for
            // completion before that AtomicBorrowMut went out of scope.
            //
            // But the non-interrupt code also can't be running this .take() call, because we're in
            // an interrupt handler, and there's no way the interrupt could have fired in that
            // context.  The only callers of .queue_dma_chunk are:
            //
            // - The Reader impl, which only runs when new data is buffered, but an interrupt can't
            //   trigger while that's running because it only happens after the previous DMA
            //   finished.  (So the sequence is, someone buffers data, Reader impl starts up DMA,
            //   we get an interrupt, we call .bytes_consumed() and advance our tail pointer all
            //   the way to head.  But then the interrupt completes, we get through borrow_mut
            //   here, and we do *not* start another DMA.  Eventually someone buffers more data,
            //   which calls into here.  But an interrupt cannot possibly happen at that point
            //   because the previous interrupt handler already completed, and the interrupt is
            //   higher priority than normal code.)
            //
            // - The SERIAL unpause sequence, but likewise; interrupts can't happen while that's
            //   going on, because the pause sequence waited until they fired with the previous
            //   data.
            //
            // - The interrupt handler, but that's us.
            self.dma_xaction.borrow_mut().take();

            // Then tell the buffer that the previous transmit is done.
            //
            // This can't happen before we clean up dma_xaction because as soon as it happens, an
            // interrupt that logs something can call into here again and start a new dma_xaction.
            // If that happens, the borrow_mut().take() might destroy the interrupt's xaction,
            // which will cause another DMA-done interrupt to call into here and double-borrow the
            // xaction ref-cell.
            let size = self.xfer_size.swap(0usize);
            self.buffer.bytes_consumed(size, *r);
            self.buffer_state.borrow_mut().take();

            if self.paused.get() {
                return;
            }

            {
                // SerialDma is a blocking reader, so the overran return value will always be zero,
                // so ignore it.
                let state = self.buffer.get_bytes(*r);
                // Hold on to the size of this transmission for the interrupt handler to send to
                // the buffer.
                self.xfer_size.set(0usize, state.buffer.len()).expect("Somehow xfer_size got updated?!");

                // If we got nothing, then stop DMAing until we get another notification via the
                // Reader interface.
                if state.buffer.len() == 0 {
                    return;
                }

                *self.buffer_state.borrow_mut() = Some(state);
            }

            // Set up DMA
            USART1().s.set_tc(false);
            let stream = DMA2().stream(UART1_TX_STREAM);
            let tr = stream.to_periph(&USART1().d, UART1_TX_CHANNEL, self.buffer_state.borrow().as_ref().unwrap().buffer);

            // Start it, and store away the transaction in self until we're done with it next
            // time around.
            //
            // Disable the DMA interrupt while we're doing those because we can't have it trigger
            // before the .borrow_mut() return value goes out of scope and its Drop impl finishes.
            // (Else the interrupt could see an already-borrowed AtomicRefCell and panic.)
            // Alternately, it could work to do these *borrow_mut() = Some(tr) and .start()
            // operations the other way around, except that the *borrow_mut() = Some(tr) op moves
            // the DmaTransaction out of tr.  (And DmaTransaction can't be made Copy because its
            // Drop method is not idempotent.)
            NVIC().disable(Interrupt::DMA2_Stream7);
            tr.start();
            *self.dma_xaction.borrow_mut() = Some(tr);
            NVIC().enable(Interrupt::DMA2_Stream7);
        }
    }
}

impl<'a> Reader for &'a SerialDma {
    fn have_new_data(&self) {
        self.queue_dma_chunk();
    }
}

pub fn configure_serial_port() {
    // 8 data bits
    USART1().c.set_m(false);
    // 1 stop bit
    USART1().c2.set_stop(0);
    
    // fPCLK is 60MHz and oversampling defaults to 16

    //   19200 bit/s is 0xc35
    //  115200 bit/s is 0x208
    // 3000000 bit/s is 0x014
    USART1().br.write(0x014);

    USART1().c.update()
        .set_te(true)
        // enable
        .set_ue(true);
}

pub struct Serial {
    pub block: &'static UsartBlock,
}

impl Serial {
    fn write_byte(&self, b: u8) -> Result<(), core::fmt::Error> {
        while !self.block.s.txe() {
        }
        self.block.d.write(b as u32);
        Ok(())
    }
    pub fn write_all(&self, buf: &[u8]) -> Result<(), core::fmt::Error> {
        if let Some(s) = unsafe { SERIAL } {
            s.pause_dma();
            while s.dma_going() {
            }
        }
        for b in buf {
            self.write_byte(*b)?;
        }
        if let Some(s) = unsafe { SERIAL } {
            s.unpause_dma();
            s.queue_dma_chunk();
        }
        Ok(())
    }
}

impl core::fmt::Write for Serial {
    fn write_str(&mut self, s: &str) -> Result<(), core::fmt::Error> {
        self.write_all(s.as_bytes())
    }
    fn write_char(&mut self, c: char) -> Result<(), core::fmt::Error> {
        // evil...
        self.write_all(&[c as u8])
    }
}

#[cfg(not(test))]
pub fn stdout() -> DebugWriter {
    DebugWriter{buffer: unsafe { &DEBUG }}
}

pub fn serial() -> Serial {
    Serial{block: USART1()}
}

#[cfg(test)]
pub fn stdout() -> std::io::Stdout {
    std::io::stdout()
}

#[cfg(not(test))]
#[macro_export]
macro_rules! print {
    ( $( $x:expr ),* ) => {{
        (write!($crate::debug::stdout(), $($x,)*)).unwrap();
    }}
}

#[cfg(not(test))]
#[macro_export]
macro_rules! println {
    ( $( $x:expr ),* ) => {{
        (write!($crate::debug::stdout(), $($x,)*)).unwrap();
        $crate::debug::stdout().write_char('\n').unwrap();
    }}
}

#[cfg(not(feature = "devicetest"))]
mod hexdump_mod {
    use debug;
    #[cfg(not(test))]
    use core::fmt::{Result, Write};

    #[cfg(test)]
    use std;
    #[cfg(test)]
    use std::io::Write;
    #[cfg(test)]
    type Result = std::io::Result<()>;

    fn format_hex_suffix<T: Write>(writer: &mut T, buf: &[u8]) -> Result {

        write!(writer, " |")?;
        for c in buf {
            let ch = match *c {
                32..=127 => *c,
                _ => '.' as u8,
            };
            #[cfg(test)] {
                writer.write_all(&[ch])?;
            }
            #[cfg(not(test))] {
                writer.write_char(ch as char)?;
            }
        }
        write!(writer, "|\n")
    }

    pub fn hexdump(buf: &[u8], padding: &str) {
        hexdump_write(&mut debug::stdout(), buf, padding).unwrap();
    }
    pub fn hexdump_write<T: Write>(writer: &mut T, buf: &[u8], padding: &str) -> Result {
        //write!(writer, "{}Bytes at 0x{:08x}\n", padding, buf.as_ptr() as usize);
        let mut line_start = 0;
        for i in 0..buf.len() {
            if i % 16 == 0 {
                if i > 0 {
                    format_hex_suffix(writer, &buf[line_start..i])?;
                }
                write!(writer, "{}", padding)?;
                line_start = i;
            }
            if i % 16 == 8 {
                write!(writer, " ")?;
            }
            write!(writer, "{:02x} ", buf[i])?;
        }
        if buf.len() > line_start {
            for _ in buf.len()..line_start+16 {
                write!(writer, "   ")?;
            }
            if buf.len() - line_start <= 8 {
                write!(writer, " ")?;
            }
            format_hex_suffix(writer, &buf[line_start..buf.len()])?;
        }
        return Ok(());
    }
}

#[cfg(not(feature = "devicetest"))]
pub use debug::hexdump_mod::hexdump;
#[cfg(not(feature = "devicetest"))]
pub use debug::hexdump_mod::hexdump_write;


#[test]
fn test_debug_buffer() {
    struct TestReader {
        have_new_data_called: AtomicUsize,
    }
    impl Reader for TestReader {
        fn have_new_data(&self) {
            self.have_new_data_called.fetch_add(1, Ordering::SeqCst);
        }
    }

    let mut readers = [None, None, None, None];
    let mut bytes = [0u8; 8];
    let meta = DebugBufferMeta::new();
    let reader1 = TestReader{
        have_new_data_called: AtomicUsize::new(0),
    };
    let buf = DebugBuffer::new(&mut bytes, &meta, &mut readers);
    buf.buffer_str("hi\n");
    let reader1_id = buf.register_reader(&reader1, false);

    {
        let state = buf.get_bytes(reader1_id);
        assert_eq!("hi\n".as_bytes(), state.buffer);
        assert_eq!(0, state.overran);
    }

    {
        let state = buf.get_bytes(reader1_id);
        assert_eq!("hi\n".as_bytes(), state.buffer);
        assert_eq!(0, state.overran);

        buf.bytes_consumed(3, reader1_id);
    }

    {
        let state = buf.get_bytes(reader1_id);
        assert_eq!("".as_bytes(), state.buffer);
        assert_eq!(0, state.overran);
    }

    buf.buffer("foo bar".as_bytes());

    {
        let state = buf.get_bytes(reader1_id);
        assert_eq!("foo b".as_bytes(), state.buffer);
        assert_eq!(0, state.overran);

        buf.bytes_consumed(3, reader1_id);
    }

    {
        let state = buf.get_bytes(reader1_id);
        assert_eq!(" b".as_bytes(), state.buffer);
        assert_eq!(0, state.overran);

        buf.bytes_consumed(2, reader1_id);
    }

    {
        let state = buf.get_bytes(reader1_id);
        assert_eq!("ar".as_bytes(), state.buffer);
        assert_eq!(0, state.overran);
    }

    buf.buffer("abcdef".as_bytes());

    {
        let state = buf.get_bytes(reader1_id);
        assert_eq!("arabcdef".as_bytes(), state.buffer);
        assert_eq!(0, state.overran);
    }

    buf.buffer("g".as_bytes());

    {
        let state = buf.get_bytes(reader1_id);
        assert_eq!("rabcdef".as_bytes(), state.buffer);
        assert_eq!(1, state.overran);
    }

    buf.buffer("h".as_bytes());

    {
        let state = buf.get_bytes(reader1_id);
        assert_eq!("abcdef".as_bytes(), state.buffer);
        assert_eq!(2, state.overran);
    }

    buf.buffer("ijklmnop".as_bytes());

    {
        let state = buf.get_bytes(reader1_id);
        assert_eq!("ijklmn".as_bytes(), state.buffer);
        assert_eq!(10, state.overran);
    }

    buf.buffer("qrstuv".as_bytes());

    {
        let state = buf.get_bytes(reader1_id);
        assert_eq!("opqrstuv".as_bytes(), state.buffer);
        assert_eq!(16, state.overran);
    }

    buf.buffer("xyz".as_bytes());

    {
        let state = buf.get_bytes(reader1_id);
        assert_eq!("rstuv".as_bytes(), state.buffer);
        assert_eq!(19, state.overran);

        buf.bytes_consumed(state.overran + 8, reader1_id);
    }

    {
        let state = buf.get_bytes(reader1_id);
        assert_eq!("".as_bytes(), state.buffer);
        assert_eq!(0, state.overran);
    }

    buf.buffer("abcdefgh".as_bytes());

    {
        let state = buf.get_bytes(reader1_id);
        assert_eq!("abcde".as_bytes(), state.buffer);
        assert_eq!(0, state.overran);

        buf.bytes_consumed(8, reader1_id);
    }

    {
        let state = buf.get_bytes(reader1_id);
        assert_eq!("".as_bytes(), state.buffer);
        assert_eq!(0, state.overran);
    }
}
