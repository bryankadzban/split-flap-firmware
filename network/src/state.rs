// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use core;
use arena::GLOBAL_ARENA;
use core::hash::{Hash, Hasher};
use core::fmt::Write;
use utils::func::{ObservableValue};
use {ErrorCode, NetworkInterface};
use protocols::{EchoPacketBuilder, EthernetAddr, EthernetPacket, EthernetDetails,
                IcmpOption, IcmpOptionSetter, IpAddr, Ipv6PacketBuilder, Icmp6PacketBuilder,
                L3Payload, L4Payload, Icmp6Payload, NeighborAdvertisementPacketBuilder,
                NeighborSolicitationPacketBuilder, RouterSolicitationPacketBuilder, UdpPacket};
use scheduler::{RefScheduler, When, Slot};

const ALL_HOSTS_ADDR: IpAddr = IpAddr::new([
        0xff, 0x02, 0x00, 0x00,     0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00,     0x00, 0x00, 0x00, 0x01]);
const ALL_ROUTERS_ADDR: IpAddr = IpAddr::new([
        0xff, 0x02, 0x00, 0x00,     0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00,     0x00, 0x00, 0x00, 0x02]);
const LINK_LOCAL_PREFIX: IpAddr = IpAddr::new([
        0xfe, 0x80, 0x00, 0x00,     0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00,     0x00, 0x00, 0x00, 0x00]);

pub struct FnvHasher32(u32);
impl FnvHasher32 {
    pub fn new() -> FnvHasher32 {
        FnvHasher32(2166136261)
    }
}
impl FnvHasher32 {
    fn finish_u32(&self) -> u32{
        self.0
    }
}
impl Hasher for FnvHasher32 {
    fn finish(&self) -> u64 {
        self.0 as u64
    }
    fn write(&mut self, bytes: &[u8]) {
        for byte in bytes.iter() {
            self.0 = self.0 ^ (*byte as u32);
            self.0 = self.0.wrapping_mul(16777619)
        }
    }
}


struct FixedHashMap<'a, K: 'a, V: 'a> {
    entries: &'a mut [(u32, K, V)],
    mask: usize,
}
impl<'a, K: Hash + Eq, V> FixedHashMap<'a, K, V> {
    pub fn new(entries: &'a mut [(u32, K, V)]) -> Self {
        let mask = (1 << (core::mem::size_of::<usize>() * 8 -
                         (entries.len().leading_zeros() as usize) - 1)) - 1;
        FixedHashMap{
            entries: entries,
            mask: mask,
        }
    }
    pub fn clear(&mut self) {
        for entry in self.entries.iter_mut() {
            entry.0 = 0;
        }
    }
    pub fn get(&self, key: &K) -> Option<&V> {
        let hash = Self::hash_key(key);
        let start_bucket = self.hash_bucket(hash);
        let mut bucket = start_bucket;
        loop {
            let entry = &self.entries[bucket];
            if Self::is_empty(entry) {
                return None;
            }
            if entry.0 == hash && &entry.1 == key {
                return Some(&entry.2);
            }
            bucket = (bucket + 1) & self.mask;
            if bucket == start_bucket {
                return None;
            }
        }
    }
    pub fn insert(&mut self, key: K, value: V) -> Option<V> {
        let hash = Self::hash_key(&key);
        let start_bucket = self.hash_bucket(hash);
        let mut bucket = start_bucket;
        loop {
            let entry = &mut self.entries[bucket];
            if Self::is_empty(entry) {
                *entry = (hash, key, value);
                return None;
            }
            if entry.0 == hash && entry.1 == key {
                let old = core::mem::replace(entry, (hash, key, value));
                return Some(old.2);
            }
            bucket = (bucket + 1) & self.mask;
            if bucket == start_bucket {
                println!("Map is full");
                break;
            }
        }
        None
    }
    pub fn iter(&'a self) -> HashMapIter<'a, K, V> {
        HashMapIter{
            inner: self.entries.iter(),
        }
    }

    fn is_empty(entry: &(u32, K, V)) -> bool {
        (entry.0 & 0x80000000) == 0
    }
    fn hash_key(key: &K) -> u32 {
        let mut s = FnvHasher32::new();
        key.hash(&mut s);
        s.finish_u32() | 0x80000000
    }
    fn hash_bucket(&self, hash: u32) -> usize {
        return (hash as usize) & self.mask
    }
}

pub struct HashMapIter<'a, K: 'a, V: 'a> {
    inner: core::slice::Iter<'a, (u32, K, V)>,
}
impl<'a, K: Eq + Hash, V> Iterator for HashMapIter<'a, K, V> {
    type Item = (&'a K, &'a V);

    fn next(&mut self) -> Option<(&'a K, &'a V)>{
        loop {
            match self.inner.next() {
                Some(entry) => {
                    if !FixedHashMap::is_empty(entry) {
                        return Some((&entry.1, &entry.2));
                    }
                },
                None => { return None; }
            }
        }
    }

}


#[derive(Eq, PartialEq)]
struct RouterInfo {
    pub mac_addr: EthernetAddr,
    pub prefix: IpAddr,
    pub prefix_len: usize,
    pub my_ip: IpAddr,
}

pub struct Config {
    pub mac_addr: EthernetAddr,
}
pub struct SendInfo {
    pub src_ip: IpAddr,
    pub dest_mac: EthernetAddr,
}

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum Connectivity {
    NoLink,
    NoAddress,
    Connected,
}


pub struct Ipv6Stack<'a, N: 'a + NetworkInterface> {
    sched: &'a RefScheduler<'a, Ipv6Stack<'a, N>>,
    mac_addr: EthernetAddr,
    #[allow(dead_code)]
    mac_broadcast_addr: EthernetAddr,

    nic: &'a N,
    link_ip: IpAddr,
    active_router: Option<RouterInfo>,

    slot1: Slot,
    neighbors: FixedHashMap<'a, IpAddr, EthernetAddr>,
    my_ips: FixedHashMap<'a, IpAddr, bool>,

    pub out_connectivity: ObservableValue<'a, Connectivity>,
    pub udp_recv: Option<&'a mut dyn FnMut(&UdpPacket)>,
}
impl<'a, N: 'a + NetworkInterface> Ipv6Stack<'a, N> {
    pub fn new(sched: &'a RefScheduler<'a, Self>, nic: &'a N, config: Config) -> Self {
        let mut result = Ipv6Stack {
            sched: sched,
            mac_addr: config.mac_addr,
            mac_broadcast_addr: IpAddr::ipv6_link_local(config.mac_addr).solicited_node_addr().ethernet_multicast(),
            link_ip: IpAddr::ipv6_link_local(config.mac_addr),
            nic: nic,
            active_router: None,

            slot1: sched.new_slot(),
            neighbors: FixedHashMap::new(
                GLOBAL_ARENA.place_mut([(0, IpAddr::default(), EthernetAddr::default()); 128])),
            my_ips: FixedHashMap::new(
                GLOBAL_ARENA.place_mut([(0, IpAddr::default(), bool::default()); 8])),

            out_connectivity: ObservableValue::new(Connectivity::NoLink),
            udp_recv: None,
        };

        result.my_ips.insert(result.link_ip, true);
        result.update_multicast_addrs();

        result
    }
    pub fn notify_link_state(&mut self, is_up: bool) {
        if is_up {
            self.out_connectivity.set(Connectivity::NoAddress);
            self.sched.set(&self.slot1, When::AfterMs(100), Self::send_solicitation_if_necessary);
        } else {
            self.sched.cancel(&mut self.slot1);
            self.active_router = None;
            self.neighbors.clear();
            self.out_connectivity.set(Connectivity::NoLink);
        }
    }
    pub fn mac_addr(&self) -> EthernetAddr { self.mac_addr }
    #[allow(dead_code)]
    pub fn ping_internet(&mut self) {
        let dest = IpAddr::new([
            0x26, 0x04, 0xa8, 0x80,   0x00, 0x01, 0x00, 0x20,
            0x00, 0x00, 0x00, 0x00,   0x09, 0xe8, 0x20, 0x01]);
        let src = match self.active_router {
            Some(ref active_router) => active_router.my_ip,
            None => { return; }
        };
        println!("Sending ping");
        match self.send(|ip_builder| { ip_builder
            .set_src(src)
            .set_dest(dest)
            .edit_payload(|buf| { Icmp6PacketBuilder::new(buf)
                .edit_payload(|buf| { EchoPacketBuilder::new_request(buf)
                    .set_identifier(0xfa01)
                    .set_sequence_number(1)
                    .set_payload("Howeifjewiojfwoeifjeowifjweoifjweoifjweofijaofejweoifheiwufhweiufh3298fh3298h3298fh3298fh2398fh329hf2389fh3928fh2398fh2938fh98qh98fha2398fha3fhiuh2ifh2938hfowiehfiwu3h9f8h239fhwieufhi23fhw3uh3kjhfkjsdhfu3f2uh3uhello world!".as_bytes())
                    .complete()
                }).complete()
            });
        }) {
            Ok(()) => {}
            Err(ErrorCode::NotYet) => {
                println!("Ping send failed: NotYet");
            }
        }
    }

    fn update_multicast_addrs(&mut self) {
        let mut len = 0;
        let mut addrs = [EthernetAddr::default(); 10];
        addrs[len] = ALL_HOSTS_ADDR.ethernet_multicast();
        len += 1;
        for (ip, _) in self.my_ips.iter() {
            addrs[len] = ip.solicited_node_addr().ethernet_multicast();
            len += 1;
        }
        println!("call set_multicast_address");
        match self.nic.set_multicast_addresses(&addrs[0..len]) {
            Ok(()) => {}
            Err(e) => {
                println!("set_multicast_address failed: {}", e);
            }
        }
    }

    fn send_solicitation_if_necessary(&mut self) {
        if self.active_router.is_some() {
            return;
        }

        println!("Sending Router Solicitation from {}", self.link_ip);
        match self.nic.send(|builder| { builder
            .set_src(self.mac_addr)
            .set_dest(ALL_ROUTERS_ADDR.ethernet_multicast())
            .edit_payload(|buf| { Ipv6PacketBuilder::new(buf)
                .set_src(self.link_ip)
                .set_dest(ALL_ROUTERS_ADDR)
                .edit_payload(|buf| { Icmp6PacketBuilder::new(buf)
                    .edit_payload(|buf| { RouterSolicitationPacketBuilder::new(buf)
                        .add_option(IcmpOptionSetter::SourceLinkAddress(self.mac_addr))
                        .complete()
                    }).complete()
                }).complete()
            });
            Ok(())
        }) {
            Ok(()) => {}
            Err(ErrorCode::NotYet) => {
                println!("Router solicitation send failed: NotYet");
            }
        }
        self.sched.set(&self.slot1, When::AfterMs(1000), Self::send_solicitation_if_necessary);
    }
    pub fn send<F: FnOnce(&mut Ipv6PacketBuilder)>(&mut self, func: F) -> Result<(), ErrorCode> {
        self.nic.send(|ethernet| {
            let mut result = Ok(());
            ethernet.set_src(self.mac_addr);
            let mut eth_dest = EthernetAddr::default();
            ethernet.edit_payload(|buf| {
                let mut ipv6 = Ipv6PacketBuilder::new(buf);
                if let Some(ref active_router) = self.active_router {
                    ipv6.set_src(active_router.my_ip);
                }
                func(&mut ipv6);
                if let Some(info) = self.lookup_dest_info(ipv6.packet().dest()) {
                    if ipv6.packet().src() == IpAddr::default() {
                        // This breaks the checksum...
                        ipv6.set_src(info.src_ip);
                    }
                    eth_dest = info.dest_mac;
                    return ipv6.complete();
                }
                result = Err(ErrorCode::NotYet);
                EthernetDetails::default()
            });
            ethernet.set_dest(eth_dest);
            result
        })
    }
    fn lookup_dest_info(&mut self, dest: IpAddr) -> Option<SendInfo>  {
        if dest.is_link_local_multicast() {
            return Some(SendInfo{src_ip: self.link_ip, dest_mac: dest.ethernet_multicast()});
        }
        if dest.prefix(64) == LINK_LOCAL_PREFIX {
            if let Some(mac_addr) = self.neighbors.get(&dest) {
                return Some(SendInfo{src_ip: self.link_ip, dest_mac: *mac_addr});
            }
            let link_ip = self.link_ip;
            self.start_lookup(link_ip, dest);
            return None;
        }
        let mut lookup_src: Option<IpAddr> = None;
        if let Some(ref active_router) = self.active_router {
            if active_router.prefix == dest.prefix(active_router.prefix_len) {
                if let Some(mac_addr) = self.neighbors.get(&dest) {
                    return Some(SendInfo{src_ip: active_router.my_ip, dest_mac: *mac_addr});
                }
                lookup_src = Some(active_router.my_ip);
            } else {
                return Some(SendInfo{src_ip: active_router.my_ip, dest_mac: active_router.mac_addr});
            }
        }
        if let Some(lookup_src) = lookup_src {
            self.start_lookup(lookup_src, dest);
        }
        return None
    }

    fn start_lookup(&mut self, src: IpAddr, dest: IpAddr) {
        println!("Sending Neighbor Solicitation for {} from {}", dest, src);
        let mac_addr = self.mac_addr;
        match self.send(|ip_builder| { ip_builder
            .set_src(src)
            .set_dest(dest.solicited_node_addr())
            .edit_payload(|buf| { Icmp6PacketBuilder::new(buf)
                .edit_payload(|buf| { NeighborSolicitationPacketBuilder::new(buf)
                    .set_target_addr(dest)
                    .add_option(IcmpOptionSetter::SourceLinkAddress(mac_addr))
                    .complete()
                }).complete()
            });
        }) {
            Ok(()) => {}
            Err(ErrorCode::NotYet) => {
                println!("Neighbor solicitation send failed: NotYet");
            }
        }
    }

    pub fn raw_input(&mut self, p: &EthernetPacket) {
        //if p.dest() == self.mac_addr || p.dest() == self.mac_broadcast_addr {
        //    println!("Received ethernet frame from {} to {}", p.src(), p.dest());
        //} else {
        //    println!("Received unexpected ethernet frame from {} to {}", p.src(), p.dest());
        //}
        match p.payload() {
            L3Payload::Ipv6(ip) => {
                //println!("  IP bytes:");
                //hexdump(ip.raw_bytes(), "    ");
                match ip.payload() {
                    L4Payload::Udp(udp) => {
                        if let Some(ref mut udp_recv) = self.udp_recv {
                            udp_recv(&udp);
                        }
                    },
                    L4Payload::Icmp6(icmp) => {
                        match icmp.payload() {
                            Icmp6Payload::EchoRequest(echo) => {
                                println!("Received ping: id: {} seq: {} from: {}",
                                         echo.identifier(), echo.sequence_number(),
                                         ip.src());
                                match self.send(|ip_builder| { ip_builder
                                    .set_src(ip.dest())
                                    .set_dest(ip.src())
                                    .edit_payload(|buf| { Icmp6PacketBuilder::new(buf)
                                        .edit_payload(|buf| { EchoPacketBuilder::new_reply(buf)
                                            .set_identifier(echo.identifier())
                                            .set_sequence_number(echo.sequence_number())
                                            .set_payload(&echo.payload())
                                            .complete()
                                        }).complete()
                                    });
                                }) {
                                    Ok(()) => {}
                                    Err(ErrorCode::NotYet) => {
                                        println!("Echo response send failed: NotYet");
                                    }
                                }
                            },
                            Icmp6Payload::EchoReply(echo) => {
                                println!("Received ping reply: id: {} seq: {} from: {}",
                                         echo.identifier(), echo.sequence_number(),
                                         ip.src());
                            },
                            Icmp6Payload::NeighborSolicitation(payload) => {
                                if self.my_ips.get(&payload.target_addr()).is_none() {
                                    return;
                                }
                                for option in payload.options_iter() {
                                    if let IcmpOption::SourceLinkAddress(addr) = option {
                                        if let Some(mac) = EthernetAddr::from_bytes(addr.as_bytes()) {
                                            if mac == p.src() {
                                                if self.neighbors.insert(ip.src(), mac).is_none() {
                                                    println!("Discovered new neighbor from incoming solicitation {} {}", ip.src(), mac);
                                                }
                                            }
                                        }
                                    }
                                }
                                println!("Responding to neighbor solicitation");
                                let mac_addr = self.mac_addr;
                                match self.send(|ip_builder| { ip_builder
                                    .set_src(payload.target_addr())
                                    .set_dest(ip.src())
                                    .edit_payload(|buf| { Icmp6PacketBuilder::new(buf)
                                        .edit_payload(|buf| { NeighborAdvertisementPacketBuilder::new(buf)
                                            .set_is_solicited(true)
                                            .set_is_override(true)
                                            .set_target_addr(payload.target_addr())
                                            .add_option(IcmpOptionSetter::TargetLinkAddress(mac_addr))
                                            .complete()
                                        }).complete()
                                    });
                                }) {
                                    Ok(()) => {}
                                    Err(ErrorCode::NotYet) => {
                                        println!("Neighbor solicitation response send failed: NotYet");
                                    }
                                }
                            },
                            Icmp6Payload::NeighborAdvertisement(payload) => {
                                if self.neighbors.insert(payload.target_addr(), p.src()).is_none() {
                                    println!("Discovered new neighbor from incoming advertisement {} {}", ip.src(), p.src());
                                }
                            },
                            Icmp6Payload::RouterAdvertisement(payload) => {
                                 for option in payload.options_iter() {
                                    match option {
                                        IcmpOption::SourceLinkAddress(addr) => {
                                            if let Some(mac) = EthernetAddr::from_bytes(addr.as_bytes()) {
                                                if mac == p.src() {
                                                    self.neighbors.insert(ip.src(), mac);
                                                }
                                            }
                                        },
                                        IcmpOption::PrefixInfo(payload) => {
                                            let my_ip = IpAddr::combine(
                                                payload.prefix(), payload.prefix_len() as usize,
                                                self.link_ip);
                                            let new_router = Some(RouterInfo{
                                                mac_addr: p.src(),
                                                prefix: payload.prefix(),
                                                prefix_len: payload.prefix_len() as usize,
                                                my_ip: my_ip, 
                                            });
                                            if self.active_router == new_router {
                                                // This is redundant
                                                return;
                                            }
                                            self.active_router = new_router;
                                            self.sched.cancel(&mut self.slot1);
                                            println!("Associating with IP {}/{}; router mac {}",
                                                     my_ip, 128 - payload.prefix_len(), p.src());
                                            self.my_ips.insert(my_ip, true);
                                            self.out_connectivity.set(Connectivity::Connected);
                                        },
                                        _ => {}
                                    }
                                }
                            },
                            _ => {}
                        }
                    },
                    _ => {}
                }
            },
            _ => {
            }
        }
    }
}
