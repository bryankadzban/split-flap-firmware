// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use core::fmt;
use core;
use core::fmt::Write;
use utils::bits::{AsBytes, AsBytesMut, BitOpsBe};
use utils::primitive::FromPrimitive;

pub enum L3Payload<'a> {
    Unknown(&'a[u32]),
    Ipv6(Ipv6Packet<'a>),
}
impl<'a> L3Payload<'a> {
    fn from_ethertype(ethertype: u16, buf: &'a [u32]) -> Self {
        let unknown_payload = L3Payload::Unknown(buf);
        match ethertype {
            0x86dd => match Ipv6Packet::new(buf) {
                Some(p) => L3Payload::Ipv6(p), None => unknown_payload
            },
            _ => unknown_payload,
        }
    }
}

#[repr(C)]
#[derive(Clone, Copy, Eq, PartialEq)]
pub struct EthernetAddr(pub [u8; 6]);
impl EthernetAddr {
    pub fn from_bytes(buf: &[u8]) -> Option<EthernetAddr> {
        if buf.len() != 6 {
            return None;
        }
        Some(EthernetAddr([buf[0], buf[1], buf[2], buf[3], buf[4], buf[5]]))
    }
    pub fn as_bytes(&self) -> &[u8] {
        &self.0
    }
}
impl fmt::Display for EthernetAddr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:02x}:{:02x}:{:02x}:{:02x}:{:02x}:{:02x}",
               self.0[0], self.0[1], self.0[2], self.0[3], self.0[4], self.0[5])
    }
}
impl Default for EthernetAddr {
    fn default() -> Self {
        EthernetAddr([0, 0, 0, 0, 0, 0])
    }
}


pub struct EthernetPacket<'a>(&'a [u32]);
impl<'a> EthernetPacket<'a> {
    pub fn new<'b>(buf: &'b [u32]) -> Option<EthernetPacket<'b>> {
        if buf.len() < 4 {
            return None;
        }
        if buf.get_bits_be(112..128) == 0x8100 && buf.len() < 5 {
            return None;
        }
        Some(EthernetPacket(buf))
    }
    #[allow(dead_code)]
    pub fn dest(&self) -> EthernetAddr { 
        let b = self.0.as_bytes();
        EthernetAddr([b[2], b[3], b[4], b[5], b[6], b[7]])
    }
    pub fn src(&self) -> EthernetAddr {
        let b = self.0.as_bytes();
        EthernetAddr([b[8], b[9], b[10], b[11], b[12], b[13]])
    }
    pub fn ethertype(&self) -> u16 { 
        let result = self.0.get_bits_be(112..128) as u16;
        if result != 0x8100 {
            return result;
        }
        self.0.get_bits_be(144..160) as u16
    }
    #[allow(dead_code)]
    pub fn tci(&self) -> Option<TagControlInfo> {
        match self.0.get_bits_be(112..128) {
            0x8100 => Some(TagControlInfo(self.0.get_bits_be(128..144) as u16)),
            _ => None,
        }
    }
    pub fn payload(&self) -> L3Payload {
        L3Payload::from_ethertype(self.ethertype(), &self.0[self.header_wordlen()..])
    }
    fn header_wordlen(&self) -> usize {
        match self.0.get_bits_be(112..128) {
            0x8100 => 5,
            _ => 4,
        }
    }
}

pub struct EthernetPacketBuilder<'a> {
    buf: &'a mut [u32],
    byte_len: usize,
}
impl<'a> EthernetPacketBuilder<'a> {
    pub fn new(buf: &'a mut [u32]) -> Self {
        EthernetPacketBuilder {
            buf: buf,
            byte_len: 16,
        }
    }
    pub fn edit_payload<F: FnOnce(&mut [u32]) -> EthernetDetails>(
            &mut self, edit_func: F) -> &mut Self {
        let details = edit_func(&mut self.buf[4..]);
        self.set_ethertype(details.ethertype);
        self.byte_len = 16 + details.byte_len;
        self
    }
    #[inline(always)]
    pub fn set_dest(&mut self, addr: EthernetAddr) -> &mut Self {
        self.buf.as_mut_bytes()[2..8].clone_from_slice(&addr.0);
        self
    }
    #[inline(always)]
    pub fn set_src(&mut self, addr: EthernetAddr) -> &mut Self {
        self.buf.as_mut_bytes()[8..14].clone_from_slice(&addr.0);
        self
    }
    #[allow(dead_code)]
    pub fn as_bytes(&self) -> &[u8] { &self.buf.as_bytes()[0..self.byte_len] }
    pub fn complete(&self) -> u16 {
        self.byte_len as u16
    }
    fn set_ethertype(&mut self, val: u16) {
        self.buf.set_bits_be(112..128, val as u32);
    }
}

#[derive(Default)]
pub struct EthernetDetails {
    ethertype: u16,
    byte_len: usize,
}


pub struct Ipv6PacketBuilder<'a>{
    buf: &'a mut [u32],
}
impl<'a> Ipv6PacketBuilder<'a> {
    #[inline(always)]
    pub fn new(buf: &'a mut [u32]) -> Self {
        let mut result = Ipv6PacketBuilder{
            buf: buf,
        };
        result.set_version(6);
        result.set_hop_limit(255);
        result
    }

    pub fn set_version(&mut self, _version: u8) -> &mut Self { self.buf.set_bits_be(0..4, 6); self }
    #[allow(dead_code)]
    pub fn set_dscp(&mut self, val: u8) -> &mut Self { self.buf.set_bits_be(4..10, val as u32); self }
    #[allow(dead_code)]
    pub fn set_ecn(&mut self, val: Ecn) -> &mut Self { self.buf.set_bits_be(10..12, val as u32); self }
    #[allow(dead_code)]
    pub fn set_flow_label(&mut self, val: u32) -> &mut Self { self.buf.set_bits_be(12..32, val); self }

    fn set_payload_len(&mut self, val: u16) -> &mut Self { self.buf.set_bits_be(32..48, val as u32); self }
    fn set_next_header(&mut self, val: ProtocolNumber) -> &mut Self { self.buf.set_bits_be(48..56, val as u32); self }
    pub fn set_hop_limit(&mut self, val: u8) -> &mut Self { self.buf.set_bits_be(56..64, val as u32); self }

    pub fn set_src(&mut self, addr: IpAddr) -> &mut Self {
        self.buf[2..6].clone_from_slice(&addr.0);
        self
    }
    pub fn set_dest(&mut self, addr: IpAddr) -> &mut Self {
        self.buf[6..10].clone_from_slice(&addr.0);
        self
    }
    pub fn edit_payload<F: FnOnce(IpBuilderBufs) -> IpDetails>(
            &mut self, edit_func: F) -> &mut Self {
        {
            let details = {
                let (header, payload) = self.buf.split_at_mut(10);
                edit_func(IpBuilderBufs{buf: payload, pseudo_prefix: &header[2..10]})
            };
            self.set_next_header(details.proto_num);
            self.set_payload_len(details.byte_len as u16);
            self
        }
    }

    pub fn complete(&mut self) -> EthernetDetails {
        EthernetDetails{ethertype: 0x86dd, byte_len: 40 + self.packet().payload_len() as usize}
    }

    pub fn packet(&'a self) -> Ipv6Packet<'a> { Ipv6Packet(self.buf) }
}

pub struct IpBuilderBufs<'a> {
    buf: &'a mut [u32],
    pseudo_prefix: &'a [u32],
}
pub struct IpDetails {
    proto_num: ProtocolNumber,
    byte_len: usize,
}

primitive_enum! { 
    #[derive(Debug)]
    pub enum PriorityCodePoint as u8 {
        BestEffort = 0,
        Background = 1,
        ExcellentEffort = 2,
        CriticalApplication = 3,
        Unknown4 = 4,
        Unknown5 = 5,
        InterNetworkControl = 6,
        NetworkControl = 7,
    }
}

#[repr(C)]
#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub struct IpAddr([u32; 4]);
impl IpAddr {
    pub const fn new(buf: [u8; 16]) -> IpAddr {
        // TODO: Make this work for big endian
        IpAddr(
            [(buf[3] as u32) << 24 | (buf[2] as u32) << 16 | (buf[1] as u32) << 8 | (buf[0] as u32) << 0,
             (buf[7] as u32) << 24 | (buf[6] as u32) << 16 | (buf[5] as u32) << 8 | (buf[4] as u32) << 0,
             (buf[11] as u32) << 24 | (buf[10] as u32) << 16 | (buf[9] as u32) << 8 | (buf[8] as u32) << 0,
             (buf[15] as u32) << 24 | (buf[14] as u32) << 16 | (buf[13] as u32) << 8 | (buf[12] as u32) << 0,])
    }
    /// See https://tools.ietf.org/html/rfc2464#section-5
    pub fn ipv6_link_local(addr: EthernetAddr) -> IpAddr {
        Self::new(
            [0xfe, 0x80, 0x00, 0x00,  0x00, 0x00, 0x00, 0x00,
             addr.0[0] ^ 2, addr.0[1], addr.0[2], 0xff,
             0xfe, addr.0[3], addr.0[4], addr.0[5]])
    }
    /// See 
    #[inline(always)]
    pub fn solicited_node_addr(&self) -> IpAddr {
        let bytes = self.0.as_bytes();
        Self::new(
            [0xff, 0x02, 0x00, 0x00,  0x00, 0x00, 0x00, 0x00,
             0x00, 0x00, 0x00, 0x01,  0xff, bytes[13], bytes[14], bytes[15]])

    }
    pub fn is_link_local_multicast(&self) -> bool {
        self.0.get_bits_be(0..16) == 0xff02
    }

    /// See https://tools.ietf.org/html/rfc7042#section-2.3.1
    #[inline(always)]
    pub fn ethernet_multicast(self) -> EthernetAddr {
        let bytes = self.0.as_bytes();
        return EthernetAddr(
            [0x33, 0x33, bytes[12], bytes[13], bytes[14], bytes[15]])
    }
    pub fn combine(prefix: IpAddr, prefix_len: usize, remainder: IpAddr) -> IpAddr {
        let mut result = prefix;
        let mut word_start = prefix_len as u32;
        while word_start < 128 {
            let word_end = (word_start & 0x60) + 32;
            result.0.set_bits_be(
                word_start..word_end, remainder.0.get_bits_be(word_start..word_end));
            word_start = word_end;
        }
        result
    }
    pub fn prefix(&self, prefix_len: usize) -> Self {
        Self::combine(*self, prefix_len, IpAddr::default())
    }
}
impl Default for IpAddr {
    fn default() -> Self {
        IpAddr([0, 0, 0, 0])
    }
}
impl fmt::Display for IpAddr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let addr_bytes = self.0.as_bytes();
        let mut i = 0;
        while i < 16 {
            if addr_bytes[i] == 0 && addr_bytes[i+1] == 0 {
                write!(f, ":")?;
                break;
            }
            if i > 0 {
                write!(f, ":")?;
            }
            write!(f, "{:02x}{:02x}", addr_bytes[i], addr_bytes[i+1])?;
            i += 2;
        }
        while i < 16 && addr_bytes[i] == 0 && addr_bytes[i+1] == 0 {
            i += 2;
        }
        while i < 16 {
            write!(f, ":{:02x}{:02x}", addr_bytes[i], addr_bytes[i+1])?;
            i += 2;
        }
        Ok(())
    }
}

// The TCI in the ethernet frame
pub struct TagControlInfo(u16);
impl TagControlInfo {
    // 802.1p priority code point
    pub fn pcp(&self) -> PriorityCodePoint { 
        PriorityCodePoint::from_primitive(((self.0 & 0xe000) >> 13) as u8).unwrap()
    }
    // Drop eligible indicator; if true, frame can be dropped during congestion
    pub fn dei(&self) -> bool { self.0 & 0x1000 != 0 }
    // VLAN identifier
    pub fn vid(&self) -> u16 { self.0 & 0xfff }
}
impl fmt::Debug for TagControlInfo {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "[pcp: {:?}, dei: {:?}, vid: {:?}]",
               self.pcp(), self.dei(), self.vid())
    }
}

primitive_enum! {
    #[derive(Debug)]
    pub enum Ecn as u8 {
        NotCapable = 0,
        Capable1 = 1,
        Capable2 = 2,
        CongestionEncountered = 3,
    }
}

pub enum L4Payload<'a> {
    Unknown(&'a[u8]),
    Udp(UdpPacket<'a>),
    Icmp6(Icmp6Packet<'a>),
}

pub struct Ipv6Packet<'a>(&'a [u32]);
impl<'a> Ipv6Packet<'a> {
    pub fn new<'b>(buf: &'b [u32]) -> Option<Ipv6Packet<'b>> {
        if buf.len() < 10 {
            println!("Packet too small");
            return None;
        }
        let pkt = Ipv6Packet(buf);
        if pkt.version() != 6 {
            println!("Packet wrong version");
            return None;
        }
        if (pkt.payload_len() as usize) > ((buf.len() - 10) << 2) {
            println!("Packet payload is too small");
            return None;
        }
        Some(pkt)
    }
    pub fn version(&self) -> u8 { self.0.get_bits_be(0..4) as u8 }
    #[allow(dead_code)]
    pub fn dscp(&self) -> u8 { self.0.get_bits_be(4..10) as u8 }
    #[allow(dead_code)]
    pub fn ecn(&self) -> Ecn { Ecn::from_primitive(self.0.get_bits_be(10..12) as u8).unwrap() }
    #[allow(dead_code)]
    pub fn flow_label(&self) -> u32 { self.0.get_bits_be(12..32) }

    pub fn payload_len(&self) -> u16 { self.0.get_bits_be(32..48) as u16 }
    pub fn next_header(&self) -> ProtocolNumber { ProtocolNumber::from_primitive(self.0.get_bits_be(48..56) as u8).unwrap() }
    #[allow(dead_code)]
    pub fn hop_limit(&self) -> u8 { self.0.get_bits_be(56..64) as u8}

    pub fn src(&self) -> IpAddr { IpAddr([self.0[2], self.0[3], self.0[4], self.0[5]]) }
    pub fn dest(&self) -> IpAddr { IpAddr([self.0[6], self.0[7], self.0[8], self.0[9]]) }

    pub fn payload(&self) -> L4Payload {
        let payload_words = &self.0[10..10 + ((self.payload_len() as usize + 3) >> 2)];
        let unknown_payload = L4Payload::Unknown(&payload_words.as_bytes()[0..self.payload_len() as usize]);
        match self.next_header() {
            ProtocolNumber::Udp => match UdpPacket::new(payload_words) {
                Some(p) => L4Payload::Udp(p), None => unknown_payload
            },
            ProtocolNumber::Icmp6 => match Icmp6Packet::new(payload_words, self.payload_len() as usize) {
                Some(p) => L4Payload::Icmp6(p), None => unknown_payload
            },
            _ => unknown_payload,
        }
    }
    #[allow(dead_code)]
    pub fn raw_bytes(&self) -> &'a [u8] {
        &self.0.as_bytes()[0..40 + self.payload_len() as usize]
    }
}

pub struct UdpPacket<'a>(&'a [u32]);
impl<'a> UdpPacket<'a> {
    fn new<'b>(buf: &'b [u32]) -> Option<UdpPacket<'b>> {
        if buf.len() < 2 {
            return None;
        }
        let p = UdpPacket(buf);
        if buf.len() * 4 < p.len() as usize {
            return None;
        }
        Some(p)
    }
    #[allow(dead_code)]
    pub fn src_port(&self) -> u16 { self.0.get_bits_be(0..16) as u16 }
    pub fn dest_port(&self) -> u16 { self.0.get_bits_be(16..32) as u16 }
    pub fn len(&self) -> u16 { self.0.get_bits_be(32..48) as u16 }
    #[allow(dead_code)]
    pub fn checksum(&self) -> u16 { self.0.get_bits_be(48..64) as u16 }
    pub fn payload(&'a self) -> &'a [u8] { &self.0.as_bytes()[8..self.len() as usize] }
}

pub struct UdpPacketBuilder<'a> {
    buf: &'a mut [u32],
    pseudo_prefix: &'a [u32],
}
impl<'a> UdpPacketBuilder<'a> {
    pub fn new(bufs: IpBuilderBufs<'a>) -> Self {
        UdpPacketBuilder {
            buf: bufs.buf,
            pseudo_prefix: bufs.pseudo_prefix,
        }
    }
    pub fn set_src_port(&mut self, val: u16) -> &mut Self { self.buf.set_bits_be(0..16, val as u32); self }
    pub fn set_dest_port(&mut self, val: u16) -> &mut Self { self.buf.set_bits_be(16..32, val as u32); self }
    fn len(&mut self) -> u16 { self.buf.get_bits_be(32..48) as u16 }
    fn set_len(&mut self, val: u16) { self.buf.set_bits_be(32..48, val as u32); }
    fn set_checksum(&mut self, val: u16) { self.buf.set_bits_be(48..64, val as u32); }

    fn packet(&'a self) -> UdpPacket<'a> { UdpPacket(self.buf) }

    #[allow(dead_code)]
    pub fn with_byte_payload<F: Fn(&mut [u8])>(
            &mut self, len: usize, edit_func: F) -> &mut Self {
        let header_size = 8;

        let len = core::cmp::min(len, self.buf.len() * 4 - header_size);
        self.set_len((len + header_size) as u16);
        edit_func(&mut self.buf.as_mut_bytes()[header_size..header_size+len]);
        self
    }
    pub fn set_payload(&mut self, src: &[u8]) -> &mut Self {
        let header_size = 8;
        self.set_len((src.len() + header_size) as u16);
        {
            let dest = &mut self.buf.as_mut_bytes()[header_size..header_size+src.len()];
            dest.clone_from_slice(src);
        }
        self
    }

    pub fn complete(&mut self) -> IpDetails {
        self.set_checksum(0);
        let len = self.packet().len() as usize;
        let mut pseudo2 = [0u32; 2];
        pseudo2.set_bits_be(0..32, self.len() as u32);
        pseudo2.set_bits_be(56..64, ProtocolNumber::Udp as u32);
        let checksum = compute_checksum(
            self.pseudo_prefix, &pseudo2, &self.buf.as_bytes()[0..len]);
        self.set_checksum(checksum);
        IpDetails{proto_num: ProtocolNumber::Udp, byte_len: len }
    }
}

pub struct Icmp6Packet<'a>{
    buf: &'a [u32],
    byte_len: usize,
}
impl<'a> Icmp6Packet<'a> {
    fn new<'b>(buf: &'b [u32], byte_len: usize) -> Option<Icmp6Packet<'b>> {
        if buf.len() < 1 {
            return None;
        }
        Some(Icmp6Packet{buf: buf, byte_len: byte_len})
    }
    pub fn ptype_raw(&self) -> u8 { self.buf.get_bits_be(0..8) as u8 }
    pub fn ptype(&self) -> Icmp6Type { 
        Icmp6Type::from_primitive(self.ptype_raw()).unwrap_or(Icmp6Type::Unknown)
    }
    #[allow(dead_code)]
    pub fn code_raw(&self) -> u8 { self.buf.get_bits_be(8..16) as u8 }
    #[allow(dead_code)]
    pub fn checksum(&self) -> u16 { self.buf.get_bits_be(16..32) as u16 }
    #[allow(dead_code)]
    pub fn payload_raw(&self) -> &'a [u32] { &self.buf[1..] }
    pub fn payload(&'a self) -> Icmp6Payload<'a> {
        match self.ptype() {
            Icmp6Type::EchoRequest => Icmp6Payload::EchoRequest(
                EchoPacket::new(&self.buf[1..], self.byte_len - 4)),
            Icmp6Type::EchoReply => Icmp6Payload::EchoReply(
                EchoPacket::new(&self.buf[1..], self.byte_len - 4)),
            Icmp6Type::NeighborSolicitation => Icmp6Payload::NeighborSolicitation(
                NeighborSolicitationPacket(&self.buf[1..])),
            Icmp6Type::NeighborAdvertisement => Icmp6Payload::NeighborAdvertisement(
                NeighborAdvertisementPacket(&self.buf[1..])),
            Icmp6Type::RouterSolicitation => Icmp6Payload::RouterSolicitation(
                RouterSolicitationPacket(&self.buf[1..])),
            Icmp6Type::RouterAdvertisement => Icmp6Payload::RouterAdvertisement(
                RouterAdvertisementPacket(&self.buf[1..])),
            _ => Icmp6Payload::Unknown(self.buf),
        }
    }
}
pub struct Icmp6PacketBuilder<'a> {
    buf: &'a mut [u32],
    byte_len: usize,
    pseudo_prefix: &'a [u32],
}
impl<'a> Icmp6PacketBuilder<'a> {
    pub fn new(bufs: IpBuilderBufs<'a>) -> Self {
        Icmp6PacketBuilder {
            buf: bufs.buf,
            byte_len: 4,
            pseudo_prefix: bufs.pseudo_prefix,
        }
    }
    pub fn set_ptype_raw(&mut self, val: u8) -> &mut Self { self.buf.set_bits_be(0..8, val as u32); self }
    pub fn set_ptype(&mut self, val: Icmp6Type) -> &mut Self { self.set_ptype_raw(val as u8) }
    #[inline(always)]
    pub fn set_code_raw(&mut self, val: u8) -> &mut Self { self.buf.set_bits_be(8..16, val as u32); self }
    fn set_checksum(&mut self, val: u16) { self.buf.set_bits_be(16..32, val as u32); }

    #[allow(dead_code)]
    fn packet(&'a self) -> Icmp6Packet<'a> { Icmp6Packet{buf: self.buf, byte_len: self.byte_len} }

    pub fn edit_payload<F: FnOnce(Icmp6BuilderBufs) -> Icmp6Details> (
            &mut self, edit_func: F) -> &mut Self {
        {
            let details = edit_func(Icmp6BuilderBufs{buf: &mut self.buf[1..]});
            self.set_ptype(details.ptype);
            self.set_code_raw(details.code);
            self.byte_len = 4 + details.byte_len;
            self.set_checksum(0);
            let mut pseudo2 = [0u32; 2];
            pseudo2.set_bits_be(0..32, self.byte_len as u32);
            pseudo2.set_bits_be(56..64, ProtocolNumber::Icmp6 as u32);
            let checksum = compute_checksum(
                self.pseudo_prefix, &pseudo2, &self.buf.as_bytes()[0..self.byte_len]);
            self.set_checksum(checksum);
            self
        }
    }

    pub fn complete(&mut self) -> IpDetails {
        IpDetails{proto_num: ProtocolNumber::Icmp6, byte_len: self.byte_len }
    }
}
pub struct Icmp6BuilderBufs<'a> {
    buf: &'a mut [u32],
}
pub struct Icmp6Details {
    ptype: Icmp6Type,
    code: u8,
    byte_len: usize,
}


fn compute_checksum(pseudo1: &[u32], pseudo2: &[u32], data: &[u8]) -> u16 {
    fn add_to_checksum(data: &[u8], checksum: &mut u32) {
        let mut i = data.iter();
        loop {
            match i.next() {
                None => break,
                Some(high) => {
                    let low = *i.next().unwrap_or(&0);
                    *checksum = (((*high as u32) << 8) | (low as u32)) + *checksum;
                }
            }
        }
    }
    let mut result = 0u32;
    add_to_checksum(pseudo1.as_bytes(), &mut result);
    add_to_checksum(pseudo2.as_bytes(), &mut result);
    add_to_checksum(data, &mut result);
    (!((result & 0xffff) + (result >> 16))) as u16
}

primitive_enum! {
    #[derive(Debug)]
    pub enum ProtocolNumber as u8 {
        HopOpt = 0,
        Icmp = 1,
        Igmp = 2,
        Ggp = 3,
        Ipv4 = 4,
        St = 5,
        Tcp = 6,
        Cbt = 7,
        Egp = 8,
        Igp = 9,
        BbnRccMon = 10,
        Nvp = 11,
        Pup = 12,
        Argus = 13,
        Emcon = 14,
        Xnet = 15,
        Chaos = 16,
        Udp = 17,
        Mux = 18,
        DcnMeas = 19,
        Hmp = 20,
        Prm = 21,
        XnsIdp = 22,
        Trunk1 = 23,
        Trunk2 = 24,
        Leaf1 = 25,
        Leaf2 = 26,
        Rdp = 27,
        Irtp = 28,
        IsoTp4 = 29,
        NetBlt = 30,
        MfeNsp = 31,
        MeritInp = 32,
        Dccp = 33,
        _3pc = 34,
        Idpr = 35,
        Xtp = 36,
        Ddp = 37,
        IdprCmtp = 38,
        Tppp = 39,
        Il = 40,
        Ipv6 = 41,
        Sdrp = 42,
        Ipv6Route = 43,
        Ipv6Flag = 44,
        Idrp = 45,
        Rsvp = 46,
        Gre = 47,
        Dsr = 48,
        Bna = 49,
        Esp = 50,
        Ah = 51,
        Inlsp = 52,
        Swipe = 53,
        Narp = 54,
        Mobile = 55,
        Tlsp = 56,
        Skip = 57,
        Icmp6 = 58,
        Ipv6NoNxt = 59,
        Ipv6Opts = 60,
        Unknown61 = 61,
        Cftp = 62,
        Unknown63 = 63,
        SatExpak = 64,
        Kryptolan = 65,
        Rvd = 66,
        Ippc = 67,
        Unknown68 = 68,
        SatMon = 69,
        Visa = 70,
        Ipcv = 71,
        Cpnx = 72,
        Cphb = 73,
        Wsn = 74,
        Pvp = 75,
        BrSatMon = 76,
        SunNd = 77,
        WbMon = 78,
        WbExpak = 79,
        IsoIp = 80,
        Vmtp = 81,
        SecureVmtp = 82,
        Vines = 83,
        Iptm = 84,
        NsfnetIgp = 85,
        Dgp = 86,
        Tcf = 87,
        Eigrp = 88,
        Ospfigp = 89,
        SpriteRpc = 90,
        Larp = 91,
        Mtp = 92,
        Ax25 = 93,
        Ipip = 94,
        Micp = 95,
        SccSp = 96,
        Etherip = 97,
        Encap = 98,
        Unknown99 = 99,
        Gmtp = 100,
        Ifmp = 101,
        Pnni = 102,
        Pim = 103,
        Aris = 104,
        Scps = 105,
        Qnx = 106,
        An = 107,
        IpComp = 108,
        Snp = 109,
        CompaqPeer = 110,
        IpxInIp = 111,
        Vrrp = 112,
        Pgm = 113,
        Unknown114 = 114,
        L2tp = 115,
        Ddx = 116,
        Iatp = 117,
        Stp = 118,
        Srp = 119,
        Uti = 120,
        Smp = 121,
        Sm = 122,
        Ptp = 123,
        IsisOverIpv4 = 124,
        Fire = 125,
        Crtp = 126,
        Crudp = 127,
        Sscopmce = 128,
        Iplt = 129,
        Sps = 130,
        Pipe = 131,
        Sctp = 132,
        Fc = 133,
        RsvpE2eIgnore = 134,
        MobilityHeader = 135,
        UdpLite = 136,
        MplsInIp = 137,
        Manet = 138,
        Hip = 139,
        Shim6 = 140,
        Wesp = 141,
        Rohc = 142,
        Unknown143 = 143,
        Unknown144 = 144,
        Unknown145 = 145,
        Unknown146 = 146,
        Unknown147 = 147,
        Unknown148 = 148,
        Unknown149 = 149,
        Unknown150 = 150,
        Unknown151 = 151,
        Unknown152 = 152,
        Unknown153 = 153,
        Unknown154 = 154,
        Unknown155 = 155,
        Unknown156 = 156,
        Unknown157 = 157,
        Unknown158 = 158,
        Unknown159 = 159,
        Unknown160 = 160,
        Unknown161 = 161,
        Unknown162 = 162,
        Unknown163 = 163,
        Unknown164 = 164,
        Unknown165 = 165,
        Unknown166 = 166,
        Unknown167 = 167,
        Unknown168 = 168,
        Unknown169 = 169,
        Unknown170 = 170,
        Unknown171 = 171,
        Unknown172 = 172,
        Unknown173 = 173,
        Unknown174 = 174,
        Unknown175 = 175,
        Unknown176 = 176,
        Unknown177 = 177,
        Unknown178 = 178,
        Unknown179 = 179,
        Unknown180 = 180,
        Unknown181 = 181,
        Unknown182 = 182,
        Unknown183 = 183,
        Unknown184 = 184,
        Unknown185 = 185,
        Unknown186 = 186,
        Unknown187 = 187,
        Unknown188 = 188,
        Unknown189 = 189,
        Unknown190 = 190,
        Unknown191 = 191,
        Unknown192 = 192,
        Unknown193 = 193,
        Unknown194 = 194,
        Unknown195 = 195,
        Unknown196 = 196,
        Unknown197 = 197,
        Unknown198 = 198,
        Unknown199 = 199,
        Unknown200 = 200,
        Unknown201 = 201,
        Unknown202 = 202,
        Unknown203 = 203,
        Unknown204 = 204,
        Unknown205 = 205,
        Unknown206 = 206,
        Unknown207 = 207,
        Unknown208 = 208,
        Unknown209 = 209,
        Unknown210 = 210,
        Unknown211 = 211,
        Unknown212 = 212,
        Unknown213 = 213,
        Unknown214 = 214,
        Unknown215 = 215,
        Unknown216 = 216,
        Unknown217 = 217,
        Unknown218 = 218,
        Unknown219 = 219,
        Unknown220 = 220,
        Unknown221 = 221,
        Unknown222 = 222,
        Unknown223 = 223,
        Unknown224 = 224,
        Unknown225 = 225,
        Unknown226 = 226,
        Unknown227 = 227,
        Unknown228 = 228,
        Unknown229 = 229,
        Unknown230 = 230,
        Unknown231 = 231,
        Unknown232 = 232,
        Unknown233 = 233,
        Unknown234 = 234,
        Unknown235 = 235,
        Unknown236 = 236,
        Unknown237 = 237,
        Unknown238 = 238,
        Unknown239 = 239,
        Unknown240 = 240,
        Unknown241 = 241,
        Unknown242 = 242,
        Unknown243 = 243,
        Unknown244 = 244,
        Unknown245 = 245,
        Unknown246 = 246,
        Unknown247 = 247,
        Unknown248 = 248,
        Unknown249 = 249,
        Unknown250 = 250,
        Unknown251 = 251,
        Unknown252 = 252,
        Unknown253 = 253,
        Unknown254 = 254,
        Unknown255 = 255,
    }
}

#[allow(dead_code)]
enum UnreachableReason {
    NoRouteToDestination = 0,
    Prohibited = 1,
    BeyondScope = 2,
    AddressUnreachable = 3,
    PortUnreachable = 4,
    SourcePolicyFailed = 5,
    DestinationRouteRejected = 6,
    SourceRoutingHeaderError = 7,
}
#[allow(dead_code)]
enum TimeExceededReason {
    HopLimitExceeded = 0,
    FragmentReassmeblyTimeExceeded = 1,
}
#[allow(dead_code)]
enum ParameterProblemReason {
    ErroneousHeaderField = 0,
    UnrecognizedNextHeaderType = 1,
    UnrecognizedIpv6Option = 2
}
#[allow(dead_code)]
enum RouterRenumbering {
    Command = 0,
    Result = 1,
    SequenceNumberReset = 255,
}
#[allow(dead_code)]
enum DataFieldContents {
    HasIpv6 = 0,
    HasName = 1,
    HasIpv4 = 2,
}
#[allow(dead_code)]
enum IcmpNodeInfoResponseCode {
    Success = 0,
    Refused = 1,
    UnknownQType = 2,
}

primitive_enum! {
#[derive(Copy, Clone, Eq, PartialEq)]
pub enum Icmp6Type as u8 {
    Unknown = 0,
    DestinationUnreachable = 1,
    PacketTooBig = 2,
    TimeExceeded = 3,
    ParameterProblem = 4,
    EchoRequest = 128,
    EchoReply = 129,
    MulticastListenerQuery = 130,
    MulticastListenerReport = 131,
    MulticastListenerDone = 132,
    RouterSolicitation = 133,
    RouterAdvertisement = 134,
    NeighborSolicitation = 135,
    NeighborAdvertisement = 136,
    RedirectMessage = 137,
    RouterRenumbering = 138,
    IcmpNodeInformationQuery = 139,
    IcmpNodeInformationResponse = 140,
    InverseNeighborDiscoverySolicitation = 141,
    InverseNeighborDiscoveryAdvertisement = 142,
    MulticastListenerReportV2 = 143,
    HomeAgentAddressDiscoveryRequest = 144,
    HomeAgentAddressDiscoveryReply = 145,
    MobilePrefixSolicitation = 146,
    MobilePrefixAdvertisement = 147,
    CertificationPathSolicitation = 148,
    CertificationPathAdvertisement = 149,
    ExperimentalMobility = 150,
    MulticastRouterAdvertisement = 151,
    MulticastRouterSolicitation = 152,
    MulticastRouterTermination = 153,
    FmIpv6 = 154,
    RplControl = 155,
    IlnpV6LocatorUpdate = 156,
    DuplicateAddressRequest = 157,
    DuplicateAddressConfirmation = 158,
    MplControlMessage = 159,
}
}

pub struct LinkAddress<'a>(&'a [u8]);
impl<'a> LinkAddress<'a> {
    pub fn as_bytes(&self) -> &[u8] {
        self.0
    }
}

pub struct PrefixInfo<'a>(&'a [u32]); 
#[allow(dead_code)]
impl<'a> PrefixInfo<'a> {
    pub fn prefix_len(&self) -> u8 { self.0.get_bits_be(16..24) as u8 }
    pub fn on_link(&self) -> bool { self.0.get_bits_be(25..26) == 1 }
    pub fn auto_address_conf(&self) -> bool { self.0.get_bits_be(26..27) == 1 }
    pub fn valid_lifetime(&self) -> u32 { self.0[1] }
    pub fn preferred_lifetime(&self) -> u32 { self.0[2] }
    pub fn prefix(&self) -> IpAddr { IpAddr([self.0[4], self.0[5], self.0[6], self.0[7]]) }
}


pub enum IcmpOption<'a> {
    Unknown(&'a [u32]),
    SourceLinkAddress(LinkAddress<'a>),
    TargetLinkAddress(LinkAddress<'a>),
    PrefixInfo(PrefixInfo<'a>),
    RedirectedHeader(&'a [u32]),
    Mtu(u32),
}

pub enum IcmpOptionSetter {
    SourceLinkAddress(EthernetAddr),
    TargetLinkAddress(EthernetAddr),

}
impl IcmpOptionSetter {
    #[inline(always)]
    pub fn serialize(&self, buf: &mut [u32]) -> u8 {
        buf.set_bits_be(0..8, self.type_code() as u32);
        // length of option as the number of 8-byte blocks
        let len8: u8;
        match self {
            &IcmpOptionSetter::SourceLinkAddress(addr) | &IcmpOptionSetter::TargetLinkAddress(addr) => {
                len8 = 1;
                buf.as_mut_bytes()[2..8].clone_from_slice(&addr.0);
            }
        }
        buf.set_bits_be(8..16, len8 as u32);
        len8
    }
    pub fn type_code(&self) -> u8 {
        match self {
            &IcmpOptionSetter::SourceLinkAddress(_) => 1,
            &IcmpOptionSetter::TargetLinkAddress(_) => 2,
        }
    }
}

pub struct IcmpOptionsIter<'a> {
    buf: &'a [u32],
}
impl<'a> Iterator for IcmpOptionsIter<'a> {
    type Item = IcmpOption<'a>;

    fn next(&mut self) -> Option<IcmpOption<'a>> {
        if self.buf.len() < 2 {
            return None;
        }
        let opt_type = self.buf.get_bits_be(0..8);
        let len = self.buf.get_bits_be(8..16);
        let result = match opt_type {
            // TODO(kor): Fix potential runtime range errors here...
            1 => Some(IcmpOption::SourceLinkAddress(
                    LinkAddress(&self.buf.as_bytes()[2..(len*8) as usize]))),
            2 => Some(IcmpOption::TargetLinkAddress(
                    LinkAddress(&self.buf.as_bytes()[2..(len*8) as usize]))),
            3 => Some(IcmpOption::PrefixInfo(PrefixInfo(self.buf))),
            4 => Some(IcmpOption::RedirectedHeader(&self.buf[2..])),
            5 => Some(IcmpOption::Mtu(self.buf[1])),
            _ => Some(IcmpOption::Unknown(self.buf)),
        };
        self.buf = &self.buf[(len*2) as usize..];
        result
    }
}

pub struct EchoPacket<'a>{
    buf: &'a [u32],
    byte_len: usize,
}
impl<'a> EchoPacket<'a> {
    fn new(buf: &'a [u32], byte_len: usize) -> EchoPacket<'a> {
        EchoPacket{buf: buf, byte_len: byte_len}
    }
    pub fn identifier(&self) -> u16 { self.buf.get_bits_be(0..16) as u16 }
    pub fn sequence_number(&self) -> u16 { self.buf.get_bits_be(16..32) as u16 }
    pub fn payload(&self) -> &'a [u8] { &self.buf.as_bytes()[4..self.byte_len] }
}

pub struct EchoPacketBuilder<'a> {
    buf: &'a mut [u32],
    byte_len: usize,
    ptype: Icmp6Type,
}
impl<'a> EchoPacketBuilder<'a> {
    pub fn new_reply(bufs: Icmp6BuilderBufs<'a>) -> Self {
        EchoPacketBuilder {
            buf: bufs.buf,
            byte_len: 4,
            ptype: Icmp6Type::EchoReply,
        }
    }
    #[allow(dead_code)]
    pub fn new_request(bufs: Icmp6BuilderBufs<'a>) -> Self {
        EchoPacketBuilder {
            buf: bufs.buf,
            byte_len: 4,
            ptype: Icmp6Type::EchoRequest,
        }
    }

    pub fn set_identifier(&mut self, val: u16) -> &mut Self { self.buf.set_bits_be(0..16, val as u32); self }
    pub fn set_sequence_number(&mut self, val: u16) -> &mut Self { self.buf.set_bits_be(16..32, val as u32); self }
    pub fn set_payload(&mut self, bytes: &[u8]) -> &mut Self {
        self.buf.as_mut_bytes()[4..4 + bytes.len()].clone_from_slice(bytes);
        self.byte_len = bytes.len() + 4;
        self
    }
    pub fn complete(&mut self) -> Icmp6Details {
        Icmp6Details{ptype: self.ptype, code: 0, byte_len: self.byte_len }
    }
}


pub struct NeighborSolicitationPacket<'a>(&'a [u32]);
impl<'a> NeighborSolicitationPacket<'a> {
    pub fn target_addr(&self) -> IpAddr { IpAddr([self.0[1], self.0[2], self.0[3], self.0[4]]) }
    pub fn options_iter(&'a self) -> IcmpOptionsIter<'a> { IcmpOptionsIter{ buf: &self.0[5..] } }
}
pub struct NeighborSolicitationPacketBuilder<'a> {
    buf: &'a mut [u32],
    next_option_word: usize,
}
impl<'a> NeighborSolicitationPacketBuilder<'a> {
    pub fn new(bufs: Icmp6BuilderBufs<'a>) -> Self {
        NeighborSolicitationPacketBuilder {
            buf: bufs.buf,
            next_option_word: 5,
        }
    }
    pub fn set_target_addr(&mut self, addr: IpAddr) -> &mut Self {
       self.buf[1] = addr.0[0];
       self.buf[2] = addr.0[1];
       self.buf[3] = addr.0[2];
       self.buf[4] = addr.0[3];
       self
    }
    pub fn add_option(&mut self, option: IcmpOptionSetter) -> &mut Self {
        let len8 = option.serialize(&mut self.buf[self.next_option_word..]);
        self.next_option_word += (len8 * 2) as usize;
        self
    }
    fn len(&self) -> usize {
        self.next_option_word * 4
    }
    pub fn complete(&mut self) -> Icmp6Details {
        Icmp6Details{ptype: Icmp6Type::NeighborSolicitation, code: 0, byte_len: self.len() }
    }
}

pub struct NeighborAdvertisementPacket<'a>(&'a [u32]);
#[allow(dead_code)]
impl<'a> NeighborAdvertisementPacket<'a> {
    pub fn is_router(&self) -> bool { self.0.get_bits_be(0..1) == 1 }
    pub fn is_solicited(&self) -> bool { self.0.get_bits_be(1..2) == 1 }
    pub fn is_override(&self) -> bool { self.0.get_bits_be(2..3) == 1 }
    pub fn target_addr(&self) -> IpAddr { IpAddr([self.0[1], self.0[2], self.0[3], self.0[4]]) }
    pub fn options_iter(&'a self) -> IcmpOptionsIter<'a> { IcmpOptionsIter{ buf: &self.0[5..] } }
}
pub struct NeighborAdvertisementPacketBuilder<'a> {
    buf: &'a mut [u32],
    next_option_word: usize,
}
impl<'a> NeighborAdvertisementPacketBuilder<'a> {
    pub fn new(bufs: Icmp6BuilderBufs<'a>) -> Self {
        NeighborAdvertisementPacketBuilder{
            buf: bufs.buf,
            next_option_word: 5,
        }
    }
    pub fn set_target_addr(&mut self, addr: IpAddr) -> &mut Self {
       self.buf[1] = addr.0[0];
       self.buf[2] = addr.0[1];
       self.buf[3] = addr.0[2];
       self.buf[4] = addr.0[3];
       self
    }
    #[allow(dead_code)]
    pub fn set_is_router(&mut self, val: bool) -> &mut Self {
        self.buf.set_bits_be(0..1, val as u32);
        self
    }
    pub fn set_is_solicited(&mut self, val: bool) -> &mut Self {
        self.buf.set_bits_be(1..2, val as u32);
        self
    }
    pub fn set_is_override(&mut self, val: bool) -> &mut Self {
        self.buf.set_bits_be(2..3, val as u32);
        self
    }
    pub fn add_option(&mut self, option: IcmpOptionSetter) -> &mut Self {
        let len8 = option.serialize(&mut self.buf[self.next_option_word..]);
        self.next_option_word += (len8 * 2) as usize;
        self
    }
    fn len(&self) -> usize {
        self.next_option_word * 4
    }
    pub fn complete(&mut self) -> Icmp6Details {
        Icmp6Details{ptype: Icmp6Type::NeighborAdvertisement, code: 0, byte_len: self.len() }
    }
}


pub struct RouterSolicitationPacket<'a>(&'a [u32]);
impl<'a> RouterSolicitationPacket<'a> {
    #[allow(dead_code)]
    pub fn options_iter(&'a self) -> IcmpOptionsIter<'a> { IcmpOptionsIter{ buf: &self.0[1..] } }
}

pub struct RouterSolicitationPacketBuilder<'a> {
    buf: &'a mut [u32],
    next_option_word: usize,
}
impl<'a> RouterSolicitationPacketBuilder<'a> {
    pub fn new(bufs: Icmp6BuilderBufs<'a>) -> Self {
        RouterSolicitationPacketBuilder {
            buf: bufs.buf,
            next_option_word: 1,
        }
    }
    pub fn add_option(&mut self, option: IcmpOptionSetter) -> &mut Self {
        let len8 = option.serialize(&mut self.buf[self.next_option_word..]);
        self.next_option_word += (len8 * 2) as usize;
        self
    }
    fn len(&self) -> usize {
        self.next_option_word * 4
    }
    pub fn complete(&mut self) -> Icmp6Details {
        Icmp6Details{ptype: Icmp6Type::RouterSolicitation, code: 0, byte_len: self.len() }
    }
}

pub struct RouterAdvertisementPacket<'a>(&'a [u32]);
#[allow(dead_code)]
impl<'a> RouterAdvertisementPacket<'a> {
    pub fn cur_hop_limit(&'a self) -> u8 { self.0.get_bits_be(0..8) as u8 }
    pub fn dhcp_has_address(&'a self) -> bool { self.0.get_bits_be(8..9) == 1 }
    pub fn dhcp_has_other_config(&'a self) -> bool { self.0.get_bits_be(9..10) == 1 }
    pub fn router_lifetime(&'a self) -> u16 { self.0.get_bits_be(16..32) as u16 }
    pub fn reachable_time(&'a self) -> u32 { self.0[1] }
    pub fn retrans_time(&'a self) -> u32 { self.0[2] }
    pub fn options_iter(&'a self) -> IcmpOptionsIter<'a> { IcmpOptionsIter{ buf: &self.0[3..] } }
}



pub enum Icmp6Payload<'a> {
    Unknown(&'a [u32]),
    EchoRequest(EchoPacket<'a>),
    EchoReply(EchoPacket<'a>),
    NeighborSolicitation(NeighborSolicitationPacket<'a>),
    NeighborAdvertisement(NeighborAdvertisementPacket<'a>),
    RouterSolicitation(RouterSolicitationPacket<'a>),
    RouterAdvertisement(RouterAdvertisementPacket<'a>),
}

