// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#![no_std]

#![feature(const_fn)]

extern crate arena;
extern crate scheduler;
extern crate wifi;

#[macro_use]
extern crate utils;

#[macro_use]
extern crate stm32f2;

pub mod state;
pub mod protocols;

pub enum ErrorCode {
    NotYet,
}

pub trait NetworkInterface {
    // MAC/PHY layer
    fn mac_addr(&self) -> protocols::EthernetAddr;
    // It's wifi, so we have to manage joins
    fn join(&self, config: ::wifi::WifiNetworkConfig);
    fn join_failure(&self) -> bool;
    // The card filters multicast frames
    fn set_multicast_addresses(&self, addrs: &[protocols::EthernetAddr]) -> Result<(), &'static str>;
    // Layer 3/4: IPv6/UDP -- are we connected to the server
    fn is_link_up(&self) -> bool;

    // These two fns are the interface post-setup: packet transmission/reception
    fn send<F: FnOnce(&mut protocols::EthernetPacketBuilder) -> Result<(), ErrorCode>>(&self, f: F) -> Result<(), ErrorCode>;
    fn recv<'a>(&self, buf: &'a mut [u32]) -> &'a [u8];
}
