// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

extern crate glob;
extern crate protobuf;
extern crate mustache;
extern crate rustc_serialize;

use protobuf::{CodedInputStream,Message};
use protobuf::descriptor::FileDescriptorSet;

use std::env;
use std::fs::File;
use std::io::Write;
use std::path::Path;
use std::path::PathBuf;
use std::process::{Command,Stdio};
use std::string::String;

fn build_enum_name(name: &str) -> String {
    let mut saw_under = false;
    let mut first = true;
    let mut output = String::new();
    for ch in name.chars() {
        match (first, saw_under, ch) {
            (true, _, 'a'..='z') => {
                first = false;
                for new_ch in ch.to_uppercase() {
                    output.push(new_ch);
                }
            },
            (true, _, _) => { first = false; output.push(ch); },
            (false, false, '_') => { saw_under = true; },
            (false, false, _) => { output.push(ch); },
            (false, true, 'a'..='z') => {
                saw_under = false;
                for new_ch in ch.to_uppercase() {
                    output.push(new_ch);
                }
            },
            (false, true, _) => { saw_under = false; output.push(ch) },
        };
    }
    output
}

fn build_msgref_name(name: &str) -> String {
    let parts: Vec<&str> = name.split('.').collect();
    parts[2].to_owned()
}

#[derive(RustcEncodable)]
struct FileDescriptorSetWrapper<'a> {
    //proto: &'a protobuf::descriptor::FileDescriptorSet,
    file: Vec<FileDescriptorProtoWrapper<'a>>,
}
impl<'a> FileDescriptorSetWrapper<'a> {
    pub fn new(proto: &'a protobuf::descriptor::FileDescriptorSet) -> Self {
        let mut file = Vec::new();
        for proto in proto.get_file().iter() {
            file.push(FileDescriptorProtoWrapper::new(proto));
        }
        Self{
            //proto: proto,
            file: file,
        }
    }
    //pub fn get_file(&self) -> &Vec<FileDescriptorProtoWrapper> { &self.file }
}

#[derive(RustcEncodable)]
struct FileDescriptorProtoWrapper<'a> {
    //proto: &'a protobuf::descriptor::FileDescriptorProto,
    message_type: Vec<DescriptorProtoWrapper<'a>>,
}
impl<'a> FileDescriptorProtoWrapper<'a> {
    pub fn new(proto: &'a protobuf::descriptor::FileDescriptorProto) -> Self {
        let mut message_type = Vec::new();
        for proto in proto.get_message_type() {
            message_type.push(DescriptorProtoWrapper::new(proto));
        }
        Self{
            //proto: proto,
            message_type: message_type,
        }
    }
    //pub fn get_name(&self) -> &str { self.proto.get_name() }
    //pub fn get_package(&self) -> &str { self.proto.get_package() }
    //pub fn get_dependency(&self) -> &[String] { self.proto.get_dependency() }
    //pub fn get_public_dependency(&self) -> &[i32] { self.proto.get_public_dependency() }
    //pub fn get_weak_dependency(&self) -> &[i32] { self.proto.get_weak_dependency() }
    //pub fn get_message_type(&self) -> &Vec<DescriptorProtoWrapper> { &self.message_type }
    // enum_type (repeated field) unneeded
    // service (repeated field) unneeded
    // extension (repeated field) unneeded
    // options (singular field) unneeded
    // source_code_info (singular field) unneeded
    //pub fn get_syntax(&self) -> &str { self.proto.get_syntax() }
}

#[derive(RustcEncodable)]
struct DescriptorProtoWrapper<'a> {
    //proto: &'a protobuf::descriptor::DescriptorProto,
    field: Vec<FieldDescriptorProtoWrapper<'a>>,
    msg_name: &'a str,
}
impl<'a> DescriptorProtoWrapper<'a> {
    pub fn new(proto: &'a protobuf::descriptor::DescriptorProto) -> Self {
        let mut field = Vec::new();
        for proto in proto.get_field().iter() {
            field.push(FieldDescriptorProtoWrapper::new(proto));
        }
        Self{
            //proto: proto,
            field: field,
            msg_name: proto.get_name(),
        }
    }
    //pub fn get_name(&self) -> &str { self.msg_name }
    //pub fn get_field(&self) -> &Vec<FieldDescriptorProtoWrapper> { &self.field }
    // extension (repeated field) unneeded
    // nested_type (repeated field) unneeded
    // enum_type (repeated field) unneeded
    // extension_range (repeated field) unneeded
    // oneof_decl (repeated field) unneeded
    // options (singular field) unneeded
    // reserved_range (repeated field) unneeded
    // reserved_name (repeated field) unneeded
}

#[derive(RustcEncodable)]
struct FieldDescriptorProtoWrapper<'a> {
    //proto: &'a protobuf::descriptor::FieldDescriptorProto,
    //proto_field_type: protobuf::descriptor::FieldDescriptorProto_Type,
    field_name: &'a str,
    field_type: String,
    msgref_name: String,
    wire_type: &'static str,
    encoding: &'static str,
    is_message: bool,
    deserialize_fn: &'static str,
    deserialize_fn_out: &'static str,
    value_fn: String,
    value_fn_out: &'static str,
    serialize_fn: &'static str,
    serialize_suffix: &'static str,
    enum_name: String,
    const_name: String,
    number: i32,
}
impl<'a> FieldDescriptorProtoWrapper<'a> {
    pub fn new(proto: &'a protobuf::descriptor::FieldDescriptorProto) -> Self {
        use protobuf::descriptor::FieldDescriptorProto_Type as T;
        Self{
            //proto: proto,
            //proto_field_type: proto.get_field_type(),
            field_name: proto.get_name(),
            enum_name: build_enum_name(proto.get_name()),
            field_type: match proto.get_field_type() {
                T::TYPE_DOUBLE => "f64".to_owned(),
                T::TYPE_FLOAT => "f32".to_owned(),
                T::TYPE_INT64 => "i64".to_owned(),
                T::TYPE_UINT64 => "u64".to_owned(),
                T::TYPE_INT32 => "i32".to_owned(),
                T::TYPE_FIXED64 => "u64".to_owned(),
                T::TYPE_FIXED32 => "u32".to_owned(),
                T::TYPE_BOOL => "bool".to_owned(),
                T::TYPE_STRING => "&'a str".to_owned(),
                T::TYPE_GROUP => "".to_owned(),   // intentionally broken syntax; groups
                T::TYPE_MESSAGE => build_msgref_name(proto.get_type_name()) + "Reader<'a>",
                T::TYPE_BYTES => "&'a [u8]".to_owned(),
                T::TYPE_UINT32 => "u32".to_owned(),
                T::TYPE_ENUM => "".to_owned(),  // intentionally broken syntax; enums
                T::TYPE_SFIXED32 => "i32".to_owned(),
                T::TYPE_SFIXED64 => "i64".to_owned(),
                T::TYPE_SINT32 => "i32".to_owned(),
                T::TYPE_SINT64 => "i64".to_owned(),
            },
            msgref_name: match proto.get_field_type() {
                T::TYPE_MESSAGE => build_msgref_name(proto.get_type_name()),
                _ => "".to_owned(),
            },
            wire_type: match proto.get_field_type() {
                T::TYPE_DOUBLE => "WireType::Fixed64",
                T::TYPE_FLOAT => "WireType::Fixed32",
                T::TYPE_INT64 => "WireType::VarInt",
                T::TYPE_UINT64 => "WireType::VarInt",
                T::TYPE_INT32 => "WireType::VarInt",
                T::TYPE_FIXED64 => "WireType::Fixed64",
                T::TYPE_FIXED32 => "WireType::Fixed32",
                T::TYPE_BOOL => "WireType::VarInt",
                T::TYPE_STRING => "WireType::LengthDelimited",
                T::TYPE_GROUP => "",  // intentionally broken syntax; groups
                T::TYPE_MESSAGE => "WireType::LengthDelimited",
                T::TYPE_BYTES => "WireType::LengthDelimited",
                T::TYPE_UINT32 => "WireType::VarInt",
                T::TYPE_ENUM => "",  // intentionally broken syntax; enums
                T::TYPE_SFIXED32 => "WireType::Fixed32",
                T::TYPE_SFIXED64 => "WireType::Fixed64",
                T::TYPE_SINT32 => "WireType::VarInt",
                T::TYPE_SINT64 => "WireType::VarInt",
            },
            encoding: match proto.get_field_type() {
                T::TYPE_DOUBLE => "Encoding::Regular",
                T::TYPE_FLOAT => "Encoding::Regular",
                T::TYPE_INT64 => "Encoding::Regular",
                T::TYPE_UINT64 => "Encoding::Regular",
                T::TYPE_INT32 => "Encoding::Regular",
                T::TYPE_FIXED64 => "Encoding::Regular",
                T::TYPE_FIXED32 => "Encoding::Regular",
                T::TYPE_BOOL => "Encoding::Regular",
                T::TYPE_STRING => "Encoding::Regular",
                T::TYPE_GROUP => "",   // intentionally broken syntax; groups
                T::TYPE_MESSAGE => "Encoding::Regular",
                T::TYPE_BYTES => "Encoding::Regular",
                T::TYPE_UINT32 => "Encoding::Regular",
                T::TYPE_ENUM => "",  // intentionally broken syntax; enums
                T::TYPE_SFIXED32 => "Encoding::Regular",
                T::TYPE_SFIXED64 => "Encoding::Regular",
                T::TYPE_SINT32 => "Encoding::Zigzag",
                T::TYPE_SINT64 => "Encoding::Zigzag",
            },
            is_message: match proto.get_field_type() {
                T::TYPE_MESSAGE => true,
                _ => false,
            },
            deserialize_fn: match proto.get_field_type() {
                T::TYPE_DOUBLE | T::TYPE_FLOAT => {
                    "FloatLike::decode(&mut self.iter)"
                },
                T::TYPE_INT64 | T::TYPE_INT32 | T::TYPE_UINT64 | T::TYPE_UINT32 => {
                    "self.iter.decode_varint()"
                },
                T::TYPE_BOOL => {
                    "self.iter.decode_varint().map(|x: i32| { x > 0 })"
                },
                T::TYPE_FIXED64 | T::TYPE_SFIXED64 | T::TYPE_FIXED32 | T::TYPE_SFIXED32 => {
                    "self.iter.decode_fixed()"
                },
                T::TYPE_STRING => {
                    "data_len.and_then(|len| {
                            self.iter.next_n(len).and_then(|slice| {
                                core::str::from_utf8(slice).ok()"
                },
                T::TYPE_GROUP => "",   // intentionally broken syntax; groups
                T::TYPE_MESSAGE  => {
                    "data_len.and_then(|len| {
                            self.iter.next_n(len)"
                },
                T::TYPE_BYTES => {
                    "data_len.and_then(|len| {
                            self.iter.next_n(len)"
                },
                T::TYPE_ENUM => "",  // intentionally broken syntax; enums
                T::TYPE_SINT32 | T::TYPE_SINT64 => {
                    "self.iter.decode_zigzag_varint()"
                },
            },
            deserialize_fn_out: match proto.get_field_type() {
                T::TYPE_BYTES => "})",
                T::TYPE_MESSAGE => "})",
                T::TYPE_STRING => "}) })",
                _ => "",
            },
            value_fn: match proto.get_field_type() {
                T::TYPE_MESSAGE => build_msgref_name(proto.get_type_name()) + "Reader::new(",
                _ => "".to_owned(),
            },
            value_fn_out: match proto.get_field_type() {
                T::TYPE_MESSAGE => ")",
                _ => "",
            },
            serialize_fn: match proto.get_field_type() {
                T::TYPE_DOUBLE | T::TYPE_FLOAT => {
                    "write_fixed_int"
                },
                T::TYPE_INT64 | T::TYPE_INT32 | T::TYPE_UINT64 | T::TYPE_UINT32 | T::TYPE_BOOL => {
                    "write_varint"
                },
                T::TYPE_FIXED64 | T::TYPE_SFIXED64 | T::TYPE_FIXED32 | T::TYPE_SFIXED32 => {
                    "write_fixed_int"
                },
                T::TYPE_STRING => {
                    "write_slice"
                },
                T::TYPE_GROUP => "",   // intentionally broken syntax; groups
                T::TYPE_MESSAGE  => "CANTHAPPEN",  // intentionally broken syntax
                T::TYPE_BYTES => {
                    "write_slice"
                },
                T::TYPE_ENUM => "",  // intentionally broken syntax; enums
                T::TYPE_SINT32 | T::TYPE_SINT64 => {
                    "write_zigzag_varint"
                },
            },
            serialize_suffix: match proto.get_field_type() {
                T::TYPE_STRING => ".as_bytes()",
                T::TYPE_DOUBLE | T::TYPE_FLOAT => ".encode()",
                _ => "",
            },
            const_name: proto.get_name().to_owned().to_uppercase(),
            number: proto.get_number(),
        }
    }

    //pub fn get_name(&self) -> &str { self.proto.get_name() }
    //pub fn get_number(&self) -> i32 { self.number }
    // label (singular field -- optional/required/repeated) unneeded
    //pub fn get_field_type(&self) -> protobuf::descriptor::FieldDescriptorProto_Type { self.proto_field_type }
    //pub fn get_type_name(&self) -> &str { self.type_name }
    // extendee (singular field) unneeded
    // default_value (singular field) unneeded
    // oneof_index (singular field) unneeded
    // json_name (singular field) unneeded
    // options (singular field) unneeded
}

fn build_proto(src: &Path, dst: &str) {
    let child = Command::new("protoc")
        .arg("--descriptor_set_out=/dev/stdout")
        .arg(src.as_os_str())
        .stdout(Stdio::piped())
        .spawn()
        .expect(format!("spawning protoc --descriptor_set_out=/dev/stdout {}", src.as_os_str().to_string_lossy()).as_str());
    let mut reader = child.stdout.unwrap();

    let mut fds = FileDescriptorSet::new();
    fds.merge_from(&mut CodedInputStream::new(&mut reader)).expect("merge_from");
    let fds = FileDescriptorSetWrapper::new(&fds);

    let out_dir = env::var("OUT_DIR").unwrap();
    let dest_path = Path::new(&out_dir).join(dst);
    let mut f = File::create(&dest_path).unwrap();

    let tpl = mustache::compile_path(Path::new("protobuf.mustache.rs")).unwrap();
    tpl.render(&mut f, &fds).unwrap();

    f.flush().unwrap();
}

fn main() {
    use glob::glob;
    let mut srcs = vec!["protobuf.mustache.rs".to_owned(), "layout.ld".to_owned()];

    for f in glob("src/*.proto").expect("bad glob pattern") {
        match f {
            Ok(path) => {
                let out = path.file_name().unwrap().to_str().unwrap().replace(".proto", ".rs");
                build_proto(&path, out.as_str());
                srcs.push(path.to_str().unwrap().to_owned());
            },
            Err(e) => { panic!("glob: {:?}", e); },
        }
    }

    // now copy layout.ld
    let mut out = PathBuf::from(env::var_os("OUT_DIR").unwrap());
    // "out" is:
    //   target/thumb*/release/build/project-HASH/out
    // the only directories in -L flags are:
    //   target/thumb*/release/deps
    //   target/release/deps
    //   toolchain_dir (unusable)
    out.pop();
    out.pop();
    out.pop();
    out.push("deps");
    let layout_ld = include_bytes!("layout.ld");
    let mut f = File::create(out.join("layout.ld")).unwrap();
    f.write_all(layout_ld).unwrap();

    for src in &srcs {
        println!("cargo:rerun-if-changed={}", src);
    }
}
