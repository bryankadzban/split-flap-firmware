{{! Process this file with mustache to generate a protobuf wrapper }}
// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use core;

trait FloatLike : Sized {
    type Integer;

    fn decode(iter: &mut SliceIter) -> Option<Self>;
    fn encode(self) -> Self::Integer;
}
impl FloatLike for f32 {
    type Integer = u32;

    fn decode(iter: &mut SliceIter) -> Option<Self> {
        iter.decode_fixed::<u32>().map(|value| { unsafe { core::mem::transmute(value) } })
    }
    fn encode(self) -> Self::Integer {
        unsafe { core::mem::transmute(self) }
    }
}
impl FloatLike for f64 {
    type Integer = u64;

    fn decode(iter: &mut SliceIter) -> Option<Self> {
        iter.decode_fixed::<u64>().map(|value| { unsafe { core::mem::transmute(value) } })
    }
    fn encode(self) -> Self::Integer {
        unsafe { core::mem::transmute(self) }
    }
}

pub trait LeadingZeros {
    fn leading_zeros(self) -> usize;
}
impl LeadingZeros for i32 {
    fn leading_zeros(self) -> usize { self.leading_zeros() as usize }
}
impl LeadingZeros for i64 {
    fn leading_zeros(self) -> usize { self.leading_zeros() as usize }
}
impl LeadingZeros for u32 {
    fn leading_zeros(self) -> usize { self.leading_zeros() as usize }
}
impl LeadingZeros for u64 {
    fn leading_zeros(self) -> usize { self.leading_zeros() as usize }
}
impl LeadingZeros for usize {
    fn leading_zeros(self) -> usize { self.leading_zeros() as usize }
}

fn varint_len<T: Sized + LeadingZeros>(value: T) -> usize {
    let sz = core::mem::size_of::<T>() * 8;
    let lz = value.leading_zeros();
    if sz == lz {
        1
    } else {
        (sz - lz + 6) / 7
    }
}

#[derive(Copy, Clone, Debug)]
struct SliceIter<'a> {
    ptr: *const u8,
    end: *const u8,     // pointer to one past the last item
    _marker: core::marker::PhantomData<&'a [u8]>,
}
impl<'a> SliceIter<'a> {
    pub fn new(slice: &'a [u8]) -> Self {
        Self{
            ptr: slice.as_ptr(),
            end: unsafe { slice.as_ptr().offset(slice.len() as isize) },
            _marker: core::marker::PhantomData,
        }
    }
    pub fn next_n(&mut self, n: usize) -> Option<&'a [u8]> {
        let new_ptr = unsafe { self.ptr.offset(n as isize) };
        if new_ptr > self.end {
            None
        } else {
            let ret = unsafe { core::slice::from_raw_parts(self.ptr, n) };
            self.ptr = new_ptr;
            Some(ret)
        }
    }
    pub fn decode_fixed<T>(&mut self) -> Option<T>
        where T:
            Sized + Copy +
            core::ops::Shl<usize, Output=T> +
            core::ops::BitOrAssign +
            core::convert::From<u8> +
            core::default::Default {

        let size = core::mem::size_of::<T>();
        self.next_n(size).map(|slice| {
            let mut ret: T = T::default();
            for i in 0..slice.len() {
                ret |= T::from(slice[i]) << (i*8);
            }
            ret
        })
    }
    pub fn decode_varint<T>(&mut self) -> Option<T>
        where T:
            Sized + Copy +
            core::ops::Shl<usize, Output=T> +
            core::ops::BitOrAssign +
            core::convert::From<u8> +
            core::default::Default {

        let mut ret: T = T::default();
        for (i, b) in self.enumerate() {
            ret |= T::from(b & 0x7f) << (i*7);
            if b & 0x80 == 0 {
                return Some(ret);
            }
        }
        None
    }
    #[allow(dead_code)]
    pub fn decode_zigzag_varint<T>(&mut self) -> Option<T>
        where T:
            Sized + Copy +
            core::ops::Shl<usize, Output=T> +
            core::ops::BitOrAssign +
            core::ops::Shr<u8, Output=T> +
            core::ops::BitAnd<T, Output=T> +
            core::ops::Not<Output=T> +
            core::cmp::PartialEq<T> +
            core::convert::From<u8> +
            core::default::Default {

        self.decode_varint::<T>().map(|value| {
            let bits: T = value >> 1;
            if value & T::from(0x1) == T::from(0) {
                bits
            } else {
                !bits
            }
        })
    }
    fn varint_bytes(&mut self) -> Option<&'a [u8]> {
        let mut ptr = self.ptr;
        let mut len = 0usize;
        while ptr < self.end && ((unsafe { *ptr }) & 0x80) != 0 {
            ptr = unsafe { ptr.offset(1) };
            len += 1;
        }
        ptr = unsafe { ptr.offset(1) };
        len += 1;
        if ptr > self.end {
            None
        } else {
            let res = unsafe { core::slice::from_raw_parts(self.ptr, len) };
            self.ptr = ptr;
            Some(res)
        }
    }
}
impl<'a> core::iter::Iterator for SliceIter<'a> {
    type Item = &'a u8;

    fn next(&mut self) -> Option<Self::Item> {
        if self.ptr >= self.end {
            None
        } else {
            let ret = unsafe { &*self.ptr };
            self.ptr = unsafe { self.ptr.offset(1) };
            Some(ret)
        }
    }
}

#[derive(Debug)]
enum WireType {
    VarInt = 0,
    Fixed64 = 1,
    LengthDelimited = 2,
    Fixed32 = 5,
}

#[allow(dead_code)]
#[derive(Debug)]
enum Encoding {
    Regular,
    Zigzag,
}

pub trait Truncate<T> {
    fn truncate(self) -> T;
}
impl Truncate<u8> for i32 {
    fn truncate(self) -> u8 { (self & 0xffi32) as u8 }
}
impl Truncate<u8> for i64 {
    fn truncate(self) -> u8 { (self & 0xffi64) as u8 }
}
impl Truncate<u8> for u32 {
    fn truncate(self) -> u8 { (self & 0xffu32) as u8 }
}
impl Truncate<u8> for u64 {
    fn truncate(self) -> u8 { (self & 0xffu64) as u8 }
}
impl Truncate<u8> for usize {
    fn truncate(self) -> u8 { (self & 0xffusize) as u8 }
}

pub trait WriteVarint<T> {
    fn write_to_buf(&self, buf: &mut SliceIterMut<T>);
}
impl<T, U> WriteVarint<T> for U
    where T:
        Copy +
        core::ops::BitAnd<T, Output=T> +
        core::ops::BitOrAssign<T> +
        core::convert::From<u8>,
    usize: Truncate<T>,
    U:
        Sized + Copy +
        core::ops::Shr<usize, Output=U> +
        core::cmp::PartialEq<U> +
        core::default::Default +
        LeadingZeros +
        Truncate<T> {

    fn write_to_buf(&self, buf: &mut SliceIterMut<T>) {
        let buf = buf.next_n(varint_len(*self));
        let last = buf.len() - 1;
        for (i, b) in buf.iter_mut().enumerate() {
            let mut bits = (*self >> (i*7)).truncate() & 0x7fu8.into();
            if i < last { bits |= 0x80u8.into(); }
            *b = bits;
        }
    }
}
impl<T> WriteVarint<T> for bool
    where T:
        Copy +
        core::ops::BitAnd<T, Output=T> +
        core::ops::BitOrAssign<T> +
        core::convert::From<u8>,
    usize: Truncate<T> {

    fn write_to_buf(&self, buf: &mut SliceIterMut<T>) {
        buf.next_n(1)[0] = if *self { T::from(1) } else { T::from(0) };
    }
}

#[derive(Debug)]
pub struct SliceIterMut<'a, T: 'a> {
    ptr: *mut T,
    end: *mut T,
    _marker: core::marker::PhantomData<&'a mut [T]>,
}
impl<'a, T> SliceIterMut<'a, T> {
    pub fn new(buf: &'a mut [T]) -> Self {
        Self{
            ptr: buf.as_mut_ptr(),
            end: unsafe { buf.as_mut_ptr().offset(buf.len() as isize) },
            _marker: core::marker::PhantomData,
        }
    }
}
impl<'a, T> SliceIterMut<'a, T>
    where T:
        Copy +
        core::ops::BitAnd<T, Output=T> +
        core::ops::BitOrAssign<T> +
        core::convert::From<u8>,
    usize: Truncate<T> {

    pub fn get_ptr(&self) -> usize {
        self.ptr as usize
    }

    #[allow(dead_code)]
    pub fn clone(&mut self) -> Self {
        Self{
            ptr: self.ptr,
            end: self.end,
            _marker: self._marker,
        }
    }

    pub fn next_n(&mut self, n: usize) -> &'a mut [T] {
        let new_ptr = unsafe { self.ptr.offset(n as isize) };
        if new_ptr > self.end {
            panic!("tried to go past end of buffer");
        } else {
            let ret = unsafe { core::slice::from_raw_parts_mut(self.ptr, n) };
            self.ptr = new_ptr;
            ret
        }
    }
    #[allow(dead_code)]
    pub fn write_fixed_int<U>(&mut self, value: U)
        where U:
            Sized + Copy +
            core::ops::Shr<usize, Output=U> +
            Truncate<T> {

        let size = core::mem::size_of::<U>();
        let slice = self.next_n(size);
        for i in 0..slice.len() {
            let bits: U = value >> (i*8);
            slice[i] = bits.truncate();
        }
    }
    pub fn write_varint<U: WriteVarint<T>>(&mut self, value: U) {
        value.write_to_buf(self);
    }
    #[allow(dead_code)]
    pub fn write_zigzag_varint<U>(&mut self, value: U)
        where U:
            Sized + Copy +
            core::ops::Shr<usize, Output=U> +   // U is signed here, so this is an arithmetic shift
            core::cmp::PartialEq<U> +
            core::default::Default +
            core::ops::Shl<usize, Output=U> +
            core::ops::BitXor<U, Output=U> +
            LeadingZeros +
            Truncate<T> {

        let new_value = (value << 1) ^ (value >> (core::mem::size_of::<U>() * 8 - 1));
        self.write_varint(new_value);
    }
    #[allow(dead_code)]
    pub fn write_slice(&mut self, value: &[T]) {
        self.write_varint(value.len());
        self.next_n(value.len()).copy_from_slice(value);
    }

    // Add to_copy as a varint prefix in front of the to_copy bytes after the iterator's current
    // head position.  There are already allocated bytes set up to hold the length.
    #[allow(dead_code)]
    pub fn insert_len_and_copy(&mut self, allocated: usize, to_copy: usize) {
        let prefix_len = varint_len(to_copy);
        if prefix_len != allocated {
            if prefix_len > allocated && unsafe { self.ptr.offset((prefix_len + to_copy) as isize) } >= self.end {
                panic!("Tried to insert_len_and_copy {}/{} bytes at {}, off end of buffer at {}", prefix_len, to_copy, self.ptr as usize, self.end as usize);
            }
            unsafe {
                core::ptr::copy(self.ptr.offset(allocated as isize), self.ptr.offset(prefix_len as isize), to_copy);
            }
        }
        self.write_varint(to_copy);
        self.ptr = unsafe { self.ptr.offset(to_copy as isize) };
    }
}
impl<'a, T> core::iter::Iterator for SliceIterMut<'a, T> {
    type Item = &'a mut T;

    fn next(&mut self) -> Option<Self::Item> {
        if self.ptr >= self.end {
            None
        } else {
            let ret = unsafe { &mut *self.ptr };
            self.ptr = unsafe { self.ptr.offset(1) };
            Some(ret)
        }
    }
}

pub trait Writer<'a> {
    fn new_from_iter(iter: SliceIterMut<'a, u8>) -> Self;
    fn get_ptr(&self) -> usize;
}

{{#file}}

{{#message_type}}
#[allow(dead_code)]
#[derive(Debug)]
pub enum {{& msg_name }}Field<'a> {
    {{#field}}
    {{& enum_name }}{value: {{& field_type }}},
    {{/field}}
    Unknown{tag: u32, wire_type: u8, buf: &'a [u8]},
}

#[allow(dead_code)]
#[derive(Clone,Debug)]
pub struct {{& msg_name }}Reader<'a> {
    buf: &'a [u8],
    iter: SliceIter<'a>,
}

#[allow(dead_code)]
impl<'a> {{& msg_name }}Reader<'a> {
    pub fn new(buf: &'a [u8]) -> Self {
        Self {
            buf: buf,
            iter: SliceIter::new(buf),
        }
    }

    {{#field}}
    const {{& const_name }}_TAG: u32 = {{& number }};
    const {{& const_name }}_TYPE: WireType = {{& wire_type }};
    const {{& const_name }}_ENCODING: Encoding = {{& encoding }};
    {{/field}}
}

impl<'a> core::iter::Iterator for {{& msg_name }}Reader<'a> {
    type Item = {{& msg_name }}Field<'a>;

    #[allow(dead_code)]
    fn next(&mut self) -> Option<{{& msg_name }}Field<'a>> {
        match self.iter.decode_varint::<u32>() {
            None => None,
            Some(field) => {
                // Last 3 bits of the varint have the wire type in them.
                let wire_type = (field & 0x7) as u8;
                let field_num = field >> 3;
                let data_len: Option<usize>;
                if wire_type == WireType::LengthDelimited as u8 {
                    data_len = self.iter.decode_varint();
                } else {
                    data_len = None;
                }
                match field_num {
                    {{#field}}
                    Self::{{& const_name }}_TAG => {
                        {{& deserialize_fn }}.map(|val| {
                            {{& msg_name }}Field::{{& enum_name }}{
                                value: {{& value_fn }}val{{& value_fn_out }}
                            }
                        }){{& deserialize_fn_out }}
                    },
                    {{/field}}
                    _ => {
                        match wire_type {
                            x if x == WireType::VarInt as u8 => {
                                self.iter.varint_bytes().map(|slice| {
                                    {{& msg_name }}Field::Unknown{
                                        tag: field_num,
                                        wire_type: wire_type,
                                        buf: slice
                                    }
                                })
                            },
                            x if x == WireType::LengthDelimited as u8 => {
                                data_len.and_then(|len| {
                                    self.iter.next_n(len).map(|slice| {
                                        {{& msg_name }}Field::Unknown{
                                            tag: field_num,
                                            wire_type: wire_type,
                                            buf: &slice
                                        }
                                    })
                                })
                            },
                            x if x == WireType::Fixed64 as u8 => {
                                self.iter.next_n(8).map(|slice| {
                                    {{& msg_name }}Field::Unknown{
                                        tag: field_num,
                                        wire_type: wire_type,
                                        buf: &slice
                                    }
                                })
                            },
                            x if x == WireType::Fixed32 as u8 => {
                                self.iter.next_n(4).map(|slice| {
                                    {{& msg_name }}Field::Unknown{
                                        tag: field_num,
                                        wire_type: wire_type,
                                        buf: &slice
                                    }
                                })
                            },
                            _ => { None },
                        }
                    },
                }
            },
        }
    }
}

#[allow(dead_code)]
#[derive(Debug)]
pub struct {{& msg_name }}Writer<'a> {
    iter: SliceIterMut<'a, u8>,
}

impl<'a> Writer<'a> for {{& msg_name }}Writer<'a> {
    fn new_from_iter(iter: SliceIterMut<'a, u8>) -> Self {
        Self{iter: iter}
    }
    fn get_ptr(&self) -> usize {
        self.iter.get_ptr()
    }
}

#[allow(dead_code)]
impl<'a> {{& msg_name }}Writer<'a> {
    pub fn new(buf: &'a mut [u8]) -> Self {
        Self{iter: SliceIterMut::new(buf)}
    }
}

#[allow(dead_code)]
impl<'a> {{& msg_name }}Writer<'a> {
    pub fn write_field(&mut self, field: {{& msg_name }}Field) -> &mut Self {
        match field {
            {{#field}}
            {{& msg_name }}Field::{{& enum_name }}{
                {{#is_message}} value: _ {{/is_message}} {{^is_message}} value {{/is_message}}
            } => {
                {{#is_message}} unreachable!() {{/is_message}}
                {{^is_message}}
                self.iter.write_varint(({{& msg_name }}Reader::{{& const_name }}_TAG << 3) | {{& msg_name }}Reader::{{& const_name }}_TYPE as u32);
                self.iter.{{& serialize_fn }}(value{{& serialize_suffix }});
                {{/is_message}}
            },
            {{/field}}
            {{& msg_name }}Field::Unknown{tag, wire_type, buf} => {
                let tag_type: u32 = (tag << 3) | (wire_type as u32);
                self.iter.write_varint(tag_type);
                match wire_type {
                    x if x == WireType::LengthDelimited as u8 => {
                        self.iter.write_slice(buf);
                    },
                    _ => {
                        self.iter.next_n(buf.len()).copy_from_slice(buf);
                    },
                }
            },
        }
        self
    }
    {{#field}}
    {{#is_message}}
    // len_hint here is the size of *the varint*, not the length of the message
    pub fn write_{{& field_name }}<F>(&mut self, len_hint: usize, f: F) -> &mut Self
        where F: FnOnce(&mut {{& msgref_name }}Writer) {

        let mut tag_type = {{& msg_name }}Reader::{{& const_name }}_TAG << 3;
        tag_type |= {{& msg_name }}Reader::{{& const_name }}_TYPE as u32;
        self.iter.write_varint(tag_type);

        let len = {
            let mut new_iter = self.iter.clone();
            new_iter.next_n(len_hint);

            let start = new_iter.get_ptr();
            let mut child = {{& msgref_name }}Writer::new_from_iter(new_iter);
            f(&mut child);
            child.get_ptr() - start
        };
        self.iter.insert_len_and_copy(1, len);

        self
    }
    {{/is_message}}
    {{/field}}
}
{{/message_type}}

{{/file}}
