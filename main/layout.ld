__STACK_BASE = ORIGIN(STACK) + LENGTH(STACK);
_data_load = LOADADDR(.data);
_init_vector_load = LOADADDR(.init_vector);

ENTRY(begin)

MEMORY
{
    /* Monolithic fw first 2/3 of "firmware"; modular fw "system part 1" */
    APP_FLASH       (rx)  : ORIGIN = 0x08020000, LENGTH = 256K
    /* Monolithic fw "firmware" + "factory reset" + "unused"; modular fw "system
     * part 2" + "user" + "OTA backup" + "factory backup" */
    BCM_FW          (r)   : ORIGIN = 0x08060000, LENGTH = 640K - 4K
    /* Monolithic fw last 4k of "unused"; modular fw last 4k of "factory backup" */
    CONFIG          (r)   : ORIGIN = 0x080FF000, LENGTH = 4K
    SRAM            (rwx) : ORIGIN = 0x20000000, LENGTH = 64K
    PERSISTENT_SRAM (r)   : ORIGIN = 0x20010000, LENGTH = 4K+16
    STACK           (r)   : ORIGIN = 0x20011010, LENGTH = 128K -4K-16 -64K
}

ASSERT(LENGTH(APP_FLASH) + LENGTH(BCM_FW) + LENGTH(CONFIG) == 1M - 128K, "APP_FLASH+BCM_FW+CONFIG must map all flash but 128K");
ASSERT(ORIGIN(APP_FLASH) == 0x08020000, "APP_FLASH must be after the 128K bootloader region");
ASSERT(ORIGIN(BCM_FW) == ORIGIN(APP_FLASH) + LENGTH(APP_FLASH), "APP_FLASH must immediately precede BCM_FW");
ASSERT(ORIGIN(CONFIG) == ORIGIN(BCM_FW) + LENGTH(BCM_FW), "BCM_FW must immediately precede CONFIG");

ASSERT(LENGTH(PERSISTENT_SRAM) + LENGTH(SRAM) + LENGTH(STACK) == 128K, "SRAM, PERSISTENT_SRAM and STACK must be 128K together");
ASSERT(ORIGIN(SRAM) + LENGTH(SRAM) == ORIGIN(PERSISTENT_SRAM), "SRAM must immediately precede PERSISTENT_SRAM");
ASSERT(ORIGIN(PERSISTENT_SRAM) + LENGTH(PERSISTENT_SRAM) == ORIGIN(STACK), "PERSISTENT_SRAM must immediately precede STACK");

SECTIONS
{
    .module_start : {
        link_module_start = .;
    } >APP_FLASH AT>APP_FLASH

    .init_vector :  /* entry point from bootloader */
    {
        . = ALIGN(512);
        _init_vector_start = .;
        KEEP(*(*.init_vector))
        _init_vector_end = .;
    } >SRAM AT> APP_FLASH

    .module_info :
    {
        link_module_info_start = .;
        KEEP(*(.module_info))
        link_module_info_end = .;
    }>APP_FLASH  AT> APP_FLASH

    .bcm_fw :
    {
        __BCM_FW = .;
    } >BCM_FW AT>BCM_FW

    .config :
    {
        __CONFIG = .;
    } >CONFIG AT>CONFIG

    .text : ALIGN(4)
    {
        FILL(0xff)
        __BUFFER_SOME_START = .;
        *(.text._*11DebugBuffer11buffer_some*)
        __BUFFER_SOME_END = .;
        *(.text*)
        *(.rodata .rodata.*)
    } > APP_FLASH

    .data : ALIGN(4)
    {
        _data = .;

        *(SORT_BY_ALIGNMENT(.data*))
        . = ALIGN(4);

        _edata = .;
    } > SRAM AT>APP_FLASH = 0xff

    .bss : ALIGN(4)
    {
        _bss = .;

        *(.bss*)
        *(COMMON)
        . = ALIGN(4);

        _ebss = .;

        . += 4;

        __global_arena_start = .;

        . += 40K;

        __global_arena_end = .;

        __STACK_LIMIT = .;

        . += 4;

        _eglobals = .;
    } > SRAM

    .pdata : ALIGN(4)
    {
        *(.pdata*)
    } > PERSISTENT_SRAM AT> PERSISTENT_SRAM
}

__USB_FS = 0x50000000;
