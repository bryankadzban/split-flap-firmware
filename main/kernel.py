#!/usr/bin/env python

# Copyright 2018 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import math
import sys

def main(argv):
    if len(argv) < 3:
        print >>sys.stderr, ("Usage: %s points frequency clip-scale [int-max]" % argv[0])
        return 1

    points = int(argv[1])
    frequency = float(argv[2])
    scale = float(argv[3])

    do_float = True
    if len(argv) >= 5:
        do_float = False
        scaler = int(argv[4])

    samples = []

    for i in xrange(0, points + 1):
        if i == points/2:
            sinc = 2*math.pi * frequency
        else:
            sinc = math.sin(2*math.pi * frequency * (i - points/2)) / (i - points/2)
        angle = 2*math.pi * i / points
        window = (0.42 - 0.5*math.cos(angle) + 0.08*math.cos(2*angle))
        samples.append(sinc * window * scale)

    if do_float:
        for s in samples:
            print s
    else:
        total = 0.0
        for s in samples:
            total += s
        scaler = scaler * scale / total
        for i in xrange(0, len(samples)/2, 4):
            print '%d, %d, %d, %d,' % (
                    int(samples[i] * scaler),
                    int(samples[i+1] * scaler),
                    int(samples[i+2] * scaler),
                    int(samples[i+3] * scaler))
        print '%d,' % int(samples[len(samples)/2] * scaler)
        for i in xrange(len(samples)/2+1, len(samples), 4):
            print '%d, %d, %d, %d,' % (
                    int(samples[i] * scaler),
                    int(samples[i+1] * scaler),
                    int(samples[i+2] * scaler),
                    int(samples[i+3] * scaler))

if __name__ == '__main__':
    sys.exit(main(sys.argv))
