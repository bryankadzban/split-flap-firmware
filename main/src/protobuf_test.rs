// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

include!(concat!(env!("OUT_DIR"), "/protobuf_test.rs"));

#[cfg(test)]
mod test_float_like {
    use protobuf_test::*;

    #[test]
    fn test_decode_f32() {
        if let Some(actual) = f32::decode(&mut SliceIter::new(&mut [0u8; 4])) {
            assert!(actual == 0.0, "decode(0.0) -> {}", actual);
        } else { assert!(false, "decode(0.0) ran out of bytes"); }

        if let Some(actual) = f32::decode(&mut SliceIter::new(&mut [0u8, 0u8, 0x80u8, 0x3fu8])) {
            assert!(actual == 1.0, "decode(1.0) -> {}", actual);
        } else { assert!(false, "decode(1.0) ran out of bytes"); }

        if let Some(actual) = f32::decode(&mut SliceIter::new(&mut [0u8, 0u8, 0x80u8, 0xbfu8])) {
            assert!(actual == -1.0, "decode(-1.0) -> {}", actual);
        } else { assert!(false, "decode(-1.0) ran out of bytes"); }

        if let Some(actual) = f32::decode(&mut SliceIter::new(&mut [0u8, 0u8, 0u8, 0x43u8])) {
            assert!(actual == 128.0, "decode(128.0) -> {}", actual);
        } else { assert!(false, "decode(128.0) ran out of bytes"); }

        if let Some(actual) = f32::decode(&mut SliceIter::new(&mut [0u8, 0u8, 0u8, 0xc3u8])) {
            assert!(actual == -128.0, "decode(-128.0) -> {}", actual);
        } else { assert!(false, "decode(-128.0) ran out of bytes"); }

        if let Some(actual) = f32::decode(&mut SliceIter::new(&mut [0xa5u8, 0x0au8, 0x14u8, 0x50u8])) {
            assert!(actual == 9934902384.0, "decode(9934902384.0) -> {}", actual);
        } else { assert!(false, "decode(9934902384.0) ran out of bytes"); }

        if let Some(actual) = f32::decode(&mut SliceIter::new(&mut [0xa5u8, 0x0au8, 0x14u8, 0xd0u8])) {
            assert!(actual == -9934902384.0, "decode(-9934902384.0) -> {}", actual);
        } else { assert!(false, "decode(-9934902384.0) ran out of bytes"); }

        if let Some(actual) = f32::decode(&mut SliceIter::new(&mut [0u8, 0u8, 0x80u8, 0x7fu8])) {
            assert!(actual.is_infinite() && actual > 0.0, "decode(inf) -> {}", actual);
        } else { assert!(false, "decode(inf) ran out of bytes"); }

        if let Some(actual) = f32::decode(&mut SliceIter::new(&mut [0u8, 0u8, 0x80u8, 0xffu8])) {
            assert!(actual.is_infinite() && actual < 0.0, "decode(-inf) -> {}", actual);
        } else { assert!(false, "decode(-inf) ran out of bytes"); }

        if let Some(actual) = f32::decode(&mut SliceIter::new(&mut [0u8, 0u8, 0xc0u8, 0xffu8])) {
            assert!(actual.is_nan(), "decode(nan) -> {}", actual);
        } else { assert!(false, "decode(nan) ran out of bytes"); }
    }

    #[test]
    fn test_decode_f64() {
        if let Some(f) = f64::decode(&mut SliceIter::new(&mut [0; 8])) {
            assert!(f == 0.0, "decode(0.0) -> {}", f);
        } else { assert!(false, "decode(0.0) ran out of bytes"); }

        if let Some(f) = f64::decode(&mut SliceIter::new(&mut [0, 0, 0, 0, 0, 0, 0xf0, 0x3f])) {
            assert!(f == 1.0, "decode(1.0) -> {}", f);
        } else { assert!(false, "decode(1.0) ran out of bytes"); }

        if let Some(f) = f64::decode(&mut SliceIter::new(&mut [0, 0, 0, 0, 0, 0, 0xf0, 0xbf])) {
            assert!(f == -1.0, "decode(-1.0) -> {}", f);
        } else { assert!(false, "decode(-1.0) ran out of bytes"); }

        if let Some(f) = f64::decode(&mut SliceIter::new(&mut [0, 0, 0, 0, 0, 0, 0x60, 0x40])) {
            assert!(f == 128.0, "decode(128.0) -> {}", f);
        } else { assert!(false, "decode(128.0) ran out of bytes"); }

        if let Some(f) = f64::decode(&mut SliceIter::new(&mut [0, 0, 0, 0, 0, 0, 0x60, 0xc0])) {
            assert!(f == -128.0, "decode(-128.0) -> {}", f);
        } else { assert!(false, "decode(-128.0) ran out of bytes"); }

        if let Some(f) = f64::decode(&mut SliceIter::new(&mut [0x4e, 0x3c, 0xbf, 0xdb, 0xde, 0xa5, 0x41, 0x43])) {
            assert!(f == 9934902384294043.0, "decode(9934902384294043.0) -> {}", f);
        } else { assert!(false, "decode(9934902384294043.0) ran out of bytes"); }

        if let Some(f) = f64::decode(&mut SliceIter::new(&mut [0x4e, 0x3c, 0xbf, 0xdb, 0xde, 0xa5, 0x41, 0xc3])) {
            assert!(f == -9934902384294043.0, "decode(-9934902384294043.0) -> {}", f);
        } else { assert!(false, "decode(-9934902384294043.0) ran out of bytes"); }

        if let Some(f) = f64::decode(&mut SliceIter::new(&mut [0, 0, 0, 0, 0, 0, 0xf0, 0x7f])) {
            assert!(f.is_infinite() && f > 0.0, "decode(inf) -> {}", f);
        } else { assert!(false, "decode(inf) ran out of bytes"); }

        if let Some(f) = f64::decode(&mut SliceIter::new(&mut [0, 0, 0, 0, 0, 0, 0xf0, 0xff])) {
            assert!(f.is_infinite() && f < 0.0, "decode(-inf) -> {}", f);
        } else { assert!(false, "decode(-inf) ran out of bytes"); }

        if let Some(f) = f64::decode(&mut SliceIter::new(&mut [0, 0, 0, 0, 0, 0, 0xf8, 0xff])) {
            assert!(f.is_nan(), "decode(nan) -> {}", f);
        } else { assert!(false, "decode(nan) ran out of bytes"); }
    }
}

#[cfg(test)]
mod test_varint_len {
    use protobuf_test::varint_len;

    fn run_test(value: i32, expected: usize) {
        let len = varint_len(value);
        assert!(len == expected, "len({}) -> {}, not {}", value, len, expected);
    }

    #[test]
    fn test_varint_len() {
        run_test(0, 1);
        run_test(1, 1);
        run_test(127, 1);
        run_test(128, 2);
        run_test(16383, 2);
        run_test(16384, 3);
        run_test(-1, 5);
    }
}

#[cfg(test)]
mod test_decode_varint {
    use protobuf_test::{SliceIter, SliceIterMut};

    fn test_roundtrip(value: i32) {
        let mut buf = [0u8; 16];
        SliceIterMut::new(&mut buf).write_varint(value);
        let output = SliceIter::new(&buf).decode_varint::<i32>().unwrap();
        assert!(output == value, "decode(encode({})) -> {}", value, output);
    }

    fn test_zigzag_roundtrip(value: i32) {
        let mut buf = [0u8; 16];
        SliceIterMut::new(&mut buf).write_varint(value);
        let output = SliceIter::new(&buf).decode_varint::<i32>().unwrap();
        assert!(output == value, "zigzag: decode(encode({})) -> {}", value, output);
    }

    #[test]
    fn test_decode_varint() {
        let buf = [0xffu8, 1u8, 0, 0];
        let val = SliceIter::new(&buf).decode_varint::<u32>().unwrap();
        assert!(val == 255, "decode_varint::<u32>() -> {:x}, not 0xff", val);
        let val = SliceIter::new(&buf).decode_varint::<u16>().unwrap();
        assert!(val == 255, "decode_varint::<u16>() -> {:x}, not 0xff", val);
        let val = SliceIter::new(&buf).decode_fixed::<u32>().unwrap();
        assert!(val == 511, "decode_fixed::<u32>() -> {:x}, not 0x1ff", val);

        test_roundtrip(0);
        test_roundtrip(1);
        test_roundtrip(127);
        test_roundtrip(128);
        test_roundtrip(16383);
        test_roundtrip(16384);
        test_roundtrip(-1);
        test_roundtrip(-128);
        test_roundtrip(-16384);

        test_zigzag_roundtrip(0);
        test_zigzag_roundtrip(1);
        test_zigzag_roundtrip(127);
        test_zigzag_roundtrip(128);
        test_zigzag_roundtrip(16383);
        test_zigzag_roundtrip(16384);
        test_zigzag_roundtrip(-1);
        test_zigzag_roundtrip(-127);
        test_zigzag_roundtrip(-128);
        test_zigzag_roundtrip(-16384);
    }
}

#[cfg(test)]
mod test_proto {
    use protobuf_test::{TestWriter,TestField,ContainedField,WireType,TestReader,Writer};
    use core::fmt::Write;

    #[test]
    fn test_roundtrip() {
        let mut buf = [0u8; 16384];
        let bytes = [0xa5u8; 16];
        let len = {
            let mut writer = TestWriter::new(&mut buf);
            let ptr = writer.get_ptr();
            writer.write_field(TestField::Dbl{value: 11939458723.0})
                .write_field(TestField::Flt{value: -1838401.0})
                .write_field(TestField::I32{value: -1})
                .write_field(TestField::I64{value: 4294967296})
                .write_field(TestField::U32{value: 16383})
                .write_field(TestField::U64{value: 17})
                .write_field(TestField::S32{value: -1})  // zigzag
                .write_field(TestField::S64{value: -3})  // zigzag
                .write_field(TestField::F32{value: 99999}) // fixed
                .write_field(TestField::F64{value: 0xaaaa5555aaaa5555})
                .write_field(TestField::Sf32{value: -1})
                .write_field(TestField::Sf64{value: -4383})
                .write_field(TestField::B{value: true})
                .write_field(TestField::S{value: "test short string"})
                .write_field(TestField::Byt{value: &bytes})
                .write_c(1, |writer| {
                    let s = " ".repeat(250);
                    writer.write_field(ContainedField::Id{value: 17})
                        .write_field(ContainedField::Text{value: &s});  // test move
                })
                .write_field(TestField::RepsVar{value: 16})
                .write_field(TestField::RepsVar{value: 32})
                .write_field(TestField::RepsVar{value: 0})
                .write_field(TestField::RepsFixed{value: 0})
                .write_field(TestField::RepsFixed{value: 255})
                .write_field(TestField::Unknown{tag: 999, wire_type: WireType::VarInt as u8, buf: &[0u8]})
                .write_field(TestField::Unknown{tag: 998, wire_type: WireType::LengthDelimited as u8, buf: &[1u8, 3u8, 0u8]})
                .write_field(TestField::Unknown{tag: 997, wire_type: WireType::Fixed32 as u8, buf: &[5u8, 7u8, 9u8, 11u8]})
                .write_field(TestField::Unknown{tag: 996, wire_type: WireType::VarInt as u8, buf: &[135, 5]})
                .get_ptr() - ptr
        };
        print!("buffer: ");
        for i in 0..len {
            print!("0x{:x} ", buf[i]);
        }
        println!("");
        {
            for field in TestReader::new(&buf[..len]) {
                println!("field: {:?}", field);
                match field {
                    TestField::Dbl{value: 11939458723.0} => {},
                    TestField::Flt{value: -1838401.0} => {},
                    TestField::I32{value: -1} => {},
                    TestField::I64{value: 4294967296} => {},
                    TestField::U32{value: 16383} => {},
                    TestField::U64{value: 17} => {},
                    TestField::S32{value: -1} => {},
                    TestField::S64{value: -3} => {},
                    TestField::F32{value: 99999} => {},
                    TestField::F64{value: 0xaaaa5555aaaa5555} => {},
                    TestField::Sf32{value: -1} => {},
                    TestField::Sf64{value: -4383} => {},
                    TestField::B{value: true} => {},
                    TestField::S{value: "test short string"} => {},
                    TestField::Byt{value: byt} => {
                        assert!(byt == bytes, "unrecognized field {:?}", field);
                    },
                    TestField::C{value: reader} => {
                        let s = " ".repeat(250);
                        for field in reader {
                            match field {
                                ContainedField::Id{value: 17} => {},
                                ContainedField::Text{value} => {
                                    assert!(value == s, "unrecognized contained field {:?}", field);
                                },
                                _ => { panic!("unrecognized contained field {:?}", field); },
                            }
                        }
                    },
                    TestField::RepsVar{value: 16} => {},
                    TestField::RepsVar{value: 32} => {},
                    TestField::RepsVar{value: 0} => {},
                    TestField::RepsFixed{value: 0} => {},
                    TestField::RepsFixed{value: 255} => {},
                    TestField::Unknown{tag: 999, wire_type, buf} => {
                        assert!(wire_type == WireType::VarInt as u8 && buf == &[0u8],
                                "unrecognized field {:?}", field);
                    },
                    TestField::Unknown{tag: 998, wire_type, buf} => {
                        assert!(wire_type == WireType::LengthDelimited as u8 && buf == &[1u8, 3u8, 0u8],
                                "unrecognized field {:?}", field);
                    },
                    TestField::Unknown{tag: 997, wire_type, buf} => {
                        assert!(wire_type == WireType::Fixed32 as u8 && buf == &[5u8, 7u8, 9u8, 11u8],
                                "unrecognized field {:?}", field);
                    },
                    TestField::Unknown{tag: 996, wire_type, buf} => {
                        assert!(wire_type == WireType::VarInt as u8 && buf == &[135, 5],
                                "unrecognized field {:?}", field);
                    },
                    _ => { panic!("unrecognized field {:?}", field); },
                }
            }
        }
        // now make sure all fields are seen, in that they all get re-serialized in order
        let mut buf2 = [0u8; 16384];
        {
            let mut writer = TestWriter::new(&mut buf2);
            for field in TestReader::new(&buf) {
                match field {
                    TestField::C{value} => {
                        writer.write_c(1, |writer| {
                            for field in value {
                                writer.write_field(field);
                            }
                        });
                    },
                    _ => {
                        writer.write_field(field);
                    },
                }
            }
        }
        let slice1: &[u8] = &buf;
        let slice2: &[u8] = &buf2;
        assert!(slice1 == slice2, "writing fields from reader produced bad buf2: {:?}", slice2);
    }
}
