// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//   TX: GPIOA:9  7-USART1_TX              TIM1_CH2   
//   RX: GPIOA:10 7-USART1_RX              TIM1_CH3   
//  WKP: GPIOA:0                 ADC0    TIM5_CH1
//  DAC: GPIOA:4            DAC1 ADC4    
//   A5: GPIOA:7  SPI1_MOSI      ADC7    TIM3_CH2
//   A4: GPIOA:6  SPI1_MISO      ADC6    TIM3_CH1
//   A3: GPIOA:5  SPI1_SCK  DAC2 ADC5
//   A2: GPIOC:2  SPI1_SS        ADC12
//   A1: GPIOC:3                 ADC13
//   A0: GPIOC:5                 ADC15
//
//   D7: GPIOA:13             JTAG_TMS
//   D6: GPIOA:14             JTAG_TCK
//   D5: GPIOA:15 SPI3_SS     JTAG_TDI             I2S3_WS
//   D4: GPIOB:3  SPI3_SCK    JTAG_TDO             I2S3_SCK
//   D3: GPIOB:4  SPI3_MISO   JTAG_TRST  TIM3_CH1  
//   D2: GPIOB:5  SPI3_MOSI              TIM3_CH2  I2S3_SD  I2C1_SMBA
//   D1: GPIOB:6                         TIM4_CH1  I2S3_SD  I2C1_SCL
//   D0: GPIOB:7                         TIM4_CH2  I2S3_SD  I2C1_SDA
//
//  RED: GPIOA:1                         TIM2_CH2
//  GRN: GPIOA:2                         TIM2_CH3
//  BLU: GPIOA:3                         TIM2_CH4
//  SUP: GPIOC:7                         TIM3_CH2
//  RST: 
// USB+: GPIOB:15
// USB-: GPIOB:14

#[repr(C)]
pub struct ModDep {
    pub module_function: u8,
    pub module_index: u8,
    pub module_version: u16,
}

#[repr(C)]
pub struct ModInfo {
    pub module_start_address: u32,
    pub module_end_address: u32,
    pub reserved: u8,
    pub reserved2: u8,
    pub module_version: u16,
    pub platform_id: u16,
    pub module_function: u8,
    pub module_index: u8,
    pub dependency: ModDep,
    pub reserved3: u32
}
