// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#![no_std]

#![feature(lang_items)]
#![feature(core_intrinsics)]
#![feature(asm)]
#![feature(const_fn)]
#![feature(panic_info_message)]

#[cfg(test)]
#[macro_use]
extern crate std;

#[cfg(not(feature = "devicetest"))]
extern crate bcm43362;

#[cfg(not(feature = "devicetest"))]
extern crate wifi;

extern crate utils;
#[macro_use]
extern crate stm32f2;

extern crate arena;

#[cfg(not(feature = "devicetest"))]
extern crate audio;

extern crate hal;

#[cfg(not(feature = "devicetest"))]
extern crate scheduler;

#[cfg(not(feature = "devicetest"))]
extern crate network;

#[cfg(test)]
extern crate byteorder;

mod photon;
#[cfg(not(feature = "devicetest"))]
mod flapserver;

#[cfg(test)]
mod protobuf_test;

#[cfg(not(feature = "devicetest"))]
use network::protocols::{EthernetPacket, IpAddr, UdpPacketBuilder, UdpPacket};
#[cfg(not(feature = "devicetest"))]
use network::state::{Connectivity, Ipv6Stack};
#[cfg(not(feature = "devicetest"))]
use stm32f2::GpioPin;
use stm32f2::atomic::{AtomicRefCell};
#[cfg(not(feature = "devicetest"))]
use hal::timer::{SimpleTimer, Time16};
use hal::watchdog::IndependentWatchdog;
use hal::dac::Dac;
use hal::pca9505::{I2c, I2cConfig, I2cMode, IoExpanders, EXPANDER_COUNT, Pca9698, PinType, CycleState};
use hal::usb::{Usb, UsbIo};
use hal::led::{Led, Color};

#[cfg(not(feature = "devicetest"))]
use scheduler::{make_machine, RefScheduler, SimpleScheduler, Slot, When};

#[cfg(not(feature = "devicetest"))]
use hal::power::{Power, PowerConfig};

#[cfg(not(feature = "devicetest"))]
use utils::bits::{BitSetLe};

#[cfg(not(feature = "devicetest"))]
use utils::func::{ObservableValue};
#[cfg(not(feature = "devicetest"))]
use core::result::Result::{Ok};
#[cfg(not(feature = "devicetest"))]
use core::cell::RefCell;
use core::fmt::Write;
#[cfg(feature = "devicetest")]
use core::mem::MaybeUninit;
#[cfg(feature = "cortex")]
use core::panic::PanicInfo;

use stm32f2::debug;

#[cfg(feature = "cortex")]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    unsafe { dump_stack(); }

    let (file, line) = match info.location() {
        Some(l) => (l.file(), l.line()),
        None => ("unknown file", 0),
    };
    if let Some(args) = info.message() {
        write!(debug::serial(), "At {}:{}: {}", file, line, args).unwrap();
    } else {
        println!("At {}:{}: unknown panic", file, line);
    }

    loop {}
}

#[cfg(feature = "cortex")]
unsafe fn dump_stack() {
    // Save registers to argument locations so we can dump them later
    asm!("push {r0-r11}
          mov r0, sp
          mov r1, lr
          mov r2, pc
          mov r3, r12
          bl print_state_dump
          pop {r0-r11}" :::: "volatile");
}

#[cfg(feature = "cortex")]
#[no_mangle]
pub unsafe extern "C" fn print_state_dump(sp: *const usize, lr: usize, pc: usize, r12: usize, r0: usize, r1: usize, r2: usize, r3: usize, r4: usize, r5: usize, r6: usize, r7: usize, r8: usize, r9: usize, r10: usize, r11: usize) {
    for i in 0..core::cmp::min(512, (0x20020000 - sp as isize) >> 2) {
        write!(debug::serial(), "{:#010x}: {:#010x}\n", sp.offset(i) as usize, *sp.offset(i)).unwrap();
    }
    write!(debug::serial(), "R0: {:#010x}\n", r0).unwrap();
    write!(debug::serial(), "R1: {:#010x}\n", r1).unwrap();
    write!(debug::serial(), "R2: {:#010x}\n", r2).unwrap();
    write!(debug::serial(), "R3: {:#010x}\n", r3).unwrap();
    write!(debug::serial(), "R4: {:#010x}\n", r4).unwrap();
    write!(debug::serial(), "R5: {:#010x}\n", r5).unwrap();
    write!(debug::serial(), "R6: {:#010x}\n", r6).unwrap();
    write!(debug::serial(), "R7: {:#010x}\n", r7).unwrap();
    write!(debug::serial(), "R8: {:#010x}\n", r8).unwrap();
    write!(debug::serial(), "R9: {:#010x}\n", r9).unwrap();
    write!(debug::serial(), "R10: {:#010x}\n", r10).unwrap();
    write!(debug::serial(), "R11: {:#010x}\n", r11).unwrap();
    write!(debug::serial(), "R12: {:#010x}\n", r12).unwrap();
    write!(debug::serial(), "SP: {:#010x}\n", sp as usize).unwrap();
    write!(debug::serial(), "LR: {:#010x}\n", lr).unwrap();
    write!(debug::serial(), "PC: {:#010x}\n", pc).unwrap();
}

#[cfg(feature = "cortex")]
#[lang = "start"]
fn start(_: *const u8, _: isize, _: *const *const u8) -> isize {
    0
}

#[no_mangle]
pub unsafe fn __aeabi_unwind_cpp_pr0() -> () {
    println!("__aeabi_unwind");
    loop {}
}
#[no_mangle]
pub unsafe fn __aeabi_unwind_cpp_pr1() -> () {
    println!("__aeabi_unwind");
    loop {}
}


extern "C" {

    fn __STACK_BASE();

    #[link_name = "__CONFIG"]
    #[cfg(not(feature = "devicetest"))]
    static CONFIG: wifi::Config;
}

#[no_mangle]
#[link_section = ".init_vector"]
pub static mut __INIT_VECTOR: [Option<unsafe extern "C" fn()>; 97] = [
    Some(__STACK_BASE),
    Some(begin), // Reset
    Some(Handle_NMI), // NMI
    Some(Handle_HardFault), // HardFault
    Some(special_int), // MemManage
    Some(Handle_BusFault), // BusFault
    Some(Handle_UsageFault), // UsageFault
    Some(special_int),
    Some(special_int),
    Some(special_int),
    Some(special_int),
    Some(Handle_HardFault), // SVCall
    #[cfg(not(feature = "devicetest"))]
    Some(special_int), // Debug Monitor
    #[cfg(feature = "devicetest")]
    Some(Handle_Debug_Monitor), // Debug Monitor
    Some(special_int), 
    Some(special_int), // PendSV
    Some(special_int), // SysTick

    // 0
    Some(do_nothing), // WWDG
    Some(do_nothing), // PVD
    Some(do_nothing), // TAMP_STAMP
    Some(do_nothing), // RTC_WKUP
    Some(do_nothing), // FLASH
    Some(do_nothing), // RCC
    Some(do_nothing), // EXTI0
    Some(do_nothing), // EXTI1
    Some(do_nothing), // EXTI2,
    Some(do_nothing), // EXTI3,
    Some(do_nothing), // EXTI4,
    Some(do_nothing), // DMA1_Stream0
    Some(do_nothing), // DMA1_Stream1
    Some(do_nothing), // DMA1_Stream2
    Some(do_nothing), // DMA1_Stream3
    Some(do_nothing), // DMA1_Stream4

    // 16
    Some(do_nothing), // DMA1_Stream5
    Some(do_nothing), // DAM1_Stream6
    Some(do_nothing), // ADC
    Some(do_nothing), // CAN1_TX
    Some(do_nothing), // CAN1_RX0
    Some(do_nothing), // CAN1_RX1
    Some(do_nothing), // CAN1_SCE
    Some(Handle_EXTI9_5), // EXTI9_5
    Some(do_nothing), // TIM1_BRK_TIM9
    Some(do_nothing), // TIM1_UP_TIM10
    Some(do_nothing), // TIM1_TRG_COM_TIM11
    Some(do_nothing), // TIM1_CC
    Some(Handle_TIM2), // TIM2
    Some(Handle_TIM3), // TIM3
    Some(do_nothing), // TIM4
    Some(do_nothing), // I2C1_EV

    // 32
    Some(do_nothing), // I2C1_ER
    Some(do_nothing), // I2C2_EV
    Some(do_nothing), // I2C2_ER
    Some(do_nothing), // SPI1
    Some(do_nothing), // SPI2
    Some(do_nothing), // USART1
    Some(do_nothing), // USART2
    Some(do_nothing), // USART3
    Some(do_nothing), // EXT15_10
    Some(do_nothing), // RTC_Alarm
    Some(do_nothing), // OTG_FS_WKUP
    Some(do_nothing), // TIM8_BRK_TIM12
    Some(do_nothing), // TIM8_UP_TIM13
    Some(do_nothing), // TIM8_TRG_COM_TIM14
    Some(do_nothing), // TIM8_CC
    Some(do_nothing), // DMA1_Stream7

    // 48
    Some(do_nothing), // FSMC
    Some(do_nothing), // SDIO
    Some(Handle_TIM5), // TIM5
    Some(Handle_TIM5), // SPI3
    Some(do_nothing), // UART4
    Some(do_nothing), // UART5
    Some(Handle_TIM6_DAC), // TIM6_DAC
    Some(do_nothing), // TIM7
    Some(Handle_DMA2_Stream0), // DMA2_Stream0
    Some(do_nothing), // DMA2_Stream1
    Some(do_nothing), // DMA2_Stream2
    Some(do_nothing), // DMA2_Stream3
    Some(do_nothing), // DMA2_Stream4
    Some(do_nothing), // ETH
    Some(do_nothing), // ETH_WKUP
    Some(do_nothing), // CAN2_TX

    Some(do_nothing), // CAN2_RX0
    Some(do_nothing), // CAN2_RX1
    Some(do_nothing), // CAN2_SCE
    Some(do_nothing), // OTG_FS
    Some(do_nothing), // DMA2_Stream5
    Some(do_nothing), // DMA2_Stream6
    Some(debug::Handle_DMA2_Stream7), // DMA2_Stream7
    Some(do_nothing), // USART6
    Some(do_nothing), // I2C3_EV
    Some(do_nothing), // I2C3_ER
    Some(do_nothing), // OTG_HS_EP1_OUT
    Some(do_nothing), // OTG_HS_EP1_IN
    Some(do_nothing), // OTG_HS_WKUP
    Some(do_nothing), // OTG_HS
    Some(do_nothing), // DCMI
    Some(do_nothing), // CRYP

    Some(do_nothing), // HASH_RNG
];

#[allow(non_snake_case)]
unsafe extern "C" fn Handle_TIM6_DAC() {
    let dac = stm32f2::DAC();
    if dac.sr.dmaudr1() {
        debug::serial().write_str("DMA underrun, DAC1\n").unwrap();
        dac.sr.clear_dmaudr1();
    }
    if dac.sr.dmaudr2() {
        debug::serial().write_str("DMA underrun, DAC2\n").unwrap();
        dac.sr.clear_dmaudr2();
    }
}

// 0_0 00: MOTOR4_IN1_D  D1
// 0_1 01: MOTOR4_IN1_C  C1
// 0_2 02: MOTOR4_IN1_B  B1
// 0_3 03: MOTOR4_IN1_A  A1
// 0_4 04: MOTOR4_IN2_A  A1
// 0_5 05: MOTOR4_IN2_B  A2
// 0_6 06: MOTOR4_IN2_C  A3
// 0_7 07: MOTOR4_IN2_D  A4
//
// 1_0 08:  C1 sensor
// 1_1 09:  D1 sensor
// 1_2 10:  A1 sensor
// 1_3 11:  B1 sensor
// 1_4 12:  A2 sensor
// 1_5 13:  B2 sensor
// 1_6 14:  C2 sensor
// 1_7 15:  D2 sensor
//
// 2_0 16: MOTOR5_IN1_D
// 2_1 17: MOTOR5_IN1_C
// 2_2 18: MOTOR5_IN1_B
// 2_3 19: MOTOR5_IN1_A
// 2_4 20: MOTOR5_IN2_A
// 2_5 21: MOTOR5_IN2_B
// 2_6 22: MOTOR5_IN2_C
// 2_7 23: MOTOR5_IN2_D
//
// 3_0 24: chip4 sensor_select
// 3_1 25: chip5 sensor_select
// 3_2 26: chip6 sensor_select
// 3_4 28:  A3 sensor
// 3_5 29:  B3 sensor
// 3_6 30:  C3 sensor
// 3_7 31:  D3 sensor

// 4_0 32: MOTOR6_IN1_D
// 4_1 33: MOTOR6_IN1_C
// 4_2 34: MOTOR6_IN1_B
// 4_3 35: MOTOR6_IN1_A
// 4_4 36: MOTOR6_IN2_A
// 4_5 37: MOTOR6_IN2_B
// 4_6 38: MOTOR6_IN2_C
// 4_7 39: MOTOR6_IN2_D

#[cfg(not(feature = "devicetest"))]
struct LetterIoAssignment {
    motor_in1: u8,
    motor_in2: u8,
    sensor: u8,
}

#[cfg(not(feature = "devicetest"))]
static IO_ASSIGNMENTS: [LetterIoAssignment; 12] = [
    LetterIoAssignment{motor_in1: 3, motor_in2: 4, sensor: 10},  // pca:7 pca:8 pca:15
    LetterIoAssignment{motor_in1: 2, motor_in2: 5, sensor: 11},  // pca:5 pca:9 pca:16
    LetterIoAssignment{motor_in1: 1, motor_in2: 6, sensor: 8},   // pca:4 pca:10 pca:13
    LetterIoAssignment{motor_in1: 0, motor_in2: 7, sensor: 9},   // pca:3 pca:12 pca:14

    LetterIoAssignment{motor_in1: 19, motor_in2: 20, sensor: 12},
    LetterIoAssignment{motor_in1: 18, motor_in2: 21, sensor: 13},
    LetterIoAssignment{motor_in1: 17, motor_in2: 22, sensor: 14},
    LetterIoAssignment{motor_in1: 16, motor_in2: 23, sensor: 15},

    LetterIoAssignment{motor_in1: 35, motor_in2: 36, sensor: 28},
    LetterIoAssignment{motor_in1: 34, motor_in2: 37, sensor: 29},
    LetterIoAssignment{motor_in1: 33, motor_in2: 38, sensor: 30},
    LetterIoAssignment{motor_in1: 32, motor_in2: 39, sensor: 31},
];


#[cfg(not(feature = "devicetest"))]
static FAKE_MESSAGES: [&'static str; 4] = [
    "thermite fire in oi",
    "rabid bears in pg",
    "demons summoned yg",
    "killer frogs in tf",
];

#[link_section = ".module_info"]
#[no_mangle]
pub static __MODULE_INFO: photon::ModInfo = photon::ModInfo{
    module_start_address: 0x08020000,
    // this is a horrible lie to make the bootloader happy enough to jump to our code
    module_end_address: 0x0805d108,
    reserved: 0,
    reserved2: 0,
    module_version: 0x000c,
    platform_id: 0x0006,
    module_function: 0x04,
    module_index: 0x01,
    dependency: photon::ModDep {
        module_function: 0,
        module_index: 0,
        module_version: 0,
    },
    reserved3: 0,
};


#[cfg(not(feature = "devicetest"))]
struct PhotonDevice<'a, NIC: 'a + network::NetworkInterface> {
    led: Led,

    power: &'a RefCell<Power<'a>>,

    onewire_pin: GpioPin,
    setup_button: GpioPin,

    watchdog: IndependentWatchdog,

    nic: &'a NIC,
    i2c: &'a RefCell<I2c>,

    timer7: &'a SimpleTimer,

    usb: &'static AtomicRefCell<Usb>,

    dac: &'static AtomicRefCell<Dac>,

    converter: &'static AtomicRefCell<audio::Converter>,
}


#[no_mangle]
pub unsafe extern "C" fn do_nothing() {
}

#[no_mangle]
#[cfg(feature = "cortex")]
#[allow(non_snake_case)]
pub unsafe extern "C" fn Handle_HardFault() {
    let _frameptr: *const usize;
    asm!("mov r0, sp
          mov r1, r4
          mov r2, r5
          mov r3, r6
          push {r7-r11}
          bl Handle_HardFault2" :::: "volatile");
    core::intrinsics::unreachable();
}
#[cfg(not(feature = "cortex"))]
#[allow(non_snake_case)]
pub unsafe extern "C" fn Handle_HardFault() {
}

#[no_mangle]
#[allow(non_snake_case)]
pub unsafe extern "C" fn Handle_TIM5() {
    println!("Handle_TIM5");
}

#[no_mangle]
#[cfg(feature = "cortex")]
#[allow(non_snake_case)]
pub unsafe extern "C" fn Handle_HardFault2(frameptr: *const usize, r4: usize, r5: usize, r6: usize, r7: usize, r8: usize, r9: usize, r10: usize, r11: usize) {
    // Returns the same value as ReturnAddress in section B1.5.6 of the ARMv7-M
    // Architecture Reference Manual
    
    write!(debug::serial(), "PC: 0x{:x}\n", *frameptr.offset(6)).unwrap();
    write!(debug::serial(), "R0: 0x{:x}\n", *frameptr.offset(0)).unwrap();
    write!(debug::serial(), "R1: 0x{:x}\n", *frameptr.offset(1)).unwrap();
    write!(debug::serial(), "R2: 0x{:x}\n", *frameptr.offset(2)).unwrap();
    write!(debug::serial(), "R3: 0x{:x}\n", *frameptr.offset(3)).unwrap();
    write!(debug::serial(), "R4: 0x{:x}\n", r4).unwrap();
    write!(debug::serial(), "R5: 0x{:x}\n", r5).unwrap();
    write!(debug::serial(), "R6: 0x{:x}\n", r6).unwrap();
    write!(debug::serial(), "R7: 0x{:x}\n", r7).unwrap();
    write!(debug::serial(), "R8: 0x{:x}\n", r8).unwrap();
    write!(debug::serial(), "R9: 0x{:x}\n", r9).unwrap();
    write!(debug::serial(), "R10: 0x{:x}\n", r10).unwrap();
    write!(debug::serial(), "R11: 0x{:x}\n", r11).unwrap();
    write!(debug::serial(), "R12: 0x{:x}\n", *frameptr.offset(4)).unwrap();
    write!(debug::serial(), "SP: 0x{:x}\n", frameptr as usize - 8).unwrap();
    write!(debug::serial(), "LR: 0x{:x}\n", *frameptr.offset(5)).unwrap();
    write!(debug::serial(), "xPSR: 0x{:x}\n", *frameptr.offset(7)).unwrap();
    loop {
    }
}

#[no_mangle]
#[allow(non_snake_case)]
pub unsafe extern "C" fn Handle_NMI() {
    panic!("NMI");
}
#[no_mangle]
#[allow(non_snake_case)]
pub unsafe extern "C" fn Handle_MemManage() {
    panic!("MemManage");
}
#[no_mangle]
#[allow(non_snake_case)]
pub unsafe extern "C" fn Handle_BusFault() {
    panic!("BusFault");
}
#[no_mangle]
#[allow(non_snake_case)]
pub unsafe extern "C" fn Handle_UsageFault() {
    panic!("UsageFault");
}


#[no_mangle]
pub unsafe extern "C" fn special_int() {
    panic!("special_int");
}

#[allow(non_snake_case)]
pub unsafe extern "C" fn Handle_TIM2() {
    // clear the interrupt
    stm32f2::TIM2().s.set_uif(false);
}

#[allow(non_snake_case)]
pub unsafe extern "C" fn Handle_EXTI9_5() {
    // clear the interrupt
    stm32f2::EXTI().p.write_bit(7, true);
    //println!("Handle_EXT9_5");
}

#[allow(non_snake_case)]
pub unsafe extern "C" fn Handle_DMA2_Stream0() {
    // clear the interrupt
    println!("Handle_DMA2_Stream0");
    let stream = stm32f2::DMA2().stream(0);
    if stream.tcif() {
        println!("  Transfer complete");
        stream.clear_tcif();
    }
    if stream.htif() {
        println!("  Half transfer complete");
        stream.clear_htif();
    }
    if stream.teif() {
        println!("  Transfer error");
        stream.clear_teif();
    }
    if stream.dmeif() {
        println!("  direct mode error interrupt");
        stream.clear_dmeif();
    }
    if stream.feif() {
        println!("  fifo error interrupt");
        stream.clear_feif();
    }
}


#[cfg(not(feature = "devicetest"))]
struct DebounceMachine<'a, F: FnMut(bool)> {
    sched: &'a RefScheduler<'a, DebounceMachine<'a, F>>,
    slot: Slot,
    last_state: bool,
    result: bool,
    pub changed: Option<&'a mut F>,
}

#[cfg(not(feature = "devicetest"))]
impl<'a, F: FnMut(bool)> DebounceMachine<'a, F> {
    pub fn new(sched: &'a RefScheduler<'a, DebounceMachine<'a, F>>, initial_state: bool) -> Self {
        DebounceMachine {
            sched: sched,
            slot: sched.new_slot(),
            last_state: initial_state,
            result: initial_state,
            changed: None,
        }
    }
    pub fn set_input(&mut self, state: bool) {
        if state == self.last_state {
            return;
        }
        self.last_state = state;
        self.sched.set(&self.slot, When::AfterMs(5), Self::timer_fired);
    }
    fn timer_fired(&mut self) {
        if self.result != self.last_state {
            self.result = self.last_state;
            if let Some(ref mut changed) = self.changed {
                (*changed)(self.result);
            }
        }
    }
}

#[derive(Copy, Clone, PartialEq)]
#[cfg(not(feature = "devicetest"))]
struct LetterOutput {
    motor_on: bool,
    motor_step: bool,
}

#[derive(Copy, Clone, Debug, PartialEq)]
#[cfg(not(feature = "devicetest"))]
enum LetterState {
    Unknown,
    Known(u8),
}

#[cfg(not(feature = "devicetest"))]
const MOTOR_PULSE_DUR: usize = 125; // 125ms
#[cfg(not(feature = "devicetest"))]
const NUM_CHARS: u8 = 40;

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
#[cfg(not(feature = "devicetest"))]
struct MotorState {
    dir: bool,
    on: bool,
    is_power_needed: bool,
}

#[cfg(not(feature = "devicetest"))]
struct LetterMachine<'a> {
    sched: &'a RefScheduler<'a, LetterMachine<'a>>,

    state: LetterState,
    desired_state: LetterState,
    motor_timer: Slot,
    sample_timer: Slot,
    reset: bool,
    index: u32,

    out_motor_state: ObservableValue<'a, MotorState>,
}
#[cfg(not(feature = "devicetest"))]
impl<'a> LetterMachine<'a> {
    pub fn new(sched: &'a RefScheduler<'a, Self>, index: u32) -> Self {
        LetterMachine {
            sched: sched,
            state: LetterState::Unknown,
            desired_state: LetterState::Unknown,
            motor_timer: sched.new_slot(),
            sample_timer: sched.new_slot(),
            out_motor_state: ObservableValue::new(MotorState{dir: false, on: false, is_power_needed: false}),
            reset: false,
            index: index,
        }
    }

    pub fn set_desired_state(&mut self, desired_state: LetterState, delay_ms: u16) {
        self.desired_state = desired_state;
        let prev_motor_state = self.out_motor_state.get();
        self.out_motor_state.set(MotorState{is_power_needed: self.state != self.desired_state, ..prev_motor_state});
        if !self.out_motor_state.get().on {
            self.sched.set(&self.motor_timer, When::AfterMs(delay_ms), Self::rotate_if_necessary);
        }
    }

    pub fn clear_reset(&mut self) {
        self.reset = false;
    }

    pub fn shaft_reset_changed(&mut self, value: bool) {
        // When the line is pulled low while we're running, we are at letter 0
        if !value && self.out_motor_state.get().is_power_needed {
            //self.state = LetterState::Known(0);
            self.reset = true;
            if self.index == 5 {
                println!("shaft_reset_changed (reset) for 5: {:?}", self.state);
            }
        }
    }
    pub fn increment(&mut self, delay_ms: u16) {
        let prev_motor_state = self.out_motor_state.get();
        self.out_motor_state.set(MotorState{is_power_needed: true, ..prev_motor_state});
        self.sched.set(&self.motor_timer, When::AfterMs(delay_ms as u16), Self::start_inc);
    }
    fn start_inc(&mut self) {
        let prev_motor_state = self.out_motor_state.get();
        self.out_motor_state.set(MotorState{on: true, dir: !prev_motor_state.dir, ..prev_motor_state});
        self.sched.set(&self.motor_timer, When::AfterMs(MOTOR_PULSE_DUR as u16), Self::turn_off);
    }
    fn turn_off(&mut self) {
        let prev_motor_state = self.out_motor_state.get();
        self.out_motor_state.set(MotorState{on: false, dir: prev_motor_state.dir, is_power_needed: false});
    }
    fn rotate_if_necessary(&mut self) {
        if self.reset {
            self.state = LetterState::Known(0);
            self.reset = false;
        }
        let prev_motor_state = self.out_motor_state.get();
        if self.desired_state == LetterState::Unknown || self.desired_state == self.state {
            self.out_motor_state.set(MotorState{on: false, dir: prev_motor_state.dir, is_power_needed: false});
            return;
        }
        self.out_motor_state.set(MotorState{on: true, dir: !prev_motor_state.dir, is_power_needed: true});
        if let LetterState::Known(ch) = self.state {
            self.state = LetterState::Known((ch + 1) % NUM_CHARS);
        }
        self.sched.set(&self.motor_timer, When::AfterMs(MOTOR_PULSE_DUR as u16), Self::rotate_if_necessary);
        self.sched.set(&self.sample_timer, When::AfterMs(5 as u16), Self::clear_reset);
    }
}

extern "C" {
    // The address of the .data section on flash
    static _data_load: u32;

    // The start and end addresses where .data should be copied
    static mut _data: u32;
    static mut _edata: u32;

    // The address of the .init_vector section on flash
    static _init_vector_load: u32;

    // The start and end addresses where .init_vector should be copied
    static mut _init_vector_start: u32;
    static mut _init_vector_end: u32;

    // The start and end addresses of .bss
    static mut _bss: u32;
    static mut _ebss: u32;
}

#[cfg(not(feature = "devicetest"))]
static ASCII_LETTER_TABLE: [u8; 128] = [
    0, 0, 0, 0, 0, 0, 0, 0,           0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,           0, 0, 0, 0, 0, 0, 0, 0,
    0, 38, 0, 0, 0, 0, 0, 0,          0, 0, 0, 0, 0, 37, 38, 39,
    10, 1, 2, 3, 4, 5, 6, 7,          8, 9, 0, 0, 0, 0, 0, 38,
    0, 11, 12, 13, 14, 15, 16, 17,    18, 19, 20, 21, 22, 23, 24, 25,
    26, 27, 28, 29, 30, 31, 32, 33,   34, 35, 36, 0, 0, 0, 0, 0,
    0, 11, 12, 13, 14, 15, 16, 17,    18, 19, 20, 21, 22, 23, 24, 25,
    26, 27, 28, 29, 30, 31, 32, 33,   34, 35, 36, 0, 0, 0, 0, 0
];

#[cfg(not(feature = "devicetest"))]
const NA: usize = !0;

macro_rules! BATMAN {
    () => {NA}
}

#[cfg(not(feature = "devicetest"))]
static CHANNEL_INDEX_TABLE: [usize; 96] = [
    NA,  0,  1,  2,  3,  4,  5,  6, NA, NA, NA, NA,
     7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17, NA,
    NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA,
    NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA,
    NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA,
    NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA,
    NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA,
    NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, BATMAN!(),
];

#[cfg(not(feature = "devicetest"))]
fn increment_all<'a>(letters: &'a [Option<&RefCell<LetterMachine>>], power: &mut Power<'a>) {
    power.trigger(move || {
        for (i, letter) in letters.iter().enumerate() {
            let text_index = CHANNEL_INDEX_TABLE[i];
            if let Some(letter) = *letter {
                if text_index != NA {
                    letter.borrow_mut().increment((text_index * MOTOR_PULSE_DUR / 16) as u16);
                }
            }
        }
    });
}

#[cfg(not(feature = "devicetest"))]
struct FixedString<'a> {
    s: &'a mut [u8; 96],
    len: usize,
}
#[cfg(not(feature = "devicetest"))]
impl<'a> FixedString<'a> {
    pub fn new<'b>(buf: &'b mut [u8; 96], init: &str) -> Result<FixedString<'b>, &'static str> {
        if init.len() > 96 {
            return Err("Length too long");
        }
        buf[0..init.len()].copy_from_slice(init.as_bytes());
        Ok(FixedString{s: buf, len: init.len()})
    }
    pub fn get(&self) -> &str {
        // This is always valid, since the byte array always came from an str in the first place.
        core::str::from_utf8(&self.s[0..self.len]).unwrap()
    }
    pub fn set(&mut self, value: &str) -> Result<(), &'static str> {
        if value.len() > 96 {
            return Err("Length too long");
        }
        self.s[0..value.len()].copy_from_slice(value.as_bytes());
        self.len = value.len();
        Ok(())
    }
}

#[cfg(not(feature = "devicetest"))]
fn set_letters_text<'a>(letters: &'a [Option<&RefCell<LetterMachine>>], text: &'a RefCell<FixedString<'a>>, power: &mut Power<'a>) {
    power.trigger(move || {
        let fs = text.borrow();
        println!("text is {} {}", fs.get(), fs.get().len());
        let text_bytes = fs.get().as_bytes();
        for (i, letter) in letters.iter().enumerate() {
            if let Some(letter) = *letter {
                let text_index = CHANNEL_INDEX_TABLE[i];
                if text_index == NA {
                    continue;
                }
                let ch = *text_bytes.get(text_index).unwrap_or(&(' ' as u8));
                let letter_pos = *ASCII_LETTER_TABLE.get(ch as usize).unwrap_or(&0);
                println!("Setting ch '{}' as {}", ch as char, letter_pos);
                letter.borrow_mut().set_desired_state(LetterState::Known(letter_pos), (text_index * MOTOR_PULSE_DUR / 16) as u16);
                //letter.borrow_mut().set_desired_state(LetterState::Known(letter_pos), 0);
            }
        }
    });
}

pub unsafe extern "C" fn do_stuff(val: &mut u32) {
    println!("inside do_stuff with val: {}", val);
    *val = 35
}


// We have to write to the peripheral port 32 bits at a time, and the DMA controller can repack
// words but can't zero-pad.  So the input data has to be a u32 array as well.  :-/
#[cfg(not(feature = "devicetest"))]
static mut SAMPLES_1: [u32; 192] = [32767; 192];
#[cfg(not(feature = "devicetest"))]
static mut SAMPLES_2: [u32; 192] = [32767; 192];

#[allow(non_snake_case)]
pub unsafe extern "C" fn Handle_TIM3() {
    panic!("timer watchdog triggered");
}

#[allow(non_snake_case)]
pub unsafe extern "C" fn Handle_DMA1_Stream5(dac: &AtomicRefCell<Dac>) {
    // clear the interrupt
    //println!("Handle_DMA1_Stream5");
    let stream = stm32f2::DMA1().stream(5);
    if stream.htif() {
        stream.clear_htif();
    }
    if stream.tcif() {
        //println!("  Transfer complete");
        stream.clear_tcif();
        dac.borrow_mut().handle_dma_irq();
    }
    if stream.teif() {
        println!("  Transfer error");
        stream.clear_teif();
    }
    if stream.dmeif() {
        println!("  direct mode error");
        stream.clear_dmeif();
    }
    if stream.feif() {
        println!("  fifo error");
        stream.clear_feif();
    }
}

#[repr(C)]
pub struct DebugBufferState {
    buffer: [u8; 4096],
    meta: debug::DebugBufferMeta,
}

#[no_mangle]
#[link_section = ".pdata"]
static mut __GLOBAL_DEBUG : DebugBufferState = DebugBufferState{
    buffer: [0u8; 4096],
    meta: debug::DebugBufferMeta::new()
};

#[no_mangle]
pub unsafe extern "C" fn begin() {
    use stm32f2::GpioMode::{AlternateOut};
    use stm32f2::GpioOutSpeed::{High};
    use stm32f2::GpioOutType::{PushPull};
    use stm32f2::{GpioPull};
    use stm32f2::{GPIOA, NVIC, RCC, SCB};

    NVIC().clear();
    SCB().vtor.write(__INIT_VECTOR.as_ptr() as u32);
    {
        // Clear the .bss section
        let p = &mut _bss as *mut u32;
        let e = &mut _ebss as *mut u32;
        let len = (e as usize - p as usize) >> 2;
        core::intrinsics::write_bytes(p, 0, len);
    }
    {
        // copy the .init_vector section from flash
        let data = &mut _init_vector_start as *mut u32;
        let edata = &mut _init_vector_end as *mut u32;
        let data_load = &_init_vector_load as *const u32;

        let len = (edata as usize - data as usize) >> 2;
        core::intrinsics::copy_nonoverlapping(data_load, data, len);
    }
    {
        // copy the .data section from flash
        let data = &mut _data as *mut u32;
        let edata = &mut _edata as *mut u32;
        let data_load = &_data_load as *const u32;

        let len = (edata as usize - data as usize) >> 2;
        core::intrinsics::copy_nonoverlapping(data_load, data, len);
    }

    // Enable serial port for early debugging
    RCC().apb2en.update().set_usart1en(true);
    GPIOA().pin(9).configured(AlternateOut(7, PushPull, High), GpioPull::None);
    debug::configure_serial_port();
    write!(debug::serial(), "--------------------------------------\n\
                            Serial port initialized\n").unwrap();

    post_init();
}

#[cfg(feature = "devicetest")]
struct TestReader {}
#[cfg(feature = "devicetest")]
impl debug::Reader for TestReader {
    fn have_new_data(&self) {}
}
#[cfg(feature = "devicetest")]
static READER: TestReader = TestReader{};

#[cfg(feature = "devicetest")]
unsafe fn set_breakpoint(break_addr: usize, enabled: bool) {
    let addr_bits = ((break_addr & 0x1ffffffc) >> 2) as u32;
    let mode = if break_addr & 0x02 != 0 { 0x2 } else { 0x1 };

    // 0 is an instruction comparator, and thus can set BKPT
    stm32f2::FPB().fp_comp[0].reset()
        .set_replace(mode)
        .set_comp(addr_bits)
        .set_enable(enabled);
}

#[cfg(feature = "devicetest")]
#[allow(non_snake_case)]
pub unsafe extern "C" fn Handle_Debug_Monitor() {
    set_breakpoint(0, false);
    stm32f2::GPIOA().pin(1).set(false);
    stm32f2::GPIOA().pin(2).set(true);
    stm32f2::GPIOA().pin(3).set(true);
    print!("Debug_Monitor\n");
    stm32f2::GPIOA().pin(2).set(false);
    stm32f2::GPIOA().pin(3).set(false);
    set_breakpoint(SECOND_BREAK_ADDR, SECOND_BREAK_ADDR != 0);
    SECOND_BREAK_ADDR = 0;
}

#[cfg(feature = "devicetest")]
static mut SECOND_BREAK_ADDR: usize = 0;

#[cfg(feature = "devicetest")]
struct Instructions {
    current: *const u16,
    end: *const u16,
}
#[cfg(feature = "devicetest")]
impl Instructions {
    pub fn new(start: &u16, end: &u16) -> Self {
        Self{
            current: start as *const u16,
            end: end as *const u16,
        }
    }
}
#[cfg(feature = "devicetest")]
impl core::iter::Iterator for Instructions {
    type Item = usize;

    fn next(&mut self) -> Option<usize> {
        let ret = if self.current < self.end {
            Some(self.current as usize)
        } else {
            None
        };

        // 32-bit instructions have the top 5 bits set to one of these three patterns, per the
        // ARMv7-M architecture reference manual.  The rest are 16-bit.
        let offset = match (unsafe { *self.current } & 0xf800) >> 11 {
            0x1d => 2,
            0x1e => 2,
            0x1f => 2,
            _ => 1,
        };
        self.current = unsafe { self.current.offset(offset) };
        ret
    }
}

#[cfg(feature = "devicetest")]
extern "C" {
    #[link_name = "__BUFFER_SOME_START"]
    static BUFFER_SOME_FUNCTION_START: u16;
    #[link_name = "__BUFFER_SOME_END"]
    static BUFFER_SOME_FUNCTION_END: u16;
}

#[cfg(feature = "devicetest")]
pub unsafe extern "C" fn post_init() {
    use stm32f2::{RCC, GPIOA, FPB, DEMCB, DWT};
    use stm32f2::GpioMode::{Output};
    use stm32f2::GpioOutSpeed::{Low};
    use stm32f2::GpioOutType::{PushPull};
    use stm32f2::{GpioPull};

    RCC().ahb1en.update()
        .set_otghsen(true)
        .set_gpioaen(true)
        .set_gpioben(true)
        .set_gpiocen(true)
        .set_gpioden(true)
        .set_dma1en(true)
        .set_dma2en(true);

    RCC().apb2en.update()
        .set_sdioen(true);

    RCC().apb1en.update()
        .set_tim7en(true)
        .set_tim6en(true)
        .set_dacen(true)
        .set_i2c1en(true)
        .set_tim5en(true)
        .set_tim4en(true)
        .set_tim3en(true)
        .set_tim2en(true);

    // Enable the DWT-based cycle counter for busywait
    stm32f2::DEMCB().demcr.reset().set_trcena(true);

    let mut red = GPIOA().pin(1).configured(Output(PushPull, Low, false), GpioPull::None);
    let mut green = GPIOA().pin(2).configured(Output(PushPull, Low, true), GpioPull::None);
    let mut blue = GPIOA().pin(3).configured(Output(PushPull, Low, false), GpioPull::None);
    red.set(false);
    green.set(false);
    blue.set(false);

    // Start out with the breakpoints disabled, in case they were enabled at the previous reset.
    FPB().fp_ctrl.set_key_enable(0x2);
    // DO NOT just call set_mon_en, as that tries to bitband, which doesn't work for this address.
    DEMCB().demcr.reset().set_mon_en(false);
    set_breakpoint(0, false);

    let readers = arena::GLOBAL_ARENA.place_mut([
        None, None, None, None]);

    __GLOBAL_DEBUG.meta = debug::DebugBufferMeta::new();

    let debug_buffer = arena::GLOBAL_ARENA.place(debug::DebugBuffer::new(
        &mut __GLOBAL_DEBUG.buffer,
        &__GLOBAL_DEBUG.meta, readers));
    let id = debug_buffer.register_reader(&READER, false);

    // Need to do this so that the println!() in the Debug_Monitor interrupt handler can find it
    debug::DEBUG = Some(debug_buffer);

    let mut w = IndependentWatchdog{};

    fill_buffer(debug_buffer, id, &mut red, &mut green, &mut blue, &mut w);

    DWT().busywait_ms(1500);

    red.set(false);
    green.set(false);
    blue.set(false);

    // Re-disable all the breakpoints.
    FPB().fp_ctrl.set_key_enable(0x2);
    // DO NOT just call set_mon_en, as that tries to bitband, which doesn't work for this address.
    DEMCB().demcr.reset().set_mon_en(false);
    set_breakpoint(0, false);
    SECOND_BREAK_ADDR = 0;

    debug_buffer.reset_for_testing();

    overfill_buffer(debug_buffer, id, &mut red, &mut green, &mut blue, &mut w);

    DWT().busywait_ms(1500);

    red.set(false);
    green.set(false);
    blue.set(false);

    // Re-disable all the breakpoints.
    FPB().fp_ctrl.set_key_enable(0x2);
    // DO NOT just call set_mon_en, as that tries to bitband, which doesn't work for this address.
    DEMCB().demcr.reset().set_mon_en(false);
    set_breakpoint(0, false);
    SECOND_BREAK_ADDR = 0;

    debug_buffer.reset_for_testing();

    n_squared_test(debug_buffer, id, &mut red, &mut green, &mut blue, &mut w);

    DWT().busywait_ms(1500);

    red.set(false);
    green.set(false);
    blue.set(false);

    // Re-disable all the breakpoints.
    FPB().fp_ctrl.set_key_enable(0x2);
    // DO NOT just call set_mon_en, as that tries to bitband, which doesn't work for this address.
    DEMCB().demcr.reset().set_mon_en(false);
    set_breakpoint(0, false);
    SECOND_BREAK_ADDR = 0;

    debug_buffer.reset_for_testing();

    loop {}
}

#[cfg(feature = "devicetest")]
unsafe extern "C" fn fill_buffer(debug_buffer: &debug::DebugBuffer, id: debug::ReaderId, red: &mut stm32f2::GpioPin, green: &mut stm32f2::GpioPin, blue: &mut stm32f2::GpioPin, w: &mut IndependentWatchdog) {
    use stm32f2::{FPB, DEMCB};
    write!(debug::serial(), "starting fill_buffer test\n").unwrap();

    blue.set(true);

    let mut data: MaybeUninit<[u8; 4096]> = MaybeUninit::new();
    for i in 0..4096 {
        *data.as_mut_ptr().add(i) = (i & 0xff) as u8;
    }
    let mut data = data.assume_init();

    FPB().fp_ctrl.set_key_enable(0x3);
    // DO NOT just call set_mon_en, as that tries to bitband, which doesn't work for this address.
    DEMCB().demcr.reset().set_mon_en(true);

    for &start_address in [0, 2048, 3000, 4096].iter() {
        w.reset();

        for break_addr in Instructions::new(&BUFFER_SOME_FUNCTION_START, &BUFFER_SOME_FUNCTION_END) {
            red.set(false);
            green.set(false);
            blue.set(false);

            debug_buffer.reset_for_testing();
            debug_buffer.buffer(&data[0..start_address]);
            {
                let state = debug_buffer.get_bytes(id);
                debug_buffer.bytes_consumed(state.buffer.len() + state.overran, id);
            }
            {
                let state = debug_buffer.get_bytes(id);
                debug_buffer.bytes_consumed(state.buffer.len() + state.overran, id);
            }

            set_breakpoint(break_addr, true);

            debug_buffer.buffer(&data[0..4082]);

            set_breakpoint(0, false);

            let state = debug_buffer.get_bytes(id);
            let (head, allocated, readers) = debug_buffer.internal_bits();
            for r in readers.iter() {
                if let Some(ref r) = *r {
                    let t = r.tail();
                    if t != start_address {
                        write!(debug::serial(), "--- Failed at {}; tail = {}, break={:#x}\n", start_address, t, break_addr).unwrap();
                        red.set(false);
                        green.set(true);
                        blue.set(true);
                        loop {}
                    }
                }
            }
            if head != allocated {
                write!(debug::serial(), "--- Failed at {}; head = {}, allocated = {}\n", start_address, head, allocated).unwrap();
                red.set(false);
                green.set(true);
                blue.set(true);
                loop {}
            }
            if state.overran != 0 {
                write!(debug::serial(), "--- Failed at {}; overran = {}\n", start_address, state.overran).unwrap();
                red.set(false);
                green.set(true);
                blue.set(true);
                loop {}
            }
            let total1 = state.buffer.len();
            debug_buffer.bytes_consumed(total1, id);
            let state = debug_buffer.get_bytes(id);
            let (head, allocated) = debug_buffer.internal_bits();
            if head != allocated {
                write!(debug::serial(), "--- Failed at {}; head = {}, allocated = {}\n", start_address, head, allocated).unwrap();
                red.set(false);
                green.set(true);
                blue.set(true);
                loop {}
            }
            if state.overran != 0 {
                write!(debug::serial(), "--- Failed at {}; overran = {}\n", start_address, state.overran).unwrap();
                red.set(false);
                green.set(true);
                blue.set(true);
                loop {}
            }
            let total2 = state.buffer.len();
            if total1 + total2 != 4082 && total1 + total2 != 4096 {
                write!(debug::serial(), "--- Failed at {}, len1 = {}, len2 = {}\n", start_address, total1, total2).unwrap();
                red.set(false);
                green.set(true);
                blue.set(true);
                loop {}
            }

            debug_buffer.bytes_consumed(total2, id);
        }

        write!(debug::serial(), "success at {}!\n", start_address).unwrap();
    }

    red.set(true);
    green.set(false);
    blue.set(true);
}

#[cfg(feature = "devicetest")]
unsafe extern "C" fn overfill_buffer(debug_buffer: &debug::DebugBuffer, id: debug::ReaderId, red: &mut stm32f2::GpioPin, green: &mut stm32f2::GpioPin, blue: &mut stm32f2::GpioPin, w: &mut IndependentWatchdog) {
    use stm32f2::{FPB, DEMCB};
    write!(debug::serial(), "starting overfill_buffer test\n").unwrap();

    blue.set(true);

    let mut data: MaybeUninit<[u8; 4150]> = MaybeUninit::new();
    for i in 0..4150 {
        *data.as_mut_ptr().add(i) = (i & 0xff) as u8;
    }
    let mut data = data.assume_init();

    FPB().fp_ctrl.set_key_enable(0x3);
    // DO NOT just call set_mon_en, as that tries to bitband, which doesn't work for this address.
    DEMCB().demcr.reset().set_mon_en(true);

    for &start_address in [0, 2048, 3000, 4096].iter() {
        w.reset();

        for break_addr in Instructions::new(&BUFFER_SOME_FUNCTION_START, &BUFFER_SOME_FUNCTION_END) {
            red.set(false);
            green.set(false);
            blue.set(false);

            debug_buffer.reset_for_testing();
            debug_buffer.buffer(&data[0..start_address]);
            {
                let state = debug_buffer.get_bytes(id);
                debug_buffer.bytes_consumed(state.buffer.len() + state.overran, id);
            }
            {
                let state = debug_buffer.get_bytes(id);
                debug_buffer.bytes_consumed(state.buffer.len() + state.overran, id);
            }

            set_breakpoint(break_addr, true);

            debug_buffer.buffer(&data[0..4150]);

            set_breakpoint(0, false);

            let state = debug_buffer.get_bytes(id);
            let (head, allocated, readers) = debug_buffer.internal_bits();
            for r in readers.iter() {
                if let Some(ref r) = *r {
                    let t = r.tail();
                    if t != start_address {
                        write!(debug::serial(), "--- Failed at {}; tail = {}\n", start_address, t).unwrap();
                        red.set(false);
                        green.set(true);
                        blue.set(true);
                        loop {}
                    }
                }
            }
            if head != allocated {
                write!(debug::serial(), "--- Failed at {}; head = {}, allocated = {}\n", start_address, head, allocated).unwrap();
                red.set(false);
                green.set(true);
                blue.set(true);
                loop {}
            }
            if state.overran != 54 && state.overran != 68 {
                write!(debug::serial(), "--- Failed at {}; overran = {}\n", start_address, state.overran).unwrap();
                red.set(false);
                green.set(true);
                blue.set(true);
                loop {}
            }
            let total1 = state.buffer.len();
            debug_buffer.bytes_consumed(total1 + state.overran, id);
            let state = debug_buffer.get_bytes(id);
            let (head, allocated) = debug_buffer.internal_bits();
            if head != allocated {
                write!(debug::serial(), "--- Failed at {}; head = {}, allocated = {}\n", start_address, head, allocated).unwrap();
                red.set(false);
                green.set(true);
                blue.set(true);
                loop {}
            }
            if state.overran != 0 {
                write!(debug::serial(), "--- Failed at {}; overran = {}\n", start_address, state.overran).unwrap();
                red.set(false);
                green.set(true);
                blue.set(true);
                loop {}
            }
            let total2 = state.buffer.len();
            if total1 + total2 != 4096 {
                write!(debug::serial(), "--- Failed at {}, len1 = {}, len2 = {}\n", start_address, total1, total2).unwrap();
                red.set(false);
                green.set(true);
                blue.set(true);
                loop {}
            }

            debug_buffer.bytes_consumed(total2, id);
        }

        write!(debug::serial(), "success at {}!\n", start_address).unwrap();
    }

    red.set(true);
    green.set(false);
    blue.set(true);
}

#[cfg(feature = "devicetest")]
unsafe extern "C" fn n_squared_test(debug_buffer: &debug::DebugBuffer, id: debug::ReaderId, red: &mut stm32f2::GpioPin, green: &mut stm32f2::GpioPin, blue: &mut stm32f2::GpioPin, w: &mut IndependentWatchdog) {
    use stm32f2::{FPB, DEMCB};
    write!(debug::serial(), "starting N**2 test\n").unwrap();

    blue.set(true);

    let mut data: MaybeUninit<[u8; 4096]> = MaybeUninit::new();
    for i in 0..4096 {
        *data.as_mut_ptr().add(i) = (i & 0xff) as u8;
    }
    let mut data = data.assume_init();

    red.set(true);
    // Start out pointing to the middle
    debug_buffer.buffer(&data[0..2048]);
    green.set(true);
    debug_buffer.get_bytes(id);  // throw away the memory-locking wrapper

    write!(debug::serial(), "initial buffer finished, get_bytes returned\n").unwrap();

    debug_buffer.bytes_consumed(2048, id);

    let mut last_tail = 2048;

    FPB().fp_ctrl.set_key_enable(0x3);
    // DO NOT just call set_mon_en, as that tries to bitband, which doesn't work for this address.
    DEMCB().demcr.reset().set_mon_en(true);

    red.set(false);
    green.set(false);
    blue.set(false);

    for second_break_addr in Instructions::new(&BUFFER_SOME_FUNCTION_START, &BUFFER_SOME_FUNCTION_END) {
        w.reset();

        for break_addr in Instructions::new(&BUFFER_SOME_FUNCTION_START, &BUFFER_SOME_FUNCTION_END) {
            //write!(debug::serial(), "--- Start: {:x}\n", break_addr).unwrap();

            red.set(false);
            green.set(false);
            blue.set(true);

            SECOND_BREAK_ADDR = second_break_addr;
            set_breakpoint(break_addr, true);

            debug_buffer.buffer(&data[0..4068]);

            set_breakpoint(0, false);

            red.set(true);
            green.set(true);
            blue.set(false);

            let state = debug_buffer.get_bytes(id);
            let (head, allocated, readers) = debug_buffer.internal_bits();
            for r in readers.iter() {
                if let Some(ref r) = *r {
                    let t = r.tail();
                    if t != last_tail {
                        write!(debug::serial(), "--- Failed; tail = {}, wanted {}, addr = {:#x}, second_addr = {:#x}\n", t, last_tail, break_addr, second_break_addr).unwrap();
                        red.set(false);
                        green.set(true);
                        blue.set(true);
                        loop {}
                    }
                }
            }
            if head != allocated {
                write!(debug::serial(), "--- Failed; head = {}, allocated = {}\n", head, allocated).unwrap();
                red.set(false);
                green.set(true);
                blue.set(true);
                loop {}
            }
            if state.overran != 0 {
                write!(debug::serial(), "--- Failed; overran = {}\n", state.overran).unwrap();
                red.set(false);
                green.set(true);
                blue.set(true);
                loop {}
            }
            let total1 = state.buffer.len();
            debug_buffer.bytes_consumed(total1 + state.overran, id);
            let state = debug_buffer.get_bytes(id);
            let (head, allocated) = debug_buffer.internal_bits();
            if head != allocated {
                write!(debug::serial(), "--- Failed; head = {}, allocated = {}\n", head, allocated).unwrap();
                red.set(false);
                green.set(true);
                blue.set(true);
                loop {}
            }
            if state.overran != 0 {
                write!(debug::serial(), "--- Failed; overran = {}\n", state.overran).unwrap();
                red.set(false);
                green.set(true);
                blue.set(true);
                loop {}
            }
            let total2 = state.buffer.len();
            if total1 + total2 != 4068 && total1 + total2 != 4082 && total1 + total2 != 4096 {
                write!(debug::serial(), "--- Failed, len1 = {}, len2 = {}\n", total1, total2).unwrap();
                red.set(false);
                green.set(true);
                blue.set(true);
                loop {}
            }

            red.set(false);
            green.set(true);
            blue.set(false);

            //debug::serial().write_all("--- foo\n".as_bytes()).unwrap();

            debug_buffer.bytes_consumed(total2, id);
            last_tail += total2;

            red.set(true);
            green.set(false);
            blue.set(false);

            //write!(debug::serial(), "--- OK: {}\n", out1.len() + out2.len()).unwrap();
        }
        //write!(debug::serial(), "loop success\n").unwrap();
    }

    write!(debug::serial(), "success!\n").unwrap();
    red.set(true);
    green.set(false);
    blue.set(true);
}

#[allow(unused_imports)]
#[cfg(not(feature = "devicetest"))]
pub unsafe extern "C" fn post_init() {
    use stm32f2::GpioMode::{Input, Output, AlternateIn, AlternateOut, Analog};
    use stm32f2::GpioOutSpeed::{Low, Medium, Fast, High};
    use stm32f2::GpioOutType::{PushPull, OpenDrain};
    use stm32f2::{GpioIrqConfig, GpioPull};
    use stm32f2::{GPIOA, GPIOB, GPIOC, GPIOD, IWDG, NVIC, RCC, SCB, TIM2, TIM5, TIM6, USART1, DAC, DMA1, SDIO, I2C1, TIM3};

    let readers = arena::GLOBAL_ARENA.place_mut([
        None, None, None, None]);

    let debug_buffer = arena::GLOBAL_ARENA.place(debug::DebugBuffer::new(
        &mut __GLOBAL_DEBUG.buffer, &__GLOBAL_DEBUG.meta, readers));
    let serial_reader_id = debug_buffer.register_reader(&debug::SERIAL, true);
    debug::DEBUG = Some(debug_buffer);

    RCC().ahb1en.update()
        .set_otghsen(true)
        .set_gpioaen(true)
        .set_gpioben(true)
        .set_gpiocen(true)
        .set_gpioden(true)
        .set_dma1en(true)
        .set_dma2en(true);

    RCC().apb2en.update()
        .set_sdioen(true);

    RCC().apb1en.update()
        .set_tim7en(true)
        .set_tim6en(true)
        .set_dacen(true)
        .set_i2c1en(true)
        .set_tim5en(true)
        .set_tim4en(true)
        .set_tim3en(true)
        .set_tim2en(true);

    // Enable the DWT-based cycle counter for busywait
    stm32f2::DEMCB().demcr.reset().set_trcena(true);

    let led = Led::new().configured();
    led.set_color(Color::white());

    // connect RX pin to USART1
    //GPIOA().pin(10).configured(AlternateIn(7), GpioPull::None);
    let serial = arena::GLOBAL_ARENA.place(debug::SerialDma::new(debug_buffer));
    serial.configure();
    debug::SERIAL = Some(serial);
    // Send whatever data is in the buffer now
    debug_buffer.prime_reader(serial_reader_id);
    println!("============================================");

    println!("iwdg: {:#010x}, {:#010x}", IWDG().prescaler.read(), IWDG().reload.read());
    let value = RCC().pllcfg.read();
    // when src is true, use high-speed-external, which is 26MHz on the photon p0
    // when src is false, use high-speed-internal, which is 16MHz on stm32
    let input_clock = match value.src() { true => 26, false => 16 };
    let pll_in = input_clock / value.m();
    let vco = pll_in * value.n();
    let sysclk = vco / ((value.p() + 1) * 2);
    let periph = vco / value.q();
    println!("PLL config: src={} ({}MHz), m={} (PLLin={}MHz), n={} (VCO={}MHz), p={} (sysclk={}MHz), q={} (periph={}MHz)",
             value.src(), input_clock, value.m(), pll_in, value.n(), vco, value.p(), sysclk,
             value.q(), periph);

    TIM3().cr1.reset()
        .set_urs(true)     // interrupt only on overflow/underflow
        .set_dir(false);   // upcounter
    TIM3().die.reset()
        .set_uie(true);
    TIM3().cnt.write(0);   // 1.5s
    TIM3().ar.write(3000);   // 3s
    TIM3().psc.write(59999);  // 1 pulse per ms
    TIM3().eg.set_ug(true);
    TIM3().s.update()
        .set_tif(false)
        .set_uif(false);
    TIM3().cr1.set_cen(true);

    // DMA2 stream7 is the serial DMA interrupt, which should not be preempted by anything other
    // than the onewire stuff (which does not log).
    // Use the top 2 bits to control relative priority since we don't really know for sure how many
    // bits are honored.  (Though on stm32, it should be 4.)
    NVIC().set_all_prios_except(stm32f2::Interrupt::DMA2_Stream7, 192);
    NVIC().set_priority(stm32f2::Interrupt::TIM4, 0);
    NVIC().set_priority(stm32f2::Interrupt::DMA2_Stream7, 64);

    NVIC().enable(stm32f2::Interrupt::DMA2_Stream7);
    NVIC().enable(stm32f2::Interrupt::TIM6_DAC);
    NVIC().enable(stm32f2::Interrupt::TIM3);

    // SimpleTimer forces use of TIM7
    let timer7 = arena::GLOBAL_ARENA.place(SimpleTimer{}.configured());

    let usb = arena::GLOBAL_ARENA.place_mut(AtomicRefCell::new(Usb::new(UsbIo{
        data_plus_pin: GPIOB().pin(15),
        data_minus_pin: GPIOB().pin(14),
    }, debug_buffer)));

    println!("fancy_reg is 0x{:x}", stm32f2::fancy_reg);

    println!("Enable TIM2 interrupt");
    NVIC().enable(stm32f2::Interrupt::TIM2);
    println!("TIM2 interrupt enabled");

    println!("Enable TIM4 interrupt");
    NVIC().enable(stm32f2::Interrupt::TIM4);
    println!("TIM4 interrupt enabled");

    println!("Enable TIM5 interrupt");
    NVIC().enable(stm32f2::Interrupt::TIM5);
    println!("TIM5 interrupt enabled");

    println!("Enable EXT5 interrupt");
    NVIC().enable(stm32f2::Interrupt::EXTI9_5);
    println!("EXT5 interrupt enabled");

    NVIC().enable(stm32f2::Interrupt::DMA2_Stream0);

    {
        // Give the USB device 400ms for SET_CONFIGURATION to be called
        let usb_init_start_time = timer7.counter();
        while !usb.borrow().configured() {
            usb.borrow_mut().poll();
            if timer7.counter().wrapping_sub(usb_init_start_time) > 400 {
                println!("USB SET_CONFIGURATION not received within 400ms; continuing with startup");
                break;
            }
        }
    }

    let id = debug_buffer.register_reader(usb, false);
    usb.borrow_mut().set_reader_id(id);

    let wifi = arena::GLOBAL_ARENA.place(bcm43362::RefWifi::new(RefCell::new(bcm43362::Wifi::new(&timer7, bcm43362::WifiIo{
        sdio_ck: GPIOC().pin(12),
        sdio_cmd: GPIOD().pin(2),
        sdio_d0: GPIOC().pin(8),
        sdio_d1: GPIOC().pin(9),
        sdio_d2: GPIOC().pin(10),
        sdio_d3: GPIOC().pin(11),
        oob_irq: GPIOB().pin(0).configured(Input, GpioPull::None),

        pin_power: GPIOC().pin(7),
        pin_reset: GPIOC().pin(1),
        pin_32k_clk: GPIOB().pin(1),
    }))));

    let i2c_buf = arena::GLOBAL_ARENA.place_mut([0u8; 6]);
    let i2c = arena::GLOBAL_ARENA.place(RefCell::new(I2c::new(I2cConfig{
        sda: GPIOB().pin(7),
        scl: GPIOB().pin(6),
        mode: I2cMode::Fast,
    }, i2c_buf)));

    stm32f2::FLASH().optkey.write(0x08192A3B);
    stm32f2::FLASH().optkey.write(0x4C5D6E7F);
    println!("Flash optlock: {}", stm32f2::FLASH().optc.optlock());
    for i in 0..11 {
        println!("Flash write allowed #{}: {}", i, stm32f2::FLASH().optc.wrp(i));
    }

    let converter = arena::GLOBAL_ARENA.place(AtomicRefCell::new(audio::Converter::new()));

    // DMA controller 1, stream 5, channel 7, driven by timer 6
    let fill: &'static mut hal::dac::Callback = arena::GLOBAL_ARENA.place_mut(move |buf: &mut [u32]| {
        converter.borrow_mut().convert_to_buf(buf);
    });
    let dac: &AtomicRefCell<Dac> = arena::GLOBAL_ARENA.place(AtomicRefCell::new(Dac::new(GPIOA().pin(4), Some(fill))));
    let thunk = stm32f2::Thunk::new(Handle_DMA1_Stream5, dac);
    let _map = stm32f2::InterruptMapping::new(&thunk, stm32f2::Interrupt::DMA1_Stream5);
    NVIC().enable(stm32f2::Interrupt::DMA1_Stream5);

    run(&mut PhotonDevice{
        led: led,
        setup_button: GPIOC().pin(7).configured(Input, GpioPull::Up).with_interrupts(
            GpioIrqConfig{irq: true, wakeup: false, rising: true, falling: true}),
        onewire_pin: GPIOA().pin(10).configured(Output(OpenDrain, High, true), GpioPull::None),
        power: arena::GLOBAL_ARENA.place(RefCell::new(Power::new(PowerConfig{
            ps_on:  GPIOA().pin(0),
            pwr_ok: GPIOB().pin(5),
            // The highest number of characters we might have to advance happens when the letter is
            // on "1" -- right after " " -- and we want it to go to "/", right before " ", but we
            // don't know where it is.  It will have to go N-1 positions to " ", then N-1 more
            // positions to "Z".  Each advance will take MOTOR_PULSE_DUR milliseconds.
            on_time_ms: (MOTOR_PULSE_DUR as u32) * ((NUM_CHARS as u32) - 1) * 2,
        }))),

        watchdog: IndependentWatchdog{},

        timer7: &timer7,

        i2c: &i2c,
        nic: wifi,
        dac: dac,
        usb: usb,
        converter: converter,
    });
    loop {
    }
}

#[cfg(feature = "cortex")]
pub fn wfi() {
    unsafe {
        asm!("wfi" :::: "volatile");
    }
}

#[cfg(not(feature = "cortex"))]
pub fn wfi() {}

#[cfg(not(feature = "devicetest"))]
struct FlipboardNetsync<'a, NIC: 'a + network::NetworkInterface>{
    sched: &'a RefScheduler<'a, FlipboardNetsync<'a, NIC>>,
    ip_stack: &'a RefCell<Ipv6Stack<'a, NIC>>,
    keepalive_slot: scheduler::Slot,
}
#[cfg(not(feature = "devicetest"))]
impl<'a, NIC: 'a + network::NetworkInterface> FlipboardNetsync<'a, NIC> {
    fn new(sched: &'a RefScheduler<'a, FlipboardNetsync<'a, NIC>>,
           ip_stack: &'a RefCell<Ipv6Stack<'a, NIC>>) -> Self {
        FlipboardNetsync {
            sched: sched,
            ip_stack: ip_stack,
            keepalive_slot: sched.new_slot(),
        }
    }
    fn register(&mut self) {
        use flapserver::Writer;

        let dest = IpAddr::new([
            0x26, 0x04, 0xa8, 0x80,   0x00, 0x01, 0x00, 0x20,
            0x00, 0x00, 0x00, 0x00,   0x09, 0xe8, 0x20, 0x01]);
        let mut data = [0u8; 128];
        let len = {
            let mut writer = flapserver::ProtocolPacketWriter::new(&mut data);
            let start = writer.get_ptr();
            writer.write_register_client(1, |writer| {
                for buf in unsafe { &CONFIG.client_id }
                    .splitn(2, |&b| { b == 0 })
                    .take(1)
                    .filter(|x| { x.len() > 0 }) {

                    if let Ok(id) = core::str::from_utf8(buf) {
                        writer.write_field(flapserver::RegisterClientField::Id{value: id});
                    }
                }
                writer.write_field(flapserver::RegisterClientField::MacAddr{value: self.ip_stack.borrow().mac_addr().as_bytes()});
            }).get_ptr() - start
        };
        match self.ip_stack.borrow_mut().send(|ip_builder| { ip_builder
            .set_dest(dest)
            .edit_payload(|buf| { UdpPacketBuilder::new(buf)
                .set_src_port(4235)
                .set_dest_port(8082)
                .set_payload(&data[0..len])
                .complete()
            });
        }) {
            Ok(()) => {}
            Err(network::ErrorCode::NotYet) => {
                println!("FlipboardNetsync::register -> send: NotYet");
            }
        }
        self.sched.set(&self.keepalive_slot, When::AfterMs(10000), Self::register);
    }
}

#[cfg(not(feature = "devicetest"))]
#[derive(Eq, PartialEq)]
enum MessageCycleMode {
    Message,
    MotorTest,
}

#[cfg(not(feature = "devicetest"))]
struct WifiMonitor<'a, 'b, NIC: 'b + network::NetworkInterface> {
    wifi: &'b NIC,
    sched: &'a SimpleScheduler<'a>,
    join_slot: &'a Slot,
}

#[cfg(not(feature = "devicetest"))]
impl<'a, 'b, NIC: network::NetworkInterface> WifiMonitor<'a, 'b, NIC> {
    fn join(&'a self) {
        unsafe {
            if CONFIG.header == 0x30465053 && CONFIG.network_count > 0 {
                println!("Joining network");
                self.wifi.join(CONFIG.networks[0]);
            } else {
                println!("No wifi networks configured; please use dfu-util to write an \
                          instance of wifi::Config to 0x080ff000");
            }
        }
        self.sched.set(self.join_slot, When::AfterMs(30000), move || {
            println!("Wifi join timeout...trying again");
            self.join();
        });
    }
    fn notify_joined(&self) {
        self.sched.cancel(self.join_slot);
    }
}

#[cfg(not(feature = "devicetest"))]
fn connectivity_color(connectivity: Connectivity, counter: u16) -> Color {
    let blink = (counter & 0x1ff) > 0xff;
    match connectivity {
        Connectivity::NoLink => if blink { Color::magenta() } else { Color::red() },
        Connectivity::NoAddress => if blink { Color::yellow() } else { Color::red() },
        Connectivity::Connected => {
            let hue = (counter & 0xfff) >> 4;    // 0-1, 8 bits int, 8 bits frac, on a 4.096s cycle
            let hprime = (hue * 6) & 0x1ff;
            let diff = hprime as i16 - 0xff;
            let x = (0xff - if diff < 0 { -diff } else { diff }) as u8;
            match hue * 6 {
                0x000..=0x0ff => { Color::rgb(255, x, 0) },
                0x100..=0x1ff => { Color::rgb(x, 255, 0) },
                0x200..=0x2ff => { Color::rgb(0, 255, x) },
                0x300..=0x3ff => { Color::rgb(0, x, 255) },
                0x400..=0x4ff => { Color::rgb(x, 0, 255) },
                0x500..=0x5ff => { Color::rgb(255, 0, x) },
                _ => { Color::rgb(0, 0, 0) },
            }
        },
    }
}

#[cfg(not(feature = "devicetest"))]
const BITS_PER_WORD: usize = 32;  // Change this if motor_states' type changes!
#[cfg(not(feature = "devicetest"))]
const NUM_STATE_WORDS: usize = (NUM_CHARS as usize + BITS_PER_WORD - 1) / BITS_PER_WORD;

#[cfg(not(feature = "devicetest"))]
fn run<NIC: network::NetworkInterface>(dev: &mut PhotonDevice<NIC>) {
    println!("Config: network_count: {}", unsafe { CONFIG.network_count });
    println!("Config: ssid: {}", unsafe { CONFIG.networks[0].ssid });
    println!("Config: key: {}", unsafe { CONFIG.networks[0].wpa2_key });
    println!("Calling timer.configure");
    println!("timer.configure() finished");

    //let last_button = true;
    //let message: [u8; 4] = [35, 25, 38, 0];
    // _(1-9)0(A-Z)-./
    let func_queue = arena::GLOBAL_ARENA.place(utils::queue::FuncQueue::new());

    let onewire = hal::onewire::OneWireBus::new(&func_queue, dev.onewire_pin);
    onewire.borrow_mut().start();   // scan for temp sensors

    let sched = arena::GLOBAL_ARENA.place(SimpleScheduler::new(Time16(dev.timer7.counter() as u16)));
    let ip_config = network::state::Config{
        mac_addr: dev.nic.mac_addr(),
    };
    let connectivity_slot = sched.new_slot();
    let setup_debounce = make_machine(sched, |sched| DebounceMachine::new(
            sched, dev.setup_button.get()));

    let wifi_monitor = arena::GLOBAL_ARENA.place(WifiMonitor{
        wifi: dev.nic,
        sched: sched,
        join_slot: arena::GLOBAL_ARENA.place(sched.new_slot()),
    });

    dev.watchdog.reset();
    let ip_stack = make_machine(sched, |sched| Ipv6Stack::new(sched, dev.nic, ip_config));

    unsafe {
        dev.dac.borrow_mut().start_dma(&mut SAMPLES_1, &mut SAMPLES_2);
    }

    // By now the analog-in has taken effect, so enable the DAC
    dev.dac.borrow().enable();

    let i2c = &mut dev.i2c;
    i2c.borrow_mut().clear_bus();
    i2c.borrow_mut().reset();
    let connectivity = arena::GLOBAL_ARENA.place(RefCell::new(Connectivity::NoLink));

    let io_expanders = arena::GLOBAL_ARENA.place_mut(IoExpanders::new());
    let letters: &mut [Option<&RefCell<LetterMachine>>; 96] = arena::GLOBAL_ARENA.place_mut([None; 96]);
    let motor_states: &RefCell<[u32; NUM_STATE_WORDS]> = arena::GLOBAL_ARENA.place_mut(RefCell::new([0u32; NUM_STATE_WORDS]));
    for j in 0..EXPANDER_COUNT {
        let address = 0x40 + ((j as u8) << 1);
        let io_expander = arena::GLOBAL_ARENA.place(RefCell::new(Pca9698::new(dev.i2c, address)));
        for i in 24..28 {
            // Set chip select pins to high impedance; this sends power to the reset hall-effect sensors
            // instead of the the next-letter hall-effect sensors
            io_expander.borrow_mut().configure_pin(i, PinType::Input);
        }
        for i in 0..12 {
            let assignment = &IO_ASSIGNMENTS[i];
            let letter = make_machine(sched, |sched| LetterMachine::new(sched, i as u32));
            io_expander.borrow_mut().configure_pin(assignment.motor_in1, PinType::Output);
            io_expander.borrow_mut().configure_pin(assignment.motor_in2, PinType::Output);
            io_expander.borrow_mut().listen_input(assignment.sensor, arena::GLOBAL_ARENA.place_mut(move |val| {
                letter.borrow_mut().shaft_reset_changed(val);
            }));
            let index = j*12 + i;
            letter.borrow_mut().out_motor_state.changed = Some(arena::GLOBAL_ARENA.place_mut(move |motor_state: MotorState| {
                //println!("New motor state for {}: {:?}", i, motor_state);
                let mut io_expander = io_expander.borrow_mut();
                io_expander.set_pin(
                    assignment.motor_in1, if motor_state.on { motor_state.dir } else { false });
                io_expander.set_pin(
                    assignment.motor_in2, if motor_state.on { !motor_state.dir } else { false });
                motor_states.borrow_mut().set_bit_le(
                    index, motor_state.is_power_needed);
            }));
            letters[j*12 + i] = Some(letter);
        }

        while match io_expander.borrow_mut().update() {
            Ok(CycleState::InProgress) => {
                true
            },
            Ok(CycleState::Done) => {
                println!("Found IO Expander at address 0x{:02x}", address);
                io_expanders.add(io_expander);
                false
            },
            Err(err) => {
                println!("Error talking to IO expander at address 0x{:02x}: {}", address, err);
                false
            },
        } {}
    }
    let letters = &*letters;
    let power = dev.power;

    let message_buf = arena::GLOBAL_ARENA.place_mut([0u8; 96]);
    let message = arena::GLOBAL_ARENA.place(RefCell::new(FixedString::new(&mut *message_buf, "").unwrap()));

    println!("adding setup_debounce_listener");
    {
        let mut message_index = 0;
        let mut setup_down_time = 0;
        let mut cycle_mode = MessageCycleMode::Message;
        let timer = dev.timer7;

        setup_debounce.borrow_mut().changed = Some(arena::GLOBAL_ARENA.place_mut(move |value: bool| {
            let now = timer.counter() as u16;

            //if !value {
            //    onewire.borrow_mut().start();
            //}

            if !value {
                debug::hexdump("need more letters".as_bytes(), "  ");
            }

            if !value {
                setup_down_time = now;
                println!("Setting message to {}", FAKE_MESSAGES[message_index]);
                if cycle_mode == MessageCycleMode::MotorTest {
                    increment_all(letters, &mut power.borrow_mut());
                }
            }
            if value {
                if now.wrapping_sub(setup_down_time) > 1200 {
                    cycle_mode = match cycle_mode {
                        MessageCycleMode::MotorTest => MessageCycleMode::Message,
                        MessageCycleMode::Message => MessageCycleMode::MotorTest,
                    };
                    return;
                }
                if cycle_mode == MessageCycleMode::Message {
                    message.borrow_mut().set(FAKE_MESSAGES[message_index]).unwrap();
                    set_letters_text(letters, message, &mut power.borrow_mut());
                    message_index = (message_index + 1) & 0x3;
                }
            }
            //ip_stack.borrow_mut().ping_internet();
        }));
    }

    wifi_monitor.join();

    /*
    register_with_flapserver = Some(arena.place(move || {
    }));
    */

    let netsync = make_machine(sched, |sched| FlipboardNetsync::new(
            sched, ip_stack));

    ip_stack.borrow_mut().out_connectivity.changed = Some(arena::GLOBAL_ARENA.place_mut(move |new_connectivity| {
        *connectivity.borrow_mut() = new_connectivity;
        if new_connectivity == Connectivity::Connected {
            sched.set(&connectivity_slot, When::AfterMs(0), move || {
                println!("Calling register");
                netsync.borrow_mut().register();
            });
        }
    }));

    let converter = dev.converter;

    ip_stack.borrow_mut().udp_recv = Some(arena::GLOBAL_ARENA.place_mut(move |udp: &UdpPacket| {
        if udp.dest_port() == 4235 {
            if udp.payload().len() < 32 {
                println!("Received udp packet");
                debug::hexdump(udp.payload(), "  ");
            } else {
                //debug::hexdump(&udp.payload()[0..32], "  ");
            }
            for field in flapserver::ProtocolPacketReader::new(udp.payload()) {
                match field {
                    flapserver::ProtocolPacketField::SetMessage{value: set_message} => {
                        for field in set_message {
                            match field {
                                flapserver::SetMessageField::Message{value: text} => {
                                    println!("Setting message to {}", text);
                                    message.borrow_mut().set(if text.len() > 96 { &text[0..96] } else { text }).unwrap();
                                    set_letters_text(letters, message, &mut power.borrow_mut());
                                },
                                x => {
                                    println!("Unknown SetMessage field: {:?}", x);
                                },
                            }
                        }
                    },
                    flapserver::ProtocolPacketField::PlayAudio{value: play_audio} => {
                        let mut last_sample = 0i32;
                        let mut last_index = 0u32;
                        let mut seqno = 0u32;
                        for field in play_audio.clone() {
                            match field {
                                flapserver::PlayAudioField::SequenceNumber{value} => {
                                    seqno = value;
                                },
                                flapserver::PlayAudioField::LastSample{value} => {
                                    last_sample = value;
                                },
                                flapserver::PlayAudioField::Index{value} => {
                                    last_index = value;
                                },
                                flapserver::PlayAudioField::AdpcmData{value: _} => {},
                                x => {
                                    println!("Unknown PlayAudio field: {:?}", x);
                                },
                            }
                        }
                        for field in play_audio {
                            match field {
                                flapserver::PlayAudioField::AdpcmData{value: buf} => {
                                    // Disable the DAC handler interrupt while we run code that
                                    // borrows the atomic refcells, because the interrupt handler
                                    // mutably borrows them.
                                    //
                                    // TODO: maybe make the interrupt handler borrow a smaller
                                    // chunk of the buffer memory; this should always be out in
                                    // front of the conversion by a fair bit, so there should not
                                    // be multiple contexts using any individual byte...
                                    stm32f2::NVIC().disable(stm32f2::Interrupt::DMA1_Stream5);
                                    converter.borrow_mut().buffer(seqno as usize, last_sample, last_index, buf);
                                    stm32f2::NVIC().enable(stm32f2::Interrupt::DMA1_Stream5);
                                },
                                _ => {},
                            }
                        }
                    },
                    flapserver::ProtocolPacketField::RegisterClientResponse{value: _} => {
                        // We don't really care.  Response packets keep our address in the neighbor
                        // cache of the router, but don't really affect us much.  We don't need to
                        // use their absence to re-request a registration with the server, even,
                        // because we re-register every few seconds anyway.
                    },
                    flapserver::ProtocolPacketField::RegisterClient{value: register_client} => {
                        println!("Unexpected register_client message: {:?}", register_client);
                    },
                    x => {
                        println!("Unknown ProtocolPacket field: {:?}", x);
                    },
                }
            }
        }
    }));

    //let msg_counter: usize = 0;

    let mut packet_buf = [0u32; 512];
    let mut was_link_up = false;
    let mut loop_iterations = 0u32;
    let mut next_notify_counter = (dev.timer7.counter() as u16).wrapping_add(1000);
    let mut onewire_loops = 0u32;
    loop {
        dev.usb.borrow_mut().poll();
        dev.power.borrow_mut().poll();

        func_queue.run_next_func(());
        let counter = dev.timer7.counter() as u16;

        loop_iterations += 1;
        if next_notify_counter.wrapping_sub(counter) >= 0x8000 {
            next_notify_counter = next_notify_counter.wrapping_add(1000);
            println!("did {} iterations in 1 second", loop_iterations);
            loop_iterations = 0;
        }

        sched.notify_time(Time16(counter));
        dev.watchdog.reset();
        let color = connectivity_color(*connectivity.borrow(), counter);
        dev.led.set_color(color);

        io_expanders.update();

        let packet_len = dev.nic.recv(&mut packet_buf).len();
        if packet_len > 0 {
            if let Some(p) =  EthernetPacket::new(&packet_buf[4..]) {
                ip_stack.borrow_mut().raw_input(&p);
            }
        }
        if dev.nic.join_failure() {
            wifi_monitor.join();
        }
        let is_link_up = dev.nic.is_link_up();
        if is_link_up != was_link_up {
            was_link_up = is_link_up;
            println!("Link changed to {}", is_link_up);
            ip_stack.borrow_mut().notify_link_state(is_link_up);
            if is_link_up {
                wifi_monitor.notify_joined();
            }
        }

        let all_letters_done = motor_states.borrow().iter().all(|w| *w == 0);
        if all_letters_done && dev.power.borrow().pwr_ok() && dev.power.borrow().running() {
            println!("Shutting power off early");
            dev.power.borrow_mut().reset();
        }

        setup_debounce.borrow_mut().set_input(dev.setup_button.get());

        if onewire.borrow().conversion_done() {
            if let Some(temp) = onewire.borrow().last_temp_in_c() {
                println!("logging temp {}", temp);
                if temp != 85 {
                    if temp > 32 {
                        dev.power.borrow_mut().reset();
                    }
                    if temp > 37 {
                        panic!("Temperature too high: {} > 37", temp);
                    }
                }
            }
            onewire_loops += 1;
            if onewire_loops > 10 {
                onewire.borrow_mut().start();
                onewire_loops = 0;
            } else {
                onewire.borrow_mut().start_conversion();  // next temp sensor
            }
        }

        /*
        dev.letter.step(Time16(counter));
        
        let button = dev.setup_button.get();
        if button != last_button {
            last_button = button;
            if !button {
                //dev.letter.machine.manual_flip = true;
                dev.letter.machine.desired_state = LetterState::Known(message[msg_counter]);
                msg_counter = msg_counter + 1;
                if msg_counter >= message.len() {
                    msg_counter = 0;
                }
            }
        }
        */
    }

    /*
    let mut toggle = false;
    loop {
        dev.watchdog.reset();
        match dev.ev_queue.lock().pop_front() {
            Event::None => wfi(),
            Event::ToggleTimer => {
                println!("ToggleTimer");
                toggle = !toggle;
            },
            _ => {}
        }
        let button_down = dev.setup_button.get();
        dev.blue.set(toggle);
        dev.red.set(button_down);
        dev.green.set(button_down);
    }
    */
}

// dummy
fn main() {}
