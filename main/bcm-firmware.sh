#!/bin/bash

# Copyright 2018 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

if [ -z "$PARTICLE_FW_PATH" ] ; then
	echo "Need to find the spark firmware repository; set PARTICLE_FW_PATH" >&2
	exit 1
fi

# make relative paths work right
PARTICLE_FW_PATH=$(cd $PARTICLE_FW_PATH ; pwd)

DIR=$(mktemp -d)

trap 'rm -rf "$DIR"' EXIT

(cd "$DIR" ; ar p $PARTICLE_FW_PATH/hal/src/photon/lib/resources.a >bcm-fw.o)

dfu-suffix -v 2B04 -p D006 -a "$DIR/bcm-fw.o"

SIZE=$(stat -c %s "$DIR/bcm-fw.o")

ls -l "$DIR"

dfu-util -d 0x2B04:0xD006 -a 0 -s 0x08060000:$SIZE -D "$DIR/bcm-fw.o"
