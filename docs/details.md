# Way way more details

## The split-flap module

Each module consists of a motor coil and two Hall-effect sensors.  One sensor
(we'll call it the reset sensor) is driven low when the module is displaying the
" " (space) character.  The other sensor alternates each time the module
advances.  Only one of the sensors can be powered at a given time.  Our
experiments show that only the reset sensor is necessary to drive the module
reliably.

To increment the module to the next character, the polarity of the signal sent
to the motor must be inverted from the previous increment, and 100mA of current
must be supplied for at least 125ms.

## Logical block diagram

![](block-diagram.svg)

## Power and Temperature Control

When idle, the microcontroller and I/O expanders are powered from the 5Vstby
power of the ATX power supply.  In order to send current to the motor coils,
the firmware must first send a signal (PS\_ON#) to the power supply to turn on
the 5V and 12V rails, which power the motor driver ICs.  The firmware must wait
for the ATX power supply to signal that the voltage is stable (via the PWR\_OK
signal) before sending current to the motors.

If the temperature measured near the motors exceeds 32°C, the firmware should
stop sending current to the motors.  If the temperature exceeds 37°C, or there
is a fault reading the temperature sensors, the firmware should cut power from
the ATX supply, and leave the power cut until a human can look at the system
and reset the microcontroller.

As a precaution against a flaky CPU or firmware making the temperature logic
above inoperable, the ATX PS\_ON# signal will not be driven directly by the
firmware.  Instead, the PS\_ON# GPIO pin will be driven by one of the
microcontroller's hardware timers.  Before sending commands to the motors, the
firmware will restart this timer, which will supply power for 78 * 125ms (enough
time to flip the maximum number of characters that could ever be needed).  The
timer would only be restarted in very select circumstances (receiving a new
message from the control VM), making it unlikely to trigger after memory
corruption.

## The Microcontroller

The system uses a [Particle
Photon](http://docs.particle.io/datasheets/photon-datasheet/), an extremely
compact PCB that integrates an
[STM32F205RGY6](http://www.st.com/content/ccc/resource/technical/document/datasheet/bc/21/42/43/b0/f3/4d/d3/CD00237391.pdf/files/CD00237391.pdf/jcr:content/translations/en.CD00237391.pdf)
microcontroller with an 802.11b/g wireless NIC.  The Photon ships with [semi
proprietary firmware](https://github.com/spark/firmware) that integrates with
Particle's "IoT cloud platform", but this git repository contains a replacement
firmware written in [Rust](http://www.rust-lang.org/).  The stock Photon
bootloader is still used to upload the firmware over USB, which leaves the JTAG
pins free for other uses.  Eliminating the bootloader would free up 16K of
flash.

This microcontroller is overkill for our needs.  It provides 128k of volatile
SRAM, 4k of battery-backed-up SRAM, and 1MB of flash.  The system is designed to
execute code directly from flash, so the .rodata and .text
[sections](https://en.wikipedia.org/wiki/Data_segment) can be read from flash,
and only the .bss and .data sections, the global arena, and the stack need to be
stored in RAM.

So far, this firmware is about 150k, implementing motor control logic, I2C
communication code, a simple IPv6 stack, audio playback using the DAC and DMA
(and encoding audio using ADPCM), USB debug-message support, and a custom
protobuf encoder / in-place-decoder.  It runs at about 12000 main-loop cycles
per second; without the audio resampling, that increases to about 20000 cycles
per second.

## The Language and Runtime Environment

The firmware is 100% written in the Rust programming language, including the
IPv6 stack and protobuf layer.  C is a much more common choice for this
application, but this project is a fantastic test for evaluating Rust in a
resource-constrained environment.  So far, all experiments have shown extremely
small size and efficient code generation, comparable to that generated by a C
compiler but with stronger memory-safety guarantees.

Because Rust's standard library doesn't yet support bare-metal development, we
are forced to use the [core library](https://doc.rust-lang.org/core/).

We avoid dynamic memory allocation entirely.  This eliminates the possibility of
heap fragmentation, which is a concern because we only have 128k of RAM.
Instead, all data structures (hash tables, priority queues, etc.) are allocated
statically on the stack or in the global arena at startup.  In standard Rust,
[dynamically-dispatched](https://doc.rust-lang.org/book/trait-objects.html#dynamic-dispatch)
closures that outlive their parent function are usually stored on the heap using
Box objects.  In our firmware, the trait object containing the closed-over state
is stored in the arena, and can therefore never be freed.  Loosely coupled
objects can use these closures to communicate and schedule timer callbacks.

Here's some example code:

``` rust
let setup_button = GPIOC().pin(7).configured(Input, GpioPull::Up)
    .with_interrupts(
        GpioIrqConfig{irq: true, wakeup: false, rising: true, falling: true});
let sched = arena::GLOBAL_ARENA.place() <- (
      SimpleScheduler::new(dev.timer2));
let setup_button_debouncer = make_machine_refcell(
      &arena::GLOBAL_ARENA, sched, move |sched| DebounceMachine::new(sched, setup_button));
let ip_stack = make_machine_refcell(
      &arena::GLOBAL_ARENA, sched, |sched| Ipv6Stack::new(
          sched, &arena::GLOBAL_ARENA, dev.wifi, ip_config));
setup_button_debouncer.borrow_mut().changed_callback.set(move |value| {
    println!("The setup button is {}",
             if value { "up" } else { "down" });
    if !value {
        ip_stack.borrow_mut().send_ping(
            IpAddr::from_str("2620::1000:fd35:fcdf:3ca4:24a0:e81b"));
    }
});
```

## Networking

The firmware joins the GoogleGuest-Legacy network, and performs all
communication over IPv6.  The [Neighbor Discovery
Protocol](https://en.wikipedia.org/wiki/Neighbor_Discovery_Protocol) is used
to find a default gateway to the Internet.

In an ideal world, the firmware would have a full TCP/HTTP2/TLS/GRPC
implementation and could make calls to a Google-hosted backend securely and
directly.  However, these protocols are complex to implement and hard to get
correct (especially around security), so the initial version will communicate
with a "control" VM over a simple UDP protocol with application-level
retransmits.

GoogleGuest-Legacy has a stateful firewall, so the firmware periodically sends
packets to the control VM (which mirrors them back) to keep the paths open.
These packets could also contain diagnostic information such as temperatures and
fault conditions, and we could page split-flap-oncall when something goes wrong.
However none of this is implemented yet.

Another option would be to get a publicly routable IPv6 address for each
microcontroller, write a TCP stack, and implement SMTP.  This would remove the
need for the central VM to route messages, but would not provide an easy way to
play audio or remotely fetch the debug log ring-buffer contents.

## Security

We are not expecting to implement any encryption or authentication for
communication between the control VM and the microcontroller.  If an attacker
can hijack the IP address of the control VM, they will be able to control the
display.  Similarly, anyone with visibility to the packets (or control over the
VM) will be able to see the content of the split-flap messages.

If encryption is absolutely required, we could use ChaCha20/Poly1305 or AES-GCM
to quickly symmetrically encrypt the communication packets with a key shared
between the firmware and the VM.  We could also make the VM a dumb packet
forwarder and do the encryption outside it, but that would prevent the VM from
handling audio encoding and resampling as it does today.

The STM32F2 has a hardware random number generator that can be used for crypto
quality entropy if needed.
