// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

extern crate libusb;

use libusb::{Context, ConfigDescriptor, InterfaceDescriptor, DeviceHandle, EndpointDescriptors, Error};
use std::io::{self, Write};
use std::time::Duration;
use std::thread;

const VENDOR_ID_GOOGLE: u16 = 0x18d1;
const PRODUCT_ID_FLIPBOARD: u16 = 0x1002;  // XXX: Subject to change!

fn main() {
    loop {
        let ctx = Context::new().unwrap();
        let mut cd: ConfigDescriptor;
        let desc: InterfaceDescriptor;
        let mut handle: Result<DeviceHandle, Error> = Err(Error::Access);
        let mut intf_num: u8 = 0;
        let mut eps: Option<EndpointDescriptors> = None;

        for device in ctx.devices().unwrap().iter() {
            let device_desc = device.device_descriptor().unwrap();

            if device_desc.vendor_id() == VENDOR_ID_GOOGLE &&
                device_desc.product_id() == PRODUCT_ID_FLIPBOARD {
                cd = device.active_config_descriptor().unwrap();
                if cd.num_interfaces() == 1 {
                    let intf = cd.interfaces().next().unwrap();
                    intf_num = intf.number();

                    desc = intf.descriptors().next().unwrap();
                    eps = Some(desc.endpoint_descriptors());
                    handle = device.open();
                    break;
                }
            }
        }


        match handle {
            Err(Error::Access) => {
                println!("Couldn't find a device with vendor:product ID 0x18d1:0x1002, or couldn't open it");
                thread::sleep(Duration::new(1, 0));
            }
            Err(e) => {
                println!("Error trying to open 0x18d1:0x1002 device: {}", e);
                thread::sleep(Duration::new(1, 0));
            }
            Ok(mut handle) => {
                use libusb::TransferType::Bulk;
                use libusb::Direction::In;
                use libusb::EndpointDescriptor;

                handle.claim_interface(intf_num).unwrap();

                let bulk_eps = eps.unwrap().filter(|d| {d.transfer_type() == Bulk});
                let mut in_eps = bulk_eps.filter(|d| {d.direction() == In});
                let ep: EndpointDescriptor = in_eps.next().unwrap();

                println!("Bound to {:?}", ep);

                let mut buf = vec![0u8; ep.max_packet_size() as usize];

                loop {
                    match handle.read_bulk(ep.address(), buf.as_mut_slice(), Duration::new(3600, 0)) {
                        Err(Error::Timeout) => {}
                        Err(Error::NoDevice) => {
                            // Close and re-scan
                            println!("Device disconnected; rescanning");
                            break;
                        }
                        Err(e) => {
                            println!("Error receiving data: {}", e);
                            thread::sleep(Duration::new(1, 0) / 2);
                        }
                        Ok(len) => {
                            io::stdout().write(&buf[0..len]).unwrap();
                        }
                    }
                }
            }
        }
    }
}
