// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#![cfg_attr(not(test), no_std)]

#[cfg(test)]
extern crate core;
#[cfg(test)]
extern crate byteorder;

extern crate utils;

use utils::bits::BitSetLe;

const INDEXES: [i32; 16] = [
    -1, -1, -1, -1, 2, 4, 6, 8,
    -1, -1, -1, -1, 2, 4, 6, 8,
];

const STEPSIZES: [i32; 89] = [
    7, 8, 9, 10, 11, 12, 13, 14, 16, 17,
    19, 21, 23, 25, 28, 31, 34, 37, 41, 45,
    50, 55, 60, 66, 73, 80, 88, 97, 107, 118,
    130, 143, 157, 173, 190, 209, 230, 253, 279, 307,
    337, 371, 408, 449, 494, 544, 598, 658, 724, 796,
    876, 963, 1060, 1166, 1282, 1411, 1552, 1707, 1878, 2066,
    2272, 2499, 2749, 3024, 3327, 3660, 4026, 4428, 4871, 5358,
    5894, 6484, 7132, 7845, 8630, 9493, 10442, 11487, 12635, 13899,
    15289, 16818, 18500, 20350, 22385, 24623, 27086, 29794, 32767
];

struct AdpcmSampleSplitter {}
impl AdpcmSampleSplitter {
    pub fn split(sample: u8) -> (u8, u8) {
        // ima-adpcm is big-"nibble-ian"
        (sample >> 4, sample & 0xf)
    }
}

pub struct AdpcmDecoder {
    index: usize,
    previous_value: i32,
}

impl AdpcmDecoder {
    pub fn new() -> Self {
        Self{
            index: 0usize,
            previous_value: 0i32,
        }
    }

    pub fn reinit(&mut self, last_sample: i32, index: u32) {
        self.index = index as usize;
        self.previous_value = last_sample;
    }

    pub fn decode(&mut self, delta: u8) -> i16 {
        let step = STEPSIZES[self.index];
        // advance the index
        let index = (self.index as i32) + INDEXES[usize::from(delta)];
        self.index = match index {
            x @ 0..=88 => x as usize,
            x if x > 88 => 88usize,
            _ => 0usize,
        };
        // top bit is the sign; next 3 are the magnitude
        let sign = (delta & 0x8) > 0;
        // calculate (magnitude of delta+.5) * step / 4
        let vpdiff = (((((delta & 0x7) as i32)<<1) | 1) * step) >> 3;
        if sign {
            self.previous_value -= vpdiff;
        } else {
            self.previous_value += vpdiff;
        }
        // clamp
        if self.previous_value > 32767 { self.previous_value = 32767; }
        if self.previous_value < -32768 { self.previous_value = -32768; }
        self.previous_value as i16
    }
}

// To generate this sequence:
//   kernelsize=$(python -c 'print int(4 / (8. / 192))')
//   cornerfreq=$(python -c 'print 20000. / 192000')
//   ./kernel.py $kernelsize $cornerfreq 0.7 262144
//
//  ...and then ensure their sum is really 0.7*262144, because the kernel.py script does it by
//  scaling the floating-point values instead of the integer ones, so rounding error sometimes adds
//  up.
//
//  Also pad it at the end with 4 extra zeros, to avoid overrunning the end with the last input
//  sample.
static KERNEL: [i32; 101] = [
    0, 0, -1, -4,
    -4, 1, 14, 28,
    34, 19, -17, -68,
    -107, -105, -42, 75,
    205, 280, 236, 51,
    -230, -496, -602, -438,
    0, 577, 1046, 1139,
    701, -207, -1272, -2018,
    -1991, -994, 759, 2634,
    3765, 3396, 1265, -2151,
    -5646, -7587, -6460, -1459,
    7098, 17705, 28011, 35494,
    38228,
    35494, 28011, 17705, 7098,
    -1459, -6460, -7587, -5646,
    -2151, 1265, 3396, 3765,
    2634, 759, -994, -1991,
    -2018, -1272, -207, 701,
    1139, 1046, 577, 0,
    -438, -602, -496, -230,
    51, 236, 280, 205,
    75, -42, -105, -107,
    -68, -17, 19, 34,
    28, 14, 1, -4,
    -4, -1, 0, 0,
    0, 0, 0, 0
];

struct Resampler {
    buffer: [i32; 75],
    ptr: usize,
}
impl Resampler {
    pub fn new() -> Self {
        Self{
            buffer: [0i32; 75],
            ptr: 0usize,
        }
    }

    //pub fn reset(&mut self) {
    //    self.buffer = [0i16; 97];
    //    self.ptr = 0usize;
    //}

    #[inline(always)]
    fn mulacc(&self, ptr: usize) -> i32 {
        /*let mut sum = 0i32;
        for i in 0..25 {
            sum += self.buffer[i + (ptr>>2)] * KERNEL[i*4 + 3 - (ptr&3)]
        }
        sum*/
        // Unroll it:
        /*let b = &self.buffer[ptr>>2..(ptr>>2)+25];
        let k = &KERNEL[3-(ptr&3)..97+3-(ptr&3)];
        b[0] * k[0] +
            b[1] * k[4] +
            b[2] * k[8] +
            b[3] * k[12] +
            b[4] * k[16] +
            b[5] * k[20] +
            b[6] * k[24] +
            b[7] * k[28] +
            b[8] * k[32] +
            b[9] * k[36] +
            b[10] * k[40] +
            b[11] * k[44] +
            b[12] * k[48] +
            b[13] * k[52] +
            b[14] * k[56] +
            b[15] * k[60] +
            b[16] * k[64] +
            b[17] * k[68] +
            b[18] * k[72] +
            b[19] * k[76] +
            b[20] * k[80] +
            b[21] * k[84] +
            b[22] * k[88] +
            b[23] * k[92] +
            b[24] * k[96]*/
        // Remove the slice bounds checks:
        unsafe {
            let b = self.buffer.as_ptr().offset((ptr >> 2) as isize);
            let k = KERNEL.as_ptr().offset(3 - (ptr & 3) as isize);
            *b * *k +
                *b.offset(1) * *k.offset(4) +
                *b.offset(2) * *k.offset(8) +
                *b.offset(3) * *k.offset(12) +
                *b.offset(4) * *k.offset(16) +
                *b.offset(5) * *k.offset(20) +
                *b.offset(6) * *k.offset(24) +
                *b.offset(7) * *k.offset(28) +
                *b.offset(8) * *k.offset(32) +
                *b.offset(9) * *k.offset(36) +
                *b.offset(10) * *k.offset(40) +
                *b.offset(11) * *k.offset(44) +
                *b.offset(12) * *k.offset(48) +
                *b.offset(13) * *k.offset(52) +
                *b.offset(14) * *k.offset(56) +
                *b.offset(15) * *k.offset(60) +
                *b.offset(16) * *k.offset(64) +
                *b.offset(17) * *k.offset(68) +
                *b.offset(18) * *k.offset(72) +
                *b.offset(19) * *k.offset(76) +
                *b.offset(20) * *k.offset(80) +
                *b.offset(21) * *k.offset(84) +
                *b.offset(22) * *k.offset(88) +
                *b.offset(23) * *k.offset(92) +
                *b.offset(24) * *k.offset(96)
        }
    }
    /*#[inline(always)]
    #[cfg(not(test))]
    fn mulacc(&self, ptr: usize) -> i32 {
        let ret: i32;
        unsafe {
            let _temp1: usize;
            let _temp2: usize;
            let _temp3: usize;
            let _temp4: usize;
            let _temp5: usize;
            let _temp6: usize;
            let _temp7: usize;
            let _temp8: usize;
            // We could use ldm here (if we could ensure that $1 and $3 were allocated in order),
            // except that ldm can't pipeline with preceding or following load instructions, where
            // a sequence of individual loads can.
            asm!("mov $0, 0
                  ldr $1, [$9, #0]
                  ldr $3, [$9, #4]
                  ldr $5, [$9, #8]
                  ldr $7, [$9, #12]
                  ldr $2, [$10, #0]
                  ldr $4, [$10, #16]
                  ldr $6, [$10, #32]
                  ldr $8, [$10, #48]
                  mla $0, $1, $2, $0
                  mla $0, $3, $4, $0
                  mla $0, $5, $6, $0
                  mla $0, $7, $8, $0
                  ldr $1, [$9, #16]
                  ldr $3, [$9, #20]
                  ldr $5, [$9, #24]
                  ldr $7, [$9, #28]
                  ldr $2, [$10, #64]
                  ldr $4, [$10, #80]
                  ldr $6, [$10, #96]
                  ldr $8, [$10, #112]
                  mla $0, $1, $2, $0
                  mla $0, $3, $4, $0
                  mla $0, $5, $6, $0
                  mla $0, $7, $8, $0
                  ldr $1, [$9, #32]
                  ldr $3, [$9, #36]
                  ldr $5, [$9, #40]
                  ldr $7, [$9, #44]
                  ldr $2, [$10, #128]
                  ldr $4, [$10, #144]
                  ldr $6, [$10, #160]
                  ldr $8, [$10, #176]
                  mla $0, $1, $2, $0
                  mla $0, $3, $4, $0
                  mla $0, $5, $6, $0
                  mla $0, $7, $8, $0
                  ldr $1, [$9, #48]
                  ldr $3, [$9, #52]
                  ldr $5, [$9, #56]
                  ldr $7, [$9, #60]
                  ldr $2, [$10, #192]
                  ldr $4, [$10, #208]
                  ldr $6, [$10, #224]
                  ldr $8, [$10, #240]
                  mla $0, $1, $2, $0
                  mla $0, $3, $4, $0
                  mla $0, $5, $6, $0
                  mla $0, $7, $8, $0
                  ldr $1, [$9, #64]
                  ldr $3, [$9, #68]
                  ldr $5, [$9, #72]
                  ldr $7, [$9, #76]
                  ldr $2, [$10, #256]
                  ldr $4, [$10, #272]
                  ldr $6, [$10, #288]
                  ldr $8, [$10, #304]
                  mla $0, $1, $2, $0
                  mla $0, $3, $4, $0
                  mla $0, $5, $6, $0
                  mla $0, $7, $8, $0
                  ldr $1, [$9, #80]
                  ldr $3, [$9, #84]
                  ldr $5, [$9, #88]
                  ldr $7, [$9, #92]
                  ldr $2, [$10, #320]
                  ldr $4, [$10, #336]
                  ldr $6, [$10, #352]
                  ldr $8, [$10, #368]
                  mla $0, $1, $2, $0
                  mla $0, $3, $4, $0
                  mla $0, $5, $6, $0
                  mla $0, $7, $8, $0
                  ldr $1, [$9, #96]
                  ldr $2, [$10, #384]
                  mla $0, $1, $2, $0
                  " :
                  "=&l"(ret), "=&r"(_temp1), "=&r"(_temp2), "=&r"(_temp3), "=&r"(_temp4), "=&r"(_temp5), "=&r"(_temp6), "=&r"(_temp7), "=&r"(_temp8) :
                  "l"(self.buffer.as_ptr().offset((ptr >> 2) as isize)), "l"(KERNEL.as_ptr().offset(3 - (ptr & 3) as isize)) :
                  :
                  "volatile");
            ret
        }
    }*/
    #[inline(never)]
    pub fn resample(&mut self, sample: i16) -> (i16, i16, i16, i16) {
        self.buffer[self.ptr] = sample.into();
        self.buffer[self.ptr + 25] = sample.into();
        self.buffer[self.ptr + 50] = sample.into();
        let p = 96 + (self.ptr << 2);
        self.ptr = if self.ptr >= 24 { 0 } else { self.ptr + 1 };

        ((self.mulacc(p) >> 16) as i16,
         (self.mulacc(p + 1) >> 16) as i16,
         (self.mulacc(p + 2) >> 16) as i16,
         (self.mulacc(p + 3) >> 16) as i16)
    }
    #[cfg(test)]
    pub fn convolution_resample(&mut self, sample: i16) -> (i16, i16, i16, i16) {
        self.buffer[self.ptr] = sample.into();
        self.buffer[self.ptr + 25] = sample.into();
        self.buffer[self.ptr + 50] = sample.into();
        //println!("{}", self.ptr<<2);
        let p = 96 + (self.ptr << 2);
        self.ptr = if self.ptr >= 24 { 0 } else { self.ptr + 1 };
        ((self.mulacc(p) >> 16) as i16,
         (self.mulacc(p + 1) >> 16) as i16,
         (self.mulacc(p + 2) >> 16) as i16,
         (self.mulacc(p + 3) >> 16) as i16)
    }
}

struct Noise {
    state: u32,
}
impl Noise {
    pub fn new() -> Self {
        Self{state: 0x49a39b4f}
    }
    pub fn next(&mut self) -> (u16, u16) {
        // xorshift!
        self.state ^= self.state << 13;
        self.state ^= self.state >> 17;
        self.state ^= self.state << 5;
        ((self.state & 0xffff) as u16, ((self.state >> 16) as u16))
    }
}

#[cfg(test)]
struct RawScaler {}
#[cfg(test)]
impl RawScaler {
    pub fn scale(sample: i16) -> i16 {
        sample
    }
}

struct DacScaler {}
impl DacScaler {
    pub fn scale(sample: i16) -> u16 {
        (((sample as i32) + 32768) & 0xffff) as u16
    }
}

pub struct Converter {
    decoder: AdpcmDecoder,
    resampler: Resampler,
    noise: Noise,
    input_buf: [u8; 1024*16],
    present: [u32; 1],  // one bit for each half of each input_buf chunk
    last_sample: [i32; 32],
    last_index: [u32; 32],
    read_offset: usize,
    buffer_done: bool,   // whether we've ever seen more than half the buffer
}

impl Converter {
    pub fn new() -> Self {
        Self{
            decoder: AdpcmDecoder::new(),
            resampler: Resampler::new(),
            noise: Noise::new(),
            input_buf: [0u8; 1024*16],
            present: [0u32],
            last_sample: [0i32; 32],
            last_index: [0u32; 32],
            read_offset: 0usize,
            buffer_done: false,
        }
    }

    pub fn buffer(&mut self, seqno: usize, last_sample: i32, last_index: u32, adpcm: &[u8]) {
        // seqno must be either at the start of one of the 1K chunks or right in the middle
        if seqno & 0x1ff != 0 {
            // This panic is required to stop the copy_nonoverlapping calls from overrunning the
            // end of self.input_buf.
            panic!("nonzero offset: {}", seqno);   // TODO: should this be a Result?
        }
        if adpcm.len() != 1024 {
            panic!("bad length of adpcm input data: {}", adpcm.len());
        }
        if seqno == 0 {
            self.present[0] = 0u32;
        }
        self.present.set_bit_le((seqno >> 9) & 31, true);
        self.present.set_bit_le(((seqno >> 9) + 1) & 31, true);
        self.last_sample[(seqno >> 9) & 31] = last_sample;
        self.last_index[(seqno >> 9) & 31] = last_index;
        if (seqno >> 9) & 31 == 31 {
            // special case the end, and do two copies
            unsafe {
                let dst1 = self.input_buf.as_mut_ptr();
                let dst2 = dst1.offset((seqno & 16383) as isize);
                core::ptr::copy_nonoverlapping(adpcm.as_ptr(), dst2, 512);
                core::ptr::copy_nonoverlapping(adpcm.as_ptr().offset(512), dst1, 512);
            }
        } else {
            unsafe {
                let dst = self.input_buf.as_mut_ptr().offset((seqno & 16383) as isize);
                core::ptr::copy_nonoverlapping(adpcm.as_ptr(), dst, 1024);
            }
        }
    }

    fn convert_two(&mut self, input: u8) -> (i16, i16) {
        let (sample0, sample1) = AdpcmSampleSplitter::split(input);
        (self.decoder.decode(sample0), self.decoder.decode(sample1))
    }

    #[cfg(test)]
    pub fn convert_no_resample(&mut self, input: u8) -> (i16, i16) {
        let (adpcm0, adpcm1) = self.convert_two(input);
        (RawScaler::scale(adpcm0), RawScaler::scale(adpcm1))
    }

    fn convert_two_and_resample(&mut self, input: u8) -> (i16, i16, i16, i16, i16, i16, i16, i16) {
        let (adpcm0, adpcm1) = self.convert_two(input);
        let (a, b, c, d) = self.resampler.resample(adpcm0);
        let (e, f, g, h) = self.resampler.resample(adpcm1);
        (a, b, c, d, e, f, g, h)
    }

    fn advance_read_offset(&mut self) {
        self.read_offset += 1;
        if self.read_offset & 511 == 0 {
            if self.present.get_bit_le((self.read_offset >> 9) & 31) &&
                !self.present.get_bit_le(((self.read_offset - 1) >> 9) & 31) {

                self.decoder.reinit(self.last_sample[(self.read_offset >> 9) & 31],
                                    self.last_index[(self.read_offset >> 9) & 31]);
            }
            self.present.set_bit_le(((self.read_offset - 1) >> 9) & 31, false);
        }
    }

    fn convert(&mut self) -> (u16, u16, u16, u16, u16, u16, u16, u16) {
        if !self.buffer_done && self.present[0] > 0x00008000 {
            self.buffer_done = true;
        }
        if !self.buffer_done {
            (32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767)
        } else if self.present.get_bit_le((self.read_offset >> 9) & 31) {
            let input = self.input_buf[self.read_offset & 16383];
            self.advance_read_offset();

            let (a, b, c, d, e, f, g, h) = self.convert_two_and_resample(input);
            let (noise1, noise2) = self.noise.next();
            (DacScaler::scale(a) + (noise1 & 0xf),
             DacScaler::scale(b) + ((noise1 >> 4) & 0xf),
             DacScaler::scale(c) + ((noise1 >> 8) & 0xf),
             DacScaler::scale(d) + ((noise1 >> 12) & 0xf),
             DacScaler::scale(e) + (noise2 & 0xf),
             DacScaler::scale(f) + ((noise2 >> 4) & 0xf),
             DacScaler::scale(g) + ((noise2 >> 8) & 0xf),
             DacScaler::scale(h) + ((noise2 >> 12) & 0xf))
        } else {
            self.advance_read_offset();
            /*let (a, b, c, d, e, f, g, h) = self.convert_two_and_resample(0);
            let (noise1, noise2) = self.noise.next();
            (DacScaler::scale(a) + (noise1 & 0xf),
             DacScaler::scale(b) + ((noise1 >> 4) & 0xf),
             DacScaler::scale(c) + ((noise1 >> 8) & 0xf),
             DacScaler::scale(d) + ((noise1 >> 12) & 0xf),
             DacScaler::scale(e) + (noise2 & 0xf),
             DacScaler::scale(f) + ((noise2 >> 4) & 0xf),
             DacScaler::scale(g) + ((noise2 >> 8) & 0xf),
             DacScaler::scale(h) + ((noise2 >> 12) & 0xf)) */
            (32767, 32767, 32767, 32767, 32767, 32767, 32767, 32767)
        }
    }

    pub fn convert_to_buf(&mut self, buf: &mut [u32]) {
        for i in 0..(buf.len() >> 3) {
            let (a, b, c, d, e, f, g, h) = self.convert();
            buf[(i<<3) + 0] = a.into();
            buf[(i<<3) + 1] = b.into();
            buf[(i<<3) + 2] = c.into();
            buf[(i<<3) + 3] = d.into();
            buf[(i<<3) + 4] = e.into();
            buf[(i<<3) + 5] = f.into();
            buf[(i<<3) + 6] = g.into();
            buf[(i<<3) + 7] = h.into();
        }
    }
}

#[cfg(test)]
mod test {
    use ::{Converter,Resampler};
    /*use std::fs::File;
    use std::io::Write;
    use std::slice;*/
    use std::vec::Vec;

    use byteorder::{ByteOrder,LE};

    #[test]
    fn test_decode() {
        let adpcm = include_bytes!("../testdata/adpcm-input");

        // This was generated from adpcm-input with:
        //
        // sox -t ima -e ima-adpcm -r 44100 adpcm-input \
        //     -t raw -e signed -r 44100 -b 16 raw-output-from-sox
        let expected_u8 = include_bytes!("../testdata/raw-output-from-sox");
        let mut expected: Vec<i16> = Vec::new();
        expected.resize(expected_u8.len() / 2, 0i16);
        LE::read_i16_into(expected_u8, expected.as_mut_slice());

        let mut actual: Vec<i16> = Vec::new();
        actual.resize(adpcm.len() * 2, 0i16);

        let mut converter = Converter::new();
        for (i, &b) in adpcm.iter().enumerate() {
            let (sample0, sample1) = converter.convert_no_resample(b);
            actual[i*2] = sample0;
            actual[i*2 + 1] = sample1;
        }

        /*let mut out = File::create("test.decoded.out.raw").unwrap();
        {
            let actual_bytes = unsafe { slice::from_raw_parts(actual.as_slice().as_ptr() as *const u8, actual.len() * 2) };
            out.write_all(actual_bytes).unwrap();
        }*/

        // we know the lengths are the same
        // we used convert_no_resample, so we know the values will match exactly
        for i in 0..actual.len() {
            assert_eq!(expected[i], actual[i], "at {}", i);
        }
    }

    #[test]
    fn test_resample() {
        let mut input: Vec<i16> = Vec::new();
        input.resize(48, 0);
        input[0] = 0;
        input[1] = 12539;
        input[2] = 23169;
        input[3] = 30272;
        input[4] = 32767;
        input[5] = 30272;
        input[6] = 23169;
        input[7] = 12539;
        input[8] = 0;
        input[9] = -12539;
        input[10] = -23169;
        input[11] = -30272;
        input[12] = -32767;
        input[13] = -30272;
        input[14] = -23169;
        input[15] = -12539;
        for i in 0..16 {
            input[i+16] = input[i];
            input[i+32] = input[i];
        }

        let mut output: Vec<i16> = Vec::new();
        output.resize(192, 0);

        let mut resampler = Resampler::new();
        for i in 0..48 {
            let (a, b, c, d) = resampler.convolution_resample(input[i]);
            output[i*4] = a;
            output[i*4+1] = b;
            output[i*4+2] = c;
            output[i*4+3] = d;
        }

        for i in 0..48 {
            println!("{}, {}, {}, {},", output[i*4], output[i*4+1], output[i*4+2], output[i*4+3]);
        }
    }

    #[test]
    fn test_adpcm_to_dma() {
        let buf = [
            0x07,0x77,0x77,0x77,0x77,0x30,0x11,0x01,0x01,0x11,0x01,0x01,0x01,0x80,0x08,0x89,
            0xAA,0xBD,0xBC,0xBC,0xCB,0xCB,0xCB,0xCB,0xBD,0xAB,0xCB,0xBB,0xCB,0xCB,0xBB,0xBC,
            0xBC,0xAB,0xAB,0xBC,0xAA,0xAB,0xAA,0x9A,0x89,0x80,0x12,0x35,0x43,0x54,0x34,0x35,
            0x33,0x44,0x33,0x43,0x43,0x34,0x34,0x33,0x34,0x33,0x43,0x33,0x43,0x23,0x42,0x23,
            0x23,0x22,0x22,0x11,0x00,0x8A,0xAC,0xCC,0xCC,0xBC,0xCB,0xCB,0xCC,0xBB,0xCB,0xBD,
            0xAC,0xAB,0xBC,0xBB,0xBC,0xBB,0xBC,0xBB,0xCA,0xBB,0xAB,0xBB,0xBB,0xAB,0x9A,0x98,
            0x01,0x23,0x64,0x35,0x35,0x34,0x34,0x35,0x24,0x24,0x23,0x34,0x34,0x33,0x34,0x34,
            0x23,0x42,0x33,0x33,0x34,0x32,0x33,0x33,0x23,0x21,0x20,0x08,0x9B,0xCD,0xBD,0xBD,
            0xBC,0xCB,0xBD,0xBB,0xCC,0xBB,0xBD,0xAC,0xAB,0xBC,0xBB,0xBC,0xBB,0xBC,0xBB,0xBB,
            0xCA,0xBB,0xAB,0xBA,0xAA,0x99,0x90,0x02,0x34,0x54,0x35,0x35,0x33,0x53,0x35,0x33,
            0x43,0x35,0x24,0x23,0x34,0x33,0x34,0x33,0x34,0x33,0x34,0x32,0x33,0x33,0x32,0x32,
            0x21,0x10,0x89,0xAC,0xCC,0xCC,0xBD,0xBC,0xBC,0xCB,0xBC,0xBC,0xBC,0xBC,0xAC,0xAB,
            0xBC,0xAC,0xAB,0xAC,0xAB,0xAB,0xBB,0xCA,0xBA,0xBA,0xAA,0xAA,0x98,0x80,0x13,0x44,
            0x44,0x35,0x34,0x43,0x43,0x35,0x33,0x43,0x43,0x42,0x42,0x33,0x42,0x34,0x23,0x34,
            0x23,0x33,0x33,0x34,0x22,0x32,0x21,0x20,0x00,0x99,0xBC,0xDB,0xDB,0xCC,0xBD,0xBB,
            0xCB,0xCC,0xAC,0xAB,0xCB,0xBB,0xCB,0xCA,0xCA,0xBB,0xBB,0xBC,0xBB,0xCA,0xBA,0xBA,
            0xBA,0xAA,0xA9,0x90,0x81,0x24,0x44,0x35,0x43,0x43,0x53,0x34,0x43,0x34,0x34,0x33,
            0x43,0x42,0x42,0x33,0x34,0x33,0x34,0x32,0x34,0x23,0x22,0x32,0x22,0x22,0x01,0x88,
            0x9B,0xBE,0xCB,0xDB,0xCC,0xBC,0xBC,0xCA,0xCB,0xBB,0xCC,0xBB,0xBC,0xBC,0xAC,0xAB,
            0xBB,0xBC,0xBB,0xBB,0xBC,0xAB,0xBA,0xBA,0xAA,0x99,0x88,0x12,0x35,0x44,0x43,0x44,
            0x43,0x34,0x43,0x35,0x33,0x34,0x43,0x33,0x43,0x43,0x33,0x35,0x23,0x33,0x33,0x43,
            0x23,0x33,0x32,0x32,0x21,0x00,0x89,0xAD,0xBD,0xCB,0xDB,0xCC,0xBB,0xDB,0xBC,0xBC,
            0xCA,0xBC,0xBB,0xBC,0xBC,0xBB,0xBB,0xCB,0xCA,0xBB,0xBB,0xBC,0xAB,0xAA,0xAA,0xA9,
            0x99,0x00,0x13,0x44,0x44,0x43,0x44,0x34,0x42,0x43,0x34,0x34,0x33,0x43,0x34,0x34,
            0x33,0x34,0x33,0x34,0x33,0x33,0x34,0x32,0x32,0x32,0x22,0x11,0x08,0x99,0xCC,0xBD,
            0xCB,0xDB,0xCB,0xCC,0xBB,0xCB,0xCB,0xCB,0xBC,0xBC,0xBB,0xBC,0xBC,0xAB,0xBC,0xAB,
            0xBB,0xBC,0xAB,0xAA,0xBA,0xAA,0xA9,0x88,0x01,0x33,0x63,0x54,0x34,0x43,0x43,0x43,
            0x43,0x43,0x43,0x34,0x34,0x33,0x34,0x34,0x23,0x34,0x23,0x34,0x23,0x23,0x23,0x32,
            0x32,0x21,0x10,0x09,0x9B,0xDC,0xCB,0xDB,0xDB,0xBD,0xBB,0xDB,0xBC,0xBC,0xBB,0xCB,
            0xBC,0xBC,0xBB,0xBC,0xBB,0xBC,0xBB,0xBB,0xBC,0xAB,0xBA,0xAB,0xA9,0x99,0x80,0x12,
            0x35,0x44,0x44,0x34,0x34,0x43,0x43,0x35,0x33,0x35,0x24,0x23,0x34,0x24,0x23,0x33,
            0x34,0x33,0x33,0x43,0x23,0x33,0x23,0x22,0x21,0x00,0x8A,0xAD,0xBD,0xCC,0xBC,0xCB,
            0xCB,0xCB,0xCC,0xAC,0xAB,0xCA,0xCA,0xBB,0xCB,0xBB,0xCB,0xBB,0xCA,0xCA,0xAB,0xAB,
            0xBA,0xBA,0xBA,0x9A,0x98,0x00,0x23,0x54,0x44,0x34,0x43,0x53,0x34,0x34,0x42,0x42,
            0x34,0x33,0x34,0x34,0x24,0x23,0x33,0x34,0x23,0x34,0x23,0x23,0x23,0x22,0x22,0x10,
            0x08,0x9A,0xBE,0xBD,0xBD,0xBC,0xBD,0xBB,0xCC,0xBB,0xCB,0xCB,0xBC,0xBC,0xBB,0xBC,
            0xBB,0xCB,0xBB,0xCB,0xAB,0xCA,0xAB,0xAB,0xAA,0xAA,0x99,0x90,0x01,0x34,0x44,0x44,
            0x34,0x43,0x43,0x44,0x24,0x33,0x34,0x42,0x42,0x33,0x43,0x33,0x43,0x33,0x43,0x24,
            0x22,0x33,0x23,0x23,0x22,0x21,0x10,0x09,0xAB,0xDC,0xCC,0xBD,0xBC,0xBC,0xBC,0xCB,
            0xBC,0xBC,0xBB,0xCB,0xCA,0xCA,0xBB,0xBC,0xBB,0xBC,0xBB,0xBB,0xBC,0xAB,0xAB,0xAA,
            0xAA,0x98,0x80,0x12,0x44,0x44,0x35,0x34,0x43,0x35,0x33,0x44,0x24,0x23,0x43,0x33,
            0x43,0x43,0x33,0x42,0x42,0x32,0x33,0x33,0x42,0x32,0x23,0x12,0x11,0x18,0x89,0xBC,
            0xCC,0xCC,0xBD,0xBB,0xDB,0xBD,0xBB,0xCB,0xBD,0xAC,0xAB,0xBC,0xAC,0xAB,0xBB,0xBC,
            0xBB,0xCA,0xBA,0xBB,0xBB,0xBB,0xAA,0xAA,0x89,0x01,0x24,0x44,0x44,0x35,0x34,0x34,
            0x43,0x34,0x34,0x34,0x33,0x43,0x43,0x33,0x43,0x42,0x33,0x42,0x33,0x33,0x42,0x32,
            0x23,0x22,0x21,0x11,0x08,0x9B,0xCC,0xCC,0xBD,0xBC,0xCB,0xBD,0xBB,0xCC,0xBB,0xBD,
            0xBB,0xBC,0xBC,0xAC,0xAB,0xBB,0xBC,0xBB,0xBB,0xCA,0xBA,0xBB,0xAB,0xAA,0x99,0x88,
            0x11,0x43,0x54,0x35,0x43,0x35,0x34,0x34,0x34,0x33,0x44,0x33,0x34,0x34,0x24,0x23,
            0x33,0x34,0x33,0x34,0x32,0x33,0x33,0x23,0x23,0x21,0x00,0x09,0xBC,0xCC,0xCC,0xCB,
            0xCB,0xCC,0xBC,0xBB,0xDB,0xBB,0xDA,0xCA,0xBB,0xCA,0xCA,0xBB,0xBB,0xCB,0xBB,0xBC,
            0xBA,0xBB,0xBA,0xBA,0xAA,0x98,0x80,0x23,0x45,0x35,0x43,0x44,0x34,0x34,0x42,0x42,
            0x42,0x34,0x33,0x34,0x34,0x24,0x23,0x33,0x34,0x33,0x33,0x34,0x32,0x32,0x32,0x22,
            0x11,0x00,0x9A,0xBD,0xCC,0xBD,0xCB,0xBD,0xBC,0xBC,0xBC,0xBC,0xBB,0xCB,0xBC,0xBC,
            0xBB,0xBC,0xBB,0xCA,0xBB,0xCA,0xBA,0xBB,0xBB,0xAB,0xAB,0x9A,0x88,0x01,0x24,0x53,
            0x53,0x53,0x44,0x33,0x53,0x34,0x42,0x42,0x34,0x33,0x34,0x34,0x33,0x33,0x43,0x42,
            0x33,0x33,0x34,0x23,0x22,0x32,0x12,0x01,0x88,0x9B,0xCC,0xCC,0xCB,0xCC,0xBC,0xBC,
            0xCB,0xBC,0xBB,0xDA,0xCA,0xBB,0xCA,0xCA,0xBB,0xBC,0xAB,0xBC,0xAB,0xBA,0xBB,0xBB,
            0xBB,0xAA,0x99,0x80,0x12,0x36,0x43,0x53,0x53,0x43,0x44,0x33,0x43,0x43,0x43,0x34,
            0x34,0x33,0x34,0x34,0x23,0x34,0x23,0x33,0x34,0x23,0x22,0x32,0x22,0x21,0x00,0x89,
            0xBB,0xEB,0xDC,0xBC,0xCB,0xCB,0xCB,0xCB,0xCB,0xCB,0xBC,0xBC,0xAC,0xAB,0xBC,0xAB,
            0xBC,0xBA,0xBC,0xAB,0xAB,0xAB,0xBA,0xBA,0xA9,0x98,0x81,0x13,0x54,0x43,0x53,0x53,
            0x35,0x33,0x53,0x34,0x33,0x52,0x42,0x33,0x42,0x42,0x33,0x33,0x43,0x34,0x23,0x23,
            0x33,0x33,0x32,0x22,0x11,0x08,0x8B,0xCC,0xCC,0xCB,0xDB,0xCB,0xCC,0xBB,0xCB,0xCB,
            0xCB,0xCA,0xCA,0xBB,0xCA,0xCA,0xBA,0xCA,0xBA,0xBB,0xBC,0xAB,0xAB,0xAA,0xAA,0xA8,
        ];
        let mut converter = Converter::new();

        converter.buffer(0, 0, 0, &buf);

        for _ in 0..128 {
            let (a, b, c, d, e, f, g, h) = converter.convert();
            println!("{}, {}, {}, {}, {}, {}, {}, {},", a, b, c, d, e, f, g, h);
        }
    }
}
