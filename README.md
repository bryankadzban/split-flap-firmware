# Firmware to drive a split-flap display

## This is not an official Google product

The authors just think it's cool.

## Background info

Google SRE recently acquired two used Solari split-flap displays manufactured in
the 1970s.  These displays shipped with 19 alphanumeric split-flap modules, plus
one larger special-purpose module, but there is enough room in the case for up to
60 alphanumeric modules.

![Initial unboxing](docs/unboxing.jpg)

![Original internals](docs/internals.jpg)

![Module rear view](docs/modules.jpg)

The electronics necessary to drive the display were not included.  In the
original installation, we believe that centrally located electronics were used
to power the motors and read the sensors of a large number of signs.  We
designed a set of PCBs that can drive a maximum of 96 split-flap modules, 12 per
expansion board.  Schematics and board designs are available in the
[flipboard](https://bitbucket.org/bryankadzban/flipboard) git repository.

### Way more information

See [the "details" doc](docs/details.md) for far far more information on the
hardware, as well as this git repo's software stack.

## How to build:

1. Bootstrap rust and dfu-util:

        (cd bootstrap ; make PREFIX=/home/$USER/split-flap-firmware-build)

1. Set environment variables:

        export RUSTUP_HOME=/home/$USER/split-flap-firmware-build
        export CARGO_HOME=/home/$USER/split-flap-firmware-build
        export PATH=$CARGO_HOME/bin:$PATH

1. Add the RUSTUP\_HOME, CARGO\_HOME, and PATH settings to your shell startup files too
1. Write an implementation of the NetworkDevice rust trait for your wifi chip, and put it into the ../bcm-driver directory
1. Build a customized wifi settings file, using wifi-config:

        make wifi-config
        wifi-config/target/debug/wifi-config -f config.dfu -d
        wifi-config/target/debug/wifi-config -f config.dfu -u -n 0 -s YourNetworkSSID -g ArbitraryGroupName

    - The ArbitraryGroupName above is which group of displays this one will show up in on the web interface.
    - If this network requires a key, add `-k "wifi password"` to wifi-config.

1. Build the firmware:

        make

1. Connect the split-flap controller to a USB port
1. Put the controller into DFU mode: hold down the SETUP button, push RESET, and hold SETUP until the LED flashes yellow
1. Flash config.dfu onto the controller:

        main/config.sh config.dfu

1. Download the Particle firmware and flash the Broadcom firmware object file onto the controller:

        (cd .. ; git clone https://github.com/particle-iot/firmware.git particle-fw)
        PARTICLE_FW_PATH=../particle-fw main/bcm-firmware.sh

1. Flash the firmware onto the controller:

        make flash

Only the firmware needs to be re-flashed if you're developing it; the Particle firmware's Broadcom firmware and config.dfu files do not generally change.

## Features

- The VM sends message updates to the controller board, which displays the message on the connected letters.
- The firmware logs to both USB and serial (though panic messages only go to serial)
    - To see the logs, connect a USB cable, run ```make host-usb```, and run the host-usb tool
    - To see the serial logs, connect a UART to the "TX" pin on the microcontroller board, configure the serial port to 3000 kbaud/8/N/1, and read debug messages from it.
- The LED displays network status:
    - Black at initial boot time
    - Solid white shortly after boot time
    - Flashing between red and magenta when waiting for wifi association
    - Flashing between red and yellow when waiting for an IPv6 router advertisement
    - Flashing between green and cyan when a router advertisement is seen
- The firmware talks to remote temperature sensors over 1-wire, and kills power if any sensor is above 32C.  It shuts down completely if any sensor is above 37C.
- The firmware plays audio out the 3-pin audio connector if the VM streams any audio to the controller
