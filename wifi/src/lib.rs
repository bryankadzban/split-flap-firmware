// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#![no_std]

#[derive(Clone, Copy, Default)]
#[repr(C)]
pub struct SSID {
    ssid_len: u32,
    ssid: [u8; 32],
}
impl SSID {
    pub fn as_str<'a>(&'a self) -> &'a str {
        core::str::from_utf8(&self.ssid[0..self.ssid_len as usize]).unwrap()
    }
    pub fn from_str(s: &str) -> SSID {
        let mut result = SSID {
            ssid_len: s.len() as u32,
            ssid: [0u8; 32],
        };
        result.ssid[0..s.len()].clone_from_slice(s.as_bytes());
        result
    }
}
impl core::fmt::Display for SSID {
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        write!(f, "{}", self.as_str())
    }
}
impl core::fmt::Debug for SSID {
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        write!(f, "SSID {{ ssid_len: {}, ssid: {} }}", self.ssid_len, self.as_str())
    }
}

#[repr(C)]
pub struct Wpa2Key {
    len: u32,
    text: [u8; 64],
}
impl Wpa2Key {
    pub fn is_set(&self) -> bool {
        self.len > 0
    }
    pub fn as_str<'a>(&'a self) -> &'a str {
        core::str::from_utf8(&self.text[0..self.len as usize]).unwrap()
    }
    #[allow(dead_code)]
    pub fn from_str(s: &str) -> Wpa2Key {
        assert!(s.len() <= 63);
        let mut result = Wpa2Key {
            len: s.len() as u32,
            text: [0u8; 64],
        };
        result.text[0..s.len()].clone_from_slice(s.as_bytes());
        result
    }
}
impl core::fmt::Display for Wpa2Key {
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        write!(f, "{}", self.as_str())
    }
}
impl Copy for Wpa2Key {}
impl Clone for Wpa2Key { fn clone(&self) -> Wpa2Key { *self } }
impl Default for Wpa2Key { fn default() -> Self { Self{len: 0, text: [0u8; 64]} } }
impl core::fmt::Debug for Wpa2Key {
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        write!(f, "Wpa2Key {{ len: {}, text: {:?} }}", self.len, &self.text[0..self.len as usize])
    }
}

#[repr(u8)]
#[allow(dead_code)]
#[derive(Clone, Copy, Debug)]
pub enum ScanType {
    Active = 1,
    Passive = 0,
}
impl Default for ScanType { fn default() -> Self { ScanType::Active } }

#[derive(Clone, Copy, Default)]
#[repr(C)]
pub struct WifiNetworkConfig {
    pub ssid: SSID,
    pub wpa2_key: Wpa2Key,
    pub scan_type: ScanType,
    padding1: [u8; 24],
    padding2: [u8; 32],
    padding3: [u8; 32],
    padding4: [u8; 32],
    padding5: [u8; 32],
}
// Don't bother formatting the padding bytes
impl core::fmt::Debug for WifiNetworkConfig {
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        write!(f, "WifiNetworkConfig {{\n  ssid: {:#?},\n  wpa2_key: {:#?},\n  scan_type: {:#?}\n}}", self.ssid, self.wpa2_key, self.scan_type)
    }
}

#[derive(Clone, Copy, Debug, Default)]
#[repr(C)]
pub struct Config {
    pub header: u32,  // 0x30465053: 'SPF0' little-endian
    pub network_count: u32,
    pub networks: [WifiNetworkConfig; 4],
    pub client_id: [u8; 16],
}
