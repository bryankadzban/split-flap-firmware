// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

pub trait FromPrimitive<T : Sized> : Sized {
    fn from_primitive(val: T) -> Option<Self>;
}

#[macro_export]
macro_rules! primitive_enum {
    (
        $(#[$attr:meta])*
        enum $name:ident as $primitive_type:ident {
            $( $item:ident = $value: expr,)*
        }
    ) => {
        $(#[$attr])*
        #[repr($primitive_type)]
        enum $name {
            $($item = $value,)*
        }
        impl $crate::primitive::FromPrimitive<$primitive_type> for $name {
            fn from_primitive(val: $primitive_type) -> Option<Self> {
                match val {
                    $($value => Some($name::$item),)*
                    _ => None,
                }
            }
        }
    };
    (
        $(#[$attr:meta])*
        pub enum $name:ident as $primitive_type:ident {
            $( $item:ident = $value: expr,)*
        }
    ) => {
        $(#[$attr])*
        pub enum $name {
            $($item = $value,)*
        }
        impl $crate::primitive::FromPrimitive<$primitive_type> for $name {
            fn from_primitive(val: $primitive_type) -> Option<Self> {
                match val {
                    $($value => Some($name::$item),)*
                    _ => None,
                }
            }
        }
    };
}
