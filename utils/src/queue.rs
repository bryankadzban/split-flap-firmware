// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use func::SendFuncRef;
use core::cell::{Cell, UnsafeCell};
use core::ops;

struct FuncQueueState<'a, TArgs: 'a> {
    events: [Option<SendFuncRef<'a, TArgs>>; 16],
    add_pos: usize,
    rm_pos: usize,
    full: bool,
}

pub struct FuncQueue<'a, TArgs: 'a> {
    state: IrqMutex<FuncQueueState<'a, TArgs>>,
}
impl<'a, TArgs> FuncQueue<'a, TArgs> {
    pub fn new() -> Self {
        FuncQueue{
            state: IrqMutex::new(FuncQueueState{
                add_pos: 0,
                rm_pos: 0,
                full: false,
                events: [
                    None, None, None, None,
                    None, None, None, None,
                    None, None, None, None,
                    None, None, None, None,
                ],
            }),
        }
    }
    #[cfg(not(feature = "devicetest"))]
    pub fn push_back<F: 'a + Send + FnMut<TArgs, Output=()>>(&self, f: F) -> bool {
        self.push_back_func_ref(SendFuncRef::from(f))
    }
    fn push_back_func_ref(&self, f: SendFuncRef<'a, TArgs>) -> bool {
        let mut state = self.state.lock();
        if state.full {
            return false;
        }
        let pos = state.add_pos;
        state.events[pos] = Some(f);
        state.add_pos += 1;
        if state.add_pos >= state.events.len() {
            state.add_pos = 0;
        }
        if state.add_pos == state.rm_pos {
            state.full = true;
        }
        true
    }
    pub fn run_next_func(&self, args: TArgs) {
        let f = {
            let mut state = self.state.lock();
            if !state.full && state.add_pos == state.rm_pos {
                return;
            }
            let i = state.rm_pos;
            state.rm_pos += 1;
            if state.rm_pos >= state.events.len() {
                state.rm_pos = 0;
            }
            state.full = false;
            state.events[i].take()
        };
        if let Some(mut f) = f {
            f.call_mut(args);
        }
    }
}
unsafe impl<'a, TArgs> Sync for FuncQueue<'a, TArgs> { }

pub struct IrqMutex<T> {
    data: UnsafeCell<T>,
    locked: Cell<bool>,
}

impl<T> IrqMutex<T> {
    pub fn new(data: T) -> IrqMutex<T> {
        IrqMutex{
            data: UnsafeCell::new(data),
            locked: Cell::new(false),
        }
    }
    pub fn lock(&self) -> IrqMutexGuard<T> {
        disable_interrupts();
        if self.locked.get() {
            // This should only happen if lock() is called while locked. We can't allow this as
            // having two mutable references to the data violates Rust's invariants.
            panic!("Re-entered IrqMutex");
        }
        // This doesn't need to be atomic because interrupts have been disabled and we only have a
        // single core.
        self.locked.set(true);
        IrqMutexGuard{
            __data: unsafe { &mut *self.data.get() },
            parent: self,
        }
    }
}
unsafe impl<T: Send> Sync for IrqMutex<T> { }

pub struct IrqMutexGuard<'a, T: 'a> {
    __data: &'a mut T,
    parent: &'a IrqMutex<T>,
}
impl<'a, T> ops::Deref for IrqMutexGuard<'a, T> {
    type Target = T;
    fn deref(&self) -> &T { self.__data }
}
impl<'a, T> ops::DerefMut for IrqMutexGuard<'a, T> {
    fn deref_mut(&mut self) -> &mut T { self.__data }
}
impl<'a, T> Drop for IrqMutexGuard<'a, T> {
    fn drop(&mut self) {
        self.parent.locked.set(false);
        enable_interrupts();
    }
}

#[cfg(feature = "cortex")]
pub fn disable_interrupts() {
    unsafe {
        asm!("cpsid i" :::: "volatile");
    }
}

#[cfg(feature = "cortex")]
pub fn enable_interrupts() {
    unsafe {
        asm!("cpsie i" :::: "volatile");
    }
}


#[cfg(not(feature = "cortex"))]
pub fn enable_interrupts() {}

#[cfg(not(feature = "cortex"))]
pub fn disable_interrupts() {}


