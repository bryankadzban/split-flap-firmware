// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use core;
use core::ops::Range;

pub trait BitOpsLe {
    fn get_bits_le(&self, r: Range<u32>) -> u32;
    fn set_bits_le(&mut self, r: Range<u32>, val: u32);
}
impl BitOpsLe for [u32] {
    #[inline(always)]
    fn get_bits_le(&self, r: Range<u32>) -> u32 {
        // TODO(kor): Make this work when the range straddles word boundaries
        let b = u32::from_le(self[(r.start >> 5) as usize]);
        let width = r.end - r.start;
        let mask = if width == 32 { 0xffffffff } else { (1 << (width)) - 1 };
        let shift = r.start;
        return ((mask << shift) & b) >> shift;
    }
    #[inline(always)]
    fn set_bits_le(&mut self, r: Range<u32>, val: u32) {
        // TODO(kor): Make this work when the range straddles word boundaries
        let idx = (r.start >> 5) as usize;
        let b = u32::from_le(self[idx]);
        let width = r.end - r.start;
        let mask = if width == 32 { 0xffffffff } else { (1 << (width)) - 1 };
        let shift = r.start;
        self[idx] = u32::to_le((b & !(mask << shift)) | ((val & mask) << shift));
    }
}

pub trait BitOpsBe {
    fn get_bits_be(&self, r: Range<u32>) -> u32;
    fn get_byte_be(&self, index: usize) -> u8;

    fn set_bits_be(&mut self, r: Range<u32>, val: u32);
}
impl BitOpsBe for [u32] {
    #[inline(always)]
    fn get_bits_be(&self, r: Range<u32>) -> u32 {
        let b1_mask = 0xffffffff >> (r.start & 0x1f);
        let b2_mask = !((1 << (0u32.wrapping_sub(r.end) & 0x1f)) - 1);
        let i1 = r.start >> 5;
        let i2 = (r.end - 1) >> 5;
        let b1 = u32::from_be(self[i1 as usize]);
        let b2 = u32::from_be(self[i2 as usize]);
        if i1 == i2 {
            (b1 & (b1_mask & b2_mask)).rotate_left(r.end & 0x1f)
        } else {
            ((b1 & b1_mask) | (b2 & b2_mask)).rotate_left(r.end & 0x1f)
        }
    }
    #[inline(always)]
    fn set_bits_be(&mut self, r: Range<u32>, val: u32) {
        // TODO(kor): Make this work when the range straddles word boundaries
        let idx = (r.start >> 5) as usize;
        let b = u32::from_be(self[idx]);
        let width = r.end - r.start;
        let mask = if width == 32 { 0xffffffff } else { (1 << (width)) - 1 };
        let shift = 32 - r.end;
        self[idx] = u32::to_be((b & !(mask << shift)) | ((val & mask) << shift));
    }
    #[inline(always)]
    fn get_byte_be(&self, index: usize) -> u8 {
        let word = u32::from_be(self[index >> 2]);
        match index & 0x3 {
            0 => ((word & 0xff000000) >> 24) as u8,
            1 => ((word & 0x00ff0000) >> 16) as u8,
            2 => ((word & 0x0000ff00) >> 8) as u8,
            3 => ((word & 0x000000ff) >> 0) as u8,
            _ => unreachable!(),
        }
    }
}

pub trait BitSetLe {
    fn get_bit_le(&self, index: usize) -> bool;
    fn set_bit_le(&mut self, index: usize, val: bool);
}
impl BitSetLe for [u8] {
    #[inline(always)]
    fn get_bit_le(&self, index: usize) -> bool {
        let byte_index = index >> 3;
        (self[byte_index] & (1 << (index & 0x7))) != 0
    }
    #[inline(always)]
    fn set_bit_le(&mut self, index: usize, val: bool) {
        let byte_index = index >> 3;
        let bit_mask = (1 << (index & 0x7)) as u8;
        let current_value = self[byte_index];
        self[byte_index] = if val { current_value | bit_mask } else { current_value & !bit_mask };
    }
}
impl BitSetLe for [u32] {
    #[inline(always)]
    fn get_bit_le(&self, index: usize) -> bool {
        let byte_index = index >> 5;
        (self[byte_index] & (1 << (index & 0x1f))) != 0
    }
    #[inline(always)]
    fn set_bit_le(&mut self, index: usize, val: bool) {
        let byte_index = index >> 5;
        let bit_mask = (1 << (index & 0x1f)) as u32;
        let current_value = self[byte_index];
        self[byte_index] = if val { current_value | bit_mask } else { current_value & !bit_mask };
    }
}

#[cfg(test)]
mod test {
    use bits::BitSetLe;

    #[test]
    fn test_get_bit_le() {
        let bits = [0x9au8, 0x7b];
        assert_eq!(false, bits.get_bit_le(0));
        assert_eq!(true, bits.get_bit_le(1));
        assert_eq!(false, bits.get_bit_le(2));
        assert_eq!(true, bits.get_bit_le(3));
        assert_eq!(true, bits.get_bit_le(4));
        assert_eq!(false, bits.get_bit_le(5));
        assert_eq!(false, bits.get_bit_le(6));
        assert_eq!(true, bits.get_bit_le(7));

        assert_eq!(true, bits.get_bit_le(8));
        assert_eq!(true, bits.get_bit_le(9));
        assert_eq!(false, bits.get_bit_le(10));
        assert_eq!(true, bits.get_bit_le(11));
        assert_eq!(true, bits.get_bit_le(12));
        assert_eq!(true, bits.get_bit_le(13));
        assert_eq!(true, bits.get_bit_le(14));
        assert_eq!(false, bits.get_bit_le(15));
    }

    #[test]
    #[should_panic]
    fn test_get_bit_le_out_of_bounds() {
        let bits = [0x9au8, 0x7b];
        bits.get_bit_le(16);
    }
}

pub trait AsBytes {
    fn as_bytes<'a>(&'a self) -> &'a [u8];
}

pub trait AsBytesMut {
    fn as_mut_bytes<'a>(&'a mut self) -> &'a mut [u8];
}
impl AsBytes for [u32] {
    fn as_bytes<'a>(&'a self) -> &'a [u8] {
        unsafe { core::slice::from_raw_parts(self.as_ptr() as *const u8, self.len() << 2) }
    }
}
impl AsBytesMut for [u32] {
    fn as_mut_bytes<'a>(&'a mut self) -> &'a mut [u8] {
        unsafe { core::slice::from_raw_parts_mut(self.as_mut_ptr() as *mut u8, self.len() << 2) }
    }
}
impl AsBytes for [usize] {
    fn as_bytes<'a>(&'a self) -> &'a [u8] {
        unsafe { core::slice::from_raw_parts(self.as_ptr() as *const u8, self.len() * core::mem::size_of::<usize>()) }
    }
}

pub trait AsU32sLe {
    type Iter: Iterator<Item=u32>;
    fn as_u32s_le(self) -> Self::Iter;
}
impl<'a, T: Iterator<Item=&'a u8>> AsU32sLe for T {
    type Iter = U8AsU32LeIterator<'a, T>;
    fn as_u32s_le(self) -> Self::Iter {
        U8AsU32LeIterator::<T>(self)
    }
}

pub struct U8AsU32LeIterator<'a, T: Iterator<Item=&'a u8>>(T);
impl<'a, T: Iterator<Item=&'a u8>> Iterator for U8AsU32LeIterator<'a, T> {
    type Item = u32;
    fn next(&mut self) -> Option<Self::Item> {
        if let Some(b0) = self.0.next() {
            let b0 = *b0;
            let b1 = *self.0.next().unwrap_or(&0);
            let b2 = *self.0.next().unwrap_or(&0);
            let b3 = *self.0.next().unwrap_or(&0);
            Some(((b0 as u32) << 0) | 
                 ((b1 as u32) << 8) |
                 ((b2 as u32) << 16) |
                 ((b3 as u32) << 24))
        } else {
            None
        }
    }
}
