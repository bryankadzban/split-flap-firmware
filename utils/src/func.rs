// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use core;
use core::mem;
use core::cell::{Cell, UnsafeCell};

pub struct FuncRef<'a, TArgs: 'a> {
    pub mem: UnsafeCell<[usize; 4]>,
    vtable: Cell<*mut ()>,
    //func: Cell<Option<&'a Fn<TArgs, Output=()>>>,
    phantom: core::marker::PhantomData<&'a TArgs>,
}
impl<'a, TArgs> FuncRef<'a, TArgs> {
    pub fn new() -> Self {
        FuncRef{
            mem: UnsafeCell::new([0; 4]),
            vtable: Cell::new(core::ptr::null_mut()),
            phantom: core::marker::PhantomData,
        }
    }
    #[cfg(not(feature = "devicetest"))]
    pub fn from<F: 'a + FnMut<TArgs, Output=()>>(func: F) -> Self {
        let mut result = Self::new();
        result.place(&func); // <- func;
        result
    }
    // TODO: should we add an F:Copy bound?
    pub fn place<'b, F: 'a + FnMut<TArgs, Output=()>>(&'b mut self, f: &F) { // -> FuncRefPlace<'a, 'b, TArgs, F> {
        // TODO: Do these checks at compile time as soon as the language allows it
        if mem::size_of::<F>() > (unsafe{*self.mem.get()}.len() * mem::size_of::<usize>()) {
            panic!("Not enough room in ValueRef");
        }
        if mem::align_of::<F>() > mem::align_of::<usize>() {
            panic!("ValueRef doesn't support larger alignments than size_of(usize)");
        }
        unsafe {
            core::intrinsics::copy_nonoverlapping(
                f as *const F,
                (*self.mem.get()).as_mut().as_ptr() as *mut usize as *mut F,
                mem::size_of::<F>());
        }
        self.set_ref(f);
    }

    #[cfg(not(feature = "devicetest"))]
    pub fn is_set(&self) -> bool {
        self.vtable.get().is_null()
    }
    pub fn get(&mut self) -> Option<&'a mut dyn FnMut<TArgs, Output=()>> {
        if self.vtable.get().is_null() {
            return None;
        }
        unsafe {Some(core::mem::transmute(core::raw::TraitObject {
            data: self.mem.get() as *mut (),
            vtable: self.vtable.get(),
        }))}
    }
    #[allow(dead_code)]
    pub fn set<F: 'a + FnMut<TArgs, Output=()>>(&mut self, func: F) {
        self.place(&func); // <- func;
    }
    fn set_ref(&self, val: &'a dyn FnMut<TArgs, Output=()>) {
        let raw_obj: core::raw::TraitObject = unsafe { mem::transmute(val) };
        self.vtable.set(raw_obj.vtable);
    }
}

pub struct SendFuncRef<'a, TArgs: 'a>(FuncRef<'a, TArgs>);
impl<'a, TArgs> SendFuncRef<'a, TArgs> {
    #[allow(dead_code)]
    pub fn new() -> Self {
        SendFuncRef(FuncRef::new())
    }
    #[allow(dead_code)]
    pub fn place<'b, F: 'a + Send + FnMut<TArgs, Output=()>>(&'b mut self, f: &F) { // -> FuncRefPlace<'a, 'b, TArgs, F> {
        self.0.place(f)
    }
    #[cfg(not(feature = "devicetest"))]
    pub fn from<F: 'a + Send + FnMut<TArgs, Output=()>>(func: F) -> Self {
        SendFuncRef(FuncRef::from(func))
    }
    pub fn get(&mut self) -> Option<&'a mut dyn FnMut<TArgs, Output=()>> {
        self.0.get()
    }
}

#[cfg(not(feature = "devicetest"))]
pub struct ObservableValue<'a, T: 'a + Copy + Eq> {
    value: T,
    pub changed: Option<&'a mut dyn FnMut(T)>,
}
#[cfg(not(feature = "devicetest"))]
impl<'a, T: 'a + Copy + Eq> ObservableValue<'a, T> {
    pub fn new(init_value: T) -> Self {
        ObservableValue{
            value: init_value,
            changed: None,
        }
    }
    pub fn get(&self) -> T {
        self.value
    }
    pub fn set(&mut self, value: T) {
        if value != self.value {
            self.value = value;
            if let Some(ref mut changed) = self.changed {
                (*changed)(self.value);
            }
        }
    }
}

/*
impl<'a, Args> Fn<Args> for FuncRef<'a, Args> {
    extern "rust-call" fn call(&self, args: Args) {
        if let Some(f) = self.get() {
            f.call(args);
        }
    }
}
*/
impl<'a, Args> FnMut<Args> for FuncRef<'a, Args> {
    extern "rust-call" fn call_mut(&mut self, args: Args) {
        if let Some(f) = self.get() {
            f.call_mut(args);
        }
    }
}
impl<'a, Args> FnOnce<Args> for FuncRef<'a, Args> {
    type Output = ();
    extern "rust-call" fn call_once(mut self, args: Args) {
        if let Some(f) = self.get() {
            f.call_mut(args);
        }
    }
}
impl<'a, Args> FnMut<Args> for SendFuncRef<'a, Args> {
    extern "rust-call" fn call_mut(&mut self, args: Args) {
        if let Some(f) = self.get() {
            f.call_mut(args);
        }
    }
}
impl<'a, Args> FnOnce<Args> for SendFuncRef<'a, Args> {
    type Output = ();
    extern "rust-call" fn call_once(mut self, args: Args) {
        if let Some(f) = self.get() {
            f.call_mut(args);
        }
    }
}
