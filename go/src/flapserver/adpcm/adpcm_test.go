// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package adpcm

import (
	"encoding/binary"
	"io/ioutil"
	"os"
	"testing"
)

func TestDecode(t *testing.T) {
	bytes, err := ioutil.ReadFile("../sine.raw")
	if err != nil {
		t.Errorf("failed to read sine.raw: %v", err)
		return
	}
	samples := make([]int16, len(bytes)/2)
	for i := range samples {
		samples[i] = int16(binary.LittleEndian.Uint16(bytes[i*2 : i*2+2]))
	}

	expected, err := ioutil.ReadFile("../sine.adpcm")
	if err != nil {
		t.Errorf("Failed to read sine.adpcm: %v", err)
		return
	}

	state := New(0, 0)
	buf := make([]byte, len(samples)/2)
	for i := range buf {
		sample, index := state.State()
		buf[i] = state.EncodeTwo(samples[i*2], samples[i*2+1])
		if buf[i] != expected[i] {
			t.Errorf("At %v; in %v/%v, expected %v (%x), got %v (%x), s %v, i %v", i, samples[i*2], samples[i*2+1], expected[i], expected[i], buf[i], buf[i], sample, index)
		}
	}

	ioutil.WriteFile("../sine.go-encoded", buf, os.FileMode(0666))
}
