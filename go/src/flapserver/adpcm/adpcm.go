// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package adpcm

func min(a, b int32) int32 {
	if a < b {
		return a
	} else {
		return b
	}
}

var STEPS = [...]int32{
	7, 8, 9, 10, 11, 12, 13, 14,
	16, 17, 19, 21, 23, 25, 28, 31,
	34, 37, 41, 45, 50, 55, 60, 66,
	73, 80, 88, 97, 107, 118, 130, 143,
	157, 173, 190, 209, 230, 253, 279, 307,
	337, 371, 408, 449, 494, 544, 598, 658,
	724, 796, 876, 963, 1060, 1166, 1282, 1411,
	1552, 1707, 1878, 2066, 2272, 2499, 2749, 3024,
	3327, 3660, 4026, 4428, 4871, 5358, 5894, 6484,
	7132, 7845, 8630, 9493, 10442, 11487, 12635, 13899,
	15289, 16818, 18500, 20350, 22385, 24623, 27086, 29794,
	32767,
}
var CHANGES = [...]int8{-1, -1, -1, -1, 2, 4, 6, 8}

type AdpcmState struct {
	last_sample int16
	last_index  uint8
}

func (s *AdpcmState) State() (int16, uint8) {
	return s.last_sample, s.last_index
}
func (s *AdpcmState) SetState(last_sample int16, last_index uint8) {
	s.last_sample = last_sample
	s.last_index = last_index
}

func (s *AdpcmState) EncodeTwo(sample1, sample2 int16) byte {
	delta := int32(sample1) - int32(s.last_sample)
	sign1 := 0
	if delta < 0 {
		sign1 = 8
		delta = -delta
	}
	code1 := min((delta<<2)/STEPS[s.last_index], 7)
	//code1 = sign1 | code1

	samp := (code1 << 1) | 1
	samp = (STEPS[s.last_index] * samp) >> 3
	if sign1 == 8 {
		samp = -samp
	}
	samp += int32(s.last_sample)
	if samp < -32768 {
		samp = -32768
	}
	if samp > 32767 {
		samp = 32767
	}
	if code1 < 4 {
		s.last_index -= 1
	} else {
		s.last_index += uint8(CHANGES[code1])
	}
	if s.last_index > 0x7f {
		s.last_index = 0
	}
	if s.last_index > 88 {
		s.last_index = 88
	}
	s.last_sample = int16(samp)

	delta = int32(sample2) - samp
	sign2 := 0
	if delta < 0 {
		sign2 = 8
		delta = -delta
	}
	code2 := min((delta<<2)/STEPS[s.last_index], 7)
	//code2 = sign2 | code2

	samp = (code2 << 1) | 1
	samp = (STEPS[s.last_index] * samp) >> 3
	if sign2 == 8 {
		samp = -samp
	}
	samp += int32(s.last_sample)
	if samp < -32768 {
		samp = -32768
	}
	if samp > 32767 {
		samp = 32767
	}
	if CHANGES[code2] < 0 {
		s.last_index -= uint8(-CHANGES[code2])
	} else {
		s.last_index += uint8(CHANGES[code2])
	}
	if s.last_index > 0x7f {
		s.last_index = 0
	}
	if s.last_index > 88 {
		s.last_index = 88
	}
	s.last_sample = int16(samp)

	// ima-adpcm is big-"nibble-ian"
	return ((uint8(sign1) | uint8(code1)) << 4) | uint8(sign2) | uint8(code2)
}

func New(last_sample int16, last_index uint8) *AdpcmState {
	return &AdpcmState{
		last_sample: last_sample,
		last_index:  last_index,
	}
}
