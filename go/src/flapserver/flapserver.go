// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"encoding/binary"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"

	"flapserver/adpcm"
	pb "flapserver/pb"

	"github.com/golang/protobuf/proto"

	"github.com/jhillyerd/enmime"
	"github.com/siebenmann/smtpd"
)

type server struct {
	mutex          sync.Mutex
	conn           *net.UDPConn
	tcpConn        net.Listener
	audio          []int16
	flipboardConns map[string]*flipboardConn
}

func newServer() (*server, error) {
	addr, err := net.ResolveUDPAddr("udp", ":8082")
	if err != nil {
		return nil, err
	}
	conn, err := net.ListenUDP("udp", addr)
	if err != nil {
		return nil, err
	}
	tcpConn, err := net.Listen("tcp", ":25")
	if err != nil {
		return nil, err
	}

	//audio, err := ioutil.ReadFile("sine.adpcm")
	//if err != nil {
	//  return nil, err
	//}
	audio, err := ioutil.ReadFile("sine.raw")
	if err != nil {
		return nil, err
	}
	data := make([]int16, len(audio)/2)
	for i := range data {
		data[i] = int16(binary.LittleEndian.Uint16(audio[i*2 : i*2+2]))
	}

	s := &server{
		flipboardConns: make(map[string]*flipboardConn),
		conn:           conn,
		tcpConn:        tcpConn,
		audio:          data,
	}
	go s.listenForEmail()
	go s.listenForFlipboards()
	return s, nil
}

func (s *server) Connections() []*flipboardConn {
	ret := make([]*flipboardConn, 0, len(s.flipboardConns))
	for _, v := range s.flipboardConns {
		ret = append(ret, v)
	}
	return ret
}

func (s *server) handleRoot(w http.ResponseWriter, r *http.Request) {
	t, _ := template.ParseFiles("index.html")
	s.mutex.Lock()
	defer s.mutex.Unlock()
	t.Execute(w, s)
}

func (s *server) handleSetMessage(w http.ResponseWriter, r *http.Request) {
	s.setMessage(r.FormValue("id"), r.FormValue("message"))
	http.Redirect(w, r, "/", http.StatusFound)
}

func (s *server) setMessageInternal(group, message string) {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	found := false
	for _, flipboardConn := range s.flipboardConns {
		if group == "all" || flipboardConn.group == group {
			found = true
			flipboardConn.newMessage <- message
		}
	}
	if !found {
		log.Printf("could not find flipboard with group %v", group)
	}
}

func (s *server) setMessage(group, message string) {
	go s.setMessageInternal(group, message)
	go func() {
		time.Sleep(250 * time.Millisecond)
		s.setMessageInternal(group, message)
	}()
	go func() {
		time.Sleep(500 * time.Millisecond)
		s.setMessageInternal(group, message)
	}()
}

func (s *server) getOrCreateFlipboardConn(conn *net.UDPConn, addr *net.UDPAddr) *flipboardConn {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	flipConn, ok := s.flipboardConns[addr.String()]
	if !ok {
		flipConn = &flipboardConn{
			s:           s,
			conn:        conn,
			addr:        addr,
			group:       "mtv",
			isNewClient: false, // to start with
			audio:       s.audio,
			state:       adpcm.New(0, 0),
			packetIn:    make(chan []byte, 2),
			newMessage:  make(chan string, 2),
		}
		s.flipboardConns[addr.String()] = flipConn
		go flipConn.run()
	}
	return flipConn
}

func (s *server) removeFlipboardConn(addr *net.UDPAddr) {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	delete(s.flipboardConns, addr.String())
}

func (s *server) listenForFlipboards() error {
	log.Printf("Listening for flipboards on UDP %v", s.conn.LocalAddr())
	for {
		buf := make([]byte, 1550)
		length, addr, err := s.conn.ReadFromUDP(buf)
		if err != nil {
			return err
		}
		flipConn := s.getOrCreateFlipboardConn(s.conn, addr)
		flipConn.packetIn <- buf[:length]
	}
}

func (s *server) listenForEmail() {
	log.Printf("Listening for SMTP on TCP %v", s.tcpConn.Addr().String())
	for {
		client, err := s.tcpConn.Accept()
		if err != nil {
			log.Printf("Error in accept: %v", err)
			continue
		}

		go s.handleEmail(client)
	}
}

func (s *server) handleEmail(client net.Conn) {
	defer client.Close()

	conn := smtpd.NewConn(client, smtpd.Config{
		TLSConfig: nil,
		Limits:    &smtpd.DefaultLimits,
		Auth:      nil,
		Delay:     0 * time.Millisecond,
		SayTime:   true,
		LocalName: "flipboard.kadzban.net",
		SftName:   "smtpd",
		Announce:  "",
	}, os.Stdout)

	var rcpt string
	var from string

Outer:
	for {
		evt := conn.Next()
		if evt.What == smtpd.DONE || evt.What == smtpd.ABORT {
			break
		}
		switch evt.What {
		case smtpd.COMMAND:
			if evt.Cmd == smtpd.RCPTTO {
				rcpt = evt.Arg
			}
			if evt.Cmd == smtpd.MAILFROM {
				from = evt.Arg
			}
		case smtpd.GOTDATA:
			data := evt.Arg
			localpart := strings.SplitN(rcpt, "@", 2)[0]
			server := strings.SplitN(from, "@", 2)[1]
			var group string

			if !strings.HasPrefix(localpart, "flipboard") {
				log.Println(fmt.Sprintf("Address <%s> is not any flipboard", rcpt))
				continue Outer
			}
			if localpart != "flipboard" {
				group = strings.SplitN(localpart, "-", 2)[1]
			} else {
				group = "mtv"
			}

			if server != "google.com" && !strings.HasSuffix(server, ".google.com") {
				log.Printf("Only accept emails from google.com, not %v (%v)", from, server)
				continue Outer
			}

			env, err := enmime.ReadEnvelope(strings.NewReader(data))
			if err != nil {
				log.Printf("ReadEnvelope failed: %v", err)
				continue Outer
			}

			s.setMessage(group, formatSubject(env.GetHeader("Subject"))+env.Text)
		}
	}
}

const LINE_LEN = 18

func formatSubject(subject string) string {
	if len(subject) < LINE_LEN {
		subject = subject + strings.Repeat(" ", LINE_LEN)
	}
	return subject[:LINE_LEN]
}

type flipboardConn struct {
	s           *server
	conn        *net.UDPConn
	addr        *net.UDPAddr
	group       string
	message     string
	isNewClient bool
	audio       []int16
	state       *adpcm.AdpcmState
	//lastSamples []FIXME
	packetIn   chan []byte
	newMessage chan string
}

func (f *flipboardConn) Id() string {
	return f.group
}
func (f *flipboardConn) Message() string {
	return f.message
}

func (f *flipboardConn) Audio() string {
	return fmt.Sprintf("%d", len(f.audio))
}

func (f *flipboardConn) sendAudio(shutdown chan struct{}) {
	if !f.isNewClient {
		return
	}

	// 10649.6 microseconds ... have to round
	timer := time.NewTicker(10650 * time.Microsecond)
	defer timer.Stop()
	offset := uint32(0)

	// size := uint32(16384)
	size := uint32(len(f.audio))
	// block := uint32(512)
	block := uint32(1024)
	adpcm_block := 512

	for {
		select {
		case <-timer.C:
			sample, index := f.state.State()
			message := &pb.ProtocolPacket{
				PlayAudio: &pb.PlayAudio{
					// TODO: this is wrong in the second and future packets...
					LastSample:     int32(sample),
					Index:          uint32(index),
					SequenceNumber: offset,
				},
			}

			inner_offset := offset % size
			inner_offset_2 := (offset + block) % size
			buf := make([]byte, adpcm_block*2)
			//var mid_sample int16 = 0
			//var mid_index uint8 = 0
			set := false
			for i := range buf {
				var sample1 int16
				var sample2 int16
				if i < adpcm_block {
					sample1 = f.audio[inner_offset+uint32(i)*2]
					sample2 = f.audio[inner_offset+uint32(i)*2+1]
				} else {
					if !set {
						//mid_sample = f.state.last_sample
						//mid_index = f.state.last_index
						set = true
					}
					value := inner_offset_2 + uint32(i-adpcm_block)*2
					if value < 0 || value >= uint32(len(f.audio)) {
						panic(fmt.Sprintf("value: %v, inner_offset_2: %v, i: %v, adpcm_block: %v", value, inner_offset_2, i, adpcm_block))
					}
					sample1 = f.audio[value]
					sample2 = f.audio[value+1]
				}
				buf[i] = f.state.EncodeTwo(sample1, sample2)
			}
			offset += block
			if set {
				//f.state.last_sample = mid_sample
				//f.state.last_index = mid_index
			}

			message.PlayAudio.AdpcmData = buf

			packet, err := proto.Marshal(message)
			if err != nil {
				log.Printf("Error marshaling play-audio protobuf: %v", err)
				continue
			}
			f.conn.WriteTo(packet, f.addr)

			if offset > 1048575 {
				offset = 0
			}
		case <-shutdown:
			return
		}
	}
}

func (f *flipboardConn) run() {
	timeoutCh := time.After(150 * time.Second)
	var shutdown chan struct{}
outer:
	for {
		select {
		case packet := <-f.packetIn:
			log.Printf("Received packet from %v", f.addr)
			if len(packet) == 2 && packet[0] == 0x48 && packet[1] == 0x69 {
				// "Hi" -- old version client
				f.isNewClient = false
				f.group = "mtv" // just hardcode something
			} else {
				f.isNewClient = true
				p := &pb.ProtocolPacket{}
				proto.Unmarshal(packet, p)
				if p.RegisterClient != nil {
					// id packet ... this should always be valid utf8
					f.group = p.RegisterClient.Id
					if f.group == "" {
						hwAddr := net.HardwareAddr(p.RegisterClient.MacAddr)
						f.group = hwAddr.String()
					}
					resp := &pb.ProtocolPacket{
						RegisterClientResponse: &pb.RegisterClientResponse{
							Id:      p.RegisterClient.Id,
							MacAddr: p.RegisterClient.MacAddr,
						},
					}
					buf, err := proto.Marshal(resp)
					if err != nil {
						log.Printf("Error marshaling set-message protobuf: %v", err)
						continue
					}
					f.conn.WriteTo(buf, f.addr)
				}
			}
			timeoutCh = time.After(150 * time.Second)
		case m := <-f.newMessage:
			if shutdown != nil {
				close(shutdown)
			}
			f.message = m
			if f.isNewClient {
				p := &pb.ProtocolPacket{
					SetMessage: &pb.SetMessage{
						Message: m,
					},
				}
				buf, err := proto.Marshal(p)
				if err != nil {
					log.Printf("Error marshaling set-message protobuf: %v", err)
					continue
				}
				f.conn.WriteTo(buf, f.addr)
			} else {
				f.conn.WriteTo([]byte(m), f.addr)
			}
			log.Printf("Sending new message %q to %v", m, f.addr)
			shutdown = make(chan struct{})
			go f.sendAudio(shutdown)
		case <-timeoutCh:
			log.Printf("Haven't heard from %v for 150 seconds; cleaning up", f.addr)
			if shutdown != nil {
				close(shutdown)
			}
			f.s.removeFlipboardConn(f.addr)
			break outer
		}
	}
}

func main() {
	s, err := newServer()
	if err != nil {
		log.Fatal(err)
	}
	http.HandleFunc("/", s.handleRoot)
	http.HandleFunc("/set_message", s.handleSetMessage)
	port := ":8080"
	log.Printf("Listening on %v", port)
	err = http.ListenAndServe(port, nil)
	if err != nil {
		log.Fatal(err)
	}
}
