#!/usr/bin/python

# Copyright 2018 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# usage:
#  sine.py | sox -D -t raw -e signed -r 48000 -b 16 /dev/stdin -t ima -e ima-adpcm -r 48000 sine.adpcm
#
# The sample rate there is a lie, but it works because it's the same on input and output.

import sys
import math
import struct

def main(argv):
    end_sample_rate = 60000000 / 312.
    low_sample_rate = end_sample_rate / 4  # approx 48kHz
    # generate 16384 samples of a 440Hz sine wave sampled at 192307.7Hz
    for i in range(32768):
        sample = math.sin(2 * math.pi * i * 440 / low_sample_rate)
        sys.stdout.write(struct.pack("<h", int(sample * 32768)))

if __name__ == '__main__':
    sys.exit(main(sys.argv))
