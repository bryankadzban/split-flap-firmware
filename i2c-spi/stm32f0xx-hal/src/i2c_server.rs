use core::option::Option;

use crate::i2c::{SclPin, SdaPin};
use crate::rcc::Rcc;
use stm32f0::stm32f0x0::{I2C1, i2c1::isr::R};

pub enum I2cMode {
    // Standard,   // not handled
    Fast,
    FastPlus,
}

pub struct I2cServer<Scl: SclPin<I2C1>, Sda: SdaPin<I2C1>> {
    regs: I2C1,
    scl: Scl,
    sda: Sda,
}

impl<Scl: SclPin<I2C1>, Sda: SdaPin<I2C1>> I2cServer<Scl, Sda> {
    pub fn new(regs: I2C1, scl: Scl, sda: Sda, mode: I2cMode, address: u8, irq_address: Option<u8>, rcc: &mut Rcc) -> Self {
        rcc.regs.cfgr3.modify(|_, w| { w.i2c1sw().sysclk() });
        rcc.regs.apb1enr.modify(|_, w| { w.i2c1en().enabled() });

        // Make sure the I2C unit is disabled so we can configure it
        regs.cr1.modify(|_, w| { w.pe().disabled() });

        // Analog filter, but no digital filter.  Min 50ns, max 260ns.
        regs.cr1.modify(|_, w| {
            w
                .anfoff().enabled()
                .dnf().no_filter()
                .gcen().disabled()
                .addrie().enabled()
                .txie().enabled()
                .stopie().enabled()
        });

        // These values all depend on a 48MHz SYSCLK ... could maybe calculate
        // them in the code, but meh, this works for me for now.
        regs.timingr.modify(|_, w| {
            w
                .presc().bits(0)
                .sdadel().bits(0)
                .scldel().bits(match mode {
                    I2cMode::Fast => 8,
                    I2cMode::FastPlus => 5,
                })
        });

        // Enable clock stretching
        regs.cr1.modify(|_, w| { w.nostretch().enabled() });

        // OA1 needs to be disabled before it can be changed
        regs.oar1.modify(|_, w| { w.oa1en().disabled() });
        regs.oar1.write(|w| {
            w
                .oa1mode().bit7()
                // The I2C peripheral ignores the bottom bit
                .oa1().bits(address.into())
                .oa1en().enabled()
        });

        regs.oar2.modify(|_, w| { w.oa2en().disabled() });
        irq_address.map(|addr| {
            regs.oar2.write(|w| {
                w
                    .oa2().bits(addr.into())
                    .oa2msk().no_mask()
                    .oa2en().enabled()
            });
        });

        Self{
            regs: regs,
            scl: scl,
            sda: sda,
        }
    }

    pub fn enable(&mut self) {
        self.regs.cr1.modify(|_, w| { w.pe().enabled() });
    }        

    pub fn read_irq_status(&self) -> R {
        let isr = self.regs.isr.read();

        if isr.addr().is_match_() {
            // flush transmit register
            self.regs.isr.write(|w| { w.txe().empty() });
        }

        isr
    }

    pub fn clear_irq_status(&mut self, isr: R) {
        self.regs.icr.write(|w| {
            let w = if isr.addr().is_match_() {
                w.addrcf().clear()
            } else {
                w
            };
            let w = if isr.stopf().is_stop() {
                w.stopcf().clear()
            } else {
                w
            };
            w
        });
    }

    // Sends out the given byte
    pub fn write(&mut self, b: u8) {
        self.regs.txdr.write(|w| { w.txdata().bits(b) });
    }

    pub fn release(self) -> (I2C1, Scl, Sda) {
        (self.regs, self.scl, self.sda)
    }
}
