#![no_std]

#![allow(incomplete_features)]
#![feature(const_generics)]   // "may crash the compiler" ... let's find out!  :)

use core::marker::PhantomData;

use stm32f0xx_hal::rcc::Rcc;
use stm32f0xx_hal::gpio::{gpioa::{PA4, PA6, PA7}, gpiob::{PB0, PB1}, Alternate, AF1, AF4};
use stm32f0xx_hal::stm32::{TIM3, TIM14, RCC};

// PWM first

pub trait TimerPwmExt {
    type Parts;

    fn split(self, prescale: u16, period: u16, rcc: &mut Rcc) -> Self::Parts;
}

pub trait PwmTimerOps {
    fn send_uev(&self);
    fn arr(&self) -> u16;
    fn set_arr(&self, value: u16);
    fn set_cen(&self, value: bool);
}
pub trait PwmTimerChannelOps {
    fn ccr(&self) -> u16;
    fn set_ccr(&self, value: u16);
}

// API outline:
//
// use the TimerPwmExt trait to split() a timer block; you get either
// a Parts struct that has 4 channels, or a Parts struct that has 1,
// depending on the timer block's capabilities
//
// attach_to_hardware_pin or attach_to_software_pin to hook up either
// interrupts or pin AF registers or whatever else, and enable PWMing;
// we determine which timer/channel pairs can do hardware (and which
// pin configs that timer/channel pair requires), and which pairs can
// only do software.
//
// treat the output as an embedded_hal::Pwm.  note that the software
// variant has an explicit API to set_high and set_low and toggle the
// pin; these are intended to be used by an interrupt handler.
//
// maybe in the future: release() the end struct to get the timer
// block back?  or maybe release() up the chain, so that the entire
// Parts struct needs to be release()d to get the timer?  the last
// seems necessary.  it might be possible to repopulate the Parts
// struct's public fields with release() calls of the "attached" Pwm
// instances that we got out of it, and then release() the Parts?

pub struct PwmTimer<TimerBlock> {
    _block: PhantomData<TimerBlock>,
}

pub struct PwmSubTimer<TimerBlock, const CHANNEL_INDEX: u8> {
    _block: PhantomData<TimerBlock>,
}

pub struct PwmPinTimer<SubTimer, PinType> {
    timer: SubTimer,
    _pin: PinType,
}

impl<SubTimer, PinType> embedded_hal::Pwm for PwmPinTimer<SubTimer, PinType>
where SubTimer: PwmTimerOps + PwmTimerChannelOps {
    type Channel = ();
    type Duty = u16;
    type Time = u16;

    fn enable(&mut self, _channel: Self::Channel) {
        self.timer.send_uev();
        self.timer.set_cen(true);
    }
    fn disable(&mut self, _channel: Self::Channel) {
        self.timer.set_cen(false);
    }

    fn get_max_duty(&self) -> Self::Duty {
        self.timer.arr()
    }

    fn get_duty(&self, _channel: Self::Channel) -> Self::Duty {
        self.timer.ccr()
    }
    fn set_duty(&mut self, _channel: Self::Channel, duty: Self::Duty) {
        self.timer.set_ccr(duty);
    }

    fn get_period(&self) -> Self::Time {
        self.timer.arr()
    }
    fn set_period<P>(&mut self, period: P)
        where P: Into<Self::Time> {
        self.timer.set_arr(period.into());
    }
}

impl PwmTimerOps for PwmSubTimer<TIM14, 1> {
    fn send_uev(&self) {
        let timer = unsafe { &*TIM14::ptr() };
        // all the other bits in EGR are write-1-to-trigger
        timer.egr.write(|w| { w.ug().set_bit() });
    }
    fn arr(&self) -> u16 {
        let timer = unsafe { &*TIM14::ptr() };
        timer.arr.read().arr().bits()
    }
    fn set_arr(&self, value: u16) {
        let timer = unsafe { &*TIM14::ptr() };
        timer.arr.modify(|_, w| { w.arr().bits(value) });
    }
    fn set_cen(&self, value: bool) {
        let timer = unsafe { &*TIM14::ptr() };
        timer.cr1.modify(|_, w| { w.cen().bit(value) });
    }
}
impl PwmTimerChannelOps for PwmSubTimer<TIM14, 1> {
    fn ccr(&self) -> u16 {
        let timer = unsafe { &*TIM14::ptr() };
        timer.ccr1.read().ccr().bits()
    }
    fn set_ccr(&self, value: u16) {
        let timer = unsafe { &*TIM14::ptr() };
        timer.ccr1.modify(|_, w| { w.ccr().bits(value) });
    }
}

impl<const CHANNEL: u8> PwmTimerOps for PwmSubTimer<TIM3, CHANNEL> {
    fn send_uev(&self) {
        let timer = unsafe { &*TIM3::ptr() };
        // all the other bits in EGR are write-1-to-trigger
        timer.egr.write(|w| { w.ug().set_bit() });
    }
    fn arr(&self) -> u16 {
        let timer = unsafe { &*TIM3::ptr() };
        timer.arr.read().arr().bits()
    }
    fn set_arr(&self, value: u16) {
        let timer = unsafe { &*TIM3::ptr() };
        timer.arr.modify(|_, w| { w.arr().bits(value) });
    }
    fn set_cen(&self, value: bool) {
        let timer = unsafe { &*TIM3::ptr() };
        timer.cr1.modify(|_, w| { w.cen().bit(value) });
    }
}
impl PwmTimerChannelOps for PwmSubTimer<TIM3, 1> {
    fn ccr(&self) -> u16 {
        let timer = unsafe { &*TIM3::ptr() };
        timer.ccr1.read().ccr().bits()
    }
    fn set_ccr(&self, value: u16) {
        let timer = unsafe { &*TIM3::ptr() };
        timer.ccr1.modify(|_, w| { w.ccr().bits(value) });
    }
}
impl PwmTimerChannelOps for PwmSubTimer<TIM3, 2> {
    fn ccr(&self) -> u16 {
        let timer = unsafe { &*TIM3::ptr() };
        timer.ccr2.read().ccr().bits()
    }
    fn set_ccr(&self, value: u16) {
        let timer = unsafe { &*TIM3::ptr() };
        timer.ccr2.modify(|_, w| { w.ccr().bits(value) });
    }
}
impl PwmTimerChannelOps for PwmSubTimer<TIM3, 3> {
    fn ccr(&self) -> u16 {
        let timer = unsafe { &*TIM3::ptr() };
        timer.ccr3.read().ccr().bits()
    }
    fn set_ccr(&self, value: u16) {
        let timer = unsafe { &*TIM3::ptr() };
        timer.ccr3.modify(|_, w| { w.ccr().bits(value) });
    }
}
impl PwmTimerChannelOps for PwmSubTimer<TIM3, 4> {
    fn ccr(&self) -> u16 {
        let timer = unsafe { &*TIM3::ptr() };
        timer.ccr4.read().ccr().bits()
    }
    fn set_ccr(&self, value: u16) {
        let timer = unsafe { &*TIM3::ptr() };
        timer.ccr4.modify(|_, w| { w.ccr().bits(value) });
    }
}

impl PwmSubTimer<TIM14, 1> {
    pub fn attach_to_hardware_pin(self, pin: PA4<Alternate<AF4>>) -> PwmPinTimer<Self, PA4<Alternate<AF4>>> {
        let timer = unsafe { &*TIM14::ptr() };
        timer.ccer.modify(|_, w| {
            w
                .cc1p().clear_bit()   // active means high (led driver's enable: high is on)
                .cc1e().set_bit()     // enable pwming
        });
        PwmPinTimer{
            timer: self,
            _pin: pin,
        }
    }
}

impl PwmSubTimer<TIM3, 1> {
    pub fn attach_to_hardware_pin(self, pin: PA6<Alternate<AF1>>) -> PwmPinTimer<Self, PA6<Alternate<AF1>>> {
        let timer = unsafe { &*TIM3::ptr() };
        timer.ccer.modify(|_, w| {
            w
                .cc2p().clear_bit()   // active means high (led driver's enable: high is on)
                .cc2e().set_bit()     // enable pwming
        });
        PwmPinTimer{
            timer: self,
            _pin: pin,
        }
    }
}
impl PwmSubTimer<TIM3, 2> {
    pub fn attach_to_hardware_pin(self, pin: PA7<Alternate<AF1>>) -> PwmPinTimer<Self, PA7<Alternate<AF1>>> {
        let timer = unsafe { &*TIM3::ptr() };
        timer.ccer.modify(|_, w| {
            w
                .cc2p().clear_bit()   // active means high (led driver's enable: high is on)
                .cc2e().set_bit()     // enable pwming
        });
        PwmPinTimer{
            timer: self,
            _pin: pin,
        }
    }
}
impl PwmSubTimer<TIM3, 3> {
    pub fn attach_to_hardware_pin(self, pin: PB0<Alternate<AF1>>) -> PwmPinTimer<Self, PB0<Alternate<AF1>>> {
        let timer = unsafe { &*TIM3::ptr() };
        timer.ccer.modify(|_, w| {
            w
                .cc3p().clear_bit()   // active means high (led driver's enable: high is on)
                .cc3e().set_bit()     // enable pwming
        });
        PwmPinTimer{
            timer: self,
            _pin: pin,
        }
    }
}
impl PwmSubTimer<TIM3, 4> {
    pub fn attach_to_hardware_pin(self, pin: PB1<Alternate<AF1>>) -> PwmPinTimer<Self, PB1<Alternate<AF1>>> {
        let timer = unsafe { &*TIM3::ptr() };
        timer.ccer.modify(|_, w| {
            w
                .cc4p().clear_bit()   // active means high (led driver's enable: high is on)
                .cc4e().set_bit()     // enable pwming
        });
        PwmPinTimer{
            timer: self,
            _pin: pin,
        }
    }
}

pub struct FourParts<TimerBlock> {
    pub channel1: PwmSubTimer<TimerBlock, 1>,
    pub channel2: PwmSubTimer<TimerBlock, 2>,
    pub channel3: PwmSubTimer<TimerBlock, 3>,
    pub channel4: PwmSubTimer<TimerBlock, 4>,
}
pub struct OnePart<TimerBlock> {
    pub channel1: PwmSubTimer<TimerBlock, 1>,
}

impl TimerPwmExt for TIM3 {
    type Parts = FourParts<TIM3>;

    fn split(self, prescale: u16, period: u16, _rcc: &mut Rcc) -> FourParts<TIM3> {
        // NOTE(unsafe): We can't do this through the RCC that we take a mutable
        // reference to, because the regs member of that struct is pub(crate)
        // instead of pub.  (Which probably makes a lot of sense.)  But it's
        // still safe because of the mut ref.
        unsafe { &*RCC::ptr() }.apb1enr.modify(|_, w| { w.tim3en().enabled() });

        self.cr1.modify(|_, w| {
            w
                .arpe().enabled()
                .dir().up()
                .urs().counter_only()   // do I want UG or slave-mode updates to generate an irq?
                .cen().disabled()
        });
        self.smcr.modify(|_, w| { w.sms().disabled() });
        self.ccmr1_output_mut().modify(|_, w| {
            unsafe {
                // arrrrrg, these should have enums in the svd file!
                w
                    .cc1s().bits(0)
                    .oc1m().bits(0x6) // active when counter < ccmr1 value
                    .cc2s().bits(0)
                    .oc2m().bits(0x6) // active when counter < ccmr1 value
            }
            .oc1pe().set_bit()
                .oc2pe().set_bit()
        });
        self.ccmr2_output_mut().modify(|_, w| {
            unsafe {
                // arrrrrg, these should have enums in the svd file!
                w
                    .cc3s().bits(0)
                    .oc3m().bits(0x6) // active when counter < ccmr1 value
                    .cc4s().bits(0)
                    .oc4m().bits(0x6) // active when counter < ccmr1 value
            }
            .oc3pe().set_bit()
                .oc4pe().set_bit()
        });
        self.psc.modify(|_, w| {
            w.psc().bits(prescale)
        });
        self.arr.modify(|_, w| {
            w.arr().bits(period)
        });

        FourParts{
            channel1: PwmSubTimer{ _block: PhantomData },
            channel2: PwmSubTimer{ _block: PhantomData },
            channel3: PwmSubTimer{ _block: PhantomData },
            channel4: PwmSubTimer{ _block: PhantomData },
        }
    }
}

impl TimerPwmExt for TIM14 {
    type Parts = OnePart<TIM14>;

    fn split(self, prescale: u16, period: u16, _rcc: &mut Rcc) -> OnePart<TIM14> {
        // NOTE(unsafe): We can't do this through the RCC that we take a mutable
        // reference to, because the regs member of that struct is pub(crate)
        // instead of pub.  (Which probably makes a lot of sense.)  But it's
        // still safe because of the mut ref.
        unsafe { &*RCC::ptr() }.apb1enr.modify(|_, w| { w.tim14en().enabled() });
        self.cr1.modify(|_, w| {
            w
                .arpe().enabled()
                .urs().counter_only()   // do I want UG or slave-mode updates to generate an irq?
                .cen().disabled()
        });
        self.ccmr1_output_mut().modify(|_, w| {
            unsafe {
                // arrrrrg, these should have enums in the svd file!
                w
                    .cc1s().bits(0)
                    .oc1m().bits(0x6) // active when counter < ccmr1 value
            }.oc1pe().set_bit()
        });
        self.psc.modify(|_, w| {
            w.psc().bits(prescale)
        });
        self.arr.modify(|_, w| {
            w.arr().bits(period)
        });

        OnePart{
            channel1: PwmSubTimer{ _block: PhantomData },
        }
    }
}

// LEDs second

// python -c $'import math\nfor i in xrange(1,257):\n  print int(192 * math.log(i)/math.log(255))'
const LOG_SCALE: [u8; 256] = [
      0,  24,  38,  48,  55,  62,  67,  72,  76,  79,  83,  86,  88,  91,  93,  96,
     98, 100, 102, 103, 105, 107, 108, 110, 111, 112, 114, 115, 116, 117, 118, 120,
    121, 122, 123, 124, 125, 126, 126, 127, 128, 129, 130, 131, 131, 132, 133, 134,
    134, 135, 136, 136, 137, 138, 138, 139, 140, 140, 141, 141, 142, 143, 143, 144,
    144, 145, 145, 146, 146, 147, 147, 148, 148, 149, 149, 150, 150, 150, 151, 151,
    152, 152, 153, 153, 153, 154, 154, 155, 155, 155, 156, 156, 157, 157, 157, 158,
    158, 158, 159, 159, 159, 160, 160, 160, 161, 161, 161, 162, 162, 162, 163, 163,
    163, 164, 164, 164, 165, 165, 165, 165, 166, 166, 166, 167, 167, 167, 167, 168,
    168, 168, 168, 169, 169, 169, 169, 170, 170, 170, 170, 171, 171, 171, 171, 172,
    172, 172, 172, 173, 173, 173, 173, 174, 174, 174, 174, 174, 175, 175, 175, 175,
    176, 176, 176, 176, 176, 177, 177, 177, 177, 177, 178, 178, 178, 178, 178, 179,
    179, 179, 179, 179, 180, 180, 180, 180, 180, 181, 181, 181, 181, 181, 181, 182,
    182, 182, 182, 182, 183, 183, 183, 183, 183, 183, 184, 184, 184, 184, 184, 184,
    185, 185, 185, 185, 185, 185, 186, 186, 186, 186, 186, 186, 187, 187, 187, 187,
    187, 187, 187, 188, 188, 188, 188, 188, 188, 189, 189, 189, 189, 189, 189, 189,
    190, 190, 190, 190, 190, 190, 190, 191, 191, 191, 191, 191, 191, 191, 192, 192, 
];
pub struct RgbLed<RedPwm, GreenPwm, BluePwm>
where RedPwm: embedded_hal::Pwm, RedPwm::Duty: From<u8>, RedPwm::Channel: Copy,
      GreenPwm: embedded_hal::Pwm, GreenPwm::Duty: From<u8>, GreenPwm::Channel: Copy,
      BluePwm: embedded_hal::Pwm, BluePwm::Duty: From<u8>, BluePwm::Channel: Copy {
    pwm_r: RedPwm,
    pwm_g: GreenPwm,
    pwm_b: BluePwm,

    ch_r: RedPwm::Channel,
    ch_g: GreenPwm::Channel,
    ch_b: BluePwm::Channel,
}

impl<RedPwm, GreenPwm, BluePwm> RgbLed<RedPwm, GreenPwm, BluePwm>
where RedPwm: embedded_hal::Pwm, RedPwm::Duty: From<u8>, RedPwm::Channel: Copy,
      GreenPwm: embedded_hal::Pwm, GreenPwm::Duty: From<u8>, GreenPwm::Channel: Copy,
      BluePwm: embedded_hal::Pwm, BluePwm::Duty: From<u8>, BluePwm::Channel: Copy {
    pub fn new(pwm_r: RedPwm, pwm_g: GreenPwm, pwm_b: BluePwm,
               ch_r: RedPwm::Channel, ch_g: GreenPwm::Channel, ch_b: BluePwm::Channel) -> Self {
        Self{
            pwm_r: pwm_r,
            pwm_g: pwm_g,
            pwm_b: pwm_b,

            ch_r: ch_r,
            ch_g: ch_g,
            ch_b: ch_b,
        }
    }

    pub fn enable(&mut self) {
        self.pwm_r.enable(self.ch_r);
        self.pwm_g.enable(self.ch_g);
        self.pwm_b.enable(self.ch_b);
    }

    pub fn set_rgb(&mut self, r: u8, g: u8, b: u8) {
        self.pwm_r.set_duty(self.ch_r, LOG_SCALE[usize::from(r)].into());
        self.pwm_g.set_duty(self.ch_g, LOG_SCALE[usize::from(g)].into());
        self.pwm_b.set_duty(self.ch_b, LOG_SCALE[usize::from(b)].into());
    }
}
