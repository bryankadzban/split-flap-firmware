// I'd really like to:
//#![deny(unsafe_code)]
// here, but the single-core version of heapless::Queue is unsafe (because the
// caller needs to ensure that the producer and consumer always stay on the same
// core... which for this microcontroller, which only has one, is trivial).  And
// I want to use the single-core variant, because it's 16 bytes less code per
// enqueue/dequeue pair.
#![deny(warnings)]
#![no_main]
#![no_std]

use cortex_m_semihosting::{/*debug,*/ hprintln};
use panic_abort as _;

use nb::{Result, block};

use heapless::spsc::{Queue, Producer, Consumer, SingleCore};
use heapless::consts::U64;
use heapless::i::Queue as ConstQueue;

use stm32f0xx_hal::serial::Serial;
use stm32f0xx_hal::spi::{Spi, DataSize, Mode, NoPin, Phase, Polarity, Error};
use stm32f0xx_hal::i2c_server::{I2cServer, I2cMode};
use stm32f0xx_hal::rcc::USBClockSource::PLL;
use stm32f0xx_hal::rcc::HSEBypassMode::NotBypassed;
use stm32f0xx_hal::gpio::{gpioa::{PA1, PA2, PA3, PA4, PA5, PA6, PA7, PA9, PA10}, gpiob::PB1};
use stm32f0xx_hal::gpio::{Alternate, Output, AF0, AF1, AF4, PushPull};
use stm32f0xx_hal::stm32::{SPI1, TIM3, TIM14, USART2, Interrupt};

use stm32f0xx_hal::gpio::GpioExt;
use stm32f0xx_hal::rcc::RccExt;
use stm32f0xx_hal::time::U32Ext;
use led_pwm::TimerPwmExt;
use embedded_hal::digital::v2::{InputPin, OutputPin, StatefulOutputPin};
use embedded_hal::spi::FullDuplex;
use core::fmt::Write;

// Wrap an SPI instance plus a last-pair u16 into one resource, so we only have
// to lock one thing
pub struct SpiAndLastPair {
    spi: Spi<SPI1, PA5<Alternate<AF0>>, PA6<Alternate<AF0>>, NoPin>,
    last_pair: u16,
}
impl SpiAndLastPair {
    // read pays attention to a clamp in last_pair: if it's zero, then we never
    // delegate to the SPI instance.  Bonus: we also never block.  We know that
    // all reads will always be zero after that (due to the hardware setup), so
    // there's no point in clocking the SPI bus or slowing down the caller.
    fn read(&mut self) -> Result<u16, Error> {
        if self.last_pair == 0 {
            Ok(0u16)
        } else {
            self.spi.read().map(|val| { self.last_pair = val; val })
        }
    }
    // send pays attention to the same clamp: As soon as read has seen the clamp
    // value, sends stop blocking and return without poking the SPI bus.
    //
    // It will *always* result in an SPI1 interrupt eventually being handled.
    fn send(&mut self, byte: u16) -> Result<(), Error> {
        if self.last_pair == 0 {
            // We need to trigger an SPI receive interrupt, so the user keeps
            // calling read!
            rtfm::pend(Interrupt::SPI1);
            Ok(())
        } else {
            self.spi.send(byte)
        }
    }
    // force_send undoes the clamp (see read and send) and sends the given u16.
    fn force_send(&mut self, byte: u16) -> Result<(), Error> {
        self.last_pair = 1;
        self.spi.send(byte)
    }
}

pub enum SensorIrqState {
    Sensor,
    Irq,
}
pub struct SensorIrqControl(PA3<Output<PushPull>>);
impl SensorIrqControl {
    pub fn new(pin: PA3<Output<PushPull>>) -> Self {
        Self(pin)
    }
    pub fn state(&self) -> SensorIrqState {
        if self.0.is_set_high().unwrap() {
            SensorIrqState::Sensor
        } else {
            SensorIrqState::Irq
        }
    }
    pub fn set_state(&mut self, state: SensorIrqState) {
        match state {
            SensorIrqState::Sensor => self.0.set_high().unwrap(),
            SensorIrqState::Irq => self.0.set_low().unwrap(),
        }
    }
    pub fn release(self) -> PA3<Output<PushPull>> {
        self.0
    }
}

type RedTimer = led_pwm::PwmPinTimer<led_pwm::PwmSubTimer<TIM14, 1>, PA4<Alternate<AF4>>>;
type GreenTimer = led_pwm::PwmPinTimer<led_pwm::PwmSubTimer<TIM3, 2>, PA7<Alternate<AF1>>>;
type BlueTimer = led_pwm::PwmPinTimer<led_pwm::PwmSubTimer<TIM3, 4>, PB1<Alternate<AF1>>>;

// Offset into this array is the byte that came back from the SPI bus.  The
// value is either the letter position displayed, or 255 if that byte can't be
// reached with the sensor sequence we're using.  This is for 80 positions and
// (obviously) 8 sensors; if we had 6 sensors and 60 positions, we'd need both
// a different table, and a way to determine which table to use.  The raw bit
// values are not disjoint between 80 and 60, but neither uses all-zeros or all-
// ones.  And with a single unknown sensor value (that's in both tables), the
// *next* sensor value will only be the proper next sensor value in one of the
// tables, so we *could use that to determine the table.
//
// OTOH the 60-position sensor is very unlikely to be used in the display; it's
// better to use that space for like ten normal 80-position letters.
//
// NOTE.  There are 80 possible positions in the gray code sequence, however
// there are only 40 possible physical positions of the letter.  So each of the
// displayed-letter values corresponds to two different gray-code bytes; you
// will notice several byte values repeated in here.  We intend for 0 and 1 to
// be the same position, not 79 and 0.
#[allow(dead_code)]
static GRAY_CODE_LOOKUP: [u8; 256] = [
    127, 255,  11, 255,  10,   9,  11, 255,
    255,   8,  12,   6, 255,   8, 255, 255,
    255, 255, 255,   5,  10,   9, 255,   4,
     34, 255, 255,   6,  35, 255, 255, 255,
     31, 255, 255, 255, 255, 255, 255,   3,
     33,   7, 255,   7, 255, 255, 255,   1,
    255,  25, 255, 255, 255,  25, 255,   3,
     33, 255, 255, 255, 255, 255, 255, 255,
     30,  30, 255, 255, 255, 255, 255, 255,
    255, 255, 255, 255, 255, 255,  39,   0,
     29,  29, 255,   5, 255, 255, 255,   4,
     34, 255, 255, 255,  35, 255,  39,   0,
     31, 255, 255, 255, 255, 255, 255, 255,
    255, 255, 255, 255, 255, 255, 255,   1,
    255,  24,  23,  23, 255,  24, 255, 255,
    255, 255,  22, 255, 255, 255, 255, 255,
    255,  14,  13,  13, 255,  14, 255, 255,
    255, 255,  12, 255,  36, 255,  38, 255,
     28, 255,  27, 255, 255, 255, 255, 255,
    255, 255, 255, 255,  36, 255, 255, 255,
     32, 255, 255, 255, 255, 255, 255,   2,
     32, 255, 255, 255,  37, 255,  37,   2,
     26,  26,  27, 255, 255, 255, 255, 255,
    255, 255, 255, 255, 255, 255, 255, 255,
    255,  15, 255, 255, 255,  15, 255, 255,
     16,  16,  17, 255, 255, 255,  38, 255,
     28, 255, 255, 255, 255, 255, 255, 255,
    255, 255, 255, 255, 255, 255, 255, 255,
    255, 255, 255, 255,  19,  19, 255, 255,
     18, 255,  17, 255,  18, 255, 255, 255,
    255, 255,  21, 255,  20,  20,  21, 255,
    255, 255,  22, 255, 255, 255, 255, 255,
];

type I2c = I2cServer<PA9<Alternate<AF4>>, PA10<Alternate<AF4>>>;

#[rtfm::app(device = stm32f0::stm32f0x0, peripherals = true)]
const APP: () = {
    struct Resources {
        clr_load: PA1<Output<PushPull>>,
        sensor_irq_switch: SensorIrqControl,
        serial: Serial<USART2, PA2<Alternate<AF1>>, ()>,
        spi: SpiAndLastPair,
        i2c: I2c,
        rgb: led_pwm::RgbLed<RedTimer, GreenTimer, BlueTimer>,
        p: Producer<'static, u8, U64, u8, SingleCore>,
        c: Consumer<'static, u8, U64, u8, SingleCore>,
    }

    #[init()]
    fn init(cx: init::Context) -> init::LateResources {
        // Explanation of types:
        // u8 is the type of each item
        // U64 is the "constant" for 64 items (power of 2 for more efficient
        //  queue implementation code) expressed as a type because const bounds
        //  aren't stable yet
        // u8 is the type of the capacity value (max capacity 255)
        // SingleCore makes the atomics only use a compiler barrier, since the
        //  single processor doesn't need a memory barrier.
        static mut Q: Queue<u8, U64, u8, SingleCore> = Queue(unsafe { ConstQueue::u8_sc() });

        // Device specific peripherals
        let mut device: stm32f0::stm32f0x0::Peripherals = cx.device;

        let q: &'static mut Queue<_, _, _, _> = Q;
        let (p, c) = q.split();

        // This is undesirable, as init() is called with interrupts disabled
        // already.  It's a little annoying that init doesn't give me a critical
        // section to use, and that the HAL crate requires a critical section to
        // configure GPIOs.
        cortex_m::interrupt::free(|cs| {
            let mut rcc = device.RCC.configure()
                .hse(4.mhz(), NotBypassed)  // hse crystal present, runs at 4mhz
                .sysclk(48.mhz()) // main clock speed
                .hclk(48.mhz())   // AHB speed
                .pclk(48.mhz())   // APB speed
                .usbsrc(PLL)
                .freeze(&mut device.FLASH);

            let gpioa = device.GPIOA.split(&mut rcc);
            let gpiob = device.GPIOB.split(&mut rcc);

            // first read the i2c address bits
            let bit0 = gpioa.pa4.into_floating_input(cs);
            let bit1 = gpioa.pa7.into_floating_input(cs);
            let bit2 = gpiob.pb1.into_floating_input(cs);

            let i2c_addr = ({
                let mut last = 0;
                let mut cur = 7;

                while cur != last {
                    last = cur;
                    cur =
                        (u8::from(bit2.is_high().unwrap()) << 2) |
                        (u8::from(bit1.is_high().unwrap()) << 1) |
                        u8::from(bit0.is_high().unwrap());
                }
                cur
            } | 0x40) << 1;  // 0x80 in the high bits, 0 in the lowest bit
            // The GPIO chip puts 0x40 in the high bits and 0 in the lowest bit
            // with the address line setup we use, so we can coexist with it.
            // From its datasheet, if we manage to use the SDA/SCL lines, we
            // could make the top nibble be any of (hex) 2, 3, 4, 5, A, B, C, or
            // E, so not 0, 1, 6, 7, 8, 9, D, or F.  I think that 0 and F are
            // reserved by I2C.  6 is nice as we can flip one bit to move from
            // the GPIO chip to the micro; 1/7/8/D all need 2 bits, and 9 needs
            // 3.  OTOH it's not like flipping more bits is more work for the
            // main micro...
            //
            // Moving from 4 to 8 seems reasonable, let's do that.

            // We need a third address, for reading interrupt bit status (one
            // byte of data, always followed by zeros until the client sends a
            // nak and a stop condition).  If the top nibble set to 4 is the
            // GPIO chip, and set to 8 is the sensor-status chip, then we could
            // set it to C ... wait, no, that's used by the GPIO chip in some
            // connections.  D works, from the point of view of lots of 1 bits.
            let i2c_irq_addr = i2c_addr | 0x60;

            // Now that we have a stable I2C address read in, set up the rest of
            // the GPIOs.

            // led versus i2c address switch, plus usb versus i2c hardware switch
            let mut led_usb_switch = gpioa.pa0.into_push_pull_output(cs);
            led_usb_switch.set_high().unwrap(); // enable I2C lines and LEDs

            // whether the spi bus is connected to the sensor outputs or the
            // per-motor-driver-chip alarm outputs.  there's an external pullup
            // resistor on this line since we want it to be high when the micro
            // is resetting, but it's a very low current pullup, and we want to
            // have a fast rise time, so enable the p-channel mosfet.
            let mut sensor_irq_switch = SensorIrqControl::new(
                gpioa.pa3.into_push_pull_output(cs));
            sensor_irq_switch.set_state(SensorIrqState::Sensor);

            let led_r = bit0.into_alternate_af4(cs);      // tim14_ch1
            let led_g = bit1.into_alternate_af1(cs);      // tim3_ch2
            let led_b = bit2.into_alternate_af1(cs);      // tim3_ch4

            // Clock inputs are the APB clock if it's the same speed as AHB, or
            // double the frequency of the APB clock if it's scaled from AHB.
            // We left AHB and APB equal, so the clock input is 48MHz.
            //
            // We want a period of 25kHz, and 200 steps of resolution.  The
            // smallest step, then, means we need to count at 5MHz.  Best we can
            // do is 48MHz / 9 = 5.3333MHz.  213 steps is 25.039kHz.  Eeew.  :(
            //
            // Or, we do 48MHz / 10 = 4.8MHz (above the min response time of the
            // LED controller); then 100 steps (percentage?) is 48kHz, still
            // above the range of hearing.  192 steps is 25kHz.  Let's do 192.
            let parts = device.TIM3.split(10, 192, &mut rcc);
            let mut rgb = led_pwm::RgbLed::new(
                device.TIM14.split(10, 192, &mut rcc).channel1.attach_to_hardware_pin(led_r),
                parts.channel2.attach_to_hardware_pin(led_g),
                parts.channel4.attach_to_hardware_pin(led_b),
                (), (), ());
            rgb.set_rgb(127, 0, 0);   // dim red: LED set up

            // Hook up PA9 and PA10 to the physical pins (not 11/12).  The call
            // to _rmp may not be necessary, but after flash the bootloader has
            // has USB enabled, so if we've just booted that way, we do need it.
            // The fmp() stuff is desired for sure though.
            device.SYSCFG.cfgr1.modify(|_, w| {
                w
                    .pa11_pa12_rmp().not_remapped()
                    .i2c_pa9_fmp().fmp()
                    .i2c_pa10_fmp().fmp()
            });

            rgb.set_rgb(255, 0, 0);   // bright red: remaps and FM+ set up

            // !clr_load-trigger gpio
            let mut clr_load = gpioa.pa1.into_push_pull_output(cs); // needs pullup
            clr_load.set_high().unwrap();

            let uart_tx = gpioa.pa2.into_alternate_af1(cs);
            // sensor_irq_switch is PA3
            // led_r is PA4
            let sck = gpioa.pa5.into_alternate_af0(cs);
            let miso = gpioa.pa6.into_alternate_af0(cs);
            // led_g is PA7
            // PA8 is not brought out to a pin on this chip
            let scl = gpioa.pa9.into_alternate_af4(cs);
            let sda = gpioa.pa10.into_alternate_af4(cs);
            // led_b is PB1

            rgb.set_rgb(127, 127, 0);   // dim yellow: GPIOs configured

            let mut serial = Serial::usart2tx(device.USART2, uart_tx, 3_000_000.bps(), &mut rcc);
            serial.write_str("Hello World!\n").unwrap();

            rgb.set_rgb(255, 255, 0);   // bright yellow: serial running

            // Configure SPI with 18MHz rate
            // The frequency is fPCLK / {2,4,8,16,...,256}, so with fPCLK at
            // 48MHz, we can do 24MHz, 12, 6, 3, 1.5, 750kHz, 375, or 187.5.
            // The datasheet claims the max SPI rate is 18MHz, though, not 24?
            // Not sure what's going on there.
            let spi = Spi::spi1(device.SPI1, (sck, miso, NoPin{}), Mode{
                // We need to sample on the rising edge of the clock, while the
                // last shift register is going to push out its first bit on the
                // falling edlge of the clock.  So we need to introduce a
                // falling edge, then a rising edge, when starting from idle
                // (when we send a !CLR_LOAD command).  So we need to idle high,
                // transition to low, then transition back to high, and capture
                // the input on the second of those transitions.
                polarity: Polarity::IdleHigh,
                phase: Phase::CaptureOnSecondTransition,
            }, DataSize::SixteenBit, 18.mhz(), true, &mut rcc);

            let mut i2c = I2cServer::new(device.I2C1, scl, sda, I2cMode::FastPlus, i2c_addr, Some(i2c_irq_addr), &mut rcc);
            i2c.enable();

            rgb.set_rgb(255, 255, 255);   // bright white: SPI and I2C set up

            hprintln!("init").unwrap();

            init::LateResources{
                clr_load: clr_load,
                sensor_irq_switch: sensor_irq_switch,
                serial: serial,
                spi: SpiAndLastPair{
                    spi: spi,
                    last_pair: 0u16,
                },
                i2c: i2c,
                rgb: rgb,
                p: p,
                c: c,
            }
        })
    }

    #[task(binds = I2C1, priority = 1, resources = [c, spi, i2c, clr_load, rgb, sensor_irq_switch])]
    fn i2c1(mut cx: i2c1::Context) {
        static mut NEED_SPI_WRITE: bool = false;
        let need_spi_write = NEED_SPI_WRITE;
        let isr = cx.resources.i2c.read_irq_status();
        if isr.addr().is_match_() {
            if (isr.addcode().bits() << 1) & 0xf0 == 0xd0 {
                // if all 3 bits are set, the other micro is asking for irqs
                cx.resources.sensor_irq_switch.lock(|s| { s.set_state(SensorIrqState::Irq); });
            } else {
                cx.resources.sensor_irq_switch.lock(|s| { s.set_state(SensorIrqState::Sensor); });
            }
            // Trigger the shift registers to read the sensors.
            // At 48mhz, this is 20.8333ns; the min time on the shift
            // registers is 8ns at 3.3v and 125 degrees C.
            cx.resources.clr_load.set_low().unwrap();
            cx.resources.clr_load.set_high().unwrap();
            // Run 16 spi clock cycles, by writing to the transmit register.
            // This will trigger the SPI1 interrupt below, when the clocking
            // finishes.
            cx.resources.spi.lock(|spi| { block!(spi.force_send(0u16)).unwrap(); });
        }
        let mut sent = false;
        if isr.txis().is_empty() {    // might also be true during ADDR
            cx.resources.c.dequeue().map(|b| {
                // If nothing is waiting, we won't clear the txis bit by writing
                // to txdr, so we'll come back and spin until something gets
                // written to the queue.  The SPI interrupt is higher priority,
                // so it will eventually run when the clocking finishes.
                cx.resources.i2c.write(b);
                // We have to dequeue and write two bytes for every 0u16 we send
                // on the SPI interface.  We store the current state in
                // *need_spi_write.
                if *need_spi_write {
                    // Note, this is highly likely to immediately preempt to
                    // handle a fake interrupt.  (Well... the block!() will
                    // finish, since the .lock() call suspends the SPI1
                    // interrupt.  But as soon as the .lock() call returns,
                    // we'll get suspended and let SPI1 run.) That's fine; the
                    // handler for the SPI1 interrupt only calls read (which
                    // will just return the clamp 0 value) and drop that zero
                    // onto the P/C queue.  And when we get resumed, we don't do
                    // another read until the other micro asks for another byte.
                    cx.resources.spi.lock(|spi| {
                        block!(spi.send(0u16)).unwrap();
                    });
                }
                *need_spi_write = !*need_spi_write;
                sent = true;
            });
        }
        if isr.stopf().is_stop() && isr.txis().is_empty() && !sent {
            cx.resources.i2c.write(0u8);   // clear txis bit; no send happens
        }
        cx.resources.i2c.clear_irq_status(isr);
    }

    #[task(binds = SPI1, priority = 2, resources = [p, spi, sensor_irq_switch])]
    fn spi1(cx: spi1::Context) {
        let buf = block!(cx.resources.spi.read()).unwrap().to_le_bytes();
        cx.resources.p.enqueue(buf[0]).unwrap();
        cx.resources.p.enqueue(
            match cx.resources.sensor_irq_switch.state() {
                SensorIrqState::Irq => buf[1],
                SensorIrqState::Sensor => GRAY_CODE_LOOKUP[usize::from(buf[1])],
            }).unwrap();
    }
};
